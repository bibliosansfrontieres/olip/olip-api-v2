module.exports = {
  ignoreFileRegex:
    '.*(.entity.ts|.dto.ts|-request.ts|-response.ts|.controller.ts|\\/errors\\/.|\\/responses\\/.)',
  ignoreMemberNames: ['onModuleInit', 'onModuleDestroy'],
  /**
   * Ignore members decorated by MobX's @disposeOnUnmount
   */
  ignoreDecoratorNames: ['disposeOnUnmount'],
  /**
   * Ignore members initialized with MobX's reaction()
   * e.g. public myReaction = reaction(...);
   */
  ignoreInitializerNames: ['reaction'],
};
