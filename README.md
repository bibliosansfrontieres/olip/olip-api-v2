# OLIP API v2

## Run for development

Requires :

- Node version 18 (or more)

Create a `.env` file that contains the same keys that `.env.example` :

```
cp .env.example .env
```

Just run those commands to start development :

```
yarn install
yarn start:dev
```

## Run with docker

Requires :

- Docker
- Docker Compose plugin

Create a `.docker.env` file that contains the same keys that `.docker.env.example` :

```
cp .docker.env.example .docker.env
```

Just run this command to start with docker :

```
docker compose up
```
