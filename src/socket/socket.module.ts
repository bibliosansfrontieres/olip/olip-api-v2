import { Module, Global, forwardRef } from '@nestjs/common';
import { SocketGateway } from './socket.gateway';
import { HardwareModule } from 'src/hardware/hardware.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { ConfigModule } from '@nestjs/config';
import { StatsModule } from 'src/stats/stats.module';

@Global()
@Module({
  imports: [
    forwardRef(() => HardwareModule),
    JwtModule,
    UsersModule,
    ConfigModule,
    forwardRef(() => StatsModule),
  ],
  providers: [SocketGateway],
  exports: [SocketGateway],
})
export class SocketModule {}
