import { Inject, forwardRef } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { OnEvent } from '@nestjs/event-emitter';
import { JwtService } from '@nestjs/jwt';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import ms, { StringValue } from 'ms';
import { Server, Socket } from 'socket.io';
import { Application } from 'src/applications/entities/application.entity';
import { SocketAuth } from 'src/auth/decorators/socket-auth.decorator';
import { SocketAuthGuard } from 'src/auth/guards/socket-auth.guard';
import { HardwareService } from 'src/hardware/hardware.service';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { StatsService } from 'src/stats/stats.service';
import { UsersService } from 'src/users/users.service';
import { extractAuthFromClient } from 'src/utils/extract-auth-from-client.util';

@WebSocketGateway({ cors: { origin: '*' } })
export class SocketGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  server: Server;

  constructor(
    private jwtService: JwtService,
    private configService: ConfigService,
    private usersService: UsersService,
    @Inject(forwardRef(() => HardwareService))
    private hardwareService: HardwareService,
    @Inject(forwardRef(() => StatsService))
    private statsService: StatsService,
  ) {}

  onApplicationBootstrap() {
    setInterval(
      this.emitBatteryInformation.bind(this),
      ms(
        this.configService.get<StringValue>(
          'EMIT_BATTERY_INFORMATION_INTERVAL',
        ),
      ),
    );
  }

  async handleConnection(client: Socket) {
    const realIpHeader = client.handshake.headers['x-real-ip'];
    const realIp = Array.isArray(realIpHeader) ? realIpHeader[0] : realIpHeader;
    const ip = realIp || client.request.socket.remoteAddress;
    const socketId = client.id;
    const user = await SocketAuthGuard.extractUser(
      this.jwtService,
      this.configService,
      this.usersService,
      client,
    );
    const { clientId } = extractAuthFromClient(client);
    await this.statsService.createConnectedUser(
      { ip, socketId, clientId },
      user,
    );
  }

  async handleDisconnect(client: Socket) {
    const socketId = client.id;
    await this.statsService.removeConnectedUser(socketId);
  }

  @SocketAuth(PermissionEnum.READ_STORAGE)
  @SubscribeMessage('storage')
  async handleStorage(
    @MessageBody() data: string,
    @ConnectedSocket() client: Socket,
  ) {
    client.emit('storage', await this.hardwareService.getStorage());
  }

  async emitStorage() {
    this.server.emit('storage', await this.hardwareService.getStorage());
  }

  @SocketAuth(PermissionEnum.READ_STORAGE)
  @SubscribeMessage('live-users')
  async handleLiveUsers(
    @MessageBody() data: string,
    @ConnectedSocket() client: Socket,
  ) {
    client.emit('live-users', await this.statsService.getLiveUsers());
  }

  async emitLiveUsers() {
    this.server.emit('live-users', await this.statsService.getLiveUsers());
  }

  @SubscribeMessage('internet-connection')
  async handleInternetConnection(
    @MessageBody() data: string,
    @ConnectedSocket() client: Socket,
  ) {
    const isConnected = await this.hardwareService.hasInternet();
    client.emit('internet-connection', {
      isConnected,
    });
  }

  @OnEvent('internet.changed')
  async emitInternetConnection() {
    const isConnected = await this.hardwareService.hasInternet();
    this.server.emit('internet-connection', {
      isConnected,
    });
  }

  async emitApplication(application: Application) {
    this.server.emit('application', { application });
  }

  @SubscribeMessage('battery-information')
  async handleBatteryInformation(@ConnectedSocket() client: Socket) {
    const batteryInformation =
      await this.hardwareService.getBatteryInformation();
    client.emit('battery-information', {
      ...batteryInformation,
    });
  }

  async emitBatteryInformation() {
    const batteryInformation =
      await this.hardwareService.getBatteryInformation();
    this.server.emit('battery-information', {
      ...batteryInformation,
    });
  }
}
