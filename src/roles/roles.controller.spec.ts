import { Test, TestingModule } from '@nestjs/testing';
import { RolesController } from './roles.controller';
import { RolesService } from './roles.service';
import { Repository } from 'typeorm';
import { Role } from './entities/role.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { RoleTranslation } from './entities/role-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';
import { PermissionsService } from 'src/permissions/permissions.service';
import { permissionsServiceMock } from 'src/tests/mocks/providers/permissions-service.mock';

describe('RolesController', () => {
  let controller: RolesController;
  let roleRepository: Repository<Role>;
  let roleTranslationRepository: Repository<RoleTranslation>;

  const ROLE_REPOSITORY_TOKEN = getRepositoryToken(Role);
  const ROLE_TRANSLATION_REPOSITORY_TOKEN = getRepositoryToken(RoleTranslation);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RolesController],
      providers: [
        RolesService,
        { provide: ROLE_REPOSITORY_TOKEN, useClass: Repository<Role> },
        {
          provide: ROLE_TRANSLATION_REPOSITORY_TOKEN,
          useClass: Repository<RoleTranslation>,
        },
        LanguagesService,
        PermissionsService,
      ],
    })
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .overrideProvider(PermissionsService)
      .useValue(permissionsServiceMock)
      .compile();

    controller = module.get<RolesController>(RolesController);
    roleRepository = module.get<Repository<Role>>(ROLE_REPOSITORY_TOKEN);
    roleTranslationRepository = module.get<Repository<RoleTranslation>>(
      ROLE_TRANSLATION_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('GET /', () => {
    it('no DTO to test here', () => {
      expect(true).toBe(true);
    });
  });
});
