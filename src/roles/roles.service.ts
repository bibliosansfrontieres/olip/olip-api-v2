import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from './entities/role.entity';
import { Repository } from 'typeorm';
import { RoleTranslation } from './entities/role-translation.entity';
import { CreateRoleDto } from './dto/create-role.dto';
import { CreateRoleTranslationDto } from './dto/create-role-translation.dto';
import { LanguagesService } from 'src/languages/languages.service';
import { RoleEnum } from './enums/role.enum';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { Permission } from 'src/permissions/entities/permission.entity';
import { PermissionsService } from 'src/permissions/permissions.service';
import { invalidateCache } from 'src/utils/caching.util';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Role) private roleRepository: Repository<Role>,
    @InjectRepository(RoleTranslation)
    private roleTranslationRepository: Repository<RoleTranslation>,
    private languagesService: LanguagesService,
    private permissionsService: PermissionsService,
  ) {}

  async create(
    createRoleDto: CreateRoleDto,
    createRoleTranslationDto: CreateRoleTranslationDto,
  ) {
    const language = await this.languagesService.findOne({
      where: { id: createRoleTranslationDto.languageId },
    });
    if (language === null) {
      return null;
    }
    const alreadyExists = await this.findRoleByName(createRoleDto.name);
    if (alreadyExists !== null) {
      return null;
    }
    let role = new Role(createRoleDto);
    role = await this.roleRepository.save(role);
    let roleTranslation = new RoleTranslation(
      createRoleTranslationDto,
      role,
      language,
    );
    roleTranslation =
      await this.roleTranslationRepository.save(roleTranslation);
    role.roleTranslations.push(roleTranslation);
    await invalidateCache(Role);
    return role;
  }

  async addPermissionToRole(roleId: number, permissions: PermissionEnum[]) {
    let role = await this.findRoleById(roleId);
    if (role === null) return null;
    const perms: Permission[] = [];
    for (const value of permissions) {
      const permission =
        await this.permissionsService.findOnePermissionByName(value);
      if (permission !== null) {
        perms.push(permission);
      }
    }
    role.permissions = [...role.permissions, ...perms];
    role = await this.roleRepository.save(role);
    await invalidateCache(Role);
    return role;
  }

  findRoleById(id: number) {
    return this.roleRepository.findOne({
      where: { id },
      relations: ['permissions'],
    });
  }

  findRoleByName(name: RoleEnum) {
    return this.roleRepository.findOne({ where: { name } });
  }

  async findAllRoles() {
    return this.roleRepository.find();
  }

  async getRoles(languageIdentifier: string) {
    const roles = await this.roleRepository.find({
      relations: { permissions: true },
    });
    for (const role of roles) {
      const translation = await this.languagesService.getTranslation<
        Role,
        RoleTranslation
      >('role', role, this.roleTranslationRepository, languageIdentifier);
      role.roleTranslations = [translation];
    }
    return roles;
  }
}
