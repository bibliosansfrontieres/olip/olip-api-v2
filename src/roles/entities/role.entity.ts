import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { RoleTranslation } from './role-translation.entity';
import { Permission } from 'src/permissions/entities/permission.entity';
import { User } from 'src/users/entities/user.entity';
import { RoleEnum } from '../enums/role.enum';
import { CreateRoleDto } from '../dto/create-role.dto';
import { Visibility } from 'src/visibilities/entities/visibility.entity';

@Entity()
export class Role {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: RoleEnum;

  @ManyToMany(() => Permission, (permission) => permission.roles)
  @JoinTable()
  permissions: Permission[];

  @OneToMany(() => RoleTranslation, (roleTranslation) => roleTranslation.role)
  roleTranslations: RoleTranslation[];

  @OneToMany(() => User, (user) => user.role)
  users: User[];

  @ManyToMany(() => Visibility, (visibility) => visibility.roles)
  visibilities: Visibility[];

  constructor(createRoleDto: CreateRoleDto) {
    if (!createRoleDto) return;
    this.name = createRoleDto.name;
    this.roleTranslations = [];
    this.permissions = [];
    this.users = [];
  }
}
