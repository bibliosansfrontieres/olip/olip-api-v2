import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Role } from './role.entity';
import { Language } from 'src/languages/entities/language.entity';
import { CreateRoleTranslationDto } from '../dto/create-role-translation.dto';

@Entity()
export class RoleTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @ManyToOne(() => Role, (role) => role.roleTranslations)
  role: Role;

  @ManyToOne(() => Language, (language) => language.roleTranslations)
  language: Language;

  constructor(
    createRoleTranslationDto: CreateRoleTranslationDto,
    role: Role,
    language: Language,
  ) {
    if (!createRoleTranslationDto) return;
    this.label = createRoleTranslationDto.label;
    this.role = role;
    this.language = language;
  }
}
