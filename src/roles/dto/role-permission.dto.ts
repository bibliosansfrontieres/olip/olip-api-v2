import { ApiProperty } from '@nestjs/swagger';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';

export class RolePermissionDto {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: PermissionEnum.READ_USERS })
  name: string;
}
