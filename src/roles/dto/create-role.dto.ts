import { IsString } from 'class-validator';
import { RoleEnum } from '../enums/role.enum';

export class CreateRoleDto {
  @IsString()
  name: RoleEnum;
}
