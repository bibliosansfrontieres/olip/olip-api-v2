import { IsNumber, IsString } from 'class-validator';

export class CreateRoleTranslationDto {
  @IsString()
  label: string;

  @IsNumber()
  languageId: number;
}
