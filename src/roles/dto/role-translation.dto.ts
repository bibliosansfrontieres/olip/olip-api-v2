import { ApiProperty } from '@nestjs/swagger';
import { LanguageDto } from 'src/languages/dto/language.dto';

export class RoleTranslationDto {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: 'Administrator' })
  label: string;

  @ApiProperty({ type: LanguageDto })
  language: LanguageDto;
}
