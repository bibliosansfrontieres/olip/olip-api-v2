import { ApiProperty } from '@nestjs/swagger';
import { RoleEnum } from '../enums/role.enum';
import { RoleTranslationDto } from './role-translation.dto';
import { RolePermissionDto } from './role-permission.dto';

export class RoleDto {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: RoleEnum.ADMIN })
  name: RoleEnum;

  @ApiProperty({ isArray: true, type: RolePermissionDto })
  permissions: RolePermissionDto[];

  @ApiProperty({ isArray: true, type: RoleTranslationDto })
  roleTranslations: RoleTranslationDto[];
}
