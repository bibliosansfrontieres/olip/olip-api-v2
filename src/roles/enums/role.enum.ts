import { PermissionEnum } from 'src/permissions/enums/permission.enum';

export enum RoleEnum {
  ADMIN = 'admin',
  ANIMATOR = 'animator',
  USER = 'user',
  USER_NO_PASSWORD = 'user_no_password',
}

export const rolesTrads = [
  {
    name: RoleEnum.ADMIN,
    label: 'Administrator',
  },
  {
    name: RoleEnum.ANIMATOR,
    label: 'Animator',
  },
  {
    name: RoleEnum.USER,
    label: 'User',
  },
  {
    name: RoleEnum.USER_NO_PASSWORD,
    label: 'User without password',
  },
];

const allPermissions = () => {
  const perms = [];
  for (const value in PermissionEnum) {
    perms.push(PermissionEnum[value]);
  }
  return perms;
};

export const rolesPermissions = [
  {
    name: RoleEnum.ADMIN,
    permissions: allPermissions(),
  },
  {
    name: RoleEnum.ANIMATOR,
    permissions: [
      PermissionEnum.CREATE_EDITO,
      PermissionEnum.DELETE_EDITO,
      PermissionEnum.UPDATE_HIGHLIGHT,
      PermissionEnum.UPDATE_SETTING,
      PermissionEnum.CREATE_CATEGORY,
      PermissionEnum.UPDATE_CATEGORY,
      PermissionEnum.READ_ACTIVITY,
      PermissionEnum.READ_SSID,
      PermissionEnum.READ_STORAGE,
      PermissionEnum.READ_USERS,
    ],
  },
  {
    name: RoleEnum.USER,
    permissions: [],
  },
  {
    name: RoleEnum.USER_NO_PASSWORD,
    permissions: [],
  },
];
