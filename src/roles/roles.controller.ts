import { Controller, Get, Query } from '@nestjs/common';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { RolesService } from './roles.service';
import { RoleDto } from './dto/role.dto';
import { LanguageNotFound } from 'src/languages/responses/errors/language-not-found';
import { cachedMethod } from 'src/utils/caching.util';
import { Role } from './entities/role.entity';

@ApiTags('roles')
@Controller('api/roles')
export class RolesController {
  constructor(private roleService: RolesService) {}

  @Get()
  @ApiOperation({ summary: 'Get all roles' })
  @ApiOkResponse({ type: RoleDto, isArray: true })
  @ApiNotFoundResponse({ type: LanguageNotFound })
  @ApiQuery({ name: 'languageIdentifier', required: true })
  getRoles(@Query('languageIdentifier') languageIdentifier: string) {
    return cachedMethod(
      'rolesController',
      'getRoles',
      [{ languageIdentifier }],
      async () => {
        return this.roleService.getRoles(languageIdentifier);
      },
      [Role],
    );
  }
}
