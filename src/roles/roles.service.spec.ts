import { Test, TestingModule } from '@nestjs/testing';
import { RolesService } from './roles.service';
import { Repository } from 'typeorm';
import { Role } from './entities/role.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { RoleTranslation } from './entities/role-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';
import { PermissionsService } from 'src/permissions/permissions.service';
import { permissionsServiceMock } from 'src/tests/mocks/providers/permissions-service.mock';
import { roleMock } from 'src/tests/mocks/objects/role.mock';
import { roleTranslationMock } from 'src/tests/mocks/objects/role-translation.mock';
import { LanguageNotFound } from 'src/languages/responses/errors/language-not-found';
import { NotFoundException } from '@nestjs/common';

describe('RolesService', () => {
  let service: RolesService;
  let roleRepository: Repository<Role>;
  let roleTranslationRepository: Repository<RoleTranslation>;
  let languagesService: LanguagesService;

  const ROLE_REPOSITORY_TOKEN = getRepositoryToken(Role);
  const ROLE_TRANSLATION_REPOSITORY_TOKEN = getRepositoryToken(RoleTranslation);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RolesService,
        { provide: ROLE_REPOSITORY_TOKEN, useClass: Repository<Role> },
        {
          provide: ROLE_TRANSLATION_REPOSITORY_TOKEN,
          useClass: Repository<RoleTranslation>,
        },
        LanguagesService,
        PermissionsService,
      ],
    })
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .overrideProvider(PermissionsService)
      .useValue(permissionsServiceMock)
      .compile();

    service = module.get<RolesService>(RolesService);
    roleRepository = module.get<Repository<Role>>(ROLE_REPOSITORY_TOKEN);
    roleTranslationRepository = module.get<Repository<RoleTranslation>>(
      ROLE_TRANSLATION_REPOSITORY_TOKEN,
    );
    languagesService = module.get<LanguagesService>(LanguagesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getRoles()', () => {
    const rolesArray = [roleMock(), roleMock()];
    it('should return an array of roles', async () => {
      jest.spyOn(roleRepository, 'find').mockResolvedValue(rolesArray);
      jest
        .spyOn(languagesService, 'getTranslation')
        .mockResolvedValue(roleTranslationMock());
      const result = await service.getRoles('fra');
      expect(result).toBeDefined();
      expect(result).toEqual(rolesArray);
    });
    it('should throw error if not language found', async () => {
      jest.spyOn(roleRepository, 'find').mockResolvedValue(rolesArray);
      jest
        .spyOn(languagesService, 'getTranslation')
        .mockRejectedValue(new NotFoundException());
      await expect(service.getRoles('fra')).rejects.toThrowError();
    });
  });
});
