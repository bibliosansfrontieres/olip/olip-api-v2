import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { createReadStream, existsSync, statSync } from 'fs';
import { FastifyReply, FastifyRequest } from 'fastify';
import { resolve } from 'path';
import * as mime from 'mime-types';

const allowedExt = [
  '.js',
  '.ico',
  '.css',
  '.png',
  '.jpg',
  '.woff2',
  '.woff',
  '.ttf',
  '.svg',
  '.webp',
  '.mp3',
  '.pdf',
  '.mp4',
  '.html',
];

const resolvePath = (file: string) => resolve(`./data/static/${file}`);

@Injectable()
export class FilesMiddleware implements NestMiddleware {
  use(req: FastifyRequest['raw'], res: FastifyReply['raw']) {
    //@ts-expect-error originalUrl exists
    const { originalUrl } = req;
    if (
      allowedExt.filter((ext) => originalUrl.indexOf(ext) > 0).length > 0 &&
      existsSync(resolvePath(originalUrl))
    ) {
      const filePath = resolvePath(originalUrl);
      const stat = statSync(filePath);
      const fileSize = stat.size;
      const mimeType = mime.lookup(originalUrl) || 'text/plain';

      res.setHeader('Content-Type', mimeType);
      res.setHeader('Accept-Ranges', 'bytes');

      const range = req.headers.range;

      if (range) {
        // Handle range request
        const [startStr, endStr] = range.replace(/bytes=/, '').split('-');
        const start = parseInt(startStr, 10);
        const end = endStr ? parseInt(endStr, 10) : fileSize - 1;

        // Validate range
        if (start >= fileSize || end >= fileSize || start > end) {
          res.statusCode = 416; // Requested Range Not Satisfiable
          res.setHeader('Content-Range', `bytes */${fileSize}`);
          return res.end();
        }

        const chunkSize = end - start + 1;
        res.statusCode = 206; // Partial Content
        res.setHeader('Content-Range', `bytes ${start}-${end}/${fileSize}`);
        res.setHeader('Content-Length', chunkSize);

        const stream = createReadStream(filePath, { start, end });
        return stream.pipe(res);
      } else {
        // Serve the full file
        res.setHeader('Content-Length', fileSize);
        const stream = createReadStream(filePath);
        return stream.pipe(res);
      }
    } else {
      throw new HttpException('Not Found', HttpStatus.NOT_FOUND);
    }
  }
}
