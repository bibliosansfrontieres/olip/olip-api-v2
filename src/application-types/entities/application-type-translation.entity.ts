import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ApplicationType } from './application-type.entity';
import { Language } from 'src/languages/entities/language.entity';
import { CreateApplicationTypeTranslationDto } from '../dto/create-application-type-translation.dto';

@Entity()
export class ApplicationTypeTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  description: string;

  @ManyToOne(
    () => ApplicationType,
    (applicationType) => applicationType.applicationTypeTranslations,
  )
  applicationType: ApplicationType;

  @ManyToOne(() => Language, (language) => language.applicationTypeTranslations)
  language: Language;

  constructor(
    createApplicationTypeTranslationDto: CreateApplicationTypeTranslationDto,
    language: Language,
  ) {
    if (!createApplicationTypeTranslationDto) return;
    this.label = createApplicationTypeTranslationDto.label;
    this.description = createApplicationTypeTranslationDto.description;
    this.language = language;
  }
}
