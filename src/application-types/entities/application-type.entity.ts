import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm';
import { Application } from 'src/applications/entities/application.entity';
import { ApplicationTypeTranslation } from './application-type-translation.entity';
import { CreateApplicationTypeDto } from '../dto/create-application-type.dto';

@Entity()
export class ApplicationType {
  @PrimaryColumn()
  id: string;

  @Column()
  name: string;

  @Column()
  md5: string;

  @OneToMany(() => Application, (application) => application.applicationType)
  applications: Application[];

  @OneToMany(
    () => ApplicationTypeTranslation,
    (applicationTypeTranslation) => applicationTypeTranslation.applicationType,
    { cascade: true },
  )
  applicationTypeTranslations: ApplicationTypeTranslation[];

  constructor(createApplicationTypeDto: CreateApplicationTypeDto) {
    if (!createApplicationTypeDto) return;
    this.id = createApplicationTypeDto.id;
    this.name = createApplicationTypeDto.name;
    this.md5 = createApplicationTypeDto.md5;
  }
}
