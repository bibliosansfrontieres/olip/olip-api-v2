export class CreateApplicationTypeTranslationDto {
  label: string;
  description: string;
  languageIdentifier: string;
}
