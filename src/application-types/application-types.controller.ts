import { Controller, Get, Query } from '@nestjs/common';
import { ApplicationTypesService } from './application-types.service';

@Controller('api/application-types')
export class ApplicationTypesController {
  constructor(
    private readonly applicationTypesServices: ApplicationTypesService,
  ) {}

  @Get()
  getAll(@Query('languageIdentifier') languageIdentifier: string) {
    return this.applicationTypesServices.getAll(languageIdentifier);
  }
}
