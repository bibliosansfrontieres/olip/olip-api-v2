import { Injectable } from '@nestjs/common';
import { FindOneOptions, Repository } from 'typeorm';
import { ApplicationType } from './entities/application-type.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { ApplicationTypeTranslation } from './entities/application-type-translation.entity';
import { CreateApplicationTypeDto } from './dto/create-application-type.dto';
import { LanguagesService } from 'src/languages/languages.service';

@Injectable()
export class ApplicationTypesService {
  constructor(
    @InjectRepository(ApplicationType)
    private applicationTypesRepository: Repository<ApplicationType>,
    @InjectRepository(ApplicationTypeTranslation)
    private applicationTypeTranslationsRepository: Repository<ApplicationTypeTranslation>,
    private languagesService: LanguagesService,
  ) {}

  async getAll(languageIdentifier: string) {
    const applicationTypes = await this.applicationTypesRepository.find();
    for (const applicationType of applicationTypes) {
      await this.getApplicationTypeTranslations(
        applicationType,
        languageIdentifier,
      );
    }
    return { applicationTypes };
  }

  findOne(options?: FindOneOptions<ApplicationType>) {
    return this.applicationTypesRepository.findOne(options);
  }

  async create(createApplicationTypeDto: CreateApplicationTypeDto) {
    const applicationTypeTranslations = [];
    for (const createApplicationTypeTranslation of createApplicationTypeDto.applicationTypeTranslations) {
      const language = await this.languagesService.findOneByIdentifier(
        createApplicationTypeTranslation.languageIdentifier,
      );
      const applicationTypeTranslation = new ApplicationTypeTranslation(
        createApplicationTypeTranslation,
        language,
      );
      applicationTypeTranslations.push(applicationTypeTranslation);
    }
    const applicationType = new ApplicationType(createApplicationTypeDto);
    applicationType.applicationTypeTranslations = applicationTypeTranslations;
    return this.applicationTypesRepository.save(applicationType);
  }

  async getApplicationTypeTranslations(
    applicationType: ApplicationType,
    languageIdentifier: string,
  ) {
    const translation = await this.languagesService.getTranslation(
      'applicationType',
      applicationType,
      this.applicationTypeTranslationsRepository,
      languageIdentifier,
    );
    applicationType.applicationTypeTranslations = [translation];
    return applicationType;
  }
}
