import { Module } from '@nestjs/common';
import { ApplicationTypesService } from './application-types.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApplicationType } from './entities/application-type.entity';
import { ApplicationTypeTranslation } from './entities/application-type-translation.entity';
import { LanguagesModule } from 'src/languages/languages.module';
import { ApplicationTypesController } from './application-types.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([ApplicationType, ApplicationTypeTranslation]),
    LanguagesModule,
  ],
  controllers: [ApplicationTypesController],
  providers: [ApplicationTypesService],
  exports: [ApplicationTypesService],
})
export class ApplicationTypesModule {}
