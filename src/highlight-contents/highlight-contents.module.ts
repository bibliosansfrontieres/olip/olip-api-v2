import { Module, forwardRef } from '@nestjs/common';
import { HighlightContentsService } from './highlight-contents.service';
import { HighlightContentsController } from './highlight-contents.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HighlightContent } from './entities/highlight-content.entity';
import { ItemsModule } from 'src/items/items.module';
import { HighlightsModule } from 'src/highlights/highlights.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    TypeOrmModule.forFeature([HighlightContent]),
    ItemsModule,
    forwardRef(() => HighlightsModule),
    JwtModule,
    UsersModule,
    ConfigModule,
  ],
  controllers: [HighlightContentsController],
  providers: [HighlightContentsService],
  exports: [HighlightContentsService],
})
export class HighlightContentsModule {}
