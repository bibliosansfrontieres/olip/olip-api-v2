import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class CreateHighlightContentDto {
  @ApiProperty({ example: '1' })
  @IsString()
  itemId: string;

  @ApiHideProperty()
  @IsString()
  @IsOptional()
  order: string;
}
