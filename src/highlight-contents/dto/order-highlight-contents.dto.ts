import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';

export class OrderHighlightContentsDto {
  @ApiProperty({ example: [1, 2, 3] })
  @IsNumber(undefined, { each: true })
  highlightContentIds: number[];
}
