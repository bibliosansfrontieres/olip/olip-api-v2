import { IsArray, IsNumber } from 'class-validator';

export class DisplayHighlightContentOptionsDto {
  @IsArray()
  @IsNumber({}, { each: true })
  highlightContentIds: number[];
}
