import { Body, Controller, Delete, Param, Patch, Post } from '@nestjs/common';
import { HighlightContentsService } from './highlight-contents.service';
import { CreateHighlightContentDto } from './dto/create-highlight-content.dto';
import { DisplayHighlightContentOptionsDto } from './dto/display-highlight-content-options.dto';
import { OrderHighlightContentsDto } from './dto/order-highlight-contents.dto';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { Auth } from 'src/auth/decorators/auth.decorator';

@ApiTags('highlight-contents')
@Controller('api/highlight-contents')
export class HighlightContentsController {
  constructor(private highlightContentsService: HighlightContentsService) {}

  @Auth(PermissionEnum.UPDATE_HIGHLIGHT)
  @ApiOperation({ summary: 'Add a highlight content to highlight' })
  @ApiCreatedResponse({ description: 'Highlight content added to highlight' })
  @ApiNotFoundResponse({ description: 'Highlight not found/Item not found' })
  @ApiBadRequestResponse({
    description: 'Highlight content already exists in highlight selection',
  })
  @Post()
  create(@Body() createHighlightContentDto: CreateHighlightContentDto) {
    return this.highlightContentsService.create(createHighlightContentDto);
  }

  @Auth(PermissionEnum.UPDATE_HIGHLIGHT)
  @ApiOperation({
    summary: 'Remove a highlight content from highlight selection',
  })
  @ApiOkResponse({ description: 'Highlight content removed from highlight' })
  @ApiNotFoundResponse({ description: 'Highlight content not found' })
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.highlightContentsService.remove(+id);
  }

  @Auth(PermissionEnum.UPDATE_HIGHLIGHT)
  @ApiOperation({ summary: 'Display highlighted contents' })
  @ApiNotFoundResponse({ description: 'Highlight content not found' })
  @ApiBadRequestResponse()
  @Patch('display')
  display(
    @Body()
    displayHighlightContentOptionsDto: DisplayHighlightContentOptionsDto,
  ) {
    return this.highlightContentsService.display(
      displayHighlightContentOptionsDto,
    );
  }

  @Auth(PermissionEnum.UPDATE_HIGHLIGHT)
  @ApiOperation({ summary: 'Order highlight contents' })
  @ApiBody({ type: OrderHighlightContentsDto })
  @ApiCreatedResponse({ description: 'Highlight contents ordered' })
  @ApiBadRequestResponse()
  @Patch('order')
  order(@Body() orderHighlightContentsDto: OrderHighlightContentsDto) {
    return this.highlightContentsService.order(orderHighlightContentsDto);
  }
}
