import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { HighlightContent } from './entities/highlight-content.entity';
import { In, Not, Repository } from 'typeorm';
import { ItemsService } from 'src/items/items.service';
import { HighlightsService } from 'src/highlights/highlights.service';
import { CreateHighlightContentDto } from './dto/create-highlight-content.dto';
import { DisplayHighlightContentOptionsDto } from './dto/display-highlight-content-options.dto';
import { OrderHighlightContentsDto } from './dto/order-highlight-contents.dto';

@Injectable()
export class HighlightContentsService {
  constructor(
    @InjectRepository(HighlightContent)
    private highlightContentsRepository: Repository<HighlightContent>,
    private itemsService: ItemsService,
    @Inject(forwardRef(() => HighlightsService))
    private highlightsService: HighlightsService,
  ) {}

  private async getLastOrder(): Promise<number> {
    const query =
      this.highlightContentsRepository.createQueryBuilder('category');
    query.select('MAX(category.order)', 'order');
    const lastOrder = await query.getRawOne();
    return (lastOrder.order as number) || 0;
  }

  async create(createHighlightContentDto: CreateHighlightContentDto) {
    const id = createHighlightContentDto.itemId;
    const item = await this.itemsService.findOne({ where: { id } });
    if (item === null) throw new NotFoundException('Item not found');
    const highlight = await this.highlightsService.get();
    if (highlight === null) throw new NotFoundException('Highlight not found');
    const alreadyExists = await this.highlightContentsRepository.findOne({
      where: { item: { id: item.id }, highlight: { id: highlight.id } },
    });
    if (alreadyExists !== null)
      throw new BadRequestException(
        'Item already exists in highlight selection',
      );
    let order = (await this.getLastOrder()) + 1;
    if (createHighlightContentDto.order) {
      order = +createHighlightContentDto.order;
    }
    const highlightContent = new HighlightContent(item, highlight, order);
    return this.highlightContentsRepository.save(highlightContent);
  }

  async remove(id: number) {
    const highlightContent = await this.highlightContentsRepository.findOne({
      where: { id },
    });
    if (highlightContent === null)
      throw new NotFoundException('HighlightContent not found');
    return this.highlightContentsRepository.remove(highlightContent);
  }

  async display(
    displayHighlightContentOptionsDto: DisplayHighlightContentOptionsDto,
  ) {
    if (displayHighlightContentOptionsDto.highlightContentIds.length > 4) {
      throw new BadRequestException('Maximum 4 highlightContent');
    }

    for (const highlightContentId of displayHighlightContentOptionsDto.highlightContentIds) {
      const highlightContent = await this.highlightContentsRepository.findOne({
        where: { id: highlightContentId },
      });

      if (highlightContent === null) {
        throw new NotFoundException(
          `HighlightContent #${highlightContentId} not found`,
        );
      }

      highlightContent.isDisplayed = true;
      await this.highlightContentsRepository.save(highlightContent);
    }

    const notDisplayedContents = await this.highlightContentsRepository.find({
      where: {
        id: Not(In(displayHighlightContentOptionsDto.highlightContentIds)),
      },
    });
    for (const highlightContent of notDisplayedContents) {
      highlightContent.isDisplayed = false;
      await this.highlightContentsRepository.save(highlightContent);
    }
  }

  async order(orderHighlightContentsDto: OrderHighlightContentsDto) {
    const actualHighlight = await this.highlightsService.get();
    const highlightContents = await this.highlightContentsRepository.find({
      where: { highlight: { id: actualHighlight.id } },
    });

    // Sort the existing items based on the order of item IDs
    const sortedHighlightContents =
      orderHighlightContentsDto.highlightContentIds
        .map((highlightContentId) =>
          highlightContents.find((hC) => hC.id === highlightContentId),
        )
        .filter(Boolean);

    for (const highlightContent of highlightContents) {
      if (!sortedHighlightContents.includes(highlightContent)) {
        sortedHighlightContents.push(highlightContent);
      }
    }

    for (let i = 0; i < sortedHighlightContents.length; i++) {
      const highlightContent = sortedHighlightContents[i];
      highlightContent.order = i + 1;
      await this.highlightContentsRepository.save(highlightContent);
    }
  }
}
