import { Test, TestingModule } from '@nestjs/testing';
import { HighlightContentsController } from './highlight-contents.controller';
import { HighlightContentsService } from './highlight-contents.service';
import { HighlightContent } from './entities/highlight-content.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ItemsService } from 'src/items/items.service';
import { HighlightsService } from 'src/highlights/highlights.service';
import { itemsServiceMock } from 'src/tests/mocks/providers/items-service.mock';
import { highlightsServiceMock } from 'src/tests/mocks/providers/highlights-service.mock';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';

describe('HighlightContentsController', () => {
  let controller: HighlightContentsController;
  let highlightContentsRepository: Repository<HighlightContent>;
  const HIGHLIGHT_TRANSLATIONS_REPOSITORY_TOKEN =
    getRepositoryToken(HighlightContent);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HighlightContentsController],
      providers: [
        HighlightContentsService,
        {
          provide: HIGHLIGHT_TRANSLATIONS_REPOSITORY_TOKEN,
          useClass: Repository<HighlightContent>,
        },
        ItemsService,
        HighlightsService,
        JwtService,
        UsersService,
        ConfigService,
      ],
    })
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .overrideProvider(HighlightsService)
      .useValue(highlightsServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    controller = module.get<HighlightContentsController>(
      HighlightContentsController,
    );
    highlightContentsRepository = module.get<Repository<HighlightContent>>(
      HIGHLIGHT_TRANSLATIONS_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
