import { Highlight } from 'src/highlights/entities/highlight.entity';
import { Item } from 'src/items/entities/item.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class HighlightContent {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  order: number;

  @Column()
  isDisplayed: boolean;

  @ManyToOne(() => Item, (item) => item.highlightContents, {
    onDelete: 'CASCADE',
  })
  item: Item;

  @ManyToOne(() => Highlight, (highlight) => highlight.highlightContents, {
    onDelete: 'CASCADE',
  })
  highlight: Highlight;

  constructor(item: Item, highlight: Highlight, order: number) {
    this.item = item;
    this.highlight = highlight;
    this.order = order;
    this.isDisplayed = false;
  }
}
