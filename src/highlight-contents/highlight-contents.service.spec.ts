import { Test, TestingModule } from '@nestjs/testing';
import { HighlightContentsService } from './highlight-contents.service';
import { ItemsService } from 'src/items/items.service';
import { Repository } from 'typeorm';
import { HighlightContent } from './entities/highlight-content.entity';
import { HighlightsService } from 'src/highlights/highlights.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { itemsServiceMock } from 'src/tests/mocks/providers/items-service.mock';
import { highlightsServiceMock } from 'src/tests/mocks/providers/highlights-service.mock';

describe('HighlightContentsService', () => {
  let service: HighlightContentsService;
  let highlightContentsRepository: Repository<HighlightContent>;
  const HIGHLIGHT_TRANSLATIONS_REPOSITORY_TOKEN =
    getRepositoryToken(HighlightContent);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        HighlightContentsService,
        {
          provide: HIGHLIGHT_TRANSLATIONS_REPOSITORY_TOKEN,
          useClass: Repository<HighlightContent>,
        },
        ItemsService,
        HighlightsService,
      ],
    })
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .overrideProvider(HighlightsService)
      .useValue(highlightsServiceMock)
      .compile();

    service = module.get<HighlightContentsService>(HighlightContentsService);
    highlightContentsRepository = module.get<Repository<HighlightContent>>(
      HIGHLIGHT_TRANSLATIONS_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
