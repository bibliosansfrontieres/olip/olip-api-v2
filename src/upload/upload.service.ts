import { Injectable, NotAcceptableException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as fileType from 'file-type';
import {
  copyFileSync,
  existsSync,
  mkdirSync,
  unlinkSync,
  writeFileSync,
} from 'fs';
import { basename, join } from 'path';
import { bytesToKb } from 'src/utils/bytes.util';
import { extractBase64Header } from 'src/utils/extract-base64-header.util';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class UploadService {
  private maxFileSizeInKb: number;

  constructor(configService: ConfigService) {
    this.maxFileSizeInKb =
      configService.get<number>('MAX_IMAGE_SIZE_UPLOAD_IN_KB') || 1024;
  }

  async delete(path: string) {
    path = path.replace('images/', '');
    const filePath = join(__dirname, `../../data/static/${path}`);
    if (existsSync(filePath)) {
      unlinkSync(filePath);
    }
  }

  async image(base64Data: string) {
    const { header, data } = extractBase64Header(base64Data);
    const fileBuffer = Buffer.from(data, 'base64');
    const fileSize = Buffer.byteLength(fileBuffer);
    const fileSizeInKb = bytesToKb(fileSize);

    // Check file size before working with it
    if (fileSizeInKb > this.maxFileSizeInKb) {
      throw new Error('file size is too big');
    }

    // Try to get extension and mime type from base64 data
    let ext: string | undefined;
    try {
      const type = await fileType.fromBuffer(fileBuffer);
      if (header.includes('image/svg+xml')) {
        ext = 'svg';
      } else {
        if (!type.mime.includes('image/')) {
          throw new NotAcceptableException('file is not an image');
        }
        ext = type.ext;
      }
    } catch (e) {
      console.error(e);
      throw new NotAcceptableException('file is not an image');
    }

    const folderPath = join(__dirname, `../../data/static/images`);
    const fileName = `${uuidv4()}.${ext}`;
    const filePath = join(folderPath, fileName);

    if (!existsSync(folderPath)) {
      mkdirSync(folderPath);
    }

    writeFileSync(filePath, fileBuffer);

    return `images/${fileName}`;
  }

  async copyImage(oldPath: string, rename = true) {
    const folderPath = join(__dirname, '../../data/static/images');
    const baseName = basename(oldPath);

    if (!existsSync(folderPath)) {
      mkdirSync(folderPath);
    }

    if (rename === true) {
      const extSplit = baseName.split('.');
      const ext = extSplit[extSplit.length - 1];
      const fileName = `${uuidv4()}.${ext}`;
      copyFileSync(oldPath, join(folderPath, fileName));
      return `images/${fileName}`;
    } else {
      copyFileSync(oldPath, join(folderPath, baseName));
      return `images/${baseName}`;
    }
  }
}
