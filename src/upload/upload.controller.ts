import { Body, Controller, Post } from '@nestjs/common';
import { UploadService } from './upload.service';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('upload')
@Controller('api/upload')
export class UploadController {
  constructor(private readonly uploadService: UploadService) {}

  @Auth()
  @Post('image')
  @ApiResponse({ type: 'string' })
  image(@Body('base64Data') base64Data: string) {
    return this.uploadService.image(base64Data);
  }
}
