import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { RolesService } from 'src/roles/roles.service';
import { rolesServiceMock } from 'src/tests/mocks/providers/roles-service.mock';
import { CreateUserDto } from './dto/create-user.dto';
import { userMock } from 'src/tests/mocks/objects/user.mock';
import { roleMock } from 'src/tests/mocks/objects/role.mock';
import { RoleEnum } from 'src/roles/enums/role.enum';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UploadService } from 'src/upload/upload.service';
import { uploadServiceMock } from 'src/tests/mocks/providers/upload-service.mock';

describe('UsersService', () => {
  let service: UsersService;
  let userRepository: Repository<User>;
  let rolesService: RolesService;

  const USER_REPOSITORY_TOKEN = getRepositoryToken(User);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        UploadService,
        { provide: USER_REPOSITORY_TOKEN, useClass: Repository<User> },
        RolesService,
      ],
    })
      .overrideProvider(UploadService)
      .useValue(uploadServiceMock)
      .overrideProvider(RolesService)
      .useValue(rolesServiceMock)
      .compile();

    service = module.get<UsersService>(UsersService);
    userRepository = module.get<Repository<User>>(USER_REPOSITORY_TOKEN);
    rolesService = module.get<RolesService>(RolesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create()', () => {
    const createUserDto: CreateUserDto = {
      username: 'test',
      password: 'test',
      photo: '',
      language: 'fra',
      roleId: 1,
    };
    it('should create an admin user', async () => {
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(null);
      jest.spyOn(userRepository, 'save').mockResolvedValueOnce(userMock());
      const result = await service.create(createUserDto);
      expect(result).toBeDefined();
    });
    it('should create a user without password', async () => {
      jest
        .spyOn(rolesService, 'findRoleById')
        .mockResolvedValueOnce(roleMock({ name: RoleEnum.USER_NO_PASSWORD }));
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(null);
      jest.spyOn(userRepository, 'save').mockResolvedValueOnce(userMock());
      delete createUserDto.password;
      const result = await service.create(createUserDto);
      expect(result).toBeDefined();
    });
    it('should throw error if no data provided', async () => {
      await expect(service.create(undefined)).rejects.toThrowError();
    });
    it('should throw error if no password and role not user_no_password', async () => {
      jest
        .spyOn(rolesService, 'findRoleById')
        .mockResolvedValueOnce(roleMock({ name: RoleEnum.ADMIN }));
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(null);
      jest.spyOn(userRepository, 'save').mockResolvedValueOnce(userMock());
      await expect(service.create(createUserDto)).rejects.toThrowError();
      jest
        .spyOn(rolesService, 'findRoleById')
        .mockResolvedValueOnce(roleMock({ name: RoleEnum.USER }));
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(null);
      jest.spyOn(userRepository, 'save').mockResolvedValueOnce(userMock());
      await expect(service.create(createUserDto)).rejects.toThrowError();
    });
    it('should throw error if role not found', async () => {
      jest.spyOn(rolesService, 'findRoleById').mockResolvedValueOnce(null);
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(null);
      jest.spyOn(userRepository, 'save').mockResolvedValueOnce(userMock());
      await expect(service.create(createUserDto)).rejects.toThrowError();
    });
    it('should throw error if username already exists', async () => {
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(userMock());
      jest
        .spyOn(rolesService, 'findRoleById')
        .mockResolvedValueOnce(roleMock({ name: RoleEnum.USER }));
      jest.spyOn(userRepository, 'save').mockResolvedValueOnce(userMock());
      await expect(service.create(createUserDto)).rejects.toThrowError();
    });
  });

  describe('updateProfile()', () => {
    const updateProfileDto: UpdateProfileDto = {
      username: 'test',
      password: 'test',
      photo: 'test',
      language: 'fra',
    };
    it('should update a user profile', async () => {
      jest
        .spyOn(userRepository, 'findOne')
        .mockResolvedValueOnce(
          userMock({ role: roleMock({ name: RoleEnum.USER }) }),
        );
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(null);
      jest.spyOn(userRepository, 'save').mockResolvedValueOnce(userMock());
      const result = await service.updateProfile(1, updateProfileDto);
      expect(result).toBeDefined();
    });
    it('should throw error if no data provided', async () => {
      await expect(service.updateProfile(1, undefined)).rejects.toThrowError();
    });
    it('should throw error if user not found', async () => {
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(null);
      jest.spyOn(userRepository, 'save').mockResolvedValueOnce(userMock());
      await expect(
        service.updateProfile(1, updateProfileDto),
      ).rejects.toThrowError();
    });
    it('should throw error if admin user edit username', async () => {
      jest
        .spyOn(userRepository, 'findOne')
        .mockResolvedValueOnce(userMock({ username: 'admin' }));
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(null);
      jest.spyOn(userRepository, 'save').mockResolvedValueOnce(userMock());
      await expect(
        service.updateProfile(1, updateProfileDto),
      ).rejects.toThrowError();
    });
    it('should throw error if username is edited and already exists', async () => {
      jest
        .spyOn(userRepository, 'findOne')
        .mockResolvedValueOnce(userMock({ username: 'thing' }));
      jest
        .spyOn(userRepository, 'findOne')
        .mockResolvedValueOnce(userMock({ username: 'test' }));
      jest.spyOn(userRepository, 'save').mockResolvedValueOnce(userMock());
      await expect(
        service.updateProfile(1, updateProfileDto),
      ).rejects.toThrowError();
    });
    it('should throw error if change password for no password role', async () => {
      jest
        .spyOn(userRepository, 'findOne')
        .mockResolvedValueOnce(
          userMock({ role: roleMock({ name: RoleEnum.USER_NO_PASSWORD }) }),
        );
      jest
        .spyOn(rolesService, 'findRoleById')
        .mockResolvedValueOnce(roleMock({ name: RoleEnum.USER_NO_PASSWORD }));
      jest
        .spyOn(userRepository, 'findOne')
        .mockResolvedValueOnce(
          userMock({ role: roleMock({ name: RoleEnum.USER_NO_PASSWORD }) }),
        );
      updateProfileDto.password = 'test';
      await expect(
        service.updateProfile(1, updateProfileDto),
      ).rejects.toThrowError();
    });
  });

  describe('update()', () => {
    const updateUserDto: UpdateUserDto = {
      username: 'test',
      password: 'test',
      photo: 'test',
      language: 'fra',
      roleId: 1,
    };
    it('should update a user', async () => {
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(userMock());
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(null);
      jest
        .spyOn(rolesService, 'findRoleById')
        .mockResolvedValueOnce(roleMock({ name: RoleEnum.ADMIN }));
      jest.spyOn(userRepository, 'save').mockResolvedValueOnce(userMock());
      const result = await service.update(1, updateUserDto, 2);
      expect(result).toBeDefined();
    });
    it('should throw error if no data provided', async () => {
      await expect(service.update(1, undefined, 2)).rejects.toThrowError();
    });
    it('should throw error if user not found', async () => {
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(null);
      await expect(service.update(1, updateUserDto, 2)).rejects.toThrowError();
    });
    it('should throw error if user is admin', async () => {
      jest
        .spyOn(userRepository, 'findOne')
        .mockResolvedValueOnce(userMock({ username: 'admin' }));
      await expect(service.update(1, updateUserDto, 2)).rejects.toThrowError();
    });
    it('should throw error if role not found', async () => {
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(userMock());
      jest.spyOn(rolesService, 'findRoleById').mockResolvedValueOnce(null);
      await expect(service.update(1, updateUserDto, 2)).rejects.toThrowError();
    });
    it('should throw error if change username and already exists', async () => {
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(userMock());
      jest
        .spyOn(rolesService, 'findRoleById')
        .mockResolvedValueOnce(roleMock());
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(userMock());
      await expect(service.update(1, updateUserDto, 2)).rejects.toThrowError();
    });
    it('should throw error if change role id with password without sending password', async () => {
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(userMock());
      jest
        .spyOn(rolesService, 'findRoleById')
        .mockResolvedValueOnce(roleMock());
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(userMock());
      delete updateUserDto.password;
      await expect(service.update(1, updateUserDto, 2)).rejects.toThrowError();
    });
    it('should throw error if change password for no password role', async () => {
      const role = roleMock({ name: RoleEnum.USER_NO_PASSWORD });
      const user = userMock({
        username: 'test',
        password: null,
        role,
      });
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(user);
      jest.spyOn(rolesService, 'findRoleById').mockResolvedValueOnce(role);
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(userMock());
      updateUserDto.password = 'test';
      await expect(service.update(1, updateUserDto, 2)).rejects.toThrowError();
    });
  });

  describe('delete()', () => {
    it('should delete a user', async () => {
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(userMock());
      jest.spyOn(userRepository, 'remove').mockResolvedValueOnce(userMock());
      const result = await service.delete(1, 2);
      expect(result).toBeDefined();
    });
    it('should throw error if user not found', async () => {
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(null);
      await expect(service.delete(1, 2)).rejects.toThrowError();
    });
    it('should throw error if delete admin', async () => {
      jest
        .spyOn(userRepository, 'findOne')
        .mockResolvedValueOnce(userMock({ username: 'admin' }));
      await expect(service.delete(1, 2)).rejects.toThrowError();
    });
  });
});
