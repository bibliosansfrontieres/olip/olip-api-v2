import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { RolesService } from 'src/roles/roles.service';
import { rolesServiceMock } from 'src/tests/mocks/providers/roles-service.mock';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { CreateUserDto } from './dto/create-user.dto';
import { validateCreateUserDto } from 'src/tests/validate/validate-create-user.dto';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { OrderEnum } from 'src/utils/classes/order.enum';
import { UserOrderDto } from './dto/user-order.dto';
import { UserOrderEnum } from './enums/user-order.enum';
import { validatePageOptionsDto } from 'src/tests/validate/validate-page-options.dto';
import { validateUserOrderDto } from 'src/tests/validate/validate-user-order.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { validateUpdateUserDto } from 'src/tests/validate/validate-update-user.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { validateUpdateProfileDto } from 'src/tests/validate/validate-update-profile.dto';
import { UploadService } from 'src/upload/upload.service';
import { uploadServiceMock } from 'src/tests/mocks/providers/upload-service.mock';

describe('UsersController', () => {
  let controller: UsersController;
  const USER_REPOSITORY_TOKEN = getRepositoryToken(User);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UsersService,
        UploadService,
        RolesService,
        JwtService,
        ConfigService,
        {
          provide: USER_REPOSITORY_TOKEN,
          useClass: Repository<User>,
        },
      ],
    })
      .overrideProvider(UploadService)
      .useValue(uploadServiceMock)
      .overrideProvider(RolesService)
      .useValue(rolesServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('POST /', () => {
    const createUserDto: CreateUserDto = {
      username: 'testest',
      password: '123456',
      language: 'eng',
      photo: 'test',
      roleId: 1,
    };
    it('should accept createUserDto', async () => {
      const errors = await validateCreateUserDto(createUserDto);
      expect(errors.length).toBe(0);
    });
    it('should accept createUserDto without photo', async () => {
      createUserDto.photo = null;
      const errors = await validateCreateUserDto(createUserDto);
      expect(errors.length).toBe(0);
    });
    it('should accept createUserDto without password', async () => {
      delete createUserDto.password;
      const errors = await validateCreateUserDto(createUserDto);
      expect(errors.length).toBe(0);
    });
    it("shouldn't accept createUserDto with password length less than 5", async () => {
      createUserDto.password = '1234';
      const errors = await validateCreateUserDto(createUserDto);
      expect(errors.length).not.toBe(0);
    });
    it("shouldn't accept createUserDto if password empty", async () => {
      createUserDto.password = '';
      const errors = await validateCreateUserDto(createUserDto);
      expect(errors.length).not.toBe(0);
    });
    it("shouldn't accept createUserDto if username empty", async () => {
      createUserDto.username = '';
      const errors = await validateCreateUserDto(createUserDto);
      expect(errors.length).not.toBe(0);
    });
    it("shouldn't accept createUserDto if no username", async () => {
      delete createUserDto.username;
      const errors = await validateCreateUserDto(createUserDto);
      expect(errors.length).not.toBe(0);
    });
    it("shouldn't accept createUserDto if language empty", async () => {
      createUserDto.language = '';
      const errors = await validateCreateUserDto(createUserDto);
      expect(errors.length).not.toBe(0);
    });
    it("shouldn't accept createUserDto if no language", async () => {
      delete createUserDto.language;
      const errors = await validateCreateUserDto(createUserDto);
      expect(errors.length).not.toBe(0);
    });
    it("shouldn't accept createUserDto if no roleId", async () => {
      delete createUserDto.roleId;
      const errors = await validateCreateUserDto(createUserDto);
      expect(errors.length).not.toBe(0);
    });
  });

  describe('GET /', () => {
    const pageOptionsDto: PageOptionsDto = {
      order: OrderEnum.ASC,
      page: 1,
      take: 5,
    };
    const userOrderDto: UserOrderDto = {
      orderByField: UserOrderEnum.ID,
    };
    it('should accept pageOptionsDto', async () => {
      const errors = await validatePageOptionsDto(pageOptionsDto);
      expect(errors.length).toBe(0);
    });
    it('should accept pageOptionsDto without order, page and take', async () => {
      let errors = await validatePageOptionsDto(pageOptionsDto);
      expect(errors.length).toBe(0);
      delete pageOptionsDto.order;
      delete pageOptionsDto.page;
      delete pageOptionsDto.take;
      errors = await validatePageOptionsDto(pageOptionsDto);
      expect(errors.length).toBe(0);
    });
    it("shouldn't accept pageOptionsDto with page < 1", async () => {
      pageOptionsDto.page = 0;
      const errors = await validatePageOptionsDto(pageOptionsDto);
      expect(errors.length).not.toBe(0);
    });
    it("shouldn't accept pageOptionsDto with take < 1", async () => {
      pageOptionsDto.take = 0;
      const errors = await validatePageOptionsDto(pageOptionsDto);
      expect(errors.length).not.toBe(0);
    });
    it("shouldn't accept pageOptionsDto with order not ASC,DESC", async () => {
      // @ts-expect-error : This is for testing purposes
      pageOptionsDto.order = 'test';
      const errors = await validatePageOptionsDto(pageOptionsDto);
      expect(errors.length).not.toBe(0);
    });
    it('should accept userOrderDto', async () => {
      const errors = await validateUserOrderDto(userOrderDto);
      expect(errors.length).toBe(0);
    });
    it("shouldn't accept userOrderDto with orderByField not username,role,id", async () => {
      // @ts-expect-error : This is for testing purposes
      userOrderDto.orderByField = 'test';
      const errors = await validateUserOrderDto(userOrderDto);
      expect(errors.length).not.toBe(0);
    });
  });

  describe('GET /:id', () => {
    it('no DTO to test here', () => {
      expect(true).toBe(true);
    });
  });

  describe('PATCH /:id', () => {
    const updateUserDto: UpdateUserDto = {
      username: 'string',
      password: 'string',
      photo: 'string',
      language: 'string',
      roleId: 1,
    };
    it('should accept updateUserDto', async () => {
      const errors = await validateUpdateUserDto(updateUserDto);
      expect(errors.length).toBe(0);
    });
    it('should accept updateUserDto without any field', async () => {
      delete updateUserDto.username;
      delete updateUserDto.password;
      delete updateUserDto.language;
      delete updateUserDto.photo;
      delete updateUserDto.roleId;
      const errors = await validateUpdateUserDto(updateUserDto);
      expect(errors.length).toBe(0);
    });
  });

  describe('DELETE /:id', () => {
    it('no DTO to test here', () => {
      expect(true).toBe(true);
    });
  });

  describe('GET /profile', () => {
    it('no DTO to test here', () => {
      expect(true).toBe(true);
    });
  });

  describe('PATCH /profile', () => {
    const updateProfileDto: UpdateProfileDto = {
      username: 'string',
      password: 'string',
      photo: 'string',
      language: 'string',
    };
    it('should accept updateProfileDto', async () => {
      const errors = await validateUpdateProfileDto(updateProfileDto);
      expect(errors.length).toBe(0);
    });
    it('should accept updateProfileDto without any field', async () => {
      delete updateProfileDto.username;
      delete updateProfileDto.password;
      delete updateProfileDto.language;
      delete updateProfileDto.photo;
      const errors = await validateUpdateProfileDto(updateProfileDto);
      expect(errors.length).toBe(0);
    });
    it("shouldn't accept updateProfileDto with password length less than 5", async () => {
      updateProfileDto.password = '1234';
      const errors = await validateUpdateProfileDto(updateProfileDto);
      expect(errors.length).not.toBe(0);
    });
  });
});
