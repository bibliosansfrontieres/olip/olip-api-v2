import {
  Body,
  Controller,
  Post,
  Get,
  Query,
  Patch,
  Param,
  Delete,
  NotFoundException,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { CreateUserReponse } from './responses/create-user-response';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { ApiPaginatedResponse } from 'src/utils/classes/api-paginated-response';
import { UserOrderDto } from './dto/user-order.dto';
import { UserId } from 'src/auth/decorators/user-id.decorator';
import { UpdateUserDto } from './dto/update-user.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { UserNotFound } from './responses/errors/user-not-found';
import { DeleteUserBadRequest } from './responses/errors/delete-user-bad-request';
import { CreateUserBadRequest } from './responses/errors/create-user-bad-request';
import { UsersListBadRequest } from './responses/errors/users-list-bad-request';
import { UpdateUserBadRequest } from './responses/errors/update-user-bad-request';
import { UpdateProfileBadRequest } from './responses/errors/update-profile-bad-request';
import { UserDto } from './dto/user.dto';
import { cachedMethod } from 'src/utils/caching.util';
import { User } from './entities/user.entity';

@ApiTags('users')
@Controller('api/users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Auth(PermissionEnum.CREATE_USER)
  @Post()
  @ApiOperation({
    summary: 'Create user',
    description: 'Needs authentification and CREATE_USER permission',
  })
  @ApiBody({ type: CreateUserDto })
  @ApiCreatedResponse({ type: CreateUserReponse })
  @ApiBadRequestResponse({ type: CreateUserBadRequest })
  async createUser(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Auth(PermissionEnum.READ_USERS)
  @Get()
  @ApiOperation({
    summary: 'Get list of users',
    description: 'Needs authentification and READ_USERS permission',
  })
  @ApiPaginatedResponse(UserDto)
  @ApiBadRequestResponse({ type: UsersListBadRequest })
  async getUsers(
    @Query() pageOptionsDto: PageOptionsDto,
    @Query() userOrderDto: UserOrderDto,
  ) {
    return cachedMethod(
      'usersService',
      'getUsersPagination',
      [pageOptionsDto],
      async () => {
        return this.usersService.getUsersPagination(
          pageOptionsDto,
          userOrderDto,
        );
      },
      [User],
    );
  }

  @Auth(PermissionEnum.READ_USERS)
  @Get(':id')
  @ApiOperation({
    summary: 'Get user by id',
    description: 'Needs authentification and READ_USERS permission',
  })
  @ApiOkResponse({ type: UserDto })
  @ApiNotFoundResponse({ type: UserNotFound })
  async getUser(@Param('id') id: number) {
    return cachedMethod(
      'usersService',
      'getUser',
      [{ id }],
      async () => {
        const user = await this.usersService.findOne({
          where: { id },
          relations: { role: { permissions: true } },
        });
        if (user === null) {
          throw new NotFoundException('user not found');
        }
        return new UserDto(user);
      },
      [User],
    );
  }

  @Auth(PermissionEnum.UPDATE_USER)
  @Patch(':id')
  @ApiOperation({
    summary: 'Update a user',
    description: 'Needs authentification and UPDATE_USER permission',
  })
  @ApiOkResponse({ type: UserDto })
  @ApiNotFoundResponse({ type: UserNotFound })
  @ApiBadRequestResponse({ type: UpdateUserBadRequest })
  async updateUser(
    @Param('id') id: number,
    @Body() updateUserDto: UpdateUserDto,
    @UserId() userId: number | undefined,
  ) {
    return this.usersService.update(id, updateUserDto, userId);
  }

  @Auth(PermissionEnum.DELETE_USER)
  @Delete(':id')
  @ApiOperation({
    summary: 'Delete a user',
    description: 'Needs authentification and DELETE_USER permission',
  })
  @ApiOkResponse({ type: UserDto })
  @ApiNotFoundResponse({ type: UserNotFound })
  @ApiBadRequestResponse({ type: DeleteUserBadRequest })
  async deleteUser(@Param('id') id: number, @UserId() userId: number) {
    const user = await this.usersService.delete(id, userId);
    return new UserDto(user);
  }

  @Auth()
  @Get('profile')
  @ApiOperation({
    summary: 'Get self user profile',
    description: 'Needs authentification',
  })
  @ApiOkResponse({ type: UserDto })
  @ApiNotFoundResponse({ type: UserNotFound })
  async getProfile(@UserId() userId: number) {
    return cachedMethod(
      'usersService',
      'getProfile',
      [{ userId }],
      async () => {
        const user = await this.usersService.findOne({
          where: { id: userId },
          relations: { role: { permissions: true } },
        });
        return new UserDto(user);
      },
      [User],
    );
  }

  @Auth()
  @Patch('profile')
  @ApiOperation({
    summary: 'Update self user profile',
    description: 'Needs authentification',
  })
  @ApiOkResponse({ type: UserDto })
  @ApiBadRequestResponse({ type: UpdateProfileBadRequest })
  @ApiNotFoundResponse({ type: UserNotFound })
  async updateProfile(
    @UserId() userId: number,
    @Body() updateProfileDto: UpdateProfileDto,
  ) {
    return this.usersService.updateProfile(userId, updateProfileDto);
  }
}
