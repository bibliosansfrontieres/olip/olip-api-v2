import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator';

export class CreateUserDto {
  @ApiProperty({ example: 'username' })
  @IsString()
  @MinLength(6)
  @IsNotEmpty()
  username: string;

  @ApiPropertyOptional({ example: 'mypassword' })
  @IsString()
  @MinLength(5)
  @IsOptional()
  password?: string;

  @ApiPropertyOptional({
    example:
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==',
  })
  @IsString()
  @IsOptional()
  photo?: string;

  @ApiProperty({ example: 'fra' })
  @IsString()
  @IsNotEmpty()
  language: string;

  @ApiProperty({ example: 1 })
  @IsNumber()
  roleId: number;
}
