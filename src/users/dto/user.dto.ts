import { ApiProperty } from '@nestjs/swagger';
import { Role } from 'src/roles/entities/role.entity';
import { User } from '../entities/user.entity';

export class UserDto {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: 'username' })
  username: string;

  @ApiProperty({
    example:
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==',
  })
  photo: string;

  @ApiProperty({ example: 'fra' })
  language: string;

  @ApiProperty({
    example: {
      id: 1,
      name: 'admin',
      permissions: [{ id: 1, name: 'update_user' }],
    },
  })
  role: Role;

  constructor(user: User) {
    this.id = user.id;
    this.username = user.username;
    this.photo = user.photo;
    this.language = user.language;
    this.role = user.role;
  }
}
