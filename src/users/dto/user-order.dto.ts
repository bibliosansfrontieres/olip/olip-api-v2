import { ApiPropertyOptional } from '@nestjs/swagger';
import { UserOrderEnum } from '../enums/user-order.enum';
import { IsEnum, IsOptional, IsString } from 'class-validator';

export class UserOrderDto {
  @ApiPropertyOptional({
    example: 'username',
    enumName: 'UserOrderEnum',
    enum: UserOrderEnum,
  })
  @IsEnum(UserOrderEnum)
  @IsOptional()
  orderByField?: UserOrderEnum;

  @ApiPropertyOptional({ example: 'username' })
  @IsString()
  @IsOptional()
  usernameLike?: string;
}
