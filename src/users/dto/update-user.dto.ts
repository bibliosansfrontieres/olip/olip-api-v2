import { ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator';

export class UpdateUserDto {
  @ApiPropertyOptional({ example: 'username' })
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  username?: string;

  @ApiPropertyOptional({ example: 'newpassword' })
  @IsString()
  @MinLength(5)
  @IsNotEmpty()
  @IsOptional()
  password?: string;

  @ApiPropertyOptional({
    example:
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==',
  })
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  photo?: string;

  @ApiPropertyOptional({ example: 'fra' })
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  language?: string;

  @ApiPropertyOptional({ example: 1 })
  @IsNumber()
  @IsOptional()
  roleId?: number;
}
