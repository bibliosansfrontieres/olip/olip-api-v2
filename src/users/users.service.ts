import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import {
  FindManyOptions,
  FindOneOptions,
  FindOptionsOrder,
  Like,
  Repository,
} from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { CreateUserReponse } from './responses/create-user-response';
import { RolesService } from 'src/roles/roles.service';
import { RoleEnum } from 'src/roles/enums/role.enum';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { PageOptions } from 'src/utils/classes/page-options';
import { PageDto } from 'src/utils/classes/page.dto';
import { UserOrderDto } from './dto/user-order.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { UserDto } from './dto/user.dto';
import { invalidateCache } from 'src/utils/caching.util';
import * as bcrypt from 'bcrypt';
import { UploadService } from 'src/upload/upload.service';
import { UserOrderEnum } from './enums/user-order.enum';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    private rolesService: RolesService,
    @Inject(forwardRef(() => UploadService))
    private uploadService: UploadService,
  ) {}

  queryBuilder(alias?: string) {
    return this.usersRepository.createQueryBuilder(alias);
  }

  async create(createUserDto: CreateUserDto) {
    if (createUserDto === undefined) {
      throw new BadRequestException('no data provided');
    }

    const alreadyExists = await this.findOne({
      where: { username: createUserDto.username },
    });
    if (alreadyExists !== null) {
      throw new HttpException(
        'username already exists',
        HttpStatus.BAD_REQUEST,
      );
    }

    const role = await this.rolesService.findRoleById(createUserDto.roleId);
    if (role === null) {
      throw new HttpException('role not found', HttpStatus.BAD_REQUEST);
    }

    if (
      role.name !== RoleEnum.USER_NO_PASSWORD &&
      createUserDto.password === undefined
    ) {
      throw new HttpException(
        'password is needed for this role',
        HttpStatus.BAD_REQUEST,
      );
    }

    if (role.name === RoleEnum.USER_NO_PASSWORD) {
      delete createUserDto.password;
    }

    if (createUserDto.photo === undefined || createUserDto.photo === null) {
      createUserDto.photo =
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAMAAABOo35HAAAAS1BMVEXFxcXT09PMzMz29vb9/f3Pz8/Hx8f////ExMTCwsLc3Nzo6Ojf39/6+vrt7e3V1dXJycn4+Pjx8fHk5OTh4eHZ2dnv7+/z8/Pq6uo9UIqsAAAGZElEQVR42u2dWc/0JgxGs5AYsu8z//+XdtRF6tdWr2Y6gJc85zKXR2CMAacoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAoffsF7KPlPTb5dqmbYr2ukP5iuqxuaqvVQ9g9TTe9e0L95fb126PqT4Ku9Xuln1utswt2N+aWZanqPbWiKO/sqO/qIx3zbha+s6WPGfgn3i1RV/z9U/R7xX8PrXrOx3ekL1jvNxlCu9B17e5dgVT7oa8a+ukWyMFAcGvuyZopGbXxdDJeLJ4vGxvCy6Kua4jLYDfQlRac2aqs9KAFuthi5vstDfwhcs8HA9aBUnOZcrZSO09bYqhyl5GkqbqV1RdQbGls1peZEvPqAzsZMTJUzmKxxDZSF0UIu31AmRgOyKBu99rDl63yyqNOdQPgn5UR38fR0WWXVqmWNlJdB8UTsKDd6z3ya7K5o07oihim/LJoxsD6I8Tqjll85ZNGu0ZY/iIdFY7GByRXtAQPLcvoQLjZZ+hbEhs0VPbTNQ883sPQNLcaBRbRiV/jB/QdleYPjlEWHh6y3mTSF+NATL6pKpjWzLE3V+IXZlaq7Dx23LNITtMLFLkvPRZGwsss6kL5bPBQb+GXpufiw8csiNcthLUCWmgvfowBZas4tnABZDyWyWiIErXeLpLMIWTpuTfpdhCwdZzy+FyGrgSxrssIoQlapQxZBFmRBFmRBFmRBFmRpS0qx3bG33XlC1vugRPMBJWR9gAhZWg7wJZQdnloOLHAUpuyQVcsZa3Hwu9JzB/fkl6XndreA3eGkRxZ/hB8KBC178V3AbbZL0UV43Fb+JGhNzLJKRdOwKHlvHa2qemkxv91R1l2StVrqFlWueNdDZe8NefPSQZkszv3hqq+xQ42B9f48fCLJep+KK3u4NLbQ4sriVXZJrHhcKW2SeGIpFJ5rDUWBofUmehsst/mHluaOwbnTB9U/s8hdfFgUuypC3sNp5X+yyPr2UH1D+CZf2NoK9eQLWwZ+pZktbHUWflTU5LF1mPipk89Sj+8LG/gMQX4rzJC8WlPbcVX4xFvq1dSfWUNSW9dS2KLCHJSQQez2XL3WxAdcfRC4Epz3uC7YlFX46C05t6YwS+z0dC9Ms0QsQjiL/yb/NXDNsd6M9UtxA6IMrvEsbkGo+m9DV322vrgLXzZvG28jyjfX12uiu5objKzQHmOU9MGNRxtsuyq3iO+nx620rCr67vBhVFeYk3SJX2d7kzGUyc54tjJYi1VJTysMTUZfJv9Nw1rayCR8m+UR8GEhpfdHppshTv+RdJXxNvxUqVYVMv+tSHGN2VfZL+DWldK52LI0HSt1Hk4z9dBXWJcPfO/vnbaM3neM3dlGXXcAuZtgaPrN6MLdMISmRcsUXAT8pMgtOqaigB/2abnoHToSgoJ0vnNSZLlOuquBBCH7VavvnSRZTnLPMSHNzf+G4KbBO4lD6k7RC3Ql9Q+aoXMSZcm8dCrTlcgMwjckFnEXbkoSjLAz2IVEI6oGsTjZsiS1l2RveKuoGhieJB4xfVcGUoCQTXU7apA1yjhPdKQCh4ClK2wNpAb2sFWSIrgz+VqTLN6X5+EgVRycYWsmZcyMA+uhTdaDb2hNpA6232JVTp8sx3RJt1Xo6mWLZdvjO1IJy023ttYpq2YYWn4jpWz5h1ZDasl/c2vTKyt7f8DG6ZXlMg+thVST9bDHH7pl5X1tt+qWlfXHycoH1mto4YxC4umFH0g9g8dSKG5B9IcFWZkWxNZZkJWpVDOQCfKcIj5syHpgVyhsh3iREa4MpxRkhuRnF1or7zzV+NWOrOTb6ZMMkbiJrp8syZrSzsPKWZKV+Hh6JlOkvVQz2pKVtEdz62zJSrmblvlW9RsSvnMNkzVZCd/0lGSOElsdAVueUNuTVaeahw0ZJFVV67QoK9X+sLYoq0b6zp7El2SSJMmDH2zKSnKQ7yebspIUtVoySorNdGVVVoIKoL2KQ8LKQ9isytri73gCmSUgZHEGrc6urPit23q7svroi+FmV1b0Z2J+tStr9VgM+ZbD0rKsyIUH31uWFblfNWTd6rXOT0R+ydPalhW3SlPZlhV3w7PYlrVgG820lbZ6WPEXUQ8tIOuTzc5lW9YVc8MTJtuyot5pC2QcyGKSVVmXVUEWj6zZuqwZsmLL+g1jStd0mHlJUAAAAABJRU5ErkJggg==';
    }
    const photoUrl = await this.uploadService.image(createUserDto.photo);

    let user = new User(createUserDto, photoUrl, role);
    user = await this.usersRepository.save(user);
    await invalidateCache(User);
    return new CreateUserReponse(user);
  }

  private count() {
    return this.usersRepository.count();
  }

  findOne(options?: FindOneOptions<User>) {
    return this.usersRepository.findOne(options);
  }

  find(options?: FindManyOptions<User>) {
    return this.usersRepository.find(options);
  }

  async updateProfile(userId: number, updateProfileDto: UpdateProfileDto) {
    if (updateProfileDto === undefined) {
      throw new BadRequestException('no data provided');
    }

    let user = await this.findOne({
      where: { id: userId },
      relations: { role: { permissions: true } },
    });

    if (user === null) {
      throw new NotFoundException('user not found');
    }

    user.username = await this.updateUserUsername(user, updateProfileDto);
    user.password = await this.updateUserPassword(user, updateProfileDto);
    user.photo = await this.updateUserPhoto(user, updateProfileDto);
    user.language = updateProfileDto.language ?? user.language;

    user = await this.usersRepository.save(user);
    await invalidateCache(User);
    return new UserDto(user);
  }

  async update(id: number, updateUserDto: UpdateUserDto, userId: number) {
    if (updateUserDto === undefined) {
      throw new BadRequestException('no data provided');
    }

    let user = await this.findOne({ where: { id }, relations: { role: true } });

    if (user === null) {
      throw new NotFoundException('user not found');
    } else if (user.username === 'admin') {
      throw new BadRequestException('admin cannot be updated');
    } else if (user.id === userId) {
      throw new BadRequestException('self account cannot be updated');
    }

    // Role
    if (updateUserDto.roleId !== undefined) {
      const role = await this.rolesService.findRoleById(updateUserDto.roleId);
      if (role === null) {
        throw new NotFoundException('role not found');
      }
      user.role = role;
      if (role.name === RoleEnum.USER_NO_PASSWORD) {
        user.password = undefined;
      } else if (
        user.password === null &&
        updateUserDto.password === undefined
      ) {
        throw new BadRequestException('password is needed for this role');
      }
    }

    user.username = await this.updateUserUsername(user, updateUserDto);
    user.password = await this.updateUserPassword(user, updateUserDto);
    user.photo = await this.updateUserPhoto(user, updateUserDto);
    user.language = updateUserDto.language ?? user.language;

    user = await this.usersRepository.save(user);
    await invalidateCache(User);
    return new UserDto(user);
  }

  async delete(id: number, userId: number) {
    const user = await this.findOne({ where: { id } });

    if (user === null) {
      throw new NotFoundException('user not found');
    } else if (user.username === 'admin') {
      throw new BadRequestException('admin cannot be deleted');
    } else if (user.id === userId) {
      throw new BadRequestException('self account cannot be deleted');
    }

    await this.usersRepository.remove(user);

    await invalidateCache(User);
    return user;
  }

  private getUsersOrder(order: 'ASC' | 'DESC', userOrderDto: UserOrderDto) {
    let findOrder: FindOptionsOrder<User> = {};
    switch (userOrderDto.orderByField) {
      case 'username':
        findOrder = { username: order };
        break;
      case 'role':
        findOrder = { role: { id: order } };
        break;
      default:
        findOrder = { id: order };
        break;
    }
    return findOrder;
  }

  async getUsersPagination(
    pageOptionsDto: PageOptionsDto,
    userOrderDto: UserOrderDto,
  ) {
    const pageOptions = new PageOptions(pageOptionsDto);
    const usernameLike = userOrderDto.usernameLike
      ? userOrderDto.usernameLike
      : '';
    let users = [];
    if (userOrderDto.orderByField === UserOrderEnum.USERNAME) {
      users = await this.usersRepository
        .createQueryBuilder('user')
        .select('user')
        .innerJoinAndSelect('user.role', 'role')
        .take(pageOptions.take)
        .skip(pageOptions.skip)
        .addSelect('"user"."username"', 'user_username')
        .orderBy('LOWER(user_username)', pageOptions.order)
        .where('LOWER(user_username) LIKE :usernameLike', {
          usernameLike: `%${usernameLike}%`,
        })
        .getMany();
    } else {
      const order = this.getUsersOrder(pageOptionsDto.order, userOrderDto);
      users = await this.usersRepository.find({
        order,
        skip: pageOptions.skip,
        take: pageOptions.take,
        relations: { role: true },
        where: { username: Like(`%${usernameLike}%`) },
      });
    }
    const data = users.map((user) => new UserDto(user));
    const totalItemsCount = await this.count();
    return new PageDto(data, totalItemsCount, pageOptions);
  }

  private async updateUserPhoto(
    user: User,
    updateUserDto: UpdateUserDto | UpdateProfileDto,
  ): Promise<string> {
    if (updateUserDto.photo !== undefined) {
      if (user.photo !== null && user.photo !== undefined) {
        await this.uploadService.delete(user.photo);
      }
      return await this.uploadService.image(updateUserDto.photo);
    }
    return user.photo;
  }

  private async updateUserPassword(
    user: User,
    updateUserDto: UpdateUserDto | UpdateProfileDto,
  ): Promise<string> {
    if (updateUserDto.password !== undefined) {
      if (user.role.name === RoleEnum.USER_NO_PASSWORD) {
        throw new BadRequestException('password is not needed for this role');
      }
      const salt = bcrypt.genSaltSync();
      return bcrypt.hashSync(updateUserDto.password, salt);
    }
    return user.password;
  }

  private async updateUserUsername(
    user: User,
    updateUserDto: UpdateUserDto | UpdateProfileDto,
  ) {
    if (
      updateUserDto.username !== undefined &&
      user.username !== updateUserDto.username
    ) {
      if (user.username === 'admin') {
        throw new HttpException(
          'admin username cannot be updated',
          HttpStatus.BAD_REQUEST,
        );
      }

      const alreadyExists = await this.findOne({
        where: { username: updateUserDto.username },
      });
      if (alreadyExists !== null) {
        throw new HttpException(
          'username already exists',
          HttpStatus.BAD_REQUEST,
        );
      }

      return updateUserDto.username;
    }
    return user.username;
  }
}
