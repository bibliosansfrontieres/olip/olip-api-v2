import { Role } from 'src/roles/entities/role.entity';
import {
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateUserDto } from '../dto/create-user.dto';
import * as bcrypt from 'bcrypt';
import { Visibility } from 'src/visibilities/entities/visibility.entity';
import { ConnectedUser } from 'src/stats/entities/connected-user.entity';
import { Category } from 'src/categories/entities/category.entity';
import { Exclude } from 'class-transformer';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Exclude()
  @Column({ nullable: true })
  password: string | null;

  @Column({ nullable: true })
  photo: string | null;

  @Column()
  language: string;

  @ManyToOne(() => Role, (role) => role.users)
  role: Role;

  @ManyToMany(() => Visibility, (visibility) => visibility.users)
  visibilities: Visibility[];

  @ManyToMany(() => ConnectedUser, (connectedUser) => connectedUser.users)
  connectedUsers: ConnectedUser[];

  @ManyToMany(() => Category, (category) => category.users)
  categories: Category[];

  constructor(createUserDto: CreateUserDto, photoUrl: string, role: Role) {
    if (!createUserDto) return;
    this.username = createUserDto.username;
    this.password = undefined;
    if (createUserDto.password !== undefined) {
      const salt = bcrypt.genSaltSync();
      this.password = bcrypt.hashSync(createUserDto.password, salt);
    }
    this.photo = photoUrl;
    this.language = createUserDto.language;
    this.role = role;
  }
}
