import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class DeleteUserBadRequest {
  @ApiProperty({ example: 400 })
  statusCode: number;

  @ApiProperty({
    example: ['admin cannot be deleted', 'self account cannot be deleted'],
  })
  message: string | string[];

  @ApiPropertyOptional({ example: 'Bad Request' })
  error?: string;
}
