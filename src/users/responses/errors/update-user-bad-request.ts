import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateUserBadRequest {
  @ApiProperty({ example: 400 })
  statusCode: number;

  @ApiProperty({
    example: [
      'no data provided',
      'unexpected end of JSON input',
      'username already exists',
      'username should not be empty',
      'username must be a string',
      'password is needed for this role',
      'password is not needed for this role',
      'password must be longer than or equal to 5 characters',
      'password must be a string',
      'language should not be empty',
      'language must be a string',
      'roleId must be a number conforming to the specified constraints',
      'self account cannot be updated',
      'admin cannot be updated',
    ],
  })
  message: string | string[];

  @ApiPropertyOptional({ example: 'Bad Request' })
  error?: string;
}
