import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class UsersListBadRequest {
  @ApiProperty({ example: 400 })
  statusCode: number;

  @ApiProperty({
    example: [
      'take must not be less than 1',
      'page must not be less than 1',
      'orderByField must be one of the following values: username, role, id',
      'order must be one of the following values: ASC, DESC',
    ],
  })
  message: string | string[];

  @ApiPropertyOptional({ example: 'Bad Request' })
  error?: string;
}
