import { ApiProperty } from '@nestjs/swagger';

export class UserNotFound {
  @ApiProperty({ example: 404 })
  statusCode: number;

  @ApiProperty({
    example: 'user not found',
  })
  message: string | string[];
}
