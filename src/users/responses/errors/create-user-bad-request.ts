import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class CreateUserBadRequest {
  @ApiProperty({ example: 400 })
  statusCode: number;

  @ApiProperty({
    example: [
      'no data provided',
      'unexpected end of JSON input',
      'username already exists',
      'username should not be empty',
      'username must be a string',
      'username must be longer than or equal to 6 characters',
      'password is needed for this role',
      'password must be a string',
      'password must be longer than or equal to 5 characters',
      'language should not be empty',
      'language must be a string',
      'roleId must be a number conforming to the specified constraints',
    ],
  })
  message: string | string[];

  @ApiPropertyOptional({ example: 'Bad Request' })
  error?: string;
}
