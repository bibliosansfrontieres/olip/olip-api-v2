import { Role } from 'src/roles/entities/role.entity';
import { User } from '../entities/user.entity';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { RoleEnum } from 'src/roles/enums/role.enum';

export class CreateUserReponse {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: 'username' })
  username: string;

  @ApiPropertyOptional({
    example:
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==',
  })
  photo?: string | null;

  @ApiProperty({ example: 'fra' })
  language: string;

  @ApiProperty({ example: { name: RoleEnum.ADMIN, roleTranslations: [] } })
  role: Role;
  constructor(user: User) {
    this.id = user.id;
    this.username = user.username;
    this.photo = user.photo;
    this.language = user.language;
    this.role = user.role;
  }
}
