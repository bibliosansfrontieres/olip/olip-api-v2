import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class SigninBadRequest {
  @ApiProperty({ example: 400 })
  statusCode: number;

  @ApiProperty({
    example: [
      'no data provided',
      'unexpected end of JSON input',
      'username should not be empty',
      'username must be a string',
      'password must be a string',
      'password is needed for this user',
      'password is not needed for this user',
    ],
  })
  message: string | string[];

  @ApiPropertyOptional({ example: 'Bad Request' })
  error?: string;
}
