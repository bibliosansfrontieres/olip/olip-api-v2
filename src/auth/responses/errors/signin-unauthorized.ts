import { ApiProperty } from '@nestjs/swagger';

export class SigninUnauthorized {
  @ApiProperty({ example: 401 })
  statusCode: number;

  @ApiProperty({
    example: 'incorrect password',
  })
  message: string | string[];
}
