import { ApiProperty } from '@nestjs/swagger';

export class SigninResponse {
  @ApiProperty({
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpYXQiOjE2ODc5NDA0MTAsImV4cCI6MTY4ODAyNjgxMH0.flXYk0alx490y6uyuGaqmIb11FE0pj0IewfsdeRvOq',
  })
  accessToken: string;
  constructor(accessToken: string) {
    this.accessToken = accessToken;
  }
}
