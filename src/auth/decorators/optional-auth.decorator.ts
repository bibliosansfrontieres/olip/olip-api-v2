import { UseGuards, applyDecorators } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { OptionalAuthGuard } from '../guards/optional-auth.guard';

export function OptionalAuth(): MethodDecorator {
  return function (
    target: object,
    propertyKey: string | symbol,
    descriptor: TypedPropertyDescriptor<any>,
  ) {
    Reflect.defineMetadata('optionalAuth', true, target, propertyKey);
    applyDecorators(UseGuards(OptionalAuthGuard), ApiBearerAuth())(
      target,
      propertyKey,
      descriptor,
    );
  };
}
