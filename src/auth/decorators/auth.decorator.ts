import { SetMetadata, UseGuards, applyDecorators } from '@nestjs/common';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { AuthGuard } from '../guards/auth.guard';
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger';

/**
 * Creates an authentication decorator with optional permission name.
 * This decorator will check if user is connected by Bearer token.
 * If a permissionName is provided, it will check if user has that permission.
 *
 * @param {PermissionEnum} permissionName - Optional permission name.
 */
export function Auth(permissionName?: PermissionEnum): MethodDecorator {
  return function (
    target: object,
    propertyKey: string | symbol,
    descriptor: TypedPropertyDescriptor<any>,
  ) {
    Reflect.defineMetadata('auth', true, target, propertyKey);
    applyDecorators(
      SetMetadata('permissionName', permissionName),
      UseGuards(AuthGuard),
      ApiUnauthorizedResponse({ description: 'Unauthorized' }),
      ApiBearerAuth(),
    )(target, propertyKey, descriptor);
  };
}
