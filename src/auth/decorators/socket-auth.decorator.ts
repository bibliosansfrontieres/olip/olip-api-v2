import { SetMetadata, UseGuards, applyDecorators } from '@nestjs/common';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { SocketAuthGuard } from '../guards/socket-auth.guard';

/**
 * Creates an authentication decorator with optional permission name.
 * This decorator will check if user is connected by Bearer token.
 * If a permissionName is provided, it will check if user has that permission.
 *
 * @param {PermissionEnum} permissionName - Optional permission name.
 */
export function SocketAuth(permissionName?: PermissionEnum) {
  return applyDecorators(
    SetMetadata('permissionName', permissionName),
    UseGuards(SocketAuthGuard),
  );
}
