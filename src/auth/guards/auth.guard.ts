import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { UsersService } from 'src/users/users.service';
import { extractTokenFromHeaders } from 'src/utils/extract-token-from-headers.util';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private jwtService: JwtService,
    private reflector: Reflector,
    private usersService: UsersService,
    private configService: ConfigService,
  ) {}

  /**
   * Asynchronously checks if the user can activate the current route.
   *
   * @param {ExecutionContext} context - the execution context
   * @return {Promise<boolean>} - a promise that resolves to a boolean indicating if the user can activate the route
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = extractTokenFromHeaders(request);

    if (!token) {
      throw new UnauthorizedException();
    }

    try {
      const payload = await this.jwtService.verifyAsync(token, {
        secret: this.configService.get<string>('JWT_SECRET') || '',
      });
      const user = await this.usersService.findOne({
        where: { id: payload.id },
      });
      if (user === null) {
        throw new UnauthorizedException();
      }
      request['user'] = payload;

      const permissionName = this.reflector.get<PermissionEnum>(
        'permissionName',
        context.getHandler(),
      );

      const isPermissionGranted = await this.isPermissionGranted(
        permissionName,
        payload.id,
      );

      if (!isPermissionGranted) {
        throw new UnauthorizedException();
      }
    } catch {
      throw new UnauthorizedException();
    }

    return true;
  }

  /**
   * Checks if the given permission is granted for the specified user.
   * If no permission provided, user is granted by default.
   *
   * @param {PermissionEnum} permissionName - The name of the permission to check.
   * @param {number} id - The ID of the user to check the permission for.
   * @return {Promise<boolean>} A promise that resolves to a boolean indicating whether the permission is granted.
   */
  private async isPermissionGranted(
    permissionName: PermissionEnum,
    id: number,
  ): Promise<boolean> {
    if (permissionName !== undefined) {
      const user = await this.usersService.findOne({
        where: { id },
        relations: { role: { permissions: true } },
      });
      const permissionFound = user.role.permissions.find(
        (permission) => permission.name === permissionName,
      );
      const isPermissionGranted = permissionFound !== undefined;
      return isPermissionGranted;
    }

    return true;
  }
}
