import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { WsException } from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { UsersService } from 'src/users/users.service';
import { extractAuthFromClient } from 'src/utils/extract-auth-from-client.util';

@Injectable()
export class SocketAuthGuard implements CanActivate {
  constructor(
    private jwtService: JwtService,
    private reflector: Reflector,
    private usersService: UsersService,
    private configService: ConfigService,
  ) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const client: Socket = context.switchToWs().getClient();
    const { token } = extractAuthFromClient(client);

    if (!token) {
      throw new WsException('Unauthorized');
    }

    try {
      const payload = await this.jwtService.verifyAsync(token, {
        secret: this.configService.get<string>('JWT_SECRET') || '',
      });
      const user = await this.usersService.findOne({
        where: { id: payload.id },
      });
      if (user === null) {
        throw new UnauthorizedException();
      }

      const permissionName = this.reflector.get<PermissionEnum>(
        'permissionName',
        context.getHandler(),
      );

      const isPermissionGranted = await this.isPermissionGranted(
        permissionName,
        payload.id,
      );

      if (!isPermissionGranted) {
        throw new WsException('Unauthorized');
      }
    } catch {
      throw new WsException('Unauthorized');
    }

    return true;
  }

  /**
   * Checks if the given permission is granted for the specified user.
   * If no permission provided, user is granted by default.
   *
   * @param {PermissionEnum} permissionName - The name of the permission to check.
   * @param {number} id - The ID of the user to check the permission for.
   * @return {Promise<boolean>} A promise that resolves to a boolean indicating whether the permission is granted.
   */
  private async isPermissionGranted(
    permissionName: PermissionEnum,
    id: number,
  ): Promise<boolean> {
    if (permissionName !== undefined) {
      const user = await this.usersService.findOne({
        where: { id },
        relations: { role: { permissions: true } },
      });
      const permissionFound = user.role.permissions.find(
        (permission) => permission.name === permissionName,
      );
      const isPermissionGranted = permissionFound !== undefined;
      return isPermissionGranted;
    }

    return true;
  }

  static async extractUser(
    jwtService: JwtService,
    configService: ConfigService,
    usersService: UsersService,
    client: Socket,
  ) {
    const { token } = extractAuthFromClient(client);

    if (!token) return null;

    try {
      const payload = await jwtService.verifyAsync(token, {
        secret: configService.get<string>('JWT_SECRET') || '',
      });
      const user = await usersService.findOne({
        where: { id: payload.id },
      });
      return user;
    } catch {
      return null;
    }
  }
}
