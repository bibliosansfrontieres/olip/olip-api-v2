import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { extractTokenFromHeaders } from 'src/utils/extract-token-from-headers.util';

@Injectable()
export class OptionalAuthGuard implements CanActivate {
  constructor(
    private jwtService: JwtService,
    private usersService: UsersService,
    private configService: ConfigService,
  ) {}

  /**
   * Asynchronously checks if a user exists with a bearer token
   *
   * @param {ExecutionContext} context - the execution context
   * @return {Promise<boolean>} - a promise that resolves to a boolean indicating if the user can activate the route
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = extractTokenFromHeaders(request);

    if (token) {
      try {
        const payload = await this.jwtService.verifyAsync(token, {
          secret: this.configService.get<string>('JWT_SECRET') || '',
        });
        const user = await this.usersService.findOne({
          where: { id: payload.id },
        });
        if (user !== null) {
          request['user'] = payload;
        }
      } catch {}
    }

    return true;
  }
}
