import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcrypt';
import { SigninDto } from './dto/signin.dto';
import { JwtService } from '@nestjs/jwt';
import { SigninResponse } from './responses/signin-response';
import { RoleEnum } from 'src/roles/enums/role.enum';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  /**
   * Sign in with the provided SigninDto.
   *
   * @param {SigninDto} signinDto - The SigninDto containing the username and password.
   * @return {Promise<SigninResponse>} A Promise that resolves to a SigninResponse.
   */
  async signIn(signinDto: SigninDto): Promise<SigninResponse> {
    if (signinDto === undefined) {
      throw new BadRequestException('no data provided');
    }

    const users = await this.usersService.find({ relations: { role: true } });
    const user = users.find(
      (user) =>
        user.username.toLowerCase() === signinDto.username.toLowerCase(),
    );

    if (user === null || user === undefined) {
      throw new NotFoundException('user not found');
    }
    if (user.role.name === RoleEnum.USER_NO_PASSWORD) {
      if (signinDto.password !== undefined) {
        throw new UnauthorizedException('password is not needed for this user');
      }
      return this.getSigninResponse(user);
    } else if (signinDto.password === undefined) {
      throw new BadRequestException('password is needed for this user');
    }
    const isMatch = await bcrypt.compare(signinDto.password, user.password);
    if (!isMatch) {
      throw new UnauthorizedException('incorrect password');
    }
    return this.getSigninResponse(user);
  }

  /**
   * Get the signin response for the given user.
   *
   * @param {User} user - The user object.
   * @return {Promise<SigninResponse>} The signin response object.
   */
  private async getSigninResponse(user: User): Promise<SigninResponse> {
    const payload = { id: user.id };
    const accessToken = await this.jwtService.signAsync(payload);
    return new SigninResponse(accessToken);
  }
}
