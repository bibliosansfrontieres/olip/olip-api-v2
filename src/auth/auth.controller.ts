import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { SigninDto } from './dto/signin.dto';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { SigninResponse } from './responses/signin-response';
import { SigninUnauthorized } from './responses/errors/signin-unauthorized';
import { SigninNotFound } from './responses/errors/signin-not-found';
import { SigninBadRequest } from './responses/errors/signin-bad-request';

@ApiTags('auth')
@Controller('api/auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('signin')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Sign in' })
  @ApiBody({ type: SigninDto })
  @ApiOkResponse({ type: SigninResponse })
  @ApiUnauthorizedResponse({ type: SigninUnauthorized })
  @ApiNotFoundResponse({ type: SigninNotFound })
  @ApiBadRequestResponse({ type: SigninBadRequest })
  signIn(@Body() signInDto: SigninDto) {
    return this.authService.signIn(signInDto);
  }
}
