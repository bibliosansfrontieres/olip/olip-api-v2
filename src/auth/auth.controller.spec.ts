import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { UsersService } from 'src/users/users.service';
import { ConfigService } from '@nestjs/config';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { AuthService } from './auth.service';
import { SigninDto } from './dto/signin.dto';
import { userMock } from 'src/tests/mocks/objects/user.mock';
import * as bcrypt from 'bcrypt';
import { validateSigninDto } from 'src/tests/validate/validate-signin.dto';

describe('AuthController', () => {
  let controller: AuthController;
  let usersService: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [JwtService, UsersService, ConfigService, AuthService],
    })
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    controller = module.get<AuthController>(AuthController);
    usersService = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('POST /signin', () => {
    const signinDto: SigninDto = {
      username: 'admin',
      password: 'admin',
    };
    it('should accept signinDto', async () => {
      const errors = await validateSigninDto(signinDto);
      expect(errors.length).toBe(0);
    });
    it('should accept signinDto without password', async () => {
      delete signinDto.password;
      const errors = await validateSigninDto(signinDto);
      expect(errors.length).toBe(0);
    });
    it("shouldn't accept signinDto with empty username", async () => {
      signinDto.username = '';
      const errors = await validateSigninDto(signinDto);
      expect(errors.length).not.toBe(0);
    });
    it("shouldn't accept signinDto with no username", async () => {
      delete signinDto.username;
      const errors = await validateSigninDto(signinDto);
      expect(errors.length).not.toBe(0);
    });
    it("shouldn't accept signinDto with empty password", async () => {
      signinDto.password = '';
      const errors = await validateSigninDto(signinDto);
      expect(errors.length).not.toBe(0);
    });
  });
});
