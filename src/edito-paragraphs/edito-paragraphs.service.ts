import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EditoParagraph } from './entities/edito-paragraph.entity';
import { Repository } from 'typeorm';
import { EditoParagraphTranslation } from './entities/edito-paragraph-translation.entity';
import { CreateEditoParagraphTranslationDto } from './dto/create-edito-paragraph-translation.dto';
import { Edito } from 'src/editos/entities/edito.entity';
import { Language } from 'src/languages/entities/language.entity';
import { LanguagesService } from 'src/languages/languages.service';

@Injectable()
export class EditoParagraphsService {
  constructor(
    @InjectRepository(EditoParagraph)
    private editoParagraphsRepository: Repository<EditoParagraph>,
    @InjectRepository(EditoParagraphTranslation)
    private editoParagraphTranslationsRepository: Repository<EditoParagraphTranslation>,
    private languagesService: LanguagesService,
  ) {}

  async create(
    edito: Edito,
    createEditoParagraphTranslationsDto: CreateEditoParagraphTranslationDto[],
    languages: Language[],
  ) {
    let editoParagraph = new EditoParagraph(edito);
    editoParagraph = await this.editoParagraphsRepository.save(editoParagraph);
    for (let i = 0; i < createEditoParagraphTranslationsDto.length; i++) {
      const createEditoParagraphTranslation =
        createEditoParagraphTranslationsDto[i];
      const language = languages[i];
      const editoParagraphTranslation = new EditoParagraphTranslation(
        editoParagraph,
        createEditoParagraphTranslation,
        language,
      );
      await this.editoParagraphTranslationsRepository.save(
        editoParagraphTranslation,
      );
    }
  }

  async getTranslation(
    editoParagraph: EditoParagraph,
    languageIdentifier: string,
    withFallback = true,
  ): Promise<EditoParagraph> {
    const translation = await this.languagesService.getTranslation<
      EditoParagraph,
      EditoParagraphTranslation
    >(
      'editoParagraph',
      editoParagraph,
      this.editoParagraphTranslationsRepository,
      languageIdentifier,
      withFallback,
    );
    editoParagraph.editoParagraphTranslations = [translation];
    return editoParagraph;
  }
}
