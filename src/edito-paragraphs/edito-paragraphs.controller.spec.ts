import { Test, TestingModule } from '@nestjs/testing';
import { EditoParagraphsController } from './edito-paragraphs.controller';
import { EditoParagraphsService } from './edito-paragraphs.service';
import { Repository } from 'typeorm';
import { EditoParagraph } from './entities/edito-paragraph.entity';
import { EditoParagraphTranslation } from './entities/edito-paragraph-translation.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LanguagesService } from 'src/languages/languages.service';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';

describe('EditoParagraphsController', () => {
  let controller: EditoParagraphsController;
  let editoParagraphsRepository: Repository<EditoParagraph>;
  const EDITO_PARAGRAPHS_REPOSITORY_TOKEN = getRepositoryToken(EditoParagraph);
  let editoParagaphTranslationsRepository: Repository<EditoParagraphTranslation>;
  const EDITO_PARAGRAPH_TRANSLATIONS_REPOSITORY_TOKEN = getRepositoryToken(
    EditoParagraphTranslation,
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EditoParagraphsController],
      providers: [
        EditoParagraphsService,
        {
          provide: EDITO_PARAGRAPHS_REPOSITORY_TOKEN,
          useClass: Repository<EditoParagraph>,
        },
        {
          provide: EDITO_PARAGRAPH_TRANSLATIONS_REPOSITORY_TOKEN,
          useClass: Repository<EditoParagraphTranslation>,
        },
        LanguagesService,
      ],
    })
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .compile();

    controller = module.get<EditoParagraphsController>(
      EditoParagraphsController,
    );
    editoParagraphsRepository = module.get<Repository<EditoParagraph>>(
      EDITO_PARAGRAPHS_REPOSITORY_TOKEN,
    );
    editoParagaphTranslationsRepository = module.get<
      Repository<EditoParagraphTranslation>
    >(EDITO_PARAGRAPH_TRANSLATIONS_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
