import { Module } from '@nestjs/common';
import { EditoParagraphsService } from './edito-paragraphs.service';
import { EditoParagraphsController } from './edito-paragraphs.controller';
import { EditoParagraph } from './entities/edito-paragraph.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EditoParagraphTranslation } from './entities/edito-paragraph-translation.entity';
import { LanguagesModule } from 'src/languages/languages.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([EditoParagraph, EditoParagraphTranslation]),
    LanguagesModule,
  ],
  controllers: [EditoParagraphsController],
  providers: [EditoParagraphsService],
  exports: [EditoParagraphsService],
})
export class EditoParagraphsModule {}
