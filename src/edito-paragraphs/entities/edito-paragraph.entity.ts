import { Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { EditoParagraphTranslation } from './edito-paragraph-translation.entity';
import { Edito } from 'src/editos/entities/edito.entity';

@Entity()
export class EditoParagraph {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Edito, (edito) => edito.editoParagraphs, {
    onDelete: 'CASCADE',
  })
  edito: Edito;

  @OneToMany(
    () => EditoParagraphTranslation,
    (editoParagraphTranslation) => editoParagraphTranslation.editoParagraph,
  )
  editoParagraphTranslations: EditoParagraphTranslation[];

  constructor(edito: Edito) {
    if (!edito) return;
    this.edito = edito;
  }
}
