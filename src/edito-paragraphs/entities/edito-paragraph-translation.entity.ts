import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { EditoParagraph } from './edito-paragraph.entity';
import { Language } from 'src/languages/entities/language.entity';
import { CreateEditoParagraphTranslationDto } from '../dto/create-edito-paragraph-translation.dto';

@Entity()
export class EditoParagraphTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  text: string;

  @ManyToOne(
    () => EditoParagraph,
    (editoParagraph) => editoParagraph.editoParagraphTranslations,
    { onDelete: 'CASCADE' },
  )
  editoParagraph: EditoParagraph;

  @ManyToOne(() => Language, (language) => language.editoParagraphTranslations)
  language: Language;

  constructor(
    editoParagraph: EditoParagraph,
    createEditoParagraphTranslation: CreateEditoParagraphTranslationDto,
    language: Language,
  ) {
    if (!editoParagraph) return;
    this.text = createEditoParagraphTranslation.text;
    this.editoParagraph = editoParagraph;
    this.language = language;
  }
}
