import { IsString, MaxLength } from 'class-validator';

export class CreateEditoParagraphTranslationDto {
  @IsString()
  languageIdentifier: string;

  @IsString()
  @MaxLength(500)
  text: string;
}
