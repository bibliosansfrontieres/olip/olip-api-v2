import { ArrayMinSize, IsArray, ValidateNested } from 'class-validator';
import { CreateEditoParagraphTranslationDto } from './create-edito-paragraph-translation.dto';
import { Type } from 'class-transformer';

export class CreateEditoParagraphDto {
  @IsArray()
  @ValidateNested({ each: true })
  @ArrayMinSize(1)
  @Type(() => CreateEditoParagraphTranslationDto)
  createEditoParagraphTranslationsDto: CreateEditoParagraphTranslationDto[];
}
