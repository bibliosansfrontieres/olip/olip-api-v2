import { Module } from '@nestjs/common';
import { InitDatabaseService } from './init-database.service';
import { LanguagesModule } from 'src/languages/languages.module';
import { ConfigModule } from '@nestjs/config';
import { CategoriesModule } from 'src/categories/categories.module';
import { ApplicationsModule } from 'src/applications/applications.module';
import { PermissionsModule } from 'src/permissions/permissions.module';
import { RolesModule } from 'src/roles/roles.module';
import { UsersModule } from 'src/users/users.module';
import { DublinCoreAudiencesModule } from 'src/dublin-core-audiences/dublin-core-audiences.module';
import { DublinCoreLicensesModule } from 'src/dublin-core-licenses/dublin-core-licenses.module';
import { DublinCoreTypesModule } from 'src/dublin-core-types/dublin-core-types.module';
import { DublinCoreItemsModule } from 'src/dublin-core-items/dublin-core-items.module';
import { UploadModule } from 'src/upload/upload.module';
import { SettingsModule } from 'src/settings/settings.module';
import { HighlightsModule } from 'src/highlights/highlights.module';
import { StatsModule } from 'src/stats/stats.module';
import { EditosModule } from 'src/editos/editos.module';
import { ApplicationTypesModule } from 'src/application-types/application-types.module';
import { MaestroModule } from 'src/maestro/maestro.module';
import { HardwareModule } from 'src/hardware/hardware.module';
import { UpdateModule } from 'src/update/update.module';

@Module({
  imports: [
    ConfigModule,
    LanguagesModule,
    CategoriesModule,
    DublinCoreItemsModule,
    ApplicationsModule,
    PermissionsModule,
    RolesModule,
    UsersModule,
    DublinCoreAudiencesModule,
    DublinCoreLicensesModule,
    DublinCoreTypesModule,
    UploadModule,
    SettingsModule,
    HighlightsModule,
    StatsModule,
    EditosModule,
    ApplicationTypesModule,
    MaestroModule,
    HardwareModule,
    UpdateModule,
  ],
  providers: [InitDatabaseService],
})
export class InitDatabaseModule {}
