import { Injectable, Logger } from '@nestjs/common';
import { join } from 'path';
import { LanguagesService } from 'src/languages/languages.service';
import { readIsoTabFile } from 'src/utils/read-iso-tab-file.util';
import { existsSync, readFileSync } from 'fs';
import { ApplicationsService } from 'src/applications/applications.service';
import { PermissionsService } from 'src/permissions/permissions.service';
import {
  PermissionEnum,
  permissionsTrads,
} from 'src/permissions/enums/permission.enum';
import { CreatePermissionDto } from 'src/permissions/dto/create-permission.dto';
import { CreatePermissionTranslationDto } from 'src/permissions/dto/create-permission-translation.dto';
import { RolesService } from 'src/roles/roles.service';
import {
  RoleEnum,
  rolesPermissions,
  rolesTrads,
} from 'src/roles/enums/role.enum';
import { CreateRoleDto } from 'src/roles/dto/create-role.dto';
import { CreateRoleTranslationDto } from 'src/roles/dto/create-role-translation.dto';
import { UsersService } from 'src/users/users.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { DublinCoreAudiencesService } from 'src/dublin-core-audiences/dublin-core-audiences.service';
import { CreateDublinCoreAudienceTranslationDto } from 'src/dublin-core-audiences/dto/create-dublin-core-audience-translation.dto';
import { CreateDublinCoreLicenseTranslationDto } from 'src/dublin-core-licenses/dto/create-dublin-core-license-translation.dto';
import { DublinCoreLicensesService } from 'src/dublin-core-licenses/dublin-core-licenses.service';
import { CreateDublinCoreTypeTranslationDto } from 'src/dublin-core-types/dto/create-dublin-core-type-translation.dto';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { UploadService } from 'src/upload/upload.service';
import { SettingsService } from 'src/settings/settings.service';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import { HighlightsService } from 'src/highlights/highlights.service';
import { CreateHighlightDto } from 'src/highlights/dto/create-highlight.dto';
import { StatsService } from 'src/stats/stats.service';
import { ApplicationTypesService } from 'src/application-types/application-types.service';
import { MaestroService } from 'src/maestro/maestro.service';
import { ConfigService } from '@nestjs/config';
import { MaestroApiService } from 'src/maestro/maestro-api.service';
import { MaestroWebsocketApiService } from 'src/maestro/maestro-websocket-api.service';
import { DeployStatusEnum } from 'src/maestro/enums/deploy-status.enum';
import { IMaestroApplication } from 'src/maestro/interfaces/maestro-application.interface';
import { HardwareService } from 'src/hardware/hardware.service';
import { UpdateService } from 'src/update/update.service';

@Injectable()
export class InitDatabaseService {
  constructor(
    private languagesService: LanguagesService,
    private applicationTypesService: ApplicationTypesService,
    private applicationsService: ApplicationsService,
    private permissionService: PermissionsService,
    private rolesService: RolesService,
    private usersService: UsersService,
    private dublinCoreAudiencesService: DublinCoreAudiencesService,
    private dublinCoreLicensesService: DublinCoreLicensesService,
    private dublinCoreTypesService: DublinCoreTypesService,
    private uploadService: UploadService,
    private settingsService: SettingsService,
    private highlightsService: HighlightsService,
    private statsService: StatsService,
    private maestroService: MaestroService,
    private configService: ConfigService,
    private maestroApiService: MaestroApiService,
    private maestroWebsocketApiService: MaestroWebsocketApiService,
    private hardwareService: HardwareService,
    private updateService: UpdateService,
  ) {}

  async onModuleInit() {
    Logger.log('Initializing database...', 'InitDatabaseService');
    await this.removeConnectedUsers();
    await this.createSettings();
    await this.createLanguages();
    await this.createDublinCoreTypes();
    await this.createDublinCoreLicenses();
    await this.createDublinCoreAudiences();
    await this.createHighlight();
    //await this.createEdito();
    await this.createApplicationTypes();
    await this.createApplications();
    await this.createPermissions();
    await this.createRoles();
    await this.createAdminUser();
    await this.checkOlipDeployed();
    //await this.checkProjectId();
    await this.integration();
    await this.checkUpdates();
    Logger.log('Database initialized !', 'InitDatabaseService');
  }

  async integration() {
    const applicationIntegration = this.configService.get(
      'APPLICATION_INTEGRATION',
    );
    if (applicationIntegration === 'true') {
      const applicationPath = join(
        __dirname,
        '../../data/integration/application',
      );
      if (existsSync(applicationPath)) {
        // TODO : Manage application's catalog
        const composePath = join(applicationPath, 'compose.yml');
        const compose = readFileSync(composePath).toString();
        const descriptionPath = join(applicationPath, 'description.json');
        const description = JSON.parse(
          readFileSync(descriptionPath).toString(),
        );
        const hooksPath = join(applicationPath, 'hooks.json');
        const hooks = JSON.parse(readFileSync(hooksPath).toString());
        const foundApplication = await this.applicationsService.findOne({
          where: { id: 'integration' },
        });
        if (foundApplication) {
          await this.applicationsService.delete('integration');
        }

        const application: IMaestroApplication = {
          ...description,
          exposeServices: description.exposeServices
            ? JSON.stringify(description.exposeServices)
            : null,
          applicationType: { name: description.type },
          compose,
          hooks,
          id: 'integration',
        };
        application.logo = await this.uploadService.image(application.logo);
        application.image = await this.uploadService.image(application.image);

        await this.applicationsService.create(application);
      }
    }
  }

  private async removeConnectedUsers() {
    await this.statsService.removeAllConnectedUsers();
  }

  private async checkOlipDeployed() {
    const vmId = this.configService.get('VM_ID');
    const isVmInstalled = await this.settingsService.findOne(
      SettingKey.IS_VM_INSTALLED,
    );
    if (vmId !== 'olip') {
      if (!isVmInstalled || isVmInstalled.value === 'false') {
        this.maestroService.deploy(false);
      } else {
        Logger.log('VM already installed', 'InitDatabaseService');
      }
    } else if (vmId === 'olip') {
      if (!this.hardwareService.hasInternet()) {
        Logger.error(
          'No internet connection, skipping OLIP deploy',
          'InitDatabaseService',
        );
        return;
      }
      const isOlipInstalled = await this.settingsService.findOne(
        SettingKey.IS_OLIP_INSTALLED,
      );
      if (!isOlipInstalled || isOlipInstalled.value === 'false') {
        this.maestroService.deploy(true);
      } else {
        Logger.log('OLIP already installed', 'InitDatabaseService');
        this.maestroWebsocketApiService.postDeploy(DeployStatusEnum.DEPLOYED);
      }
    } else {
      Logger.error(
        'No VM installed and actual OLIP is not a VM',
        'InitDatabaseService',
      );
    }
  }

  private async createSettings() {
    if (!(await this.settingsService.findOne(SettingKey.LOGO))) {
      await this.createSetting(
        SettingKey.LOGO,
        await this.uploadService.copyImage(
          join(__dirname, '../../data/images/olip-logo.webp'),
          true,
        ),
      );
    }
    await this.createSetting(SettingKey.IS_EDITO_DISPLAYED, 'true');
    await this.createSetting(SettingKey.PRINCIPAL_COLOR, '#FDB851');
    await this.createSetting(SettingKey.IS_HIGHLIGHT_DISPLAYED, 'false');
    await this.createSetting(SettingKey.PROJECT_ID, 'null');
    await this.createSetting(SettingKey.PROJECT_VERSION, 'null');
    await this.createSetting(SettingKey.IS_PROJECT_UPDATE_NEEDED, 'false');
    await this.createSetting(SettingKey.IS_VM_INSTALLED, 'false');
    await this.createSetting(SettingKey.IS_OLIP_INSTALLED, 'false');
    await this.createSetting(SettingKey.WAS_INTERNET_CONNECTED, 'false');
    await this.createSetting(SettingKey.LAST_DOCKER_IMAGES_UPDATE, '0');
    await this.createSetting(SettingKey.LAST_APPLICATIONS_UPDATE, '0');
    await this.createSetting(SettingKey.LAST_PLAYLISTS_UPDATE, '0');
  }

  private async createSetting(key: SettingKey, value: string) {
    const isKeyExists = await this.settingsService.findOne(key);
    if (isKeyExists === null) {
      await this.settingsService.create({
        key,
        value,
      });
    }
  }

  private async createLanguages() {
    const languagesCount = await this.languagesService.count();
    if (languagesCount !== 0) return;
    Logger.log('Creating languages...', 'InitDatabaseService');

    const isoFilePath = join(
      __dirname,
      '../../data/initial-data/iso-639-3.tab',
    );
    const isoValues = readIsoTabFile(isoFilePath);

    const languages = [];
    for (let i = 0; i < isoValues.length; i++) {
      const isoValue = isoValues[i];
      const oldIdentifier = isoValue.Part1 !== '' ? isoValue.Part1 : undefined;
      languages.push(
        this.languagesService.create({
          identifier: isoValue.Id,
          label: isoValue.Ref_Name,
          oldIdentifier,
        }),
      );
    }
    await this.languagesService.insert(languages);

    Logger.log('All languages successfully created !', 'InitDatabaseService');
  }

  private async createDublinCoreTypes() {
    const dublinCoreTypesCount = await this.dublinCoreTypesService.count();
    if (dublinCoreTypesCount !== 0) return;

    const json = JSON.parse(
      readFileSync(
        join(__dirname, '../../data/initial-data/dublin-core-types.json'),
      ).toString(),
    ) as { dublinCoreTypes: { title: string; description: string }[] };

    // TODO : Use front icons for dublin core type images

    const language = await this.languagesService.findOneByIdentifier('eng');
    for (const itemType of json.dublinCoreTypes) {
      const dublinCoreType =
        await this.dublinCoreTypesService.create('images/');
      const createDublinCoreTypeTranslationDto: CreateDublinCoreTypeTranslationDto =
        {
          label: itemType.title,
          description: itemType.description,
          languageId: language.id,
          dublinCoreTypeId: dublinCoreType.id,
        };
      await this.dublinCoreTypesService.createTranslation(
        createDublinCoreTypeTranslationDto,
      );
    }
  }

  private async createDublinCoreLicenses() {
    const dublinCoreLicensesCount =
      await this.dublinCoreLicensesService.count();
    if (dublinCoreLicensesCount !== 0) return;

    const licenses = [
      'Undetermined',
      'Copyright',
      'Youtube Standard',
      'Creative Commons',
      'Open Source',
      'Free distribution/open license',
      'CC 0',
      'CC BY',
      'CC BY-SA',
      'CC BY-ND',
      'CC BY-NC-SA',
      'CC BY-NC-ND',
    ];

    const language = await this.languagesService.findOneByIdentifier('eng');

    for (const license of licenses) {
      const dublinCoreLicense = await this.dublinCoreLicensesService.create();
      const createDublinCoreLicenseTranslationDto: CreateDublinCoreLicenseTranslationDto =
        {
          label: license,
          description: '',
          languageId: language.id,
          dublinCoreLicenseId: dublinCoreLicense.id,
        };
      await this.dublinCoreLicensesService.createTranslation(
        createDublinCoreLicenseTranslationDto,
      );
    }
  }

  private async createDublinCoreAudiences() {
    const dublinCoreAudiencesCount =
      await this.dublinCoreAudiencesService.count();
    if (dublinCoreAudiencesCount !== 0) return;

    const audiences = ['Adults', 'Teenagers', 'Childrens', 'Infants'];

    const language = await this.languagesService.findOneByIdentifier('eng');

    for (const audience of audiences) {
      const dublinCoreAudience = await this.dublinCoreAudiencesService.create();

      const createDublinCoreAudienceTranslationDto: CreateDublinCoreAudienceTranslationDto =
        {
          label: audience,
          description: '',
          languageId: language.id,
          dublinCoreAudienceId: dublinCoreAudience.id,
        };

      await this.dublinCoreAudiencesService.createTranslation(
        createDublinCoreAudienceTranslationDto,
      );
    }
  }

  private async createHighlight() {
    const highlightCount = await this.highlightsService.count();
    if (highlightCount !== 0) return;
    const createHighlightDto: CreateHighlightDto = {
      createHighlightTranslations: [
        { languageIdentifier: 'eng', title: 'My first highlight !' },
      ],
    };
    await this.highlightsService.create(createHighlightDto);
  }

  private async createApplicationTypes() {
    if (!this.hardwareService.hasInternet()) {
      Logger.error(
        'No internet connection, skipping application types creation...',
        'InitDatabaseService',
      );
      return;
    }
    try {
      const applicationTypes =
        await this.maestroApiService.getApplicationTypes();

      if (!applicationTypes) {
        Logger.error(
          'Could not fetch application types from Maestro API',
          'InitDatabaseService',
        );
        return;
      }

      for (const applicationType of applicationTypes) {
        const foundApplicationType = await this.applicationTypesService.findOne(
          {
            where: { id: applicationType.id },
          },
        );
        if (!foundApplicationType) {
          await this.applicationTypesService.create(applicationType);
        } else {
          if (foundApplicationType.md5 !== applicationType.md5) {
            // TODO : update application type
          }
        }
      }
    } catch (e) {
      Logger.error(
        'Could not fetch application types from Maestro API',
        'InitDatabaseService',
      );
    }
  }

  private async createApplications() {
    if (!this.hardwareService.hasInternet()) {
      Logger.error(
        'No internet connection, skipping application types creation...',
        'InitDatabaseService',
      );
      return;
    }
    try {
      const applications = await this.maestroApiService.getApplications();

      if (!applications || !applications.data) {
        Logger.error(
          'Could not fetch applications from Maestro API',
          'InitDatabaseService',
        );
        return;
      }

      for (const application of applications.data) {
        const foundApplication = await this.applicationsService.findOne({
          where: { id: application.id },
        });

        if (!foundApplication) {
          application.logo = await this.uploadService.image(application.logo);
          application.image = await this.uploadService.image(application.image);
          await this.applicationsService.create(application);
        } else {
          // TODO : md5 and update
          // check md5
          // update app metadata
        }
      }
    } catch (e) {
      Logger.error(
        'Could not fetch applications from Maestro API',
        'InitDatabaseService',
      );
    }

    // We are setting olip application as installed by default
    const olipApplication = await this.applicationsService.findOne({
      where: { id: 'olip' },
    });
    if (!olipApplication) return;
    olipApplication.isInstalled = true;
    await this.applicationsService.save(olipApplication);
  }

  private async createPermissions() {
    const permissions = await this.permissionService.findAllPermissions();
    if (permissions.length !== 0) return;
    const language = await this.languagesService.findOneByIdentifier('eng');
    for (const value in PermissionEnum) {
      const createPermissionDto: CreatePermissionDto = {
        name: PermissionEnum[value],
      };
      const permissionTrad = permissionsTrads.find(
        (trad) => trad.name === PermissionEnum[value],
      );
      const createPermissionTranslationDto: CreatePermissionTranslationDto = {
        label: permissionTrad.label,
        description: permissionTrad.description,
        languageId: language.id,
      };
      await this.permissionService.create(
        createPermissionDto,
        createPermissionTranslationDto,
      );
    }
  }

  private async createRoles() {
    const roles = await this.rolesService.findAllRoles();
    if (roles.length !== 0) return;
    const language = await this.languagesService.findOneByIdentifier('eng');
    for (const value in RoleEnum) {
      const createRoleDto: CreateRoleDto = {
        name: RoleEnum[value],
      };
      const roleTrad = rolesTrads.find((trad) => trad.name === RoleEnum[value]);
      const createRoleTranslationDto: CreateRoleTranslationDto = {
        label: roleTrad.label,
        languageId: language.id,
      };
      const role = await this.rolesService.create(
        createRoleDto,
        createRoleTranslationDto,
      );
      if (role !== null) {
        const rolePerms = rolesPermissions.find(
          (perm) => perm.name === RoleEnum[value],
        );
        await this.rolesService.addPermissionToRole(
          role.id,
          rolePerms.permissions,
        );
      } else {
        throw Error('Role not created');
      }
    }
  }

  private async createAdminUser() {
    const alreadyExists = await this.usersService.findOne({
      where: { username: 'admin' },
    });
    if (alreadyExists !== null) return;
    const role = await this.rolesService.findRoleByName(RoleEnum.ADMIN);
    if (role === null) return;
    const createUserDto: CreateUserDto = {
      username: 'admin',
      password: 'admin',
      photo:
        'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSEBIVFhUXGBgVFRYTFRcVFRcVFRYaFxUVFRUYHSggGBolHRcYITEhJSkuLi4uGB8zODMsNygtLisBCgoKDg0OGxAQGy0mICUtLy8uLS0uLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABQYDBAcCAf/EAEMQAAECAwQGBgcFBwQDAAAAAAEAAgMEEQUGITESIkFRYYETMnGRobEHQlJicsHRIySCkrIUM0OiwvDxU2Nz4USD0v/EABsBAQADAQEBAQAAAAAAAAAAAAAEBQYDAQIH/8QANhEAAgIBAgQDBgMIAwEAAAAAAAECAwQFERIhMUETUZEiMmFxgbGh0fAUFSQzQlLB4SM0Q/H/2gAMAwEAAhEDEQA/AMqyp+ihAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEPAvD0L0BAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQE5Zt1piM1rxoNa7EFzsxvoAVLqwrLFxdirv1aimTi920RE1AMN7mOza4tPaDRRpxcJOLLCqxWQU13W5nsiVEWPDhuJAc6hpnyX3TWpzUWcsu100ynHqkWO8N1IUCA6Kx8QltMHFtMXAbuKnZOFCutyTZT4Wq233KuSWz+f5lQVYaE37PseNHa50JmkGmhxANTjhXNdq8edibiQ786miSjY9tzVmZd8N2jEY5p3OFFznCUHtIkV2wsW8GmvgYl8nQIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAJseb89gh6E23PDo9w5nTldHaxxbyOsP1U5K8wJ8VW3kZDWKnDJb8+ZVL6S2hNv3PDXjmKHxaVXZ0OG17dy70i3jxkvJ7GC6o+9wfi/pK+cT+cjrqf/AFZ/L/JeL7n7nE7WfrCtc5/8LM5pC/io/X7M5kqE2Z025kn0cqwkYvq88+r4AK/wocFKMXqt3iZMvJcik3rm+lmohrg06A7G4HxqqnMnxXM0el0+HjR+PMjIEFz3NYwVc4gAbyVHjFykku5OssjXFyl0Rs2nZUaXdoxWUrk4YtPYV1tonW9pHDGzKshbwf07mkuBKC9AQBAEAQBAEAQBAEAQBAEAQBDwtF3LqOi0iR6th5huTnD+keKscbBc/an08ikz9XVe8Keb8+yJ69disMrSEwN6LWaANnrDux5KXl46dXsroVem5ko5O8373JnOVRmvC8PS3+juZpEiQ69ZocO1pofMK002ftOP1M/rtW8Iz8uRm9I0t+6iDiw+Y+a+tSh7szloVntTr+pBXQH3uF2u/Q5RMLncvqWmqv8AhZ/T7lyv2fujuLmfqVnn/wAlmf0f/tL5P7HOpaAYj2sGbnBo/EaKkhHiko+ZrLpquuU32R1qbitgQHOGUNmA4NGAWjk/Dr38kYWuLutUe7f3OQvcSSTmTU9pzWblze5vYxSWy7FzuFZGcy8b2w/6nfLvVpp9H/o/oZ3WszfamPzf5Gxf21Q1gl29Z+L+DRkO0/JfeoXJJQXU5aNiuU/GfRdPi/8ARQ1TmpCAIAgCAIAgCAIAgCAIAgCAIeF0ulditI8w3ixh8HOHkFa4eH/XMzep6nvvVU/my7gK1M+fHtBFCvGt+R6nsckt2z+gjvh7K1bxa7EfTks5kV+HY4m4wcjx6Yz79H80aC4kwlrqzPRzcI7CdA/jGiPGik4k+G1FfqdfiY0l5c/Qu985bTlHnayjx+E4+BKts6HFSzN6XZwZK+PL1KdcsffIfY/9BVXgfzl9TQ6u/wCFl9PuWq/5+6/jb81Y6h/JKPRV/E/RlZuPKac0HHKGC/mdVvme5QMCvit38i41m7gx+Ff1cvQst/ZrQl9AZxHAcm6x8h3qfqE9q9vMp9Gp48ji/tRRrHs90xFbCbt6x3NGZ/vgqiip2zUUaXLyY49Tsf0+Z1GYjQ5WBWlGQ2gADgKADiVoJONUPgjFwjPIt27tnKZ2bdFiOiP6zjU8NwHALO2WOyTkzcUUxprUF2NuwrGfMv0W4NHXdsA3DeV1x8eV0uXT9fiR83NhjQ3fXsv12J+9F1mQ4XSwDQMA0w49YZaQJ9bhtUzLw4xhxQ7FXp2qTss8Oznv0+H+inKrNEEAQBAEAQBAEAQBAEAQFpuZYPSu6aKPs2nVByc4beweascHG4nxy6dii1bP8NeDB831+B0IBXJlz6gPhQFQ9IFnaUNsdoxZqu+F2XcfMqu1CrijxrsXmiZHDY6n3+5Q1TGoPUN5aQ4ZggjtBqF7F7ST8j5nHii4+Z18gRoPCIzwe3/taT34fNGC51W/FP7HPblwyJxoOYDwe0NIKp8FbX7fM1OqyUsPfz2LL6QT92b/AMjf0uU7UP5f1KnRP+w/kzD6PZTRgvikYvdQfCz/ALJ7l86bXtW5ebPvXLd7lWuy+5D3/m9KYDBlDaPzOxPhoqNqE+KxR8ifotXDS5+b+xYblWR0MLpHjXiCvEM9UfNTcGjw4bvqyq1bL8a3gj7q+5BX6tfpH9Aw6rDV3F+7l5qHqF/E+Bdiy0bE4I+NLq+nyIiwrGfMv0W4NHXfsaOG88FGox5XS2XTuWGbmwxoc+b7I6VLwYMrBoKNYwVJPiSdpV7GMKYeSMfOduTbu+bZzy8tvumX0FRCadVu8+07j5Klysp2vZdDV6dp8ceO75yfV+XwIVRCzCAIAgCAIAgCAIAgCA3bHs90xFbCbtxcdzRmf74LrTS7ZqJFzMlY9Tm/odalYDYbGsYKNaKAcAtHGKitkYac3OTlLqzKvo+QgCAwzku2Ix0N3VcC09hC+ZxUouLPuux1yUo9UcfnJZ0J7obs2ktPLbzzWasg4ScWb2m2NsFOPRmFfB0OnXMmdOUh1zbVh/CcPCiv8OfFSmzF6rVwZUl58yGs6V6O1XgZEPePxgHzJUaqvgy2WF9vHpsW/gvQ3PSIfu7P+Qfpcumo/wAtfM4aJ/Pfy/yibsaVEGXhsPqtGl20q7xJUqmKrrSK7Jsd18pebKPYskZ2cfFcKww7TdxFdRnh3Aqqprd97m+iNFlXLDxI1x95rb82XG8dqiWgFw651WD3jt7BmrLJuVVe669iiwMV5Nyi+nVnPbEsiJNRCBWlavedlcT2uKpqaJXT29TU5eZDEr+PZfrsdKl4MGVg0FGMYKknxJO0lXsYwpht2MhOVuTbu+cmc9vJeB0y6jathA6rd59p3HgqXJync9l0NVp+nxx47vnJ9/L4Ih4EFz3BrGlzjkGipUWMXJ7RLGdkYR4pPZFwsW5WT5o/+tp/U75DvVpRp/ez0M/l61v7NPr+RW7cs0y8Z0M5ZsO9py57OSgZFLqm0/oW+DlLIqU+/c0FxJgQBAEAQBAEAQBDw6FcOzNCCYxGtEy4MGXeanuV1gU8MOJ9zJ6zkuy3gXSP3LUrApwgCAIAUBQvSBZ2i9sdowdqu+IdU8x5Kn1GnaSmu5pdDyd4up9uaKgq00BdvRzM4RYR4PHPA+QVtps91KJmter2lGf0JqYlaT8OJvhPae1pbTwd4KXKH/OpfArY2/wcoPtJfifLxyvSulmbOmDj2MY5x8qc15kw45Qj8dxhW+FGyS/t29WkZ7zTBZLRNHrOHRtpnpPOiKccV9ZMuGp7fL1PjBgp3x4ui5v6cz7d+y2y0BrMK9Z53uOfIZcl7j1KqCiMzJeTc5ehU5xkS0pohhpBh6ulsA2ni47t1FXWRllXNLoi7qnDTsfeXvS57FygQYMrBoKMYwVJPiSdpKs4xhTDySKCc7Mm3d85M55eW33TLqCrYTTqt3n2nceGxUuVlO2Wy5LsarT8CONHifvPv5GexLpxo1HRPs2b3DXI91vzPivujBnPnLkjnl6tVT7NfN/gXmzrLgSzNRobhrOd1jxLj/hW1dVdMeXqZu/JuyZe29/h/ogrbvmxlWSw03e2eoOz2vJRL8+MeVfMssPR5z9q3kvLuUmcnIkV2nFcXO3nZwA2DgFUzslN7yZpKaIUx4YLZGBfB2CAIAgCAIAgCAzyUsYsRkMZucG95xPcvuuHHNI432eFXKb7I7DLwgxoa0UDQAOwCgWmS4VsjBSk5NyfcyL0+QgCAIAgI+3ZAR4D4e0ireDhi3xXK+vxK3Ek4l7oujYu3X5dzkhaQSCKEYEbiM1mmtnsbqLTW66E7cmZ0Jto2PDmHmKjxaFMwZ8NyXmVmsVceM35bP8AwdKdCBIdtFac81e7c9zIJ8tg6ECQdorTmmyCb22MUeVD3sc7JhLgPepQHkCe9fMocTTfY+oWOMWl3NK22vijoIR0dP8AeP8AYh7ae8cgO3cudylJcEe/VnfGlCt+LPt0XmzPLwIMpBoKMhsFST4knaV7GMKYbdEj5nO3Kt36tlMno8xaUTRgtLYLTgXYN+Jx2ngMlW2O3KltFeyX1EaNPhxWPeb9SxWJdeDAo532kT2nDAH3W7O3NTKMKFXxZV5ep23+yuUfL8zJbV5YMvVtdOJ7DdnxH1fNfV2XCrl1Z84unW5HNcl5soVsW7GmTruozYxuDee89qp7smdr6/Q02Jp9OOt0t35sjFHJ2+xISViTEXqQXU3uGiO91KrvDGtn0REuz6KvekvpzNWdlXQojobxRzTQ0xGVcO9cpwcJcMup3oujdBTj0ZhXydQgCAIAgCAICx3DltKZ0iMGNLuZo0eZ7lO0+G9u/kU2t2cNCj5s6QFeGTPqAIAgCAID4UBzS+tn9FMFwGrE1h8WTh8+aos+rgs4vM12j5HiUcL6x+xCyUfo4jHj1XNd3Gqi1y4ZJljfDxK5R80zocxfOVZgC5/wNw7zRXcs6qJlK9IyZ82kvmxLXzlXGhL2fG3DvbVeQz6pfAT0fJit1s/k/wAyegR2vAcxwc05EGoPNTIyTW6K2UJQe0lse9Gi9PkiZqyjMOBmD9mDVsIHAn2oh2ngMBxUedPiP2+nkS68rwI7Ve8+/f6GxOz8CWYNNzWNGDWjM8GtC+p2V0x5vZHxVRdkz2im35/myl2peeYmSWSzHNacNQF0Q9pHV5d6rLcuy32a09jQY+m0Y6472m/w/wBmtJXQmomLmiGDtece4VPeucMG2fXl8ztbq+PWto+0/gT0lcaEMY0RzzubRrfmfFS4adBc5vcrLtctl7iS+5PSdkS8EVZDY2nrEVP5jipsKa6+iKy3KvtftybMM9eOWhYOitJ3M1z4Zc18TyqodWdKcDIt92L+Zz68loQ5iOYsNpAIAOlSpI24cKKmyrY2T4omq07Hsop4Jvf5EWoxPCAIAgCAIAgLz6OIOrGfvLW9wJ/qVtpkeUn8TM67PeUI+W7/AF6FzVoUAQBAEAQBAEBBXxs7ppd1BVzNdvLrDmK9wUXMq8Sr4osdMyPBvW/R8mcwCz7Nn2CHooh5sSdg21Eln1aSWE67NhG8bncVIx8iVUuvIhZuDDJh8ezOpysw2I1r2GrXAEHgVoIyUlxLuYqyDhJxl2PM2yIRRjg33iNIjsGXf3JJNrZPY9g4p+0tyLh3agaWnFDozzm6K4u/lGHguCxK995c38SW9RvS4YPhXkv1ub8SNAgNxMOG0bKho5BdW4VrnsiOoW3Plu2Q07fSWZgzSiH3RQd7qKNPPrj7vMsKtGyJ85bL5kBO33juwhtZDG/rO8cPBQrNRm/dWxZ06JTH323+BBTloRov72I53AnDuyUOd05+8y0qxaavcikaq5HcL09CAIAgCAIAgCA6J6PG/d3HfEPk1XWnfyvqZLW3vkL5ItCsCnCAIAgCAIAgPjggOTXis/oJh7KapOkz4XYjuxHJZ3Kr8Oxo2+n5PjUKXdcmRqjk4IAvDw6F6PpkugOYfUeadjhXzqrzT5718PkZPWqlG9SXdFnjB1NUgHeRUdwIU579ioW2/Mh5yypiJnOOaN0OGG+Na+KjWU2T5cf4E2rJpr/8k/myLNxmE1dHiOO+gr41XD93RfWTJq1ucfdgl6noXEg7YsX+X6Lz921+bD127tFfj+Z6Fxpf24ve3/5X2tOrXdny9bv8l+J7FyJb2on5h9F7+76vifP77yPh6HsXKlf9w/j/AOk/d9Pk/U8/fWT5r0Im9V3YECB0kIO0tJoxcTgc8FHy8Suuviiibp2o3338E3y2fYpyqzQoIehAEAQBAF4Dono9P3Z3/If0tV5p/wDLfzMlrS/iF8kWhTynCAIAgCAIAgCAqfpAs7ThCM0Yw8HfA76Gniq/UKuKHEuxdaNkcFrqfSX3OfqlNWEAQ8OgejuARBe85Ofh2NAFe+vcrrTo7VuXmZXW5p3KK7ItMWtMCAeIqO6oU9lMtt+ZEzhnh+7/AGd/aHtPmR4rhPx17uzJlf7I+U+JejIObtq0ofWlm03tY5472uKiTyMqPWHpzLGrCwLOlnrsvuRbr6TX+2PwH6qO8+5f/CdHRcZ9G/VHg3ym/aZ+QL5/eFp9LRsbyfqeTe+b/wBRv5G/Reft13mff7oxv7X6s8G9k3/q/wArfovP263zPf3Ti/2/izUnrcmIzdCLELm1BpRoxGWQXOeTZNbNnanAoqlxQWzI9cCaEAQBAEAQBAXr0cRtSMzc5rvzCn9Kt9NlvGS+JmNdhtOEvNben/0uSsyhCAIAgCAIAgCAxTMEPa5jhUOBB7DgvmUVJbPufUJuElJdUcgtCUMGI+E7NpI7RsPMUKzVkHCbi+xvMe5W1xmu5rr4O5u2TZkSYiBkMfE7Y0bz9F2ppla9kRcrKhjw4pfReZ1aQlGwobYbBRrRQfU8StDXBQiorsYi62Vs3OXVnqZD6fZkA+8CQe4ghey37HzHhT9rp8CFmrwvgGkzLPaNj4ZD2HmaU7Cossp1/wAyL+aLCvAjev8Ahmm/J8mbEreeUiZRmtO59WeLsF9wy6pdznZpuTDrB/Tn9jdiy0GMNZsOIN5DXeK6uFc/JkaNltL5Np+hEzdz5V+TXMPuOoO41CjzwapdtvkTqtXyYdXv8yHm7iO/hRgeD20/mH0UWem/2v1J9eur+uHoyHmrrTcP+FpDewh3hn4KNPDuj23+RYVatjT6y2+ZExoLmGj2uadzgWnuKiyhKPVFhCyE1vFp/Ixrw+wgCAIAgCAICzXAmdGYLK4PYe9pqPDSU/Tp7W7eZS65XxUKXk/udFCuzKH1AEAQBAEAQBAEBRfSFZ1HMjtGeo/tGLT3VHIKp1Grmpo0eh5HvUv5r/JTVVmhfQ6ndiLBdAa6A0MHrNGYftqcyeJWhxXCVacFsYjPhdG5q17vs/gS5cMlJIez6kXaMxGgViNb0sP1mj94zi32hwOPFcLJzh7S5ok0V12+w3wv8H+RkkLTgTLfs3B2GLTmO1pXtd1dq5eh5djXY8vaW3x/2Q1r3NhRKugHo3bs2Hl6vLuUW7AhL3Hsyfi6xbXys9pfiU+ckJiUdrBzNzmE6J7HBVk67aH5fYv6r8fLjy2fwfU2JW9M2z+LpDdEAd45+K+45t0e+5ys0rGn/Tt8iZlb9u/iwQeLHU8D9VJjqX90SBZoK/8AOXqS8rfCVf1nOYffaad4qFLhnVS+BX26Rkw7J/Ird+bRZFisENwc1ra1aairjj4AKBn2Kckolxo2POquTmtm2VpQC6CAIAgCAIAgNmzZroorIg9VwPLb4VXSmzgmmR8mnxqpQ80dghPBAINQcQeByWmT35mDaa5M9oeBAEAQBAEAQBAaNsSIjwXwj6ww4OGLT3rldX4kHE741zptjNdmcjewglrhQgkEbiMCFm2tm15G8hJSSa6MmbqWz+zRdY/ZvoH8NzuXkpWHkeFPZ9GVup4Xj1bx6rp+R0O0ZcxGVhu0XjWhu2V3He05EcVd2Rco7xfMydM1CW0luu5qWHbTY9WPGhGZhEhneMKt3hcqL1ZyfJ90SMvDdO0o84PoyHvLdk1MeUq14xc1p0a+8ymR4bVHycT+uvkyfgaktlTfzXZvn6/maFkX0iM1ZlumBhpAUeO0ZFcas+UeViJWVo0Je1S9vh2+hcZSdgTLDoOa9pzafJzTkrKFld0eXNFDbRdjT9pNP9dGQFr3Khvq6XOgfZNSw9m1qh3YEZc4cmWeNrNkOVvNefcploWdFgO0YrC3cc2nscMCqqymdb2kjRUZVV63re/3NRczuECQQ9CAIAgCAIAgCHh0e41p9LA6MnWh6v4PUPy5K9wbeOvZ9UZDV8bwruNdJffv+ZZVNKoIAgCAIAgCAID5RAc3vzZ3Rx+kaNWKK/iGDvkeZVHn1cNnEujNZo+R4lPA+sfs+hXFBLkv1xrZ02/s8Q6zRqE7Wbu0eSucDI4o8D6oyur4fhy8WK5Pr8zHfSy3NInIFWubTTLc+D+WR4LzNqcX4sO3U90rKjJPGt5p9PyN67N5WzA6OJRsUcg7i3jwXXFy1YtpdSPqGmyx/ahzj9vmYr0XXEasWDRsXMjIP7dzuK+MrDU/ah1PvT9TdPsWc4/YoTHxIT8C5j2mmFWuB3Ko3lXLyZqHGF0OezT+parHvs5tGzLdIe23rc25HkrGnUGuVvqikytET9ql/QtsGYgTMPVLIjDmDQ94ORVipV2x5c0UU4W48/a3iylXwsOBLhr4TiC406M4jiQcwBzzVVmY8K1xL0NFpWdde3Ca3SXUq6ry8CAIAgCAIAgCAICRsC1DLRmxPV6rxvac+YzXfGu8Ke/YhZ2L+0VOPfqv18TrEGIHAOaQQRUEZEHIrRJprdGIlFxezPa9PAgCAIAgCAIAgIW9lndPLuAGs3Xb2jMcxVRsurxK2l1J2nZHgXpvo+TOWgrOm2MsrMOhva9ho5pqD/exfcJuEuJHO6uNkHGXRnVrKnmTUEPAwcKPbnQ5Oaf72rRU2Ruhv6mIyaJ41vB5dH9mc6vDZbpWNRtdEnShu2gVyrvH0VLlUumzl9DV4GVHKp59ejX68y03XvSItIMcgPya/IP4Hc7zU/FzVP2ZdSl1DTHX/wAlXOPdeX+jdvJd1kyNJtGxRk7Y7g767F1ycWNy3XUjYGoTxpcL5x+3yOcTUs+G8siNLXDAg+fEKjnCUHws19VsLYqUXuj7KzL4bg6G4tcNrTTkd4SE5Qe8WLKYWR4ZrdGa07TiTDg6KQSG6IoKDtpvK+7bpWveRzxsWvHi4193uaa5EkIAgCAIAgCAIAgCAuNyLe0aS0U4H92TsO1n0/wrTByv/OX0M5q+B/7wXzX+S9hWxnQgCAIAgCAIAgPlEBym89ndBMPaBqu129jtnI1Cz2XV4dj+JtdNyPGoT7rkyKUYsCbupbP7PFo4/ZvoHcDsdy8lLw8jwp7Poyr1TCV9e695frYvdvWU2ZglmGl1mO3O2cjkrjIpVsNvQzOHlSxrVL1XwOVxIRa4tcCHA0IOYIWdkpRez6o28ZRnFSXNFyutevKDMu4MiH9L/r3q0xM3+iwz2paV1spXzX+V+RYLesOHNMx1XjqvGY4HeFMvx43L4+ZV4ebZjS5dO6OYzks6E90N9NJpoaGo71Q2QdcnFmypuVsFOPcwr4OwQBAEAQBAEAQBAEAQBN31R41uti/3TvMIgEGOftMmuPr8D73mrnEy1NcEuv3Mrqemup+JX7v2/wBFtViUoQBAEAQBAEAQFWv5Z3SQRFaNaGan4D1u7A96gZ9XFDiXYuNGyfDu4H0l9znipDWhDwv1xra02fs8Q6zBqE7Wbu0eSucDI448EuqMrq+H4c/Fh0fX5mvfuxf/ACYYywiAbtj/AJHkuefj8vEj9Tto2bs/An9PyKSqo0pOSV6I8KC6EDXCjHnNg2gb+G5S4Zk4wcdyru0qmy1Weq8yEJric9qiFmklyR8Q9CAIAgCAIAgCAIAgCAIAE5p7o8aTWzLndy99KQ5o8BE+T/r/AJVrjZ39FnqZ3P0jrOj0/Iu8OIHAEEEHEEGoI4K0TT5ozzTT2Z6Xp4EAQBAEAQHiPDDmlrhUEEEbwcCvJJSWzPqMnFprqjkFpyZgxXwj6poDvGbT3UWatrdc3DyN3i3q6qM13RqrmSDLKzLob2xGGjmmo/vcvqE3CXEuxytqjbBwl0Z1WzrQhzEDpMNEgh4OQPrNK0VVsba+L1MRfj2Y93B37f4OX2nChtivbBdpMB1Tw3caZV4KgtjCM2odDaYs7JVJ2LZmquRICAIAgCAIAgCAIAgCAIAgCAIAgJSx7ejSx1HVbtY7FvLcexSaMmdXR8vIg5Wn05HvLZ+a6l3sm9sCNQPPRu3PyJ4Oy76K1pza7OT5MzeTpV9PNLdfD8ifDhsUxPcrGmj0gCAIAgPhQFJ9IVndSYaPcf8A0nzHcqvUaeSsX1NDoeTs3S38UUpVBpAvQZGx3BpYHENcauaDgSMqhfSnJR2TObqi5KTXNGNfJ0CAIAgCAIAgCAIAgCAIAgCAIAgCAIAh4bsjaseD+6iuaN1at/KcF1rvsh7rI12HTdznFfr4k9KX5jNwiQ2P4glh+YUyGoyXvLcrLdCrfuSa/ElYF+oB68OI3s0XDzCkx1GrumiDPQ710kn+vkbbb5Sh9Zw7WO+S+/2+nzfozi9Hyl2Xqj66+Up7Tj2Mcn7fT5v0Z4tHyn2XqjTj36gDqQ4jvygea5y1GtdEzvDQ737zS9SEti9z48N0IQmta7DElzt4IyAKi3ZzsTiolji6PGmam5c0VpQC6CAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAJuwEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQH/2Q==',
      language: 'fra',
      roleId: role.id,
    };
    await this.usersService.create(createUserDto);
  }

  async checkUpdates() {
    if (this.configService.get<string>('CHECK_UPDATES_AT_STARTUP') !== 'true')
      return;

    await this.updateService.runCheckUpdates();
  }
}
