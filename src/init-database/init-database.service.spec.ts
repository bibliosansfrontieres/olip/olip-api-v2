import { Test, TestingModule } from '@nestjs/testing';
import { InitDatabaseService } from './init-database.service';
import { LanguagesService } from 'src/languages/languages.service';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';
import { CategoriesService } from 'src/categories/categories.service';
import { categoriesServiceMock } from 'src/tests/mocks/providers/categories-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';
import { PermissionsService } from 'src/permissions/permissions.service';
import { permissionsServiceMock } from 'src/tests/mocks/providers/permissions-service.mock';
import { RolesService } from 'src/roles/roles.service';
import { rolesServiceMock } from 'src/tests/mocks/providers/roles-service.mock';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { DublinCoreItemsService } from 'src/dublin-core-items/dublin-core-items.service';
import { dublinCoreItemsServiceMock } from 'src/tests/mocks/providers/dublin-core-items-service.mock';
import { DublinCoreAudiencesService } from 'src/dublin-core-audiences/dublin-core-audiences.service';
import { dublinCoreAudiencesServiceMock } from 'src/tests/mocks/providers/dublin-core-audiences-service.mock';
import { DublinCoreLicensesService } from 'src/dublin-core-licenses/dublin-core-licenses.service';
import { dublinCoreLicensesServiceMock } from 'src/tests/mocks/providers/dublin-core-licenses-service.mock';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { dublinCoreTypesServiceMock } from 'src/tests/mocks/providers/dublin-core-types-service.mock';
import { UploadService } from 'src/upload/upload.service';
import { uploadServiceMock } from 'src/tests/mocks/providers/upload-service.mock';
import { SettingsService } from 'src/settings/settings.service';
import { settingsServiceMock } from 'src/tests/mocks/providers/settings-service.mock';
import { HighlightsService } from 'src/highlights/highlights.service';
import { highlightsServiceMock } from 'src/tests/mocks/providers/highlights-service.mock';
import { StatsService } from 'src/stats/stats.service';
import { statsServiceMock } from 'src/tests/mocks/providers/stats-service.mock';
import { EditosService } from 'src/editos/editos.service';
import { editosServiceMock } from 'src/tests/mocks/providers/editos-service.mock';
import { ApplicationTypesService } from 'src/application-types/application-types.service';
import { applicationTypesServiceMock } from 'src/tests/mocks/providers/application-types-service.mock';
import { MaestroService } from 'src/maestro/maestro.service';
import { maestroServiceMock } from 'src/tests/mocks/providers/maestro-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { MaestroApiService } from 'src/maestro/maestro-api.service';
import { maestroApiServiceMock } from 'src/tests/mocks/providers/maestro-api-service.mock';
import { MaestroWebsocketApiService } from 'src/maestro/maestro-websocket-api.service';
import { maestroWebsocketApiServiceMock } from 'src/tests/mocks/providers/maestro-websocket-api-service.mock';
import { HardwareService } from 'src/hardware/hardware.service';
import { hardwareServiceMock } from 'src/tests/mocks/providers/hardware-service.mock';
import { UpdateService } from 'src/update/update.service';
import { updateServiceMock } from 'src/tests/mocks/providers/update-service.mock';

describe('InitDatabaseService', () => {
  let service: InitDatabaseService;
  let updateService: UpdateService;
  let configService: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        InitDatabaseService,
        LanguagesService,
        CategoriesService,
        DublinCoreItemsService,
        DublinCoreAudiencesService,
        DublinCoreLicensesService,
        DublinCoreTypesService,
        ApplicationsService,
        ApplicationTypesService,
        PermissionsService,
        RolesService,
        UsersService,
        UploadService,
        SettingsService,
        HighlightsService,
        EditosService,
        StatsService,
        MaestroService,
        ConfigService,
        MaestroApiService,
        MaestroWebsocketApiService,
        HardwareService,
        UpdateService,
      ],
    })
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(DublinCoreItemsService)
      .useValue(dublinCoreItemsServiceMock)
      .overrideProvider(DublinCoreAudiencesService)
      .useValue(dublinCoreAudiencesServiceMock)
      .overrideProvider(DublinCoreLicensesService)
      .useValue(dublinCoreLicensesServiceMock)
      .overrideProvider(DublinCoreTypesService)
      .useValue(dublinCoreTypesServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(PermissionsService)
      .useValue(permissionsServiceMock)
      .overrideProvider(RolesService)
      .useValue(rolesServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(UploadService)
      .useValue(uploadServiceMock)
      .overrideProvider(SettingsService)
      .useValue(settingsServiceMock)
      .overrideProvider(HighlightsService)
      .useValue(highlightsServiceMock)
      .overrideProvider(EditosService)
      .useValue(editosServiceMock)
      .overrideProvider(StatsService)
      .useValue(statsServiceMock)
      .overrideProvider(ApplicationTypesService)
      .useValue(applicationTypesServiceMock)
      .overrideProvider(MaestroService)
      .useValue(maestroServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(MaestroApiService)
      .useValue(maestroApiServiceMock)
      .overrideProvider(MaestroWebsocketApiService)
      .useValue(maestroWebsocketApiServiceMock)
      .overrideProvider(HardwareService)
      .useValue(hardwareServiceMock)
      .overrideProvider(UpdateService)
      .useValue(updateServiceMock)
      .compile();

    service = module.get<InitDatabaseService>(InitDatabaseService);
    updateService = module.get<UpdateService>(UpdateService);
    configService = module.get<ConfigService>(ConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('checkUpdates', () => {
    it('should be defined', () => {
      expect(service.checkUpdates).toBeDefined();
    });

    it('should skip if CHECK_UPDATES_AT_STARTUP variable is false', async () => {
      jest.spyOn(updateService, 'runCheckUpdates');
      jest.spyOn(configService, 'get').mockReturnValue('false');
      await service.checkUpdates();
      expect(updateService.runCheckUpdates).not.toHaveBeenCalled();
    });

    it('should run if CHECK_UPDATES_AT_STARTUP variable is true', async () => {
      jest.spyOn(updateService, 'runCheckUpdates');
      jest.spyOn(configService, 'get').mockReturnValue('true');
      await service.checkUpdates();
      expect(updateService.runCheckUpdates).toHaveBeenCalled();
    });

    it('should call updateService.runCheckUpdates', async () => {
      jest.spyOn(updateService, 'runCheckUpdates');
      await service.checkUpdates();
      expect(updateService.runCheckUpdates).toHaveBeenCalled();
    });
  });
});
