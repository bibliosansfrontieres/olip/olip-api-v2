import { Module, forwardRef } from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { CategoriesController } from './categories.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Category } from './entities/category.entity';
import { LanguagesModule } from 'src/languages/languages.module';
import { CategoryTranslation } from './entities/category-translation.entity';
import { PlaylistsModule } from 'src/playlists/playlists.module';
import { ItemsModule } from 'src/items/items.module';
import { CategoryPlaylistItemsModule } from 'src/category-playlist-items/category-playlist-items.module';
import { DublinCoreItemsModule } from 'src/dublin-core-items/dublin-core-items.module';
import { VisibilitiesModule } from 'src/visibilities/visibilities.module';
import { UsersModule } from 'src/users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';
import { DublinCoreTypesModule } from 'src/dublin-core-types/dublin-core-types.module';
import { UploadModule } from 'src/upload/upload.module';
import { CategoryPlaylistsModule } from 'src/category-playlists/category-playlists.module';
import { ApplicationsModule } from 'src/applications/applications.module';
import { SearchModule } from 'src/search/search.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Category, CategoryTranslation]),
    LanguagesModule,
    forwardRef(() => PlaylistsModule),
    DublinCoreItemsModule,
    forwardRef(() => CategoryPlaylistItemsModule),
    VisibilitiesModule,
    UsersModule,
    JwtModule,
    ConfigModule,
    forwardRef(() => ItemsModule),
    DublinCoreTypesModule,
    UploadModule,
    forwardRef(() => CategoryPlaylistsModule),
    forwardRef(() => ApplicationsModule),
    forwardRef(() => SearchModule),
  ],
  controllers: [CategoriesController],
  providers: [CategoriesService],
  exports: [CategoriesService],
})
export class CategoriesModule {}
