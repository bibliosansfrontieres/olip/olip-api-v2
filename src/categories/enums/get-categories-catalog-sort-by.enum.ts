export enum GetCategoriesCatalogSortByEnum {
  ALPHABETICAL = 'alphabetical',
  COUNT_PLAYLISTS = 'count_playlists',
  LAST_UPDATE = 'last_update',
}
