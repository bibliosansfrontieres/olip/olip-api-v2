export enum GetCategoriesOrderEnum {
  POPULARITY = 'popularity',
  ALPHABETICAL = 'alphabetical',
  COUNT_PLAYLISTS = 'count_playlists',
  LAST_UPDATE = 'last_update',
}
