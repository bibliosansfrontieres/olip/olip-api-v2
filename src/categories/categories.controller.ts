import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { GetCategoriesOptionsDto } from './dto/get-categories-options.dto';
import { GetItemsSelectionDto } from './dto/get-items-selection-options.dto';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNotAcceptableResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Category } from './entities/category.entity';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { cachedMethod } from 'src/utils/caching.util';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { PlaylistTranslation } from 'src/playlists/entities/playlist-translation.entity';
import { CategoryTranslation } from './entities/category-translation.entity';
import { Item } from 'src/items/entities/item.entity';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';
import { DublinCoreItemTranslation } from 'src/dublin-core-items/entities/dublin-core-item-translation';
import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';
import { DublinCoreTypeTranslation } from 'src/dublin-core-types/entities/dublin-core-type-translation.entity';
import { GetCategoriesResponse } from './responses/get-categories-response';
import { UserId } from 'src/auth/decorators/user-id.decorator';
import { OptionalAuth } from 'src/auth/decorators/optional-auth.decorator';
import { GetCategoryOptionsDto } from './dto/get-category-options.dto';
import { GetCategoryResponse } from './responses/get-category-response';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { OrderCategoriesDto } from './dto/order-categories.dto';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { CreateCategoryResponse } from './responses/create-category-response';
import { UpdateCategoryBadRequest } from './responses/update-category-bad-request';
import { UpdateCategoryNotAcceptable } from './responses/update-category-not-acceptable';
import { CreateCategoryNotAcceptable } from './responses/create-category-not-acceptable';
import { GetCategoryContentResponse } from './responses/get-category-content-response';
import { AddPlaylistsToCategoryDto } from './dto/add-playlists-to-category.dto';
import { Visibility } from 'src/visibilities/entities/visibility.entity';
import { GetCategoriesListResponse } from './responses/get-categories-list-response';
import { GetCategoriesListDto } from './dto/get-categories-list.dto';
import { GetCategoriesCatalogDto } from './dto/get-categories-catalog.dto';
import { GetCategoriesCatalogResponse } from './responses/get-categories-catalog-response';
import { Application } from 'src/applications/entities/application.entity';
import { GetCategoriesBackOfficeListDto } from './dto/get-categories-back-office-list.dto';
import { GetCategoriesBackOfficeListResponse } from './responses/get-categories-back-office-list-response';
import { GetCategoryBackOfficeDto } from './dto/get-category-back-office.dto';
import { GetCategoryBackOfficeResponse } from './responses/get-category-back-office-response';
import { GetCategoryBackOfficeCategoryPlaylistsDto } from './dto/get-category-back-office-category-playlists.dto';
import { GetCategoryBackOfficeCategoryPlaylistsResponse } from './responses/get-category-back-office-category-playlists-response';
import { ApplicationTranslation } from 'src/applications/entities/application-translation.entity';

@ApiTags('categories')
@Controller('api/categories')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @Auth()
  @Get('back-office/list')
  @ApiOperation({ summary: 'Get back-office categories list' })
  @ApiOkResponse({ type: GetCategoriesBackOfficeListResponse })
  getCategoriesBackOfficeList(
    @Query() getCategoriesBackOfficeListDto: GetCategoriesBackOfficeListDto,
  ) {
    return cachedMethod(
      'categoriesService',
      'getCategoriesBackOfficeList',
      [getCategoriesBackOfficeListDto],
      async () => {
        return this.categoriesService.getCategoriesBackOfficeList(
          getCategoriesBackOfficeListDto,
        );
      },
      [
        Category,
        CategoryTranslation,
        CategoryPlaylist,
        CategoryPlaylistItem,
        Playlist,
        PlaylistTranslation,
        Visibility,
      ],
    );
  }

  @Auth()
  @Get('back-office/:id/category-playlists')
  @ApiOperation({ summary: 'Get category back-office category playlists' })
  @ApiOkResponse({ type: GetCategoryBackOfficeCategoryPlaylistsResponse })
  async getCategoryBackOfficeCategoryPlaylists(
    @Param('id') id: string,
    @Query()
    getCategoryBackOfficeCategoryPlaylistsDto: GetCategoryBackOfficeCategoryPlaylistsDto,
  ) {
    return cachedMethod(
      'categoriesService',
      'getCategoryBackOfficeCategoryPlaylists',
      [id, getCategoryBackOfficeCategoryPlaylistsDto],
      async () => {
        return this.categoriesService.getCategoryBackOfficeCategoryPlaylists(
          +id,
          getCategoryBackOfficeCategoryPlaylistsDto,
        );
      },
      [
        Category,
        CategoryTranslation,
        Visibility,
        CategoryPlaylist,
        Playlist,
        PlaylistTranslation,
        Application,
        ApplicationTranslation,
      ],
    );
  }

  @Auth()
  @Get('back-office/:id')
  @ApiOperation({ summary: 'Get category back office' })
  @ApiOkResponse({ type: GetCategoryBackOfficeResponse })
  async getCategoryBackOffice(
    @Param('id') id: string,
    @Query() getCategoryBackOfficeDto: GetCategoryBackOfficeDto,
  ) {
    return cachedMethod(
      'categoriesService',
      'getCategoryBackOffice',
      [id, getCategoryBackOfficeDto],
      async () => {
        return this.categoriesService.getCategoryBackOffice(
          +id,
          getCategoryBackOfficeDto,
        );
      },
      [Category, CategoryTranslation, Visibility],
    );
  }

  @OptionalAuth()
  @Get('catalog')
  @ApiOperation({ summary: 'Get contents catalog' })
  @ApiOkResponse({ type: GetCategoriesCatalogResponse })
  getCategoriesCatalog(
    @UserId() userId: number | undefined,
    @Query() getCategoriesCatalogDto: GetCategoriesCatalogDto,
  ) {
    return cachedMethod(
      'categoriesService',
      'getCategoriesCatalog',
      [{ userId }, getCategoriesCatalogDto],
      async () => {
        return this.categoriesService.getCategoriesCatalog(
          getCategoriesCatalogDto,
          userId,
        );
      },
      [
        Category,
        CategoryTranslation,
        CategoryPlaylist,
        CategoryPlaylistItem,
        Playlist,
        PlaylistTranslation,
        Visibility,
        Application,
      ],
    );
  }

  @OptionalAuth()
  @Get('list')
  @ApiOperation({ summary: "Get simple categories's list" })
  @ApiOkResponse({ type: GetCategoriesListResponse })
  getCategoriesList(
    @UserId() userId: number | undefined,
    @Query() getCategoriesListDto: GetCategoriesListDto,
  ) {
    return this.categoriesService.getCategoriesList(
      getCategoriesListDto,
      userId,
    );
  }

  @Auth(PermissionEnum.CREATE_CATEGORY)
  @Post()
  @ApiOperation({ summary: 'Create a category (known as Thematic)' })
  @ApiBody({ type: CreateCategoryDto })
  @ApiOkResponse({ type: CreateCategoryResponse })
  @ApiNotFoundResponse({ description: 'Visibility user/role id not found' })
  @ApiNotAcceptableResponse({ type: CreateCategoryNotAcceptable })
  async createCategory(
    @UserId() userId: number,
    @Body() createCategoryDto: CreateCategoryDto,
  ) {
    const category = await this.categoriesService.create(
      createCategoryDto,
      userId,
    );
    return new CreateCategoryResponse(category);
  }

  @Auth(PermissionEnum.UPDATE_CATEGORY)
  @Patch(':id')
  @ApiOperation({ summary: 'Update a category (known as Thematic)' })
  @ApiOkResponse()
  @ApiBody({ type: UpdateCategoryDto })
  @ApiNotFoundResponse({ description: 'category not found' })
  @ApiBadRequestResponse({ type: UpdateCategoryBadRequest })
  @ApiNotAcceptableResponse({ type: UpdateCategoryNotAcceptable })
  updateCategory(
    @Param('id') id: string,
    @Body() updateCategoryDto: UpdateCategoryDto,
  ) {
    return this.categoriesService.update(+id, updateCategoryDto);
  }

  @OptionalAuth()
  @Get()
  @ApiOperation({ summary: 'Get all categories with playlists' })
  @ApiOkResponse({ type: GetCategoriesResponse, isArray: true })
  getCategories(
    @Query() getCategoriesOptions: GetCategoriesOptionsDto,
    @UserId() userId: number | undefined,
  ) {
    return cachedMethod(
      'categoriesService',
      'getCategories',
      [{ userId }, getCategoriesOptions],
      async () => {
        const categories = await this.categoriesService.getCategories(
          userId,
          getCategoriesOptions,
        );
        return categories.map(
          (category) => new GetCategoriesResponse(category),
        );
      },
      [
        Category,
        CategoryTranslation,
        CategoryPlaylist,
        CategoryPlaylistItem,
        Playlist,
        PlaylistTranslation,
        Item,
        Visibility,
      ],
    );
  }

  @OptionalAuth()
  @Get(':id/playlists')
  @ApiOperation({ summary: 'Get a category with playlists' })
  @ApiOkResponse({ type: GetCategoryResponse })
  getCategoryPlaylists(
    @Param('id') id: string,
    @Query() getCategoryOptions: GetCategoryOptionsDto,
    @Query() pageOptionsDto: PageOptionsDto,
    @UserId() userId: number | undefined,
  ) {
    return cachedMethod(
      'categoriesService',
      'getCategoryPlaylists',
      [{ userId, id }, getCategoryOptions],
      async () => {
        const category = await this.categoriesService.getCategory(
          +id,
          getCategoryOptions,
          userId,
          pageOptionsDto,
          false,
        );
        return category;
      },
      [
        Category,
        CategoryTranslation,
        CategoryPlaylist,
        Playlist,
        PlaylistTranslation,
        Visibility,
      ],
    );
  }

  @OptionalAuth()
  @Get(':id')
  @ApiOperation({ summary: 'Get a category with playlists and items' })
  @ApiOkResponse({ type: GetCategoryResponse })
  getCategory(
    @Param('id') id: string,
    @Query() getCategoryOptions: GetCategoryOptionsDto,
    @Query() pageOptionsDto: PageOptionsDto,
    @UserId() userId: number | undefined,
  ) {
    return cachedMethod(
      'categoriesService',
      'getCategory',
      [{ userId, id }, getCategoryOptions],
      async () => {
        const category = await this.categoriesService.getCategory(
          +id,
          getCategoryOptions,
          userId,
          pageOptionsDto,
        );
        return category;
      },
      [
        Category,
        CategoryTranslation,
        CategoryPlaylist,
        CategoryPlaylistItem,
        Playlist,
        PlaylistTranslation,
        Item,
        DublinCoreItem,
        DublinCoreItemTranslation,
        Visibility,
      ],
    );
  }

  @Get('selection/:categoryId')
  getItemsSelection(
    @Param('categoryId') categoryId: string,
    @Query() getItemsSelectionDto: GetItemsSelectionDto,
  ) {
    return cachedMethod(
      'categoriesService',
      'getItemsSelection',
      [{ categoryId }, getItemsSelectionDto],
      async () => {
        return this.categoriesService.getItemsSelection(
          +categoryId,
          getItemsSelectionDto,
        );
      },
      [
        Category,
        CategoryTranslation,
        CategoryPlaylistItem,
        CategoryPlaylist,
        Item,
        DublinCoreItem,
        DublinCoreItemTranslation,
        DublinCoreType,
        DublinCoreTypeTranslation,
        Visibility,
      ],
    );
  }

  @Auth(PermissionEnum.UPDATE_CATEGORY)
  @ApiOperation({ summary: 'Order categories' })
  @ApiBody({ type: OrderCategoriesDto })
  @ApiCreatedResponse({ description: 'Categories ordered' })
  @ApiBadRequestResponse()
  @Patch('order')
  order(@Body() orderCategoriesDto: OrderCategoriesDto) {
    return this.categoriesService.order(orderCategoriesDto);
  }

  @Get('content/:id')
  @ApiOperation({ summary: 'Get category content' })
  @ApiOkResponse({ type: GetCategoryContentResponse })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  getCategoryContent(
    @Param('id') id: string,
    @Query() getCategoryOptions: GetCategoryOptionsDto,
    @Query() pageOptionsDto: PageOptionsDto,
  ) {
    return this.categoriesService.getCategoryContent(
      +id,
      getCategoryOptions,
      pageOptionsDto,
    );
  }

  @Post(':id/playlists')
  @ApiOperation({ summary: 'Add playlists to a category' })
  @ApiBody({ type: AddPlaylistsToCategoryDto })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  addPlaylistToCategory(
    @Param('id') id: string,
    @Body() addPlaylistsToCategoryDto: AddPlaylistsToCategoryDto,
  ) {
    return this.categoriesService.addPlaylistsToCategory(
      addPlaylistsToCategoryDto,
      +id,
    );
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a category' })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  remove(@Param('id') id: string) {
    return this.categoriesService.delete(+id);
  }
}
