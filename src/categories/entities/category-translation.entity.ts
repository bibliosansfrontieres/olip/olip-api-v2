import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Category } from './category.entity';
import { Language } from 'src/languages/entities/language.entity';
import { CreateCategoryTranslationDto } from '../dto/create-category-translation.dto';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

@Entity()
export class CategoryTranslation {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column({ nullable: true })
  description: string | null;

  @Column({ nullable: true })
  indexId?: string;

  @ManyToOne(() => Category, (category) => category.categoryTranslations)
  category: Category;

  @ApiPropertyOptional({
    type: Language,
  })
  @ManyToOne(() => Language, (language) => language.categoryTranslations)
  language: Language;

  constructor(
    createCategoryTranslationDto: CreateCategoryTranslationDto,
    category: Category,
    language: Language,
  ) {
    if (!createCategoryTranslationDto) return;
    this.title = createCategoryTranslationDto.title;
    this.description = createCategoryTranslationDto.description;
    this.category = category;
    this.language = language;
  }
}
