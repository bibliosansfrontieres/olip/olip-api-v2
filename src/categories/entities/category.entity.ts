import {
  Column,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { CategoryTranslation } from './category-translation.entity';
import { CreateCategoryDto } from '../dto/create-category.dto';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { Visibility } from 'src/visibilities/entities/visibility.entity';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class Category {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  sourceId: number | null;

  @Column()
  pictogram: string;

  @Column()
  isHomepageDisplayed: boolean;

  @Column()
  order: number;

  @Column({ default: false })
  isUpdateNeeded: boolean;

  @OneToOne(() => Visibility, (visibility) => visibility.category)
  @JoinColumn()
  visibility: Visibility;

  @ManyToMany(() => Playlist, (playlist) => playlist.categories)
  @JoinTable()
  playlists: Playlist[];

  @OneToMany(
    () => CategoryPlaylist,
    (categoryPlaylist) => categoryPlaylist.category,
  )
  categoryPlaylists: CategoryPlaylist[];

  @OneToMany(
    () => CategoryTranslation,
    (categoryTranslation) => categoryTranslation.category,
  )
  categoryTranslations: CategoryTranslation[];

  @ManyToMany(() => User, (user) => user.categories, { onDelete: 'CASCADE' })
  @JoinTable()
  users: User[];

  @DeleteDateColumn({ name: 'deleted_at', nullable: true })
  deletedAt: Date;

  @UpdateDateColumn({
    default: () => Date.now(),
    onUpdate: Date.now().toString(),
  })
  updatedAt: number;

  constructor(
    createCategoryDto: CreateCategoryDto,
    order: number,
    pictogram: string,
  ) {
    if (!createCategoryDto) return;
    this.pictogram = pictogram;
    if (createCategoryDto.isHomepageDisplayed !== undefined) {
      this.isHomepageDisplayed = createCategoryDto.isHomepageDisplayed;
    } else {
      this.isHomepageDisplayed = true;
    }
    this.order = order;
    this.playlists = [];
    this.sourceId = createCategoryDto.sourceId;
    this.isUpdateNeeded = false;
  }
}
