import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { TrueFalseEnum } from 'src/utils/enums/true-false.enum';

export class GetCategoryOptionsDto {
  @ApiProperty({ example: 'eng' })
  @IsString()
  languageIdentifier: string;

  @IsOptional()
  @IsEnum(TrueFalseEnum)
  getAllCategoryTranslations?: string;

  @ApiPropertyOptional({ example: '1,2' })
  @IsString()
  @IsOptional()
  playlistId?: string;

  @ApiPropertyOptional({ example: '1,2' })
  @IsString()
  @IsOptional()
  dublinCoreTypeId?: string;

  @ApiPropertyOptional({ example: 'example' })
  @IsString()
  @IsOptional()
  titleLike?: string;

  @ApiPropertyOptional({ example: TrueFalseEnum.TRUE })
  @IsEnum(TrueFalseEnum)
  @IsOptional()
  isVisibilityEnabled?: TrueFalseEnum;
}
