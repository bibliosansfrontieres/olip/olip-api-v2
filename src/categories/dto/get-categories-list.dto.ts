import { IsString } from 'class-validator';

export class GetCategoriesListDto {
  @IsString()
  languageIdentifier: string;
}
