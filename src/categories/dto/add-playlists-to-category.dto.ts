import { IsArray } from 'class-validator';

export class AddPlaylistsToCategoryDto {
  @IsArray()
  playlistIds: string[];
}
