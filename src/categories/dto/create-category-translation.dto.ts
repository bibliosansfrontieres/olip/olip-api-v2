import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class CreateCategoryTranslationDto {
  @ApiProperty({ example: 'My title' })
  @IsString()
  title: string;

  @ApiPropertyOptional({ example: 'My description' })
  @IsString()
  @IsOptional()
  description?: string;

  @ApiProperty({ example: 'eng' })
  @IsString()
  languageIdentifier: string;
}
