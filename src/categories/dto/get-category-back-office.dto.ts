import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class GetCategoryBackOfficeDto {
  @ApiProperty({ example: 'eng' })
  @IsString()
  languageIdentifier: string;
}
