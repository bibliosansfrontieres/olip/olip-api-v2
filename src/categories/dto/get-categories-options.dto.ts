import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { TrueFalseEnum } from '../enums/true-false.enum';
import { GetCategoriesOrderEnum } from '../enums/get-categories-order.enum';

export class GetCategoriesOptionsDto {
  @ApiProperty({ example: 'eng' })
  @IsString()
  languageIdentifier: string;

  @ApiProperty({ enum: TrueFalseEnum, default: TrueFalseEnum.TRUE })
  @IsEnum(TrueFalseEnum)
  @IsOptional()
  withPlaylists?: TrueFalseEnum;

  @ApiProperty({
    enum: GetCategoriesOrderEnum,
    default: GetCategoriesOrderEnum.POPULARITY,
  })
  @IsEnum(GetCategoriesOrderEnum)
  @IsOptional()
  order?: GetCategoriesOrderEnum;

  @ApiPropertyOptional({ example: '1,2' })
  @IsString()
  @IsOptional()
  categoryId?: string;

  @ApiPropertyOptional({ example: '1,2' })
  @IsString()
  @IsOptional()
  applicationIds?: string;

  @ApiProperty({ example: TrueFalseEnum.TRUE })
  @IsOptional()
  @IsEnum(TrueFalseEnum)
  isVisibilityEnabled?: TrueFalseEnum;
}
