import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class GetCategoryBackOfficeCategoryPlaylistsDto {
  @ApiProperty({ example: 'eng' })
  @IsString()
  languageIdentifier: string;
}
