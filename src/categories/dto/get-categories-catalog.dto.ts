import { IsEnum, IsOptional, IsString } from 'class-validator';
import { GetCategoriesCatalogSortByEnum } from '../enums/get-categories-catalog-sort-by.enum';

export class GetCategoriesCatalogDto {
  @IsString()
  languageIdentifier: string;

  @IsOptional()
  @IsString()
  categoryIds?: string;

  @IsOptional()
  @IsString()
  applicationIds?: string;

  @IsOptional()
  @IsEnum(GetCategoriesCatalogSortByEnum)
  sortBy?: GetCategoriesCatalogSortByEnum;
}
