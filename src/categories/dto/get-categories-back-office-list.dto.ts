import { IsString } from 'class-validator';

export class GetCategoriesBackOfficeListDto {
  @IsString()
  languageIdentifier: string;
}
