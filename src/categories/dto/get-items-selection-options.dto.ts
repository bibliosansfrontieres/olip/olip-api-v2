import { IsNumberString, IsOptional, IsString } from 'class-validator';

export class GetItemsSelectionDto {
  @IsNumberString()
  @IsOptional()
  count?: string;

  @IsString()
  languageIdentifier: string;
}
