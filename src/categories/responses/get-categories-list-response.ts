import { ApiProperty } from '@nestjs/swagger';
import { Category } from '../entities/category.entity';
import { categoryMock } from 'src/tests/mocks/objects/category.mock';
import { simplifiedLanguageMock } from 'src/tests/mocks/objects/simplified-language.mock';
import { categoryTranslationMock } from 'src/tests/mocks/objects/category-translation.mock';

export class GetCategoriesListResponse {
  @ApiProperty({
    example: categoryMock({
      categoryTranslations: [
        categoryTranslationMock({
          language: simplifiedLanguageMock(),
        }),
      ],
    }),
  })
  categories: Category[];

  constructor(categories: Category[]) {
    this.categories = categories;
  }
}
