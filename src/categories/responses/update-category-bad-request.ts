import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateCategoryBadRequest {
  @ApiProperty({ example: 400 })
  statusCode: number;

  @ApiProperty({
    example: [
      'pictogram must be a string',
      'createCategoryTranslationsDto must be an array',
      'createCategoryTranslationsDto.0.title must be a string',
      'createCategoryTranslationsDto.0.description must be a string',
      'createCategoryTranslationsDto.0.languageIdentifier must be a string',
    ],
  })
  message: string | string[];

  @ApiPropertyOptional({ example: 'Bad Request' })
  error?: string;
}
