import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateCategoryNotAcceptable {
  @ApiProperty({ example: 406 })
  statusCode: number;

  @ApiProperty({ example: 'file is not an image' })
  message: string;

  @ApiPropertyOptional({ example: 'Not Acceptable' })
  error?: string;
}
