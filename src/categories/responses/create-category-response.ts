import { Category } from '../entities/category.entity';
import { ApiProperty } from '@nestjs/swagger';

export class CreateCategoryResponse {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: 'images/picto.png' })
  pictogram: string;

  @ApiProperty({ example: true })
  isHomepageDisplayed: boolean;

  @ApiProperty({ example: 1 })
  order: number;

  constructor(category: Category) {
    this.id = category.id;
    this.pictogram = category.pictogram;
    this.isHomepageDisplayed = category.isHomepageDisplayed;
    this.order = category.order;
  }
}
