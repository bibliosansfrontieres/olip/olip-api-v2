import { ApiProperty } from '@nestjs/swagger';
import { Category } from '../entities/category.entity';
import { categoryMock } from 'src/tests/mocks/objects/category.mock';
import { simplifiedLanguageMock } from 'src/tests/mocks/objects/simplified-language.mock';
import { visibilityMock } from 'src/tests/mocks/objects/visibility.mock';
import { simplifiedRoleMock } from 'src/tests/mocks/objects/simplified-role.mock';
import { simplifiedUserMock } from 'src/tests/mocks/objects/simplified-user.mock';
import { categoryTranslationMock } from 'src/tests/mocks/objects/category-translation.mock';

export class GetCategoriesBackOfficeListResponse {
  @ApiProperty({
    example: [
      {
        ...categoryMock({
          users: [simplifiedUserMock()],
          categoryTranslations: [
            categoryTranslationMock({
              language: simplifiedLanguageMock(),
            }),
          ],
          visibility: visibilityMock({
            users: [simplifiedUserMock()],
            roles: [simplifiedRoleMock()],
          }),
        }),
        categoryPlaylistsCount: 0,
      },
    ],
  })
  categories: Category[];

  constructor(categories: Category[]) {
    this.categories = categories;
  }
}
