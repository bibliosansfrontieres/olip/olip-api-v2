import { ApiProperty } from '@nestjs/swagger';
import { PageMetaDto } from 'src/utils/classes/page-meta.dto';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';

export class GetCategoryContentResponse {
  @ApiProperty({
    example: [
      {
        playlist: {
          id: 1,
          image: 'images/image.svg',
          isInstalled: true,
          isUpdateNeeded: false,
          isBroken: false,
          isManuallyInstalled: false,
          version: '1693908446',
          createdAt: 1699520052339,
          updatedAt: 1699520052339,
          playlistTranslations: [
            {
              id: 1,
              title: 'My playlist',
              description: 'My playlist description',
              language: {
                id: 1,
                identifier: 'eng',
                oldIdentifier: 'en',
                label: 'English',
              },
            },
          ],
        },
        id: 1,
        categoryPlaylistItems: [
          {
            item: {
              id: 1,
              thumbnail: 'images/image.png',
              createdCountry: 'fr',
              isInstalled: true,
              isUpdateNeeded: false,
              isBroken: false,
              isManuallyInstalled: false,
              version: null,
              createdAt: 1699520052383,
              updatedAt: 1699520052383,
              dublinCoreItem: {
                id: 1,
                dublinCoreType: {
                  id: 1,
                  image: 'images/',
                  dublinCoreTypeTranslations: [
                    {
                      id: 1,
                      label: 'Sound',
                      description: 'Description',
                      language: {
                        id: 1,
                        identifier: 'eng',
                        oldIdentifier: 'en',
                        label: 'English',
                      },
                    },
                  ],
                },
                dublinCoreItemTranslations: [
                  {
                    id: 1,
                    title: 'Item title',
                    description: 'Item description',
                    creator: '',
                    publisher: '',
                    source: '',
                    extent: '',
                    subject: '',
                    dateCreated: '',
                    language: {
                      id: 1,
                      identifier: 'eng',
                      oldIdentifier: 'en',
                      label: 'English',
                    },
                  },
                ],
              },
            },
            isHighlightable: false,
            isDisplayed: true,
            id: 1,
          },
        ],
      },
    ],
  })
  data: CategoryPlaylist[];

  @ApiProperty({
    example: {
      page: 1,
      take: 12,
      totalItemsCount: 20,
      pageCount: 2,
      hasPreviousPage: false,
      hasNextPage: true,
    },
  })
  meta: PageMetaDto;

  constructor(data: CategoryPlaylist[], meta: PageMetaDto) {
    this.data = data;
    this.meta = meta;
  }
}
