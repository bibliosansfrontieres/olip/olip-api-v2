import { ApiProperty } from '@nestjs/swagger';
import { Application } from 'src/applications/entities/application.entity';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { applicationMock } from 'src/tests/mocks/objects/application.mock';
import { categoryPlaylistMock } from 'src/tests/mocks/objects/category-playlist.mock';
import { playlistTranslationMock } from 'src/tests/mocks/objects/playlist-translation.mock';
import { playlistMock } from 'src/tests/mocks/objects/playlist.mock';
import { simplifiedApplicationMock } from 'src/tests/mocks/objects/simplified-application.mock';
import { simplifiedLanguageMock } from 'src/tests/mocks/objects/simplified-language.mock';

export class GetCategoryBackOfficeCategoryPlaylistsResponse {
  @ApiProperty({ example: [applicationMock()] })
  applications: Application[];

  @ApiProperty({
    example: [
      categoryPlaylistMock({
        playlist: playlistMock({
          playlistTranslations: [
            playlistTranslationMock({ language: simplifiedLanguageMock() }),
          ],
          application: simplifiedApplicationMock(),
        }),
      }),
    ],
  })
  categoryPlaylists: CategoryPlaylist[];

  constructor(
    applications: Application[],
    categoryPlaylists: CategoryPlaylist[],
  ) {
    this.applications = applications;
    this.categoryPlaylists = categoryPlaylists;
  }
}
