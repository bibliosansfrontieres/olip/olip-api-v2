import { ApiProperty } from '@nestjs/swagger';
import { Category } from '../entities/category.entity';
import { categoryMock } from 'src/tests/mocks/objects/category.mock';
import { categoryTranslationMock } from 'src/tests/mocks/objects/category-translation.mock';
import { categoryPlaylistMock } from 'src/tests/mocks/objects/category-playlist.mock';
import { playlistMock } from 'src/tests/mocks/objects/playlist.mock';
import { applicationMock } from 'src/tests/mocks/objects/application.mock';
import { applicationTranslationMock } from 'src/tests/mocks/objects/application-translation.mock';
import { playlistTranslationMock } from 'src/tests/mocks/objects/playlist-translation.mock';
import { Application } from 'src/applications/entities/application.entity';
import { simplifiedLanguageMock } from 'src/tests/mocks/objects/simplified-language.mock';
import { applicationTypeMock } from 'src/tests/mocks/objects/application-type.mock';
import { applicationTypeTranslationMock } from 'src/tests/mocks/objects/application-type-translation.mock';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { simplifiedApplicationMock } from 'src/tests/mocks/objects/simplified-application.mock';

export class GetCategoriesCatalogResponse {
  @ApiProperty({
    example: [
      applicationMock({
        applicationTranslations: [
          applicationTranslationMock({ language: simplifiedLanguageMock() }),
        ],
        applicationType: applicationTypeMock({
          applicationTypeTranslations: [
            applicationTypeTranslationMock({
              language: simplifiedLanguageMock(),
            }),
          ],
        }),
      }),
    ],
  })
  applications: Application[];

  @ApiProperty({
    example: [
      categoryMock({
        categoryTranslations: [
          categoryTranslationMock({ language: simplifiedLanguageMock() }),
        ],
        categoryPlaylists: [
          {
            categoryPlaylistItemsCount: 1,
            ...categoryPlaylistMock({
              playlist: playlistMock({
                playlistTranslations: [
                  playlistTranslationMock({
                    language: simplifiedLanguageMock(),
                  }),
                ],
                application: simplifiedApplicationMock(),
              }),
            }),
          } as CategoryPlaylist,
        ],
      }),
    ],
  })
  categories: Category[];

  constructor({
    applications,
    categories,
  }: {
    applications: Application[];
    categories: Category[];
  }) {
    this.applications = applications;
    this.categories = categories;
  }
}
