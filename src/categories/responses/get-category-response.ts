import { ApiProperty } from '@nestjs/swagger';
import { Category } from '../entities/category.entity';
import { Item } from 'src/items/entities/item.entity';
import { PageMetaDto } from 'src/utils/classes/page-meta.dto';

export class GetCategoryResponse {
  @ApiProperty({
    example: {
      id: 1,
      pictogram: 'images/a82763d7-6eaa-416a-8e19-2b3458690b61.webp',
      image: 'images/a82763d7-6eaa-416a-8e19-2b3458690b61.webp',
      isHomepageDisplayed: true,
      order: 1,
      visibility: {
        id: 1,
        users: [],
        roles: [],
      },
      categoryPlaylists: [
        {
          playlist: {
            id: 1,
            image: 'images/f3136246-263a-4f4d-bc74-c43596d2366d.webp',
            isInstalled: true,
            isUpdateNeeded: false,
            isBroken: false,
            isManuallyInstalled: false,
            version: '1693909672',
            createdAt: 1693992321041,
            updatedAt: 1693992321041,
            playlistTranslations: [
              {
                id: 21,
                title: 'Mon paquet 1',
                description: 'OLIPV2 Package1_1',
                language: {
                  id: 67,
                  identifier: 'eng',
                  oldIdentifier: 'en',
                  label: 'English',
                },
              },
            ],
          },
          id: 21,
        },
      ],
      categoryTranslations: [
        {
          id: 1,
          title: 'Information',
          description: 'No description found in Omeka, this is a placeholder',
          language: {
            id: 1,
            identifier: 'eng',
            oldIdentifier: 'en',
            label: 'English',
          },
        },
      ],
    },
  })
  category: Category;

  @ApiProperty({
    example: [
      {
        id: 1,
        thumbnail: 'images/1b2dd09c1b3985dfcbfac9fddeff0d8e-pdf.jpg',
        createdCountry: 'fr',
        isInstalled: true,
        isUpdateNeeded: false,
        isBroken: false,
        isManuallyInstalled: false,
        version: null,
        createdAt: 1693992324070,
        updatedAt: 1693992324070,
        dublinCoreItem: {
          id: 1,
          dublinCoreItemTranslations: [
            {
              id: 1,
              title: 'Prévenir les accidents de la vie courante',
              description:
                'Guide à destination des parents pour prévenir des accidents de la vie courante touchant les enfants. Droits d utilisation cédés à BSF.',
              creator: '',
              publisher: '',
              source: '',
              extent: '',
              subject: '',
              dateCreated: '',
              language: {
                id: 1,
                identifier: 'eng',
                oldIdentifier: 'en',
                label: 'English',
              },
            },
          ],
        },
      },
    ],
  })
  data: Item[];

  @ApiProperty({
    example: {
      page: 1,
      take: 12,
      totalItemsCount: 20,
      pageCount: 2,
      hasPreviousPage: false,
      hasNextPage: true,
    },
  })
  meta: PageMetaDto;

  constructor(category: Category, data: Item[], meta: PageMetaDto) {
    this.category = category;
    this.data = data;
    this.meta = meta;
  }
}
