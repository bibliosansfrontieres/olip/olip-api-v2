import { ApiProperty } from '@nestjs/swagger';
import { Category } from '../entities/category.entity';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { CategoryTranslation } from '../entities/category-translation.entity';
import { Visibility } from 'src/visibilities/entities/visibility.entity';
import { User } from 'src/users/entities/user.entity';

export class GetCategoriesResponse {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({
    example: 'images/2023-6-21/d2aaa36f-0adc-497f-8383-f6aca0dc964b.webp',
  })
  pictogram: string;

  @ApiProperty({
    example: 50,
  })
  totalItems: number;

  @ApiProperty({
    example: 'images/2023-6-21/d2aaa36f-0adc-497f-8383-f6aca0dc964b.webp',
  })
  image: string;

  @ApiProperty({ example: true })
  isHomepageDisplayed: boolean;

  @ApiProperty({ example: 1 })
  order: number;

  @ApiProperty({ example: { id: 1, roles: [], users: [] } })
  visibility: Visibility;

  @ApiProperty({
    example: [
      {
        id: 1,
        totalItems: 50,
        playlist: {
          id: 1,
          image: 'images/2023-6-21/d2aaa36f-0adc-497f-8383-f6aca0dc964b.webp',
          isInstalled: true,
          isUpdateNeeded: false,
          isBroken: false,
          isManuallyInstalled: false,
          version: '1678980770',
          playlistTranslations: [
            {
              id: 1,
              title: 'Aider les enfants à se séparer des parents, sans drame',
              description:
                'Ensemble de ressources aidant les parents à gérer en douceur le moment de la séparation quand ils confient leur enfant à une tierce personne.',
              language: {
                id: 67,
                identifier: 'eng',
                oldIdentifier: 'en',
                label: 'English',
              },
            },
          ],
        },
      },
    ],
  })
  categoryPlaylists: CategoryPlaylist[];

  @ApiProperty({
    example: [
      {
        id: 1,
        title: 'Information',
        language: {
          id: 67,
          identifier: 'eng',
          oldIdentifier: 'en',
          label: 'English',
        },
      },
    ],
  })
  categoryTranslations: CategoryTranslation[];

  @ApiProperty({
    example: [
      { id: 1, username: 'admin', photo: 'images/image.png', language: 'eng' },
    ],
  })
  users: User[];

  constructor(category: Category) {
    this.id = category.id;
    this.pictogram = category.pictogram;
    this.isHomepageDisplayed = category.isHomepageDisplayed;
    this.order = category.order;
    this.visibility = category.visibility;
    this.categoryPlaylists = category.categoryPlaylists;
    this.categoryTranslations = category.categoryTranslations;
    this.users = category.users;
  }
}
