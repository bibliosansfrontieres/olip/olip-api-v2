import { ApiProperty } from '@nestjs/swagger';
import { Category } from '../entities/category.entity';
import { CategoryTranslation } from '../entities/category-translation.entity';
import { categoryTranslationMock } from 'src/tests/mocks/objects/category-translation.mock';
import { simplifiedLanguageMock } from 'src/tests/mocks/objects/simplified-language.mock';
import { Visibility } from 'src/visibilities/entities/visibility.entity';
import { visibilityMock } from 'src/tests/mocks/objects/visibility.mock';
import { simplifiedRoleMock } from 'src/tests/mocks/objects/simplified-role.mock';
import { simplifiedUserMock } from 'src/tests/mocks/objects/simplified-user.mock';

export class GetCategoryBackOfficeResponse {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: 'images/picto.png' })
  pictogram: string;

  @ApiProperty({ example: true })
  isHomepageDisplayed: boolean;

  @ApiProperty({ example: 1 })
  order: number;

  @ApiProperty({ example: false })
  isUpdateNeeded: boolean;

  @ApiProperty({ example: new Date(Date.now()) })
  deletedAt: Date;

  @ApiProperty({ example: Date.now() })
  updatedAt: number;

  @ApiProperty({
    example: [categoryTranslationMock({ language: simplifiedLanguageMock() })],
  })
  categoryTranslations: CategoryTranslation[];

  @ApiProperty({
    example: visibilityMock({
      roles: [simplifiedRoleMock()],
      users: [simplifiedUserMock()],
    }),
  })
  visibility: Visibility;

  constructor(category: Category) {
    this.id = category.id;
    this.pictogram = category.pictogram;
    this.isHomepageDisplayed = category.isHomepageDisplayed;
    this.order = category.order;
    this.isUpdateNeeded = category.isUpdateNeeded;
    this.deletedAt = category.deletedAt;
    this.categoryTranslations = category.categoryTranslations;
    this.visibility = category.visibility;
  }
}
