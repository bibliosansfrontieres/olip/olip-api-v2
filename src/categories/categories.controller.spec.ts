import { Test, TestingModule } from '@nestjs/testing';
import { CategoriesController } from './categories.controller';
import { CategoriesService } from './categories.service';
import { categoriesServiceMock } from 'src/tests/mocks/providers/categories-service.mock';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { GetCategoriesCatalogDto } from './dto/get-categories-catalog.dto';
import { Category } from './entities/category.entity';
import { CategoryTranslation } from './entities/category-translation.entity';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { PlaylistTranslation } from 'src/playlists/entities/playlist-translation.entity';
import { Visibility } from 'src/visibilities/entities/visibility.entity';
import { Application } from 'src/applications/entities/application.entity';
import * as CachingUtil from 'src/utils/caching.util';
import { userMock } from 'src/tests/mocks/objects/user.mock';
import { GetCategoriesListDto } from './dto/get-categories-list.dto';
import { GetCategoriesBackOfficeListDto } from './dto/get-categories-back-office-list.dto';
import { GetCategoryBackOfficeCategoryPlaylistsDto } from './dto/get-category-back-office-category-playlists.dto';
import { ApplicationTranslation } from 'src/applications/entities/application-translation.entity';
import { GetCategoryBackOfficeDto } from './dto/get-category-back-office.dto';

describe('CategoriesController', () => {
  let controller: CategoriesController;
  let categoriesService: CategoriesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CategoriesController],
      providers: [CategoriesService, JwtService, ConfigService, UsersService],
    })
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .compile();

    controller = module.get<CategoriesController>(CategoriesController);
    categoriesService = module.get<CategoriesService>(CategoriesService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getCategoriesCatalog', () => {
    const user = userMock({ id: 1 });
    const getCategoriesCatalogDto: GetCategoriesCatalogDto = {
      languageIdentifier: 'fra',
    };

    it('should be defined', () => {
      expect(controller.getCategoriesCatalog).toBeDefined();
    });

    it('should have @Get("catalog") route', () => {
      const routeMetadata = Reflect.getMetadata(
        'path',
        controller.getCategoriesCatalog,
      );
      expect(routeMetadata).toBe('catalog');
    });

    it('should have @OptionalAuth() decorator', () => {
      const optionalAuthDecorator = Reflect.getMetadata(
        'optionalAuth',
        Object.getPrototypeOf(controller),
        'getCategoriesCatalog',
      );
      expect(optionalAuthDecorator).toBe(true);
    });

    it('should use cache', async () => {
      const cachedMethodSpy = jest.spyOn(CachingUtil, 'cachedMethod');
      await controller.getCategoriesCatalog(user.id, getCategoriesCatalogDto);
      expect(cachedMethodSpy).toHaveBeenCalled();
    });

    it('should call cachedMethod with correct parameters', async () => {
      const cachedMethodSpy = jest.spyOn(CachingUtil, 'cachedMethod');

      await controller.getCategoriesCatalog(user.id, getCategoriesCatalogDto);

      expect(cachedMethodSpy).toHaveBeenCalledWith(
        'categoriesService',
        'getCategoriesCatalog',
        [{ userId: user.id }, getCategoriesCatalogDto],
        expect.any(Function),
        [
          Category,
          CategoryTranslation,
          CategoryPlaylist,
          CategoryPlaylistItem,
          Playlist,
          PlaylistTranslation,
          Visibility,
          Application,
        ],
      );
    });

    it('should call categoriesService.getCategoriesCatalog with correct arguments', async () => {
      await controller.getCategoriesCatalog(user.id, getCategoriesCatalogDto);

      expect(categoriesService.getCategoriesCatalog).toHaveBeenCalledWith(
        getCategoriesCatalogDto,
        user.id,
      );
    });
  });

  describe('getCategoriesList', () => {
    const user = userMock({ id: 1 });
    const getCategoriesListDto: GetCategoriesListDto = {
      languageIdentifier: 'fra',
    };

    it('should be defined', () => {
      expect(controller.getCategoriesList).toBeDefined();
    });

    it('should have @Get("list") route', () => {
      const routeMetadata = Reflect.getMetadata(
        'path',
        controller.getCategoriesList,
      );
      expect(routeMetadata).toBe('list');
    });

    it('should have @OptionalAuth() decorator', () => {
      const optionalAuthDecorator = Reflect.getMetadata(
        'optionalAuth',
        Object.getPrototypeOf(controller),
        'getCategoriesList',
      );
      expect(optionalAuthDecorator).toBe(true);
    });

    it('should not use cache', async () => {
      const cachedMethodSpy = jest.spyOn(CachingUtil, 'cachedMethod');
      await controller.getCategoriesList(user.id, getCategoriesListDto);
      expect(cachedMethodSpy).not.toHaveBeenCalled();
    });

    it('should call categoriesService.getCategoriesList with correct arguments', async () => {
      await controller.getCategoriesList(user.id, getCategoriesListDto);

      expect(categoriesService.getCategoriesList).toHaveBeenCalledWith(
        getCategoriesListDto,
        user.id,
      );
    });
  });

  describe('getCategoriesBackOfficeList', () => {
    const getCategoriesBackOfficeListDto: GetCategoriesBackOfficeListDto = {
      languageIdentifier: 'fra',
    };

    it('should be defined', () => {
      expect(controller.getCategoriesBackOfficeList).toBeDefined();
    });

    it('should have @Get("back-office/list") route', () => {
      const routeMetadata = Reflect.getMetadata(
        'path',
        controller.getCategoriesBackOfficeList,
      );
      expect(routeMetadata).toBe('back-office/list');
    });

    it('should have @Auth() decorator', () => {
      const optionalAuthDecorator = Reflect.getMetadata(
        'auth',
        Object.getPrototypeOf(controller),
        'getCategoriesBackOfficeList',
      );
      expect(optionalAuthDecorator).toBe(true);
    });

    it('should use cache', async () => {
      const cachedMethodSpy = jest.spyOn(CachingUtil, 'cachedMethod');
      await controller.getCategoriesBackOfficeList(
        getCategoriesBackOfficeListDto,
      );
      expect(cachedMethodSpy).toHaveBeenCalled();
    });

    it('should call cachedMethod with correct parameters', async () => {
      const cachedMethodSpy = jest.spyOn(CachingUtil, 'cachedMethod');

      await controller.getCategoriesBackOfficeList(
        getCategoriesBackOfficeListDto,
      );

      expect(cachedMethodSpy).toHaveBeenCalledWith(
        'categoriesService',
        'getCategoriesBackOfficeList',
        [getCategoriesBackOfficeListDto],
        expect.any(Function),
        [
          Category,
          CategoryTranslation,
          CategoryPlaylist,
          CategoryPlaylistItem,
          Playlist,
          PlaylistTranslation,
          Visibility,
        ],
      );
    });

    it('should call categoriesService.getCategoriesBackOfficeList with correct arguments', async () => {
      await controller.getCategoriesBackOfficeList(
        getCategoriesBackOfficeListDto,
      );

      expect(
        categoriesService.getCategoriesBackOfficeList,
      ).toHaveBeenCalledWith(getCategoriesBackOfficeListDto);
    });
  });

  describe('getCategoryBackOfficeCategoryPlaylists', () => {
    const categoryId = '1';
    const getCategoryBackOfficeCategoryPlaylistsDto: GetCategoryBackOfficeCategoryPlaylistsDto =
      {
        languageIdentifier: 'fra',
      };

    it('should be defined', () => {
      expect(controller.getCategoryBackOfficeCategoryPlaylists).toBeDefined();
    });

    it('should have @Get("back-office/:id/category-playlists") route', () => {
      const routeMetadata = Reflect.getMetadata(
        'path',
        controller.getCategoryBackOfficeCategoryPlaylists,
      );
      expect(routeMetadata).toBe('back-office/:id/category-playlists');
    });

    it('should have @Auth() decorator', () => {
      const optionalAuthDecorator = Reflect.getMetadata(
        'auth',
        Object.getPrototypeOf(controller),
        'getCategoryBackOfficeCategoryPlaylists',
      );
      expect(optionalAuthDecorator).toBe(true);
    });

    it('should use cache', async () => {
      const cachedMethodSpy = jest.spyOn(CachingUtil, 'cachedMethod');
      await controller.getCategoryBackOfficeCategoryPlaylists(
        categoryId,
        getCategoryBackOfficeCategoryPlaylistsDto,
      );
      expect(cachedMethodSpy).toHaveBeenCalled();
    });

    it('should call cachedMethod with correct parameters', async () => {
      const cachedMethodSpy = jest.spyOn(CachingUtil, 'cachedMethod');

      await controller.getCategoryBackOfficeCategoryPlaylists(
        categoryId,
        getCategoryBackOfficeCategoryPlaylistsDto,
      );

      expect(cachedMethodSpy).toHaveBeenCalledWith(
        'categoriesService',
        'getCategoryBackOfficeCategoryPlaylists',
        [categoryId, getCategoryBackOfficeCategoryPlaylistsDto],
        expect.any(Function),
        [
          Category,
          CategoryTranslation,
          Visibility,
          CategoryPlaylist,
          Playlist,
          PlaylistTranslation,
          Application,
          ApplicationTranslation,
        ],
      );
    });

    it('should call categoriesService.getCategoryBackOfficeCategoryPlaylists with correct arguments', async () => {
      jest.spyOn(categoriesService, 'getCategoryBackOfficeCategoryPlaylists');
      await controller.getCategoryBackOfficeCategoryPlaylists(
        categoryId,
        getCategoryBackOfficeCategoryPlaylistsDto,
      );

      expect(
        categoriesService.getCategoryBackOfficeCategoryPlaylists,
      ).toHaveBeenCalledWith(
        +categoryId,
        getCategoryBackOfficeCategoryPlaylistsDto,
      );
    });
  });

  describe('getCategoryBackOffice', () => {
    const categoryId = '1';
    const getCategoryBackOfficeDto: GetCategoryBackOfficeDto = {
      languageIdentifier: 'fra',
    };

    it('should be defined', () => {
      expect(controller.getCategoryBackOffice).toBeDefined();
    });

    it('should have @Get("back-office/:id") route', () => {
      const routeMetadata = Reflect.getMetadata(
        'path',
        controller.getCategoryBackOffice,
      );
      expect(routeMetadata).toBe('back-office/:id');
    });

    it('should have @Auth() decorator', () => {
      const optionalAuthDecorator = Reflect.getMetadata(
        'auth',
        Object.getPrototypeOf(controller),
        'getCategoryBackOffice',
      );
      expect(optionalAuthDecorator).toBe(true);
    });

    it('should use cache', async () => {
      const cachedMethodSpy = jest.spyOn(CachingUtil, 'cachedMethod');
      await controller.getCategoryBackOffice(
        categoryId,
        getCategoryBackOfficeDto,
      );
      expect(cachedMethodSpy).toHaveBeenCalled();
    });

    it('should call cachedMethod with correct parameters', async () => {
      const cachedMethodSpy = jest.spyOn(CachingUtil, 'cachedMethod');

      await controller.getCategoryBackOffice(
        categoryId,
        getCategoryBackOfficeDto,
      );

      expect(cachedMethodSpy).toHaveBeenCalledWith(
        'categoriesService',
        'getCategoryBackOffice',
        [categoryId, getCategoryBackOfficeDto],
        expect.any(Function),
        [Category, CategoryTranslation, Visibility],
      );
    });

    it('should call categoriesService.getCategoryBackOfficeCategoryPlaylists with correct arguments', async () => {
      jest.spyOn(categoriesService, 'getCategoryBackOffice');
      await controller.getCategoryBackOffice(
        categoryId,
        getCategoryBackOfficeDto,
      );

      expect(categoriesService.getCategoryBackOffice).toHaveBeenCalledWith(
        +categoryId,
        getCategoryBackOfficeDto,
      );
    });
  });
});
