import { Test, TestingModule } from '@nestjs/testing';
import { CategoriesService } from './categories.service';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { Category } from './entities/category.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { CategoryTranslation } from './entities/category-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';
import { DublinCoreItemsService } from 'src/dublin-core-items/dublin-core-items.service';
import { dublinCoreItemsServiceMock } from 'src/tests/mocks/providers/dublin-core-items-service.mock';
import { CategoryPlaylistItemsService } from 'src/category-playlist-items/category-playlist-items.service';
import { categoryPlaylistItemsServiceMock } from 'src/tests/mocks/providers/category-playlist-items-service.mock';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { playlistsServiceMock } from 'src/tests/mocks/providers/playlists-service.mock';
import { VisibilitiesService } from 'src/visibilities/visibilities.service';
import { visibilitiesServiceMock } from 'src/tests/mocks/providers/visibilities-service.mock';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { dublinCoreTypesServiceMock } from 'src/tests/mocks/providers/dublin-core-types-service.mock';
import { ItemsService } from 'src/items/items.service';
import { itemsServiceMock } from 'src/tests/mocks/providers/items-service.mock';
import { UploadService } from 'src/upload/upload.service';
import { uploadServiceMock } from 'src/tests/mocks/providers/upload-service.mock';
import { CategoryPlaylistsService } from 'src/category-playlists/category-playlists.service';
import { categoryPlaylistsServiceMock } from 'src/tests/mocks/providers/category-playlists-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';
import { SearchService } from 'src/search/search.service';
import { searchServiceMock } from 'src/tests/mocks/providers/search-service.mock';
import { categoryMock } from 'src/tests/mocks/objects/category.mock';
import { userMock } from 'src/tests/mocks/objects/user.mock';
import { roleMock } from 'src/tests/mocks/objects/role.mock';
import { visibilityMock } from 'src/tests/mocks/objects/visibility.mock';
import { GetCategoriesListResponse } from './responses/get-categories-list-response';
import { GetCategoriesListDto } from './dto/get-categories-list.dto';
import { categoryTranslationMock } from 'src/tests/mocks/objects/category-translation.mock';
import { languageMock } from 'src/tests/mocks/objects/language.mock';
import { GetCategoriesCatalogDto } from './dto/get-categories-catalog.dto';
import { categoryPlaylistMock } from 'src/tests/mocks/objects/category-playlist.mock';
import { playlistMock } from 'src/tests/mocks/objects/playlist.mock';
import { playlistTranslationMock } from 'src/tests/mocks/objects/playlist-translation.mock';
import { applicationMock } from 'src/tests/mocks/objects/application.mock';
import { applicationTranslationMock } from 'src/tests/mocks/objects/application-translation.mock';
import { GetCategoriesCatalogResponse } from './responses/get-categories-catalog-response';
import { GetCategoriesBackOfficeListDto } from './dto/get-categories-back-office-list.dto';
import { GetCategoriesBackOfficeListResponse } from './responses/get-categories-back-office-list-response';
import { GetCategoryBackOfficeDto } from './dto/get-category-back-office.dto';
import { simplifiedUserMock } from 'src/tests/mocks/objects/simplified-user.mock';
import { simplifiedRoleMock } from 'src/tests/mocks/objects/simplified-role.mock';
import { simplifiedLanguageMock } from 'src/tests/mocks/objects/simplified-language.mock';
import { GetCategoryBackOfficeResponse } from './responses/get-category-back-office-response';
import * as SortCategoriesTranslationsUtil from 'src/utils/language/sort-translations/sort-categories-translations.util';
import * as SortCategoriesPlaylistsTranslationsUtil from 'src/utils/language/sort-translations/sort-categories-playlists-translations.util';
import { GetCategoryOptionsDto } from './dto/get-category-options.dto';
import { TrueFalseEnum } from './enums/true-false.enum';
import { OrderEnum } from 'src/utils/classes/order.enum';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';

describe('CategoriesService', () => {
  let service: CategoriesService;
  let usersService: UsersService;
  let applicationsService: ApplicationsService;
  let categoriesRepository: Repository<Category>;
  let categoryTranslationsRepository: Repository<CategoryTranslation>;
  const CATEGORIES_REPOSITORY_TOKEN = getRepositoryToken(Category);
  const CATEGORY_TRANSLATIONS_REPOSITORY_TOKEN =
    getRepositoryToken(CategoryTranslation);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: CATEGORIES_REPOSITORY_TOKEN,
          useClass: Repository<Category>,
        },
        {
          provide: CATEGORY_TRANSLATIONS_REPOSITORY_TOKEN,
          useClass: Repository<CategoryTranslation>,
        },
        CategoriesService,
        LanguagesService,
        DublinCoreItemsService,
        CategoryPlaylistItemsService,
        PlaylistsService,
        VisibilitiesService,
        UsersService,
        ItemsService,
        DublinCoreTypesService,
        UploadService,
        CategoryPlaylistsService,
        ApplicationsService,
        SearchService,
      ],
    })
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .overrideProvider(DublinCoreTypesService)
      .useValue(dublinCoreTypesServiceMock)
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .overrideProvider(DublinCoreItemsService)
      .useValue(dublinCoreItemsServiceMock)
      .overrideProvider(CategoryPlaylistItemsService)
      .useValue(categoryPlaylistItemsServiceMock)
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .overrideProvider(VisibilitiesService)
      .useValue(visibilitiesServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(UploadService)
      .useValue(uploadServiceMock)
      .overrideProvider(CategoryPlaylistsService)
      .useValue(categoryPlaylistsServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(SearchService)
      .useValue(searchServiceMock)
      .compile();

    service = module.get<CategoriesService>(CategoriesService);
    categoriesRepository = module.get<Repository<Category>>(
      CATEGORIES_REPOSITORY_TOKEN,
    );
    categoryTranslationsRepository = module.get<
      Repository<CategoryTranslation>
    >(CATEGORY_TRANSLATIONS_REPOSITORY_TOKEN);
    usersService = module.get<UsersService>(UsersService);
    applicationsService = module.get<ApplicationsService>(ApplicationsService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(categoriesRepository).toBeDefined();
    expect(categoryTranslationsRepository).toBeDefined();
    expect(usersService).toBeDefined();
    expect(applicationsService).toBeDefined();
  });

  const userId = 1;
  const roleId = 1;

  const categoriesMock = [
    categoryMock({
      visibility: visibilityMock({ users: [], roles: [] }),
      categoryPlaylists: [
        categoryPlaylistMock({
          playlist: playlistMock({
            playlistTranslations: [playlistTranslationMock()],
          }),
        }),
      ],
      categoryTranslations: [
        categoryTranslationMock({
          language: languageMock(),
        }),
      ],
    }),
    categoryMock({
      visibility: visibilityMock({
        users: [userMock({ id: userId })],
        roles: [],
      }),
      categoryPlaylists: [
        categoryPlaylistMock({
          playlist: playlistMock({
            playlistTranslations: [playlistTranslationMock()],
          }),
        }),
      ],
      categoryTranslations: [
        categoryTranslationMock({
          language: languageMock(),
        }),
      ],
    }),
    categoryMock({
      visibility: visibilityMock({
        roles: [roleMock({ id: roleId })],
        users: [],
      }),
      categoryPlaylists: [
        categoryPlaylistMock({
          playlist: playlistMock({
            playlistTranslations: [playlistTranslationMock()],
          }),
        }),
        categoryPlaylistMock({
          playlist: playlistMock({
            playlistTranslations: [playlistTranslationMock()],
          }),
        }),
      ],
      categoryTranslations: [
        categoryTranslationMock({
          language: languageMock(),
        }),
      ],
    }),
  ];

  describe('getCategoriesCatalog', () => {
    const getCategoriesCatalogDto: GetCategoriesCatalogDto = {
      languageIdentifier: 'fra',
    };
    const user = userMock({ id: userId, role: roleMock() });
    const mockQueryBuilder = {
      leftJoinAndSelect: jest.fn().mockReturnThis(),
      leftJoin: jest.fn().mockReturnThis(),
      where: jest.fn().mockReturnThis(),
      andWhere: jest.fn().mockReturnThis(),
      addSelect: jest.fn().mockReturnThis(),
      loadRelationCountAndMap: jest.fn().mockReturnThis(),
      orWhere: jest.fn().mockReturnThis(),
      orderBy: jest.fn().mockReturnThis(),
      getMany: jest.fn().mockResolvedValue([
        categoryMock({
          visibility: visibilityMock({ users: [], roles: [] }),
          categoryPlaylists: [
            categoryPlaylistMock({
              playlist: playlistMock({
                application: applicationMock({
                  applicationTranslations: [
                    applicationTranslationMock({ language: languageMock() }),
                  ],
                }),
                playlistTranslations: [playlistTranslationMock()],
              }),
              categoryPlaylistItems: [],
            }),
          ],
          categoryTranslations: [
            categoryTranslationMock({
              language: languageMock({ identifier: 'fra' }),
            }),
          ],
        }),
      ]),
    } as unknown as SelectQueryBuilder<Category>;

    it('should be defined', () => {
      expect(service.getCategoriesCatalog).toBeDefined();
    });

    it('should work without user logged in', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValue(mockQueryBuilder);
      jest.spyOn(applicationsService, 'find').mockResolvedValue([]);
      const result = await service.getCategoriesCatalog(
        getCategoriesCatalogDto,
      );
      expect(result).toBeInstanceOf(GetCategoriesCatalogResponse);
    });

    it('should work with user logged in', async () => {
      jest.spyOn(usersService, 'findOne').mockResolvedValue(user);
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValue(mockQueryBuilder);
      const result = await service.getCategoriesCatalog(
        getCategoriesCatalogDto,
        userId,
      );
      expect(result).toBeInstanceOf(GetCategoriesCatalogResponse);
    });

    it('should left join every needed entities', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      jest.spyOn(applicationsService, 'find').mockResolvedValue([]);

      await service.getCategoriesCatalog(getCategoriesCatalogDto);

      expect(categoriesRepository.createQueryBuilder).toHaveBeenCalledWith(
        'category',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'category.visibility',
        'visibility',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'visibility.users',
        'users',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'visibility.roles',
        'roles',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'category.categoryTranslations',
        'categoryTranslations',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'category.categoryTranslations',
        'categoryTranslations',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'categoryTranslations.language',
        'language',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'playlist.application',
        'application',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'application.visibility',
        'applicationVisibility',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'applicationVisibility.users',
        'applicationUsers',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'applicationVisibility.roles',
        'applicationRoles',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'playlist.playlistTranslations',
        'playlistTranslations',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'categoryPlaylists.categoryPlaylistItems',
        'categoryPlaylistItems',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'category.categoryPlaylists',
        'categoryPlaylists',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'categoryPlaylists.playlist',
        'playlist',
      );
      expect(mockQueryBuilder.getMany).toHaveBeenCalled();
    });

    it('should add select every needed fields', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      jest.spyOn(applicationsService, 'find').mockResolvedValue([]);

      await service.getCategoriesCatalog(getCategoriesCatalogDto);

      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'category.id',
        'category.pictogram',
        'category.isHomepageDisplayed',
        'category.order',
        'category.isUpdateNeeded',
        'category.deletedAt',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'categoryTranslations.id',
        'categoryTranslations.title',
        'categoryTranslations.description',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'language.identifier',
        'language.label',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'categoryPlaylists.id',
        'categoryPlaylists.order',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'playlist.id',
        'playlist.image',
        'playlist.isInstalled',
        'playlist.isUpdateNeeded',
        'playlist.isBroken',
        'playlist.isManuallyInstalled',
        'playlist.version',
        'playlist.size',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'playlistTranslations.id',
        'playlistTranslations.title',
        'playlistTranslations.description',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'playlistTranslationsLanguage.identifier',
        'playlistTranslationsLanguage.label',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith('application.id');

      expect(mockQueryBuilder.getMany).toHaveBeenCalled();
    });

    it('should load relation count and map', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      jest.spyOn(applicationsService, 'find').mockResolvedValue([]);

      await service.getCategoriesCatalog(getCategoriesCatalogDto);

      expect(mockQueryBuilder.loadRelationCountAndMap).toHaveBeenCalledWith(
        'categoryPlaylists.categoryPlaylistItemsCount',
        'categoryPlaylists.categoryPlaylistItems',
      );

      expect(mockQueryBuilder.getMany).toHaveBeenCalled();
    });

    it('should load use conditions needed when user not logged in', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      jest.spyOn(applicationsService, 'find').mockResolvedValue([]);

      await service.getCategoriesCatalog(getCategoriesCatalogDto);

      expect(mockQueryBuilder.where).toHaveBeenCalledWith(
        'roles.id IS NULL AND users.id IS NULL',
      );

      expect(mockQueryBuilder.andWhere).toHaveBeenCalledWith(
        'applicationUsers.id IS NULL AND applicationRoles.id IS NULL',
      );

      expect(mockQueryBuilder.getMany).toHaveBeenCalled();
    });

    it('should load use conditions needed when user logged in', async () => {
      jest.spyOn(usersService, 'findOne').mockResolvedValue(user);
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      jest.spyOn(applicationsService, 'find').mockResolvedValue([]);

      await service.getCategoriesCatalog(getCategoriesCatalogDto, user.id);

      expect(mockQueryBuilder.where).toHaveBeenCalledWith(
        'roles.id IS NULL AND users.id IS NULL',
      );

      expect(mockQueryBuilder.orWhere).toHaveBeenCalledWith(
        'roles.id = :roleId',
        { roleId: user.role.id },
      );

      expect(mockQueryBuilder.orWhere).toHaveBeenCalledWith(
        'users.id = :userId',
        { userId: user.id },
      );

      expect(mockQueryBuilder.orWhere).toHaveBeenCalledWith(
        'applicationUsers.id = :userId',
        { userId: user.id },
      );

      expect(mockQueryBuilder.orWhere).toHaveBeenCalledWith(
        'applicationRoles.id = :roleId',
        { roleId: user.role.id },
      );

      expect(mockQueryBuilder.getMany).toHaveBeenCalled();
    });

    it('should call sortCategoriesTranslations', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      const filterSpy = jest
        .spyOn(SortCategoriesTranslationsUtil, 'sortCategoriesTranslations')
        .mockReturnValue();
      await service.getCategoriesCatalog(getCategoriesCatalogDto);
      expect(filterSpy).toHaveBeenCalled();
    });

    it('should call sortPlaylistsTranslations', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      const filterSpy = jest
        .spyOn(
          SortCategoriesPlaylistsTranslationsUtil,
          'sortCategoriesPlaylistsTranslations',
        )
        .mockReturnValue();
      await service.getCategoriesCatalog(getCategoriesCatalogDto);
      expect(filterSpy).toHaveBeenCalled();
    });

    it('should return GetCategoriesCatalogResponse', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      const result = await service.getCategoriesCatalog(
        getCategoriesCatalogDto,
      );
      expect(result).toBeInstanceOf(GetCategoriesCatalogResponse);
    });
  });

  describe('getCategoriesList', () => {
    const getCategoriesListDto: GetCategoriesListDto = {
      languageIdentifier: 'fra',
    };
    const mockQueryBuilder = {
      leftJoinAndSelect: jest.fn().mockReturnThis(),
      leftJoin: jest.fn().mockReturnThis(),
      where: jest.fn().mockReturnThis(),
      addSelect: jest.fn().mockReturnThis(),
      orWhere: jest.fn().mockReturnThis(),
      orderBy: jest.fn().mockReturnThis(),
      getMany: jest.fn().mockResolvedValue([
        categoryMock({
          visibility: visibilityMock({ users: [], roles: [] }),
          categoryTranslations: [
            categoryTranslationMock({
              language: languageMock({ identifier: 'fra' }),
            }),
          ],
        }),
        categoryMock({
          visibility: visibilityMock({
            users: [userMock({ id: userId })],
            roles: [],
          }),
          categoryTranslations: [
            categoryTranslationMock({
              language: languageMock({ identifier: 'fra' }),
            }),
          ],
        }),
        categoryMock({
          visibility: visibilityMock({
            users: [],
            roles: [roleMock({ id: roleId })],
          }),
          categoryTranslations: [
            categoryTranslationMock({
              language: languageMock({ identifier: 'fra' }),
            }),
          ],
        }),
      ]),
    } as unknown as SelectQueryBuilder<Category>;

    it('should be defined', () => {
      expect(service.getCategoriesList).toBeDefined();
    });

    it('should work without user logged in', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValue(mockQueryBuilder);
      const result = await service.getCategoriesList(getCategoriesListDto);
      expect(result).toBeInstanceOf(GetCategoriesListResponse);
    });

    it('should work with user logged in', async () => {
      jest
        .spyOn(usersService, 'findOne')
        .mockResolvedValue(userMock({ role: roleMock() }));
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValue(mockQueryBuilder);
      const result = await service.getCategoriesList(
        getCategoriesListDto,
        userId,
      );
      expect(result).toBeInstanceOf(GetCategoriesListResponse);
    });

    it("should call needed entities's relations in query", async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);

      await service.getCategoriesList(getCategoriesListDto);

      expect(categoriesRepository.createQueryBuilder).toHaveBeenCalledWith(
        'category',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'category.visibility',
        'visibility',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'visibility.users',
        'users',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'visibility.roles',
        'roles',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'category.categoryTranslations',
        'categoryTranslations',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'categoryTranslations.language',
        'language',
      );
      expect(mockQueryBuilder.getMany).toHaveBeenCalled();
    });

    it('should use right addSelects in query', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);

      await service.getCategoriesList(getCategoriesListDto);

      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'category.id',
        'category.pictogram',
        'category.isHomepageDisplayed',
        'category.order',
        'category.isUpdateNeeded',
        'category.deletedAt',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'categoryTranslations.id',
        'categoryTranslations.title',
        'categoryTranslations.description',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'language.identifier',
        'language.label',
      ]);

      expect(mockQueryBuilder.getMany).toHaveBeenCalled();
    });

    it('should use needed conditions in query when not logged in', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);

      await service.getCategoriesList(getCategoriesListDto);

      expect(mockQueryBuilder.where).toHaveBeenCalledWith(
        'roles.id IS NULL AND users.id IS NULL',
      );
      expect(mockQueryBuilder.orWhere).not.toHaveBeenCalled();
    });

    it('should use needed conditions in query when logged in', async () => {
      const user = userMock({ id: userId, role: roleMock() });
      jest.spyOn(usersService, 'findOne').mockResolvedValue(user);
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);

      await service.getCategoriesList(getCategoriesListDto, user.id);

      expect(mockQueryBuilder.where).toHaveBeenCalledWith(
        'roles.id IS NULL AND users.id IS NULL',
      );
      expect(mockQueryBuilder.orWhere).toHaveBeenCalledWith(
        'roles.id = :roleId',
        { roleId: user.role.id },
      );
      expect(mockQueryBuilder.orWhere).toHaveBeenCalledWith(
        'users.id = :userId',
        { userId: user.id },
      );
    });

    it('should call sortCategoriesTranslations', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      const filterSpy = jest
        .spyOn(SortCategoriesTranslationsUtil, 'sortCategoriesTranslations')
        .mockReturnValue();
      await service.getCategoriesList(getCategoriesListDto);
      expect(filterSpy).toHaveBeenCalled();
    });

    it('should return GetCategoriesListResponse', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      const result = await service.getCategoriesList(getCategoriesListDto);
      expect(result).toBeInstanceOf(GetCategoriesListResponse);
    });
  });

  describe('getCategoriesBackOfficeList', () => {
    const getCategoriesBackOfficeListDto: GetCategoriesBackOfficeListDto = {
      languageIdentifier: 'fra',
    };
    const mockQueryBuilder = {
      leftJoinAndSelect: jest.fn().mockReturnThis(),
      leftJoin: jest.fn().mockReturnThis(),
      where: jest.fn().mockReturnThis(),
      addSelect: jest.fn().mockReturnThis(),
      orWhere: jest.fn().mockReturnThis(),
      orderBy: jest.fn().mockReturnThis(),
      loadRelationCountAndMap: jest.fn().mockReturnThis(),
      getMany: jest.fn().mockResolvedValue([
        categoryMock({
          visibility: visibilityMock({ users: [], roles: [] }),
          categoryTranslations: [
            categoryTranslationMock({
              language: languageMock({ identifier: 'fra' }),
            }),
          ],
        }),
        categoryMock({
          visibility: visibilityMock({
            users: [userMock({ id: userId })],
            roles: [],
          }),
          categoryTranslations: [
            categoryTranslationMock({
              language: languageMock({ identifier: 'fra' }),
            }),
          ],
        }),
        categoryMock({
          visibility: visibilityMock({
            users: [],
            roles: [roleMock({ id: roleId })],
          }),
          categoryTranslations: [
            categoryTranslationMock({
              language: languageMock({ identifier: 'fra' }),
            }),
          ],
        }),
      ]),
    } as unknown as SelectQueryBuilder<Category>;

    it('should be defined', () => {
      expect(service.getCategoriesBackOfficeList).toBeDefined();
    });

    it("should call needed entities's relations in query", async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);

      await service.getCategoriesBackOfficeList(getCategoriesBackOfficeListDto);

      expect(categoriesRepository.createQueryBuilder).toHaveBeenCalledWith(
        'category',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'category.users',
        'users',
      );
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'category.visibility',
        'visibility',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'visibility.users',
        'visibilityUsers',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'visibility.roles',
        'visibilityRoles',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'category.categoryTranslations',
        'categoryTranslations',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'categoryTranslations.language',
        'language',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'category.categoryPlaylists',
        'categoryPlaylists',
      );
      expect(mockQueryBuilder.getMany).toHaveBeenCalled();
    });

    it('should use right addSelects in query', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);

      await service.getCategoriesBackOfficeList(getCategoriesBackOfficeListDto);

      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'category.id',
        'category.pictogram',
        'category.isHomepageDisplayed',
        'category.order',
        'category.isUpdateNeeded',
        'category.deletedAt',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'users.id',
        'users.username',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'visibilityUsers.id',
        'visibilityUsers.username',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'visibilityRoles.id',
        'visibilityRoles.name',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'categoryTranslations.id',
        'categoryTranslations.title',
        'categoryTranslations.description',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'language.identifier',
        'language.label',
      ]);

      expect(mockQueryBuilder.getMany).toHaveBeenCalled();
    });

    it('should call sortCategoriesTranslations', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      const filterSpy = jest
        .spyOn(SortCategoriesTranslationsUtil, 'sortCategoriesTranslations')
        .mockReturnValue();
      await service.getCategoriesBackOfficeList(getCategoriesBackOfficeListDto);
      expect(filterSpy).toHaveBeenCalled();
    });

    it('should return GetCategoriesBackOfficeListResponse', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      const result = await service.getCategoriesBackOfficeList(
        getCategoriesBackOfficeListDto,
      );
      expect(result).toBeInstanceOf(GetCategoriesBackOfficeListResponse);
    });
  });

  describe('getCategoryBackOffice', () => {
    const categoryId = 1;
    const getCategoryBackOfficeDto: GetCategoryBackOfficeDto = {
      languageIdentifier: 'fra',
    };
    const mockQueryBuilder = {
      leftJoinAndSelect: jest.fn().mockReturnThis(),
      leftJoin: jest.fn().mockReturnThis(),
      where: jest.fn().mockReturnThis(),
      addSelect: jest.fn().mockReturnThis(),
      orWhere: jest.fn().mockReturnThis(),
      orderBy: jest.fn().mockReturnThis(),
      loadRelationCountAndMap: jest.fn().mockReturnThis(),
      getOne: jest.fn().mockResolvedValue(
        categoryMock({
          visibility: visibilityMock({
            users: [simplifiedUserMock()],
            roles: [simplifiedRoleMock()],
          }),
          categoryTranslations: [
            categoryTranslationMock({
              language: simplifiedLanguageMock({
                identifier: getCategoryBackOfficeDto.languageIdentifier,
              }),
            }),
          ],
        }),
      ),
    } as unknown as SelectQueryBuilder<Category>;

    it('should be defined', () => {
      expect(service.getCategoryBackOffice).toBeDefined();
    });

    it("should call needed entities's relations in query", async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);

      await service.getCategoryBackOffice(categoryId, getCategoryBackOfficeDto);

      expect(categoriesRepository.createQueryBuilder).toHaveBeenCalledWith(
        'category',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'category.visibility',
        'visibility',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'visibility.users',
        'users',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'visibility.roles',
        'roles',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'category.categoryTranslations',
        'categoryTranslations',
      );
      expect(mockQueryBuilder.leftJoin).toHaveBeenCalledWith(
        'categoryTranslations.language',
        'language',
      );
      expect(mockQueryBuilder.getOne).toHaveBeenCalled();
    });

    it('should use right addSelects in query', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);

      await service.getCategoryBackOffice(categoryId, getCategoryBackOfficeDto);

      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'category.id',
        'category.pictogram',
        'category.isHomepageDisplayed',
        'category.order',
        'category.isUpdateNeeded',
        'category.deletedAt',
        'category.updatedAt',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'categoryTranslations.id',
        'categoryTranslations.title',
        'categoryTranslations.description',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'language.identifier',
        'language.label',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'roles.id',
        'roles.name',
      ]);
      expect(mockQueryBuilder.addSelect).toHaveBeenCalledWith([
        'users.id',
        'users.username',
      ]);
    });

    it('should call sortCategoriesTranslations', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      const filterSpy = jest
        .spyOn(SortCategoriesTranslationsUtil, 'sortCategoriesTranslations')
        .mockReturnValue();
      await service.getCategoryBackOffice(categoryId, getCategoryBackOfficeDto);
      expect(filterSpy).toHaveBeenCalled();
    });

    it('should return GetCategoryBackOfficeResponse', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValueOnce(mockQueryBuilder);
      const result = await service.getCategoryBackOffice(
        categoryId,
        getCategoryBackOfficeDto,
      );
      expect(result).toBeInstanceOf(GetCategoryBackOfficeResponse);
    });
  });

  describe('filterCategoriesByVisibility', () => {
    it('should be defined', () => {
      expect(service.filterCategoriesByVisibility).toBeDefined();
    });

    it('should return a list of categories', async () => {
      const categories = await service.filterCategoriesByVisibility(
        undefined,
        categoriesMock,
      );

      expect(categories.length).toBeGreaterThanOrEqual(1);
    });

    it('should return public categories when not logged in', async () => {
      const categories = await service.filterCategoriesByVisibility(
        undefined,
        categoriesMock,
      );

      expect(categories.length).toBe(1);
    });

    it('should return public categories when logged in', async () => {
      jest
        .spyOn(usersService, 'findOne')
        .mockResolvedValue(userMock({ role: roleMock() }));
      const categories = await service.filterCategoriesByVisibility(
        userId,
        categoriesMock,
      );

      expect(categories.length).toBe(1);
    });

    it('should return categories visible to the user when logged in', async () => {
      jest
        .spyOn(usersService, 'findOne')
        .mockResolvedValue(userMock({ id: userId, role: roleMock() }));
      const categories = await service.filterCategoriesByVisibility(
        userId,
        categoriesMock,
      );

      expect(categories.length).toBe(2);

      expect(
        categories.filter((category) => category.visibility.users.length > 0)
          .length,
      ).toBe(1);
    });

    it('should return categories visible to the role when logged in', async () => {
      jest
        .spyOn(usersService, 'findOne')
        .mockResolvedValue(userMock({ role: roleMock({ id: roleId }) }));
      const categories = await service.filterCategoriesByVisibility(
        userId,
        categoriesMock,
      );

      expect(categories.length).toBe(2);

      expect(
        categories.filter((category) => category.visibility.roles.length > 0)
          .length,
      ).toBe(1);
    });
    it('should return categories visible to the user AND role when logged in', async () => {
      jest
        .spyOn(usersService, 'findOne')
        .mockResolvedValue(
          userMock({ id: userId, role: roleMock({ id: roleId }) }),
        );
      const categories = await service.filterCategoriesByVisibility(
        userId,
        categoriesMock,
      );

      expect(categories.length).toBe(3);

      expect(
        categories.filter((category) => category.visibility.roles.length > 0)
          .length,
      ).toBe(1);

      expect(
        categories.filter((category) => category.visibility.users.length > 0)
          .length,
      ).toBe(1);
    });
  });

  describe('getCategory', () => {
    const user = userMock({ id: userId, role: roleMock() });
    const categoryId = Date.now();
    const getCategoryOptionsDto: GetCategoryOptionsDto = {
      languageIdentifier: 'fra',
      getAllCategoryTranslations: TrueFalseEnum.TRUE,
      playlistId: '1,2',
      dublinCoreTypeId: '3,4',
      titleLike: 'example',
      isVisibilityEnabled: TrueFalseEnum.TRUE,
    };
    const pageOptionsDto: PageOptionsDto = {
      order: OrderEnum.ASC,
      page: 1,
      take: 5,
    };
    const withItems = false;
    const mockCategory = categoryMock({
      users: [user],
      visibility: visibilityMock({ users: [], roles: [] }),
      categoryPlaylists: [
        categoryPlaylistMock({
          playlist: playlistMock({
            application: applicationMock({
              visibility: visibilityMock({ users: [], roles: [] }),
            }),
          }),
        }),
      ],
    });
    const mockQueryBuilder = {
      leftJoinAndSelect: jest.fn().mockReturnThis(),
      leftJoin: jest.fn().mockReturnThis(),
      where: jest.fn().mockReturnThis(),
      andWhere: jest.fn().mockReturnThis(),
      addSelect: jest.fn().mockReturnThis(),
      loadRelationCountAndMap: jest.fn().mockReturnThis(),
      orWhere: jest.fn().mockReturnThis(),
      orderBy: jest.fn().mockReturnThis(),
      findOne: jest.fn().mockResolvedValue(mockCategory),
    } as unknown as SelectQueryBuilder<Category>;

    it('should be defined', () => {
      expect(service.getCategory).toBeDefined();
    });

    it('should work without user logged in', async () => {
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValue(mockQueryBuilder);
      jest
        .spyOn(categoriesRepository, 'findOne')
        .mockResolvedValue(mockCategory);
      jest
        .spyOn(service, 'getCategoriesTranslations')
        .mockResolvedValue([mockCategory]);
      jest
        .spyOn(service, 'getCategoriesPlaylistsTranslations')
        .mockResolvedValue([mockCategory]);
      const result = await service.getCategory(
        categoryId,
        getCategoryOptionsDto,
        userId,
        pageOptionsDto,
        withItems,
      );
      expect(result).toEqual({ category: mockCategory });
    });

    it('should work with user logged in', async () => {
      jest
        .spyOn(usersService, 'findOne')
        .mockResolvedValue(userMock({ role: roleMock() }));
      jest
        .spyOn(categoriesRepository, 'createQueryBuilder')
        .mockReturnValue(mockQueryBuilder);
      jest
        .spyOn(categoriesRepository, 'findOne')
        .mockResolvedValue(mockCategory);
      jest
        .spyOn(service, 'getCategoriesTranslations')
        .mockResolvedValue([mockCategory]);
      jest
        .spyOn(service, 'getCategoriesPlaylistsTranslations')
        .mockResolvedValue([mockCategory]);
      const result = await service.getCategory(
        categoryId,
        getCategoryOptionsDto,
        userId,
        pageOptionsDto,
        withItems,
      );
      expect(result).toEqual({ category: mockCategory });
    });
  });
});
