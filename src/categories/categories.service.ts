import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
  forwardRef,
} from '@nestjs/common';
import { CreateCategoryDto } from './dto/create-category.dto';
import { CreateCategoryTranslationDto } from './dto/create-category-translation.dto';
import { Category } from './entities/category.entity';
import { FindManyOptions, FindOneOptions, In, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryTranslation } from './entities/category-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';
import { GetCategoriesOptionsDto } from './dto/get-categories-options.dto';
import { GetItemsSelectionDto } from './dto/get-items-selection-options.dto';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { CategoryPlaylistItemsService } from 'src/category-playlist-items/category-playlist-items.service';
import { DublinCoreItemsService } from 'src/dublin-core-items/dublin-core-items.service';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { invalidateCache } from 'src/utils/caching.util';
import { GetCategoriesOrderEnum } from './enums/get-categories-order.enum';
import { VisibilitiesService } from 'src/visibilities/visibilities.service';
import { UsersService } from 'src/users/users.service';
import { sortByObjectPropertyLength } from 'src/utils/sort.util';
import { ItemsService } from 'src/items/items.service';
import { Item } from 'src/items/entities/item.entity';
import { GetCategoryOptionsDto } from './dto/get-category-options.dto';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { PageOptions } from 'src/utils/classes/page-options';
import { PageDto } from 'src/utils/classes/page.dto';
import { ItemDto } from 'src/items/dto/item.dto';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { OrderCategoriesDto } from './dto/order-categories.dto';
import { CreateVisibilityDto } from 'src/visibilities/dto/create-visibility.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { UploadService } from 'src/upload/upload.service';
import { CategoryPlaylistsService } from 'src/category-playlists/category-playlists.service';
import { join } from 'path';
import { ApplicationsService } from 'src/applications/applications.service';
import { TrueFalseEnum } from './enums/true-false.enum';
import { SearchService } from 'src/search/search.service';
import { AddPlaylistsToCategoryDto } from './dto/add-playlists-to-category.dto';
import { CreateCategoryPlaylistDto } from 'src/category-playlists/dto/create-category-playlist.dto';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { filterVisibility } from 'src/utils/visibility/filter-visibility.util';
import { GetCategoriesListResponse } from './responses/get-categories-list-response';
import { GetCategoriesListDto } from './dto/get-categories-list.dto';
import { GetCategoriesCatalogDto } from './dto/get-categories-catalog.dto';
import { getUserOrNull } from 'src/utils/user/get-user-or-null.util';
import { getApplicationIdsFromCategories } from 'src/utils/application/get-application-ids.util';
import { GetCategoriesCatalogResponse } from './responses/get-categories-catalog-response';
import { GetCategoriesCatalogSortByEnum } from './enums/get-categories-catalog-sort-by.enum';
import { GetCategoriesBackOfficeListDto } from './dto/get-categories-back-office-list.dto';
import { GetCategoriesBackOfficeListResponse } from './responses/get-categories-back-office-list-response';
import { GetCategoryBackOfficeResponse } from './responses/get-category-back-office-response';
import { GetCategoryBackOfficeDto } from './dto/get-category-back-office.dto';
import { GetCategoryBackOfficeCategoryPlaylistsDto } from './dto/get-category-back-office-category-playlists.dto';
import { GetCategoryBackOfficeCategoryPlaylistsResponse } from './responses/get-category-back-office-category-playlists-response';
import { sortCategoriesTranslations } from 'src/utils/language/sort-translations/sort-categories-translations.util';
import { sortCategoriesPlaylistsTranslations } from 'src/utils/language/sort-translations/sort-categories-playlists-translations.util';
import { sortApplicationsTranslations } from 'src/utils/language/sort-translations/sort-applications-translations.util';
import { Application } from 'src/applications/entities/application.entity';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(Category)
    private categoriesRepository: Repository<Category>,
    @InjectRepository(CategoryTranslation)
    private categoryTranslationsRepository: Repository<CategoryTranslation>,
    private languagesService: LanguagesService,
    private dublinCoreItemsService: DublinCoreItemsService,
    @Inject(forwardRef(() => CategoryPlaylistItemsService))
    private categoryPlaylistItemsService: CategoryPlaylistItemsService,
    @Inject(forwardRef(() => PlaylistsService))
    private playlistsService: PlaylistsService,
    private visibilitiesService: VisibilitiesService,
    private dublinCoreTypesService: DublinCoreTypesService,
    private usersService: UsersService,
    @Inject(forwardRef(() => ItemsService)) private itemsService: ItemsService,
    private uploadService: UploadService,
    @Inject(forwardRef(() => CategoryPlaylistsService))
    private categoryPlaylistsService: CategoryPlaylistsService,
    @Inject(forwardRef(() => ApplicationsService))
    private applicationsService: ApplicationsService,
    @Inject(forwardRef(() => SearchService))
    private searchService: SearchService,
  ) {}

  async indexAll() {
    const categories = await this.findAll();
    for (const category of categories) {
      await this.searchService.indexCategory(category.id);
    }
  }

  /**
   * Retrieve a category and its associated details for back office purposes.
   * The translations are sorted based on the provided language identifier.
   *
   * @param {number} id - The ID of the category to retrieve.
   * @param {GetCategoryBackOfficeDto} getCategoryBackOfficeDto - DTO containing method options
   * @returns {Promise<GetCategoryBackOfficeResponse>} The category and its associated details.
   */
  async getCategoryBackOffice(
    id: number,
    getCategoryBackOfficeDto: GetCategoryBackOfficeDto,
  ): Promise<GetCategoryBackOfficeResponse> {
    const categoriesQueryBuilder = this.categoriesRepository
      .createQueryBuilder('category')
      .addSelect([
        'category.id',
        'category.pictogram',
        'category.isHomepageDisplayed',
        'category.order',
        'category.isUpdateNeeded',
        'category.deletedAt',
        'category.updatedAt',
      ])
      // Manage categories translations
      .leftJoin('category.categoryTranslations', 'categoryTranslations')
      .addSelect([
        'categoryTranslations.id',
        'categoryTranslations.title',
        'categoryTranslations.description',
      ])
      // Manage categories translations languages
      .leftJoin('categoryTranslations.language', 'language')
      .addSelect(['language.identifier', 'language.label'])
      .leftJoin('category.visibility', 'visibility')
      .addSelect(['visibility.id'])
      .leftJoin('visibility.roles', 'roles')
      .addSelect(['roles.id', 'roles.name'])
      .leftJoin('visibility.users', 'users')
      .addSelect(['users.id', 'users.username'])
      .where('category.id = :id', { id });

    const category = await categoriesQueryBuilder.getOne();

    sortCategoriesTranslations(
      [category],
      getCategoryBackOfficeDto.languageIdentifier,
    );

    return new GetCategoryBackOfficeResponse(category);
  }

  /**
   * Retrieve the playlists and associated applications for a specific category
   * in the back-office context, with translations prioritized by the specified
   * language identifier.
   *
   * @param {number} id - The ID of the category.
   * @param {GetCategoryBackOfficeCategoryPlaylistsDto} getCategoryBackOfficeCategoryPlaylistsDto - DTO containing the language identifier for sorting translations.
   * @returns {Promise<GetCategoryBackOfficeCategoryPlaylistsResponse>} The playlists and applications associated with the category.
   */
  async getCategoryBackOfficeCategoryPlaylists(
    id: number,
    getCategoryBackOfficeCategoryPlaylistsDto: GetCategoryBackOfficeCategoryPlaylistsDto,
  ): Promise<GetCategoryBackOfficeCategoryPlaylistsResponse> {
    const categoryPlaylists =
      await this.categoryPlaylistsService.getCategoryCategoryPlaylists(
        id,
        getCategoryBackOfficeCategoryPlaylistsDto.languageIdentifier,
      );

    const applications =
      await this.categoryPlaylistsService.getCategoryPlaylistsApplications(
        categoryPlaylists,
        getCategoryBackOfficeCategoryPlaylistsDto.languageIdentifier,
      );

    return new GetCategoryBackOfficeCategoryPlaylistsResponse(
      applications,
      categoryPlaylists,
    );
  }

  /**
   * Get all categories, including their categoryPlaylists.
   * The results are filtered by the visibility of the categories.
   * If the userId is provided, the visibility filter is extended to include
   * categories that are visible to the user's role or to the user itself.
   *
   * @param {GetCategoriesCatalogDto} getCategoriesCatalogDto The options to filter the categories
   * @param {number} [userId] The id of the user to filter the categories by its visibility
   * @return {GetCategoriesCatalogResponse}
   */
  async getCategoriesCatalog(
    getCategoriesCatalogDto: GetCategoriesCatalogDto,
    userId?: number,
  ): Promise<GetCategoriesCatalogResponse> {
    const user = await getUserOrNull(this.usersService, userId);

    const categoriesQueryBuilder = this.categoriesRepository
      .createQueryBuilder('category')
      .addSelect([
        'category.id',
        'category.pictogram',
        'category.isHomepageDisplayed',
        'category.order',
        'category.isUpdateNeeded',
        'category.deletedAt',
      ])
      // Manage categories translations
      .leftJoin('category.categoryTranslations', 'categoryTranslations')
      .addSelect([
        'categoryTranslations.id',
        'categoryTranslations.title',
        'categoryTranslations.description',
      ])
      // Manage categories translations languages
      .leftJoin('categoryTranslations.language', 'language')
      .addSelect(['language.identifier', 'language.label'])
      // Manage categories categoryPlaylists
      .leftJoin('category.categoryPlaylists', 'categoryPlaylists')
      .addSelect(['categoryPlaylists.id', 'categoryPlaylists.order'])
      // Manage categoryPlaylists playlists
      .leftJoin('categoryPlaylists.playlist', 'playlist')
      .addSelect([
        'playlist.id',
        'playlist.image',
        'playlist.isInstalled',
        'playlist.isUpdateNeeded',
        'playlist.isBroken',
        'playlist.isManuallyInstalled',
        'playlist.version',
        'playlist.size',
      ])
      // Manage playlists translations
      .leftJoin('playlist.playlistTranslations', 'playlistTranslations')
      .addSelect([
        'playlistTranslations.id',
        'playlistTranslations.title',
        'playlistTranslations.description',
      ])
      // Manage playlists translations languages
      .leftJoin('playlistTranslations.language', 'playlistTranslationsLanguage')
      .addSelect([
        'playlistTranslationsLanguage.identifier',
        'playlistTranslationsLanguage.label',
      ])
      // Manage playlists application
      .leftJoin('playlist.application', 'application')
      .addSelect('application.id')
      // Manage categories visibility
      .leftJoin('category.visibility', 'visibility')
      .leftJoin('visibility.users', 'users')
      .leftJoin('visibility.roles', 'roles')
      // Manage playlists visibility
      .leftJoin('application.visibility', 'applicationVisibility')
      .leftJoin('applicationVisibility.users', 'applicationUsers')
      .leftJoin('applicationVisibility.roles', 'applicationRoles')
      // Count categoryPlaylistItemsCount
      .leftJoin(
        'categoryPlaylists.categoryPlaylistItems',
        'categoryPlaylistItems',
      )
      .loadRelationCountAndMap(
        'categoryPlaylists.categoryPlaylistItemsCount',
        'categoryPlaylists.categoryPlaylistItems',
      )
      // Allow access to public categories
      .where('roles.id IS NULL AND users.id IS NULL');

    if (getCategoriesCatalogDto.categoryIds) {
      const categoryIds = getCategoriesCatalogDto.categoryIds.split(',');
      categoriesQueryBuilder.andWhere('category.id IN (:...categoryIds)', {
        categoryIds,
      });
    }

    if (getCategoriesCatalogDto.applicationIds) {
      const applicationIds = getCategoriesCatalogDto.applicationIds.split(',');
      categoriesQueryBuilder.andWhere(
        'application.id IN (:...applicationIds)',
        { applicationIds },
      );
    }

    const sortBy = getCategoriesCatalogDto.sortBy;
    switch (sortBy) {
      case GetCategoriesCatalogSortByEnum.ALPHABETICAL:
        categoriesQueryBuilder.orderBy(
          'LOWER(categoryTranslations.title)',
          'ASC',
        );
        break;
      case GetCategoriesCatalogSortByEnum.COUNT_PLAYLISTS:
        categoriesQueryBuilder.orderBy(
          `(SELECT COUNT(*) FROM category_playlist cp WHERE cp.categoryId = category.id)`,
          'DESC',
        );
        break;
      case GetCategoriesCatalogSortByEnum.LAST_UPDATE:
        categoriesQueryBuilder.orderBy('category.updatedAt', 'DESC');
        break;
      default:
        categoriesQueryBuilder.orderBy('category.order', 'ASC');
        break;
    }

    if (!user) {
      // No user logged in, allow access to public playlists
      categoriesQueryBuilder.andWhere(
        'applicationUsers.id IS NULL AND applicationRoles.id IS NULL',
      );
    } else {
      // Allow access based on user or role for both categories and playlists
      categoriesQueryBuilder
        .orWhere('roles.id = :roleId', { roleId: user.role.id })
        .orWhere('users.id = :userId', { userId: user.id })
        .orWhere('applicationUsers.id = :userId', { userId: user.id })
        .orWhere('applicationRoles.id = :roleId', { roleId: user.role.id });
    }

    const categories = await categoriesQueryBuilder.getMany();

    sortCategoriesTranslations(
      categories,
      getCategoriesCatalogDto.languageIdentifier,
    );

    sortCategoriesPlaylistsTranslations(
      categories,
      getCategoriesCatalogDto.languageIdentifier,
    );

    const applications = await this.getCategoriesApplications(
      categories,
      getCategoriesCatalogDto.languageIdentifier,
    );

    return new GetCategoriesCatalogResponse({ applications, categories });
  }

  /**
   * Retrieve all applications from a given categories, with translations.
   * The results are sorted by the given languageIdentifier first, then by the identifier of the language.
   * @param {Category[]} categories the categories to retrieve applications from
   * @param {string} languageIdentifier the identifier of the language to prioritize the translations
   * @return {Promise<Application[]>} an array of applications with translations
   */
  async getCategoriesApplications(
    categories: Category[],
    languageIdentifier: string,
  ): Promise<Application[]> {
    const applicationIds = getApplicationIdsFromCategories(categories);

    const applications = await this.applicationsService.find({
      where: { id: In(applicationIds) },
      relations: {
        applicationTranslations: { language: true },
        applicationType: { applicationTypeTranslations: { language: true } },
      },
      select: {
        id: true,
        name: true,
        displayName: true,
        version: true,
        logo: true,
        image: true,
        url: true,
        size: true,
        isInstalled: true,
        isDownloading: true,
        isInstalling: true,
        isUp: true,
        isUpdateNeeded: true,
        isUpdating: true,
        isOlip: true,
        applicationType: {
          id: true,
          name: true,
          applicationTypeTranslations: {
            id: true,
            label: true,
            description: true,
            language: { identifier: true, label: true },
          },
        },
        applicationTranslations: {
          id: true,
          longDescription: true,
          shortDescription: true,
          language: {
            identifier: true,
            label: true,
          },
        },
      },
    });

    sortApplicationsTranslations(applications, languageIdentifier);

    return applications;
  }

  /**
   * Get all categories, including their translations.
   * The results are filtered by the visibility of the categories.
   * If the userId is provided, the visibility filter is extended to include
   * categories that are visible to the user's role or to the user itself.
   *
   * @param {GetCategoriesListDto} getCategoriesListDto The options to filter the categories.
   * @param {number} [userId] The id of the user to filter the categories by its visibility.
   * @returns {GetCategoriesListResponse} A response containing the categories.
   */
  async getCategoriesList(
    getCategoriesListDto: GetCategoriesListDto,
    userId?: number,
  ): Promise<GetCategoriesListResponse> {
    const user = await getUserOrNull(this.usersService, userId);

    const categoriesQueryBuilder = this.categoriesRepository
      .createQueryBuilder('category')
      .addSelect([
        'category.id',
        'category.pictogram',
        'category.isHomepageDisplayed',
        'category.order',
        'category.isUpdateNeeded',
        'category.deletedAt',
      ])
      .leftJoin('category.visibility', 'visibility')
      .leftJoin('visibility.users', 'users')
      .leftJoin('visibility.roles', 'roles')
      .leftJoin('category.categoryTranslations', 'categoryTranslations')
      .addSelect([
        'categoryTranslations.id',
        'categoryTranslations.title',
        'categoryTranslations.description',
      ])
      .leftJoin('categoryTranslations.language', 'language')
      .addSelect(['language.identifier', 'language.label'])
      .where('roles.id IS NULL AND users.id IS NULL')
      .orderBy('category.order', 'ASC');

    if (user) {
      categoriesQueryBuilder
        .orWhere('roles.id = :roleId', { roleId: user.role.id })
        .orWhere('users.id = :userId', { userId: user.id });
    }

    const categories = await categoriesQueryBuilder.getMany();

    sortCategoriesTranslations(
      categories,
      getCategoriesListDto.languageIdentifier,
    );

    return new GetCategoriesListResponse(categories);
  }

  /**
   * Get all categories, including their translations.
   *
   * @param {GetCategoriesBackOfficeListDto} getCategoriesBackOfficeListDto The options to filter the categories.
   * @returns {GetCategoriesBackOfficeListResponse} A response containing the categories.
   */
  async getCategoriesBackOfficeList(
    getCategoriesBackOfficeListDto: GetCategoriesBackOfficeListDto,
  ): Promise<GetCategoriesBackOfficeListResponse> {
    const categoriesQueryBuilder = this.categoriesRepository
      .createQueryBuilder('category')
      .addSelect([
        'category.id',
        'category.pictogram',
        'category.isHomepageDisplayed',
        'category.order',
        'category.isUpdateNeeded',
        'category.deletedAt',
      ])
      .leftJoin('category.users', 'users')
      .addSelect(['users.id', 'users.username'])
      .leftJoinAndSelect('category.visibility', 'visibility')
      .leftJoin('visibility.users', 'visibilityUsers')
      .addSelect(['visibilityUsers.id', 'visibilityUsers.username'])
      .leftJoin('visibility.roles', 'visibilityRoles')
      .addSelect(['visibilityRoles.id', 'visibilityRoles.name'])
      .leftJoin('category.categoryTranslations', 'categoryTranslations')
      .addSelect([
        'categoryTranslations.id',
        'categoryTranslations.title',
        'categoryTranslations.description',
      ])
      .leftJoin('categoryTranslations.language', 'language')
      .addSelect(['language.identifier', 'language.label'])
      .leftJoin('category.categoryPlaylists', 'categoryPlaylists')
      .loadRelationCountAndMap(
        'category.categoryPlaylistsCount',
        'category.categoryPlaylists',
      )
      .orderBy('category.order', 'ASC');

    const categories = await categoriesQueryBuilder.getMany();

    sortCategoriesTranslations(
      categories,
      getCategoriesBackOfficeListDto.languageIdentifier,
    );

    return new GetCategoriesBackOfficeListResponse(categories);
  }

  async addPlaylistsToCategory(
    addPlaylistsToCategoryDto: AddPlaylistsToCategoryDto,
    categoryId: number,
  ) {
    for (const playlistId of addPlaylistsToCategoryDto.playlistIds) {
      await this.addPlaylistToCategory(categoryId, playlistId);
    }
    await invalidateCache([Category, Playlist]);
  }

  async addPlaylistToCategory(categoryId: number, playlistId: string) {
    const category = await this.findOne({
      where: { id: categoryId },
    });
    const playlist = await this.playlistsService.findOne({
      where: { id: playlistId },
    });
    if (!category || !playlist) return;

    const createCategoryPlaylistDto: CreateCategoryPlaylistDto = {
      categoryId,
      playlistId,
    };
    await this.categoryPlaylistsService.create(createCategoryPlaylistDto);

    await invalidateCache([Category, Playlist]);
  }

  private async getLastOrder() {
    const query = this.categoriesRepository.createQueryBuilder('category');
    query.select('MAX(category.order)', 'order');
    const lastOrder = await query.getRawOne();
    return lastOrder.order || 0;
  }

  async create(createCategoryDto: CreateCategoryDto, userId?: number) {
    const order = await this.getLastOrder();
    let pictogram = '';
    if (createCategoryDto.pictogram) {
      pictogram = await this.uploadService.image(createCategoryDto.pictogram);
    } else {
      const placeholderPath = join(
        __dirname,
        '../../data/images/placeholder_playlists.svg',
      );
      pictogram = await this.uploadService.copyImage(placeholderPath, true);
    }
    let category = new Category(createCategoryDto, order + 1, pictogram);
    if (userId) {
      const user = await this.usersService.findOne({ where: { id: userId } });
      category.users = [user];
    }
    category = await this.categoriesRepository.save(category);
    const createVisibilityDto: CreateVisibilityDto = new CreateVisibilityDto(
      createCategoryDto,
    );
    await this.visibilitiesService.create(createVisibilityDto, category);
    for (const createCategoryTranslationDto of createCategoryDto.createCategoryTranslationsDto) {
      await this.createTranslation(createCategoryTranslationDto, category);
    }
    await invalidateCache(Category);
    return category;
  }

  private async createTranslations(
    createCategoryTranslationsDto: CreateCategoryTranslationDto[],
    category: Category,
  ) {
    const categoryTranslations = [];
    for (const createCategoryTranslation of createCategoryTranslationsDto) {
      const categoryTranslation = await this.createTranslation(
        createCategoryTranslation,
        category,
      );
      categoryTranslations.push(categoryTranslation);
    }
    await invalidateCache([Category, CategoryTranslation]);
    return categoryTranslations;
  }

  async update(id: number, updateCategoryDto: UpdateCategoryDto) {
    const category = await this.findOne({
      where: { id },
      relations: { visibility: true },
    });
    if (category === null) {
      throw new NotFoundException(`category ID ${id} not found`);
    }

    if (!updateCategoryDto) return;

    if (updateCategoryDto.pictogram) {
      const pictogram = await this.uploadService.image(
        updateCategoryDto.pictogram,
      );
      category.pictogram = pictogram;
    }

    if (updateCategoryDto.isHomepageDisplayed !== undefined) {
      category.isHomepageDisplayed = updateCategoryDto.isHomepageDisplayed;
    }

    if (
      updateCategoryDto.userIds !== undefined &&
      updateCategoryDto.roleIds !== undefined
    ) {
      throw new BadRequestException(
        'Cannot set both userIds and roleIds at the same time',
      );
    }

    if (
      updateCategoryDto.userIds !== undefined ||
      updateCategoryDto.roleIds !== undefined
    ) {
      await this.visibilitiesService.update(category.visibility.id, {
        userIds: updateCategoryDto.userIds,
        roleIds: updateCategoryDto.roleIds,
      });
    }

    if (updateCategoryDto.createCategoryTranslationsDto) {
      await this.categoryTranslationsRepository.delete({ category: { id } });
      await this.createTranslations(
        updateCategoryDto.createCategoryTranslationsDto,
        category,
      );
    }

    await invalidateCache([Category, CategoryTranslation]);
    await this.categoriesRepository.save(category);
    return;
  }

  async findOne(options?: FindOneOptions<Category>) {
    return this.categoriesRepository.findOne(options);
  }

  async findAll(options?: FindManyOptions<Category>) {
    return this.categoriesRepository.find(options);
  }

  private async createTranslation(
    createCategoryTranslationDto: CreateCategoryTranslationDto,
    category: Category,
  ): Promise<CategoryTranslation | null> {
    const language = await this.languagesService.findOne({
      where: { identifier: createCategoryTranslationDto.languageIdentifier },
    });
    if (language === null) return null;

    let categoryTranslation = new CategoryTranslation(
      createCategoryTranslationDto,
      category,
      language,
    );
    categoryTranslation =
      await this.categoryTranslationsRepository.save(categoryTranslation);
    await invalidateCache([Category, CategoryTranslation]);
    return categoryTranslation;
  }

  async getTranslation(
    category: Category,
    languageIdentifier?: string,
    getAllCategoriesTranslations?: boolean,
  ): Promise<Category> {
    if (getAllCategoriesTranslations === true) {
      const categoryTranslations =
        await this.categoryTranslationsRepository.find({
          where: { category: { id: category.id } },
          relations: { language: true },
        });
      category.categoryTranslations = categoryTranslations;
    } else {
      const translation = await this.languagesService.getTranslation<
        Category,
        CategoryTranslation
      >(
        'category',
        category,
        this.categoryTranslationsRepository,
        languageIdentifier,
      );
      category.categoryTranslations = [translation];
    }
    return category;
  }

  async getTranslations(
    categories: Category[],
    languageIdentifier?: string,
    getAllCategoriesTranslations?: boolean,
  ): Promise<Category[]> {
    for (let category of categories) {
      category = await this.getTranslation(
        category,
        languageIdentifier,
        getAllCategoriesTranslations,
      );
    }
    return categories;
  }

  async getItemsSelection(
    categoryId: number,
    getItemsSelectionDto?: GetItemsSelectionDto,
  ) {
    let count = 4;
    if (getItemsSelectionDto) {
      if (getItemsSelectionDto.count) {
        count = +getItemsSelectionDto.count;
      }
    }

    const category = await this.findOne({ where: { id: categoryId } });
    if (category === null) {
      throw new NotFoundException('category not found');
    }

    // Get items by category that are highlitable
    const categoryPlaylistItems =
      await this.categoryPlaylistItemsService.findAll({
        where: {
          categoryPlaylist: { category: { id: category.id } },
          isDisplayed: true,
        },
        relations: {
          item: {
            dublinCoreItem: { dublinCoreType: true },
            application: { applicationType: true },
            file: true,
          },
        },
        take: count,
        order: { id: 'DESC' },
      });

    // Get items translations
    for (const categoryPlaylistItem of categoryPlaylistItems) {
      const translation = await this.dublinCoreItemsService.getTranslation(
        categoryPlaylistItem.item.dublinCoreItem,
        getItemsSelectionDto.languageIdentifier,
      );
      categoryPlaylistItem.item.dublinCoreItem.dublinCoreItemTranslations = [
        translation,
      ];
      await this.applicationsService.getApplicationsTranslations(
        [categoryPlaylistItem.item.application],
        getItemsSelectionDto.languageIdentifier,
      );
    }
    return categoryPlaylistItems;
  }

  async getCategories(
    userId: number | undefined,
    getCategoriesOptions?: GetCategoriesOptionsDto,
  ) {
    const isVisibilityEnabled =
      getCategoriesOptions?.isVisibilityEnabled !== TrueFalseEnum.FALSE;
    const applicationIds =
      getCategoriesOptions && getCategoriesOptions.applicationIds
        ? getCategoriesOptions.applicationIds.split(',')
        : [];
    const order: GetCategoriesOrderEnum = getCategoriesOptions?.order
      ? getCategoriesOptions.order
      : GetCategoriesOrderEnum.POPULARITY;
    const withPlaylists =
      getCategoriesOptions?.withPlaylists === 'true' ? true : false;
    const where = [];
    if (getCategoriesOptions?.categoryId) {
      const categoryIds = getCategoriesOptions.categoryId.split(',');
      for (const categoryId of categoryIds) {
        where.push({ id: +categoryId });
      }
    }
    if (isVisibilityEnabled) {
      if (where.length > 0) {
        for (let whereLine of where) {
          whereLine = {
            ...whereLine,
            categoryPlaylists: { playlist: { items: { isInstalled: true } } },
          };
        }
      } else {
        where.push({
          categoryPlaylists: { playlist: { items: { isInstalled: true } } },
        });
      }
    }
    let categories = await this.findAll({
      relations: {
        users: true,
        visibility: { users: true, roles: true },
        categoryPlaylists: {
          playlist: { application: true },
          categoryPlaylistItems: true,
        },
      },
      order: { order: 'DESC' },
      where,
    });

    if (isVisibilityEnabled) {
      categories = await this.filterCategoriesByVisibility(userId, categories);
    }

    categories = await this.getCategoriesTranslations(
      getCategoriesOptions.languageIdentifier,
      categories,
    );

    categories = this.orderCategories(order, categories);

    categories = await this.getItemsCount(categories, withPlaylists);

    if (withPlaylists) {
      categories = await this.getCategoriesPlaylistsTranslations(
        getCategoriesOptions.languageIdentifier,
        categories,
      );
      categories = this.filterCategoriesByApplications(
        categories,
        applicationIds,
      );
    }

    if (isVisibilityEnabled) {
      categories = this.filterEmptyCategories(categories);
    }

    return categories;
  }

  private filterEmptyCategories(categories: Category[]) {
    return categories.filter(
      (category) => category.categoryPlaylists.length > 0,
    );
  }

  private filterCategoriesByApplications(
    categories: Category[],
    applicationIds: string[],
  ) {
    if (applicationIds.length === 0) return categories;
    const newCategories = [];
    for (const category of categories) {
      const newCategoryPlaylists = [];
      for (const categoryPlaylist of category.categoryPlaylists) {
        if (applicationIds.includes(categoryPlaylist.playlist.application.id)) {
          newCategoryPlaylists.push(categoryPlaylist);
        }
      }
      const newCategory = { ...category };
      newCategory.categoryPlaylists = newCategoryPlaylists;
      newCategories.push(newCategory);
    }
    return newCategories;
  }

  private async getItemsCount(categories: Category[], withPlaylists: boolean) {
    if (withPlaylists === true) {
      for (const category of categories) {
        for (const playlist of category.categoryPlaylists) {
          playlist.categoryPlaylistItems =
            playlist.categoryPlaylistItems.filter(
              (categoryPlaylistItem: CategoryPlaylistItem) =>
                categoryPlaylistItem.isDisplayed === true,
            );
          playlist['totalItems'] = playlist.categoryPlaylistItems.length;
          delete playlist.categoryPlaylistItems;
        }
      }
    }
    return categories;
  }

  async getCategoriesTranslations(
    languageIdentifier: string,
    categories: Category[],
    getAllCategoriesTranslations?: boolean,
  ) {
    for (let category of categories) {
      category = await this.getTranslation(
        category,
        languageIdentifier,
        getAllCategoriesTranslations,
      );
    }
    return categories;
  }

  async getCategoriesPlaylistsTranslations(
    languageIdentifier: string,
    categories: Category[],
  ) {
    for (const category of categories) {
      for (const categoryPlaylist of category.categoryPlaylists) {
        const playlistTranslation = await this.playlistsService.getTranslation(
          categoryPlaylist.playlist,
          languageIdentifier,
        );
        categoryPlaylist.playlist.playlistTranslations = [playlistTranslation];
      }
    }
    return categories;
  }

  private orderCategories(
    order: GetCategoriesOrderEnum,
    categories: Category[],
  ) {
    switch (order) {
      case GetCategoriesOrderEnum.ALPHABETICAL:
        categories.sort((a, b) => {
          return a.categoryTranslations[0].title.localeCompare(
            b.categoryTranslations[0].title,
          );
        });
        break;
      case GetCategoriesOrderEnum.LAST_UPDATE:
        categories.sort((a, b) => {
          const lastUpdatedA = (a.categoryPlaylists ?? []).reduce(
            (latest, categoryPlaylist) => {
              const playlistUpdatedAt =
                categoryPlaylist.playlist.updatedAt ?? 0;
              return playlistUpdatedAt > latest ? playlistUpdatedAt : latest;
            },
            0,
          );

          const lastUpdatedB = (b.categoryPlaylists ?? []).reduce(
            (latest, categoryPlaylist) => {
              const playlistUpdatedAt =
                categoryPlaylist.playlist.updatedAt ?? 0;
              return playlistUpdatedAt > latest ? playlistUpdatedAt : latest;
            },
            0,
          );

          return lastUpdatedB - lastUpdatedA;
        });
        break;
      case GetCategoriesOrderEnum.COUNT_PLAYLISTS:
        categories = sortByObjectPropertyLength(
          categories,
          'categoryPlaylists',
        );
        break;
    }
    return categories;
  }

  async filterCategoriesByVisibility(
    userId: number | undefined,
    categories: Category[],
  ) {
    const user =
      userId &&
      (await this.usersService.findOne({
        where: { id: userId },
        relations: { role: true },
      }));

    categories = categories.filter((category) =>
      filterVisibility(category.visibility, user),
    );

    return categories;
  }

  private async getItemsPagination(
    pageOptionsDto: PageOptionsDto,
    getCategoryOptionsDto: GetCategoryOptionsDto,
    category: Category,
    playlistIds: string[],
    dublinCoreTypeIds: number[],
    userId?: number,
  ) {
    const pageOptions = new PageOptions(pageOptionsDto);
    const queryBuilder = this.itemsService.getQueryBuilder('item');
    let itemsQueryBuilder = queryBuilder
      // Joins CategoryPlaylistItems from Item
      .innerJoin(
        'item.categoryPlaylistItems',
        'categoryPlaylistItems',
        'categoryPlaylistItems.itemId = item.id',
      )
      // Joins CategoryPlaylist from CategoryPlaylistItems
      .innerJoin(
        'categoryPlaylistItems.categoryPlaylist',
        'categoryPlaylist',
        'categoryPlaylist.id = categoryPlaylistItems.categoryPlaylistId',
      )
      // Joins Playlist from CategoryPlaylist
      .innerJoin(
        'categoryPlaylist.playlist',
        'playlist',
        'playlist.id = categoryPlaylist.playlistId',
      )
      // Joins DublinCoreItem from Item
      .innerJoinAndSelect(
        'item.dublinCoreItem',
        'dublinCoreItem',
        'dublinCoreItem.itemId = item.id',
      )
      // Joins DublinCoreType from DublinCoreItem
      .innerJoinAndSelect(
        'dublinCoreItem.dublinCoreType',
        'dublinCoreType',
        'dublinCoreType.id = dublinCoreItem.dublinCoreTypeId',
      )
      // Joins Application from Item
      .innerJoinAndSelect(
        'item.application',
        'application',
        'application.id = item.applicationId',
      )
      // Joins ApplicationType from Application
      .innerJoinAndSelect(
        'application.applicationType',
        'applicationType',
        'applicationType.id = application.applicationTypeId',
      )
      // Joins ApplicationVisibility from Application
      .innerJoinAndSelect(
        'application.visibility',
        'visibility',
        'visibility.id = application.visibilityId',
      )
      // Joins DublinCoreItemTranslations from DublinCoreItem
      .innerJoin(
        'dublinCoreItem.dublinCoreItemTranslations',
        'dublinCoreItemTranslations',
        'dublinCoreItemTranslations.dublinCoreItemId = dublinCoreItem.id',
      )
      .leftJoinAndSelect('visibility.users', 'users')
      .leftJoinAndSelect('visibility.roles', 'roles')
      // Select from categoryId
      .where('categoryPlaylist.categoryId = :categoryId', {
        categoryId: category.id,
      })
      // Select from categoryId
      .andWhere('categoryPlaylistItems.isDisplayed = 1')
      // application visibility here
      // Take from pageOptions
      .take(pageOptions.take)
      // Skip from pageOptions
      .skip(pageOptions.skip);

    if (userId) {
      const user = await this.usersService.findOne({
        where: { id: userId },
        relations: { role: true },
      });
      if (user) {
        itemsQueryBuilder = itemsQueryBuilder.andWhere(
          '(users.id = :userId OR roles.name = :roleName OR (users.id IS NULL AND roles.id IS NULL))',
          { userId: user.id, roleName: user.role.name },
        );
      } else {
        itemsQueryBuilder = itemsQueryBuilder.andWhere(
          '(users.id IS NULL AND roles.id IS NULL)',
        );
      }
    } else {
      itemsQueryBuilder = itemsQueryBuilder.andWhere(
        '(users.id IS NULL AND roles.id IS NULL)',
      );
    }

    // Filter by playlistIds
    if (playlistIds.length > 0) {
      itemsQueryBuilder = itemsQueryBuilder.andWhere(
        'playlist.id IN (:...playlistIds)',
        {
          playlistIds,
        },
      );
    }

    // Filter by DublinCoreTypeIds
    if (dublinCoreTypeIds.length > 0) {
      itemsQueryBuilder = itemsQueryBuilder.andWhere(
        'dublinCoreItem.dublinCoreTypeId IN (:...dublinCoreTypeIds)',
        {
          dublinCoreTypeIds,
        },
      );
    }

    // Filter by titleLike
    if (
      getCategoryOptionsDto.titleLike !== undefined &&
      getCategoryOptionsDto.titleLike !== ''
    ) {
      itemsQueryBuilder = itemsQueryBuilder.andWhere(
        'dublinCoreItemTranslations.title LIKE :titleLike',
        {
          titleLike: `%${getCategoryOptionsDto.titleLike}%`,
        },
      );
    }

    const itemsResult = await getManyAndCount<Item>(itemsQueryBuilder, [
      'item.id',
    ]);

    const totalItemsCount = itemsResult[0];
    let items = itemsResult[1];

    items = await this.getItemsDublinCoreTranslation(
      getCategoryOptionsDto.languageIdentifier,
      items,
    );
    items = await this.getItemsDublinCoreTypeTranslation(
      getCategoryOptionsDto.languageIdentifier,
      items,
    );

    for (const item of items) {
      await this.applicationsService.getApplicationsTranslations(
        [item.application],
        getCategoryOptionsDto.languageIdentifier,
      );
    }

    const data = items.map((item) => new ItemDto(item));
    return new PageDto(data, totalItemsCount, pageOptions);
  }

  async getCategory(
    categoryId: number,
    getCategoryOptionsDto: GetCategoryOptionsDto,
    userId: number,
    pageOptionsDto: PageOptionsDto,
    withItems: boolean = true,
  ) {
    const isVisibilityEnabled =
      getCategoryOptionsDto?.isVisibilityEnabled !== TrueFalseEnum.FALSE;

    const playlistIds = [];
    if (
      getCategoryOptionsDto.playlistId &&
      getCategoryOptionsDto.playlistId !== ''
    ) {
      const stringIds = getCategoryOptionsDto.playlistId.split(',');
      for (const playlistId of stringIds) {
        playlistIds.push(playlistId);
      }
    }
    const dublinCoreTypeIds = [];
    if (
      getCategoryOptionsDto.dublinCoreTypeId &&
      getCategoryOptionsDto.dublinCoreTypeId !== ''
    ) {
      const stringIds = getCategoryOptionsDto.dublinCoreTypeId.split(',');
      for (const dublinCoreTypeId of stringIds) {
        dublinCoreTypeIds.push(+dublinCoreTypeId);
      }
    }
    const categoryPlaylists = isVisibilityEnabled
      ? { playlist: { items: { isInstalled: true } } }
      : undefined;
    let category = await this.categoriesRepository.findOne({
      where: { id: categoryId, categoryPlaylists },
      relations: {
        users: true,
        visibility: { users: true, roles: true },
        categoryPlaylists: {
          playlist: {
            application: { visibility: { users: true, roles: true } },
          },
        },
      },
    });
    if (category === null) throw new NotFoundException('category not found');

    let categories = [category];

    if (isVisibilityEnabled) {
      // Here we use a function that takes multiple categories but it's ok ;)
      categories = await this.filterCategoriesByVisibility(userId, categories);
      if (categories.length === 0) throw new UnauthorizedException();
    }

    categories = await this.getCategoriesTranslations(
      getCategoryOptionsDto.languageIdentifier,
      categories,
      getCategoryOptionsDto.getAllCategoryTranslations === 'true',
    );
    categories = await this.getCategoriesPlaylistsTranslations(
      getCategoryOptionsDto.languageIdentifier,
      categories,
    );

    category = categories[0];
    if (withItems) {
      const itemsPagination = await this.getItemsPagination(
        pageOptionsDto,
        getCategoryOptionsDto,
        category,
        playlistIds,
        dublinCoreTypeIds,
        userId,
      );
      return { category, ...itemsPagination };
    }
    return { category };
  }

  private async getItemsDublinCoreTranslation(
    languageIdentifier: string,
    items: Item[],
  ): Promise<Item[]> {
    for (const item of items) {
      const dublinCoreTranslation =
        await this.dublinCoreItemsService.getTranslation(
          item.dublinCoreItem,
          languageIdentifier,
        );
      item.dublinCoreItem.dublinCoreItemTranslations = [dublinCoreTranslation];
    }
    return items;
  }

  private async getItemsDublinCoreTypeTranslation(
    languageIdentifier: string,
    items: Item[],
  ): Promise<Item[]> {
    for (const item of items) {
      const dublinCoreTypeTranslation =
        await this.dublinCoreTypesService.getTranslation(
          item.dublinCoreItem.dublinCoreType,
          languageIdentifier,
        );
      item.dublinCoreItem.dublinCoreType.dublinCoreTypeTranslations = [
        dublinCoreTypeTranslation,
      ];
    }
    return items;
  }

  async order(orderCategoriesDto: OrderCategoriesDto) {
    const categories = await this.findAll();

    // Sort the existing categories based on the order of category IDs
    const sortedCategories = orderCategoriesDto.categoryIds
      .map((categoryId) => categories.find((c) => c.id === categoryId))
      .filter(Boolean);

    for (let i = 0; i < sortedCategories.length; i++) {
      const category = sortedCategories[i];
      category.order = i + 1;
      await this.categoriesRepository.save(category);
    }
    await invalidateCache([Category]);
  }

  async getCategoryContent(
    id: number,
    getCategoryOptions: GetCategoryOptionsDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const category = await this.categoriesRepository.findOne({
      where: { id },
    });
    if (category === null) {
      throw new NotFoundException('category not found');
    }
    return this.categoryPlaylistsService.getCategoryPlaylistsPagination(
      pageOptionsDto,
      getCategoryOptions,
      category,
    );
  }

  async delete(id: number) {
    const category = await this.findOne({ where: { id } });
    if (!category) throw new NotFoundException('category not found');

    await this.categoriesRepository.softRemove(category);
    await invalidateCache([Category]);
  }

  async saveTranslation(categoryTranslation: CategoryTranslation) {
    return this.categoryTranslationsRepository.save(categoryTranslation);
  }

  save(category: Category) {
    return this.categoriesRepository.save(category);
  }

  async removePlaylistUpdateNeeded(playlistId: string) {
    const categories = await this.categoriesRepository.find({
      where: { categoryPlaylists: { playlist: { id: playlistId } } },
    });
    for (const category of categories) {
      let isUpdateNeeded = false;
      for (const categoryPlaylist of category.categoryPlaylists) {
        if (categoryPlaylist.playlist.isUpdateNeeded) {
          isUpdateNeeded = true;
          break;
        }
      }
      if (!isUpdateNeeded) {
        category.isUpdateNeeded = isUpdateNeeded;
        await this.save(category);
      }
    }
  }
}
