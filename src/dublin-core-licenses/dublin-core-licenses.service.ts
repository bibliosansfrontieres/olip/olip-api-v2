import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DublinCoreLicense } from './entities/dublin-core-license.entity';
import { FindOneOptions, Repository } from 'typeorm';
import { DublinCoreLicenseTranslation } from './entities/dublin-core-license-translation.entity';
import { CreateDublinCoreLicenseTranslationDto } from './dto/create-dublin-core-license-translation.dto';
import { LanguagesService } from 'src/languages/languages.service';

@Injectable()
export class DublinCoreLicensesService {
  constructor(
    @InjectRepository(DublinCoreLicense)
    private dublinCoreLicenseRepository: Repository<DublinCoreLicense>,
    @InjectRepository(DublinCoreLicenseTranslation)
    private dublinCoreLicenseTranslationRepository: Repository<DublinCoreLicenseTranslation>,
    private languagesService: LanguagesService,
  ) {}

  count() {
    return this.dublinCoreLicenseRepository.count();
  }

  create() {
    const dublinCoreLicense = new DublinCoreLicense();
    return this.dublinCoreLicenseRepository.save(dublinCoreLicense);
  }

  async createTranslation(
    createDublinCoreLicenseTranslationDto: CreateDublinCoreLicenseTranslationDto,
  ) {
    const dublinCoreLicense = await this.findOne({
      where: { id: createDublinCoreLicenseTranslationDto.dublinCoreLicenseId },
    });
    if (dublinCoreLicense === null) return null;

    const language = await this.languagesService.findOne({
      where: { id: createDublinCoreLicenseTranslationDto.languageId },
    });
    if (language === null) return null;

    const dublinCoreLicenseTranslation = new DublinCoreLicenseTranslation(
      createDublinCoreLicenseTranslationDto,
      dublinCoreLicense,
      language,
    );

    return this.dublinCoreLicenseTranslationRepository.save(
      dublinCoreLicenseTranslation,
    );
  }

  private async findOne(options?: FindOneOptions<DublinCoreLicense>) {
    return this.dublinCoreLicenseRepository.findOne(options);
  }
}
