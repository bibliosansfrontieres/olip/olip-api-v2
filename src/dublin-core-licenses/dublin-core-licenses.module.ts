import { Module } from '@nestjs/common';
import { DublinCoreLicensesService } from './dublin-core-licenses.service';
import { DublinCoreLicensesController } from './dublin-core-licenses.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DublinCoreLicense } from './entities/dublin-core-license.entity';
import { DublinCoreLicenseTranslation } from './entities/dublin-core-license-translation.entity';
import { LanguagesModule } from 'src/languages/languages.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([DublinCoreLicense, DublinCoreLicenseTranslation]),
    LanguagesModule,
  ],
  controllers: [DublinCoreLicensesController],
  providers: [DublinCoreLicensesService],
  exports: [DublinCoreLicensesService],
})
export class DublinCoreLicensesModule {}
