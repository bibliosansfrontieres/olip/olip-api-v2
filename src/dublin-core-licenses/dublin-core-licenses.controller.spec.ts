import { Test, TestingModule } from '@nestjs/testing';
import { DublinCoreLicensesController } from './dublin-core-licenses.controller';
import { DublinCoreLicensesService } from './dublin-core-licenses.service';
import { DublinCoreLicense } from './entities/dublin-core-license.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DublinCoreLicenseTranslation } from './entities/dublin-core-license-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';

describe('DublinCoreLicensesController', () => {
  let controller: DublinCoreLicensesController;
  let dublinCoreLicenseRepository: Repository<DublinCoreLicense>;
  let dublinCoreLicenseTranslationRepository: Repository<DublinCoreLicenseTranslation>;

  const DUBLIN_CORE_LICENSE_REPOSITORY_TOKEN =
    getRepositoryToken(DublinCoreLicense);
  const DUBLIN_CORE_LICENSE_TRANSLATION_REPOSITORY_TOKEN = getRepositoryToken(
    DublinCoreLicenseTranslation,
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DublinCoreLicensesController],
      providers: [
        {
          provide: DUBLIN_CORE_LICENSE_REPOSITORY_TOKEN,
          useClass: Repository<DublinCoreLicense>,
        },
        {
          provide: DUBLIN_CORE_LICENSE_TRANSLATION_REPOSITORY_TOKEN,
          useClass: Repository<DublinCoreLicenseTranslation>,
        },
        DublinCoreLicensesService,
        LanguagesService,
      ],
    })
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .compile();

    controller = module.get<DublinCoreLicensesController>(
      DublinCoreLicensesController,
    );
    dublinCoreLicenseRepository = module.get<Repository<DublinCoreLicense>>(
      DUBLIN_CORE_LICENSE_REPOSITORY_TOKEN,
    );
    dublinCoreLicenseTranslationRepository = module.get<
      Repository<DublinCoreLicenseTranslation>
    >(DUBLIN_CORE_LICENSE_TRANSLATION_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
