import { IsNumber, IsString } from 'class-validator';

export class CreateDublinCoreLicenseTranslationDto {
  @IsString()
  label: string;

  @IsString()
  description: string;

  @IsNumber()
  dublinCoreLicenseId: number;

  @IsNumber()
  languageId: number;
}
