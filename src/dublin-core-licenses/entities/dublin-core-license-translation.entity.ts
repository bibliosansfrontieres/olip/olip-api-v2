import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { DublinCoreLicense } from './dublin-core-license.entity';
import { Language } from 'src/languages/entities/language.entity';
import { CreateDublinCoreLicenseTranslationDto } from '../dto/create-dublin-core-license-translation.dto';

@Entity()
export class DublinCoreLicenseTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  description: string;

  @ManyToOne(
    () => DublinCoreLicense,
    (dublinCoreLicense) => dublinCoreLicense.dublinCoreLicenseTranslations,
  )
  dublinCoreLicense: DublinCoreLicense;

  @ManyToOne(
    () => Language,
    (language) => language.dublinCoreLicenseTranslations,
  )
  language: Language;

  constructor(
    createDublinCoreLicenseTranslationDto: CreateDublinCoreLicenseTranslationDto,
    dublinCoreLicense: DublinCoreLicense,
    language: Language,
  ) {
    if (!createDublinCoreLicenseTranslationDto) return;
    this.label = createDublinCoreLicenseTranslationDto.label;
    this.description = createDublinCoreLicenseTranslationDto.description;
    this.dublinCoreLicense = dublinCoreLicense;
    this.language = language;
  }
}
