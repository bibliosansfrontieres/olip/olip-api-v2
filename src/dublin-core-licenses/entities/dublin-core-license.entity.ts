import { Entity, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { DublinCoreLicenseTranslation } from './dublin-core-license-translation.entity';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';

@Entity()
export class DublinCoreLicense {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToMany(
    () => DublinCoreItem,
    (dublinCoreItem) => dublinCoreItem.dublinCoreLicenses,
  )
  dublinCoreItems: DublinCoreItem[];

  @OneToMany(
    () => DublinCoreLicenseTranslation,
    (dublinCoreLicenseTranslation) =>
      dublinCoreLicenseTranslation.dublinCoreLicense,
  )
  dublinCoreLicenseTranslations: DublinCoreLicenseTranslation[];
}
