import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Language } from 'src/languages/entities/language.entity';
import { ItemDocumentType } from './item-document-type.entity';

@Entity()
export class ItemDocumentTypeTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  description: string;

  @ManyToOne(
    () => ItemDocumentType,
    (itemDocumentType) => itemDocumentType.itemDocumentTypeTranslations,
  )
  itemDocumentType: ItemDocumentType;

  @ManyToOne(
    () => Language,
    (language) => language.itemDocumentTypeTranslations,
  )
  language: Language;
}
