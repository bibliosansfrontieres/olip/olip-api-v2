import { Test, TestingModule } from '@nestjs/testing';
import { ItemDocumentTypesController } from './item-document-types.controller';
import { ItemDocumentTypesService } from './item-document-types.service';

describe('ItemDocumentTypesController', () => {
  let controller: ItemDocumentTypesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ItemDocumentTypesController],
      providers: [ItemDocumentTypesService],
    }).compile();

    controller = module.get<ItemDocumentTypesController>(
      ItemDocumentTypesController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
