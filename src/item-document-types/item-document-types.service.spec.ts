import { Test, TestingModule } from '@nestjs/testing';
import { ItemDocumentTypesService } from './item-document-types.service';

describe('ItemDocumentTypesService', () => {
  let service: ItemDocumentTypesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ItemDocumentTypesService],
    }).compile();

    service = module.get<ItemDocumentTypesService>(ItemDocumentTypesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
