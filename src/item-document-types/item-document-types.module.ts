import { Module } from '@nestjs/common';
import { ItemDocumentTypesService } from './item-document-types.service';
import { ItemDocumentTypesController } from './item-document-types.controller';

@Module({
  controllers: [ItemDocumentTypesController],
  providers: [ItemDocumentTypesService],
})
export class ItemDocumentTypesModule {}
