import {
  Column,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PermissionTranslation } from './permission-translation.entity';
import { Role } from 'src/roles/entities/role.entity';
import { CreatePermissionDto } from '../dto/create-permission.dto';
import { PermissionEnum } from '../enums/permission.enum';

@Entity()
export class Permission {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: PermissionEnum;

  @ManyToMany(() => Role, (role) => role.permissions)
  roles: Role[];

  @OneToMany(
    () => PermissionTranslation,
    (permissionTranslation) => permissionTranslation.permission,
  )
  permissionTranslations: PermissionTranslation[];

  constructor(createPermissionDto: CreatePermissionDto) {
    if (!createPermissionDto) return;
    this.name = createPermissionDto.name;
    this.permissionTranslations = [];
  }
}
