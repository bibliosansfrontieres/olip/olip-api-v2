import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Permission } from './permission.entity';
import { Language } from 'src/languages/entities/language.entity';
import { CreatePermissionTranslationDto } from '../dto/create-permission-translation.dto';

@Entity()
export class PermissionTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  description: string;

  @ManyToOne(
    () => Permission,
    (permission) => permission.permissionTranslations,
  )
  permission: Permission;

  @ManyToOne(() => Language, (language) => language.permissionTranslations)
  language: Language;

  constructor(
    createPermissionTranslationDto: CreatePermissionTranslationDto,
    language: Language,
    permission: Permission,
  ) {
    if (!createPermissionTranslationDto) return;
    this.label = createPermissionTranslationDto.label;
    this.description = createPermissionTranslationDto.description;
    this.language = language;
    this.permission = permission;
  }
}
