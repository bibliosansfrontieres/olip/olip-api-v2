export enum PermissionEnum {
  DEPLOY_PROCESS_ID = 'deploy_process_id',
  DEPLOY_STATUS = 'deploy_status',
  INSTALL_BROKEN_PLAYLIST = 'install_broken_playlist',
  CREATE_USER = 'create_user',
  READ_USERS = 'read_users',
  UPDATE_USER = 'update_user',
  DELETE_USER = 'delete_user',
  UPDATE_SETTING = 'update_setting',
  UPDATE_CATALOG = 'update_catalog',
  CREATE_EDITO = 'create_edito',
  DELETE_EDITO = 'delete_edito',
  READ_STORAGE = 'read_storage',
  READ_SSID = 'read_ssid',
  UPDATE_HIGHLIGHT = 'update_highlight',
  UPDATE_CATEGORY = 'update_category',
  CREATE_CATEGORY = 'create_category',
  READ_ACTIVITY = 'read_activity',
  MANAGE_APPLICATIONS = 'manage_applications',
  UPDATE_PERSONALIZATION = 'update_personalization',
  CHECK_UPDATES = 'check_updates',
}

export const permissionsTrads = [
  {
    name: PermissionEnum.DEPLOY_PROCESS_ID,
    label: 'Deploy process ID',
    description: 'Deploy a new process ID',
  },
  {
    name: PermissionEnum.DEPLOY_STATUS,
    label: 'Deploy status',
    description: 'See the actual status of deploying process',
  },
  {
    name: PermissionEnum.INSTALL_BROKEN_PLAYLIST,
    label: 'Install broken playlist',
    description: 'Reinstall a broken playlist',
  },
  {
    name: PermissionEnum.CREATE_USER,
    label: 'Create user',
    description: 'Create a user',
  },
  {
    name: PermissionEnum.READ_USERS,
    label: 'List users',
    description: 'List all users',
  },
  {
    name: PermissionEnum.UPDATE_USER,
    label: 'Update user',
    description: 'Update a user',
  },
  {
    name: PermissionEnum.DELETE_USER,
    label: 'Delete user',
    description: 'Delete a user',
  },
  {
    name: PermissionEnum.UPDATE_SETTING,
    label: 'Update setting',
    description: 'Update a setting',
  },
  {
    name: PermissionEnum.UPDATE_CATALOG,
    label: 'Update catalog',
    description: 'Update the catalog',
  },
  {
    name: PermissionEnum.CREATE_EDITO,
    label: 'Create edito',
    description: 'Create an edito',
  },
  {
    name: PermissionEnum.DELETE_EDITO,
    label: 'Delete edito',
    description: 'Delete an edito',
  },
  {
    name: PermissionEnum.READ_STORAGE,
    label: 'Read storage',
    description: 'Read storage informations',
  },
  {
    name: PermissionEnum.READ_SSID,
    label: 'Read SSID',
    description: 'Read hotspot SSID',
  },
  {
    name: PermissionEnum.UPDATE_HIGHLIGHT,
    label: 'Update highlight',
    description: 'Update highlight and highlight contents',
  },
  {
    name: PermissionEnum.UPDATE_CATEGORY,
    label: 'Update category',
    description: 'Update category',
  },
  {
    name: PermissionEnum.CREATE_CATEGORY,
    label: 'Create category',
    description: 'Create category',
  },
  {
    name: PermissionEnum.READ_ACTIVITY,
    label: 'Read activity',
    description: 'Read activity',
  },
  {
    name: PermissionEnum.MANAGE_APPLICATIONS,
    label: 'Manage applications',
    description: 'Manage applications',
  },
  {
    name: PermissionEnum.UPDATE_PERSONALIZATION,
    label: 'Update frontend color & logo',
    description: 'Update frontend color & logo',
  },
  {
    name: PermissionEnum.CHECK_UPDATES,
    label: 'Check updates',
    description: 'Check available updates',
  },
];
