import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Permission } from './entities/permission.entity';
import { Repository } from 'typeorm';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { PermissionTranslation } from './entities/permission-translation.entity';
import { CreatePermissionTranslationDto } from './dto/create-permission-translation.dto';
import { LanguagesService } from 'src/languages/languages.service';
import { PermissionEnum } from './enums/permission.enum';
import { PermissionDto } from './dto/permission.dto';
import { invalidateCache } from 'src/utils/caching.util';

@Injectable()
export class PermissionsService {
  constructor(
    @InjectRepository(Permission)
    private permissionRepository: Repository<Permission>,
    @InjectRepository(PermissionTranslation)
    private permissionTranslationRepository: Repository<PermissionTranslation>,
    private languagesService: LanguagesService,
  ) {}

  async create(
    createPermissionDto: CreatePermissionDto,
    createPermissionTranslationDto: CreatePermissionTranslationDto,
  ) {
    const language = await this.languagesService.findOne({
      where: { id: createPermissionTranslationDto.languageId },
    });
    if (language === null) return null;
    let permission = new Permission(createPermissionDto);
    permission = await this.permissionRepository.save(permission);
    const permissionTranslation = new PermissionTranslation(
      createPermissionTranslationDto,
      language,
      permission,
    );
    await this.permissionTranslationRepository.save(permissionTranslation);
    permission.permissionTranslations.push(permissionTranslation);
    await invalidateCache(Permission);
    return permission;
  }

  findAllPermissions() {
    return this.permissionRepository.find();
  }

  findOnePermissionByName(name: PermissionEnum) {
    return this.permissionRepository.findOne({ where: { name } });
  }

  async getPermissions(languageIdentifier: string): Promise<PermissionDto[]> {
    const permissions = await this.permissionRepository.find();
    for (const permission of permissions) {
      const translation = await this.languagesService.getTranslation<
        Permission,
        PermissionTranslation
      >(
        'permission',
        permission,
        this.permissionTranslationRepository,
        languageIdentifier,
      );
      permission.permissionTranslations = [translation];
    }
    return permissions;
  }
}
