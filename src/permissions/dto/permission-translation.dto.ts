import { ApiProperty } from '@nestjs/swagger';
import { LanguageDto } from 'src/languages/dto/language.dto';

export class PermissionTranslationDto {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: 'Update user' })
  label: string;

  @ApiProperty({ example: 'Update a user' })
  description: string;

  @ApiProperty({ type: LanguageDto })
  language: LanguageDto;
}
