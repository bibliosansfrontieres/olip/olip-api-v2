import { ApiProperty } from '@nestjs/swagger';
import { PermissionTranslationDto } from './permission-translation.dto';

export class PermissionDto {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: 'update_user' })
  name: string;

  @ApiProperty({ isArray: true, type: PermissionTranslationDto })
  permissionTranslations: PermissionTranslationDto[];
}
