import { IsNumber, IsString } from 'class-validator';

export class CreatePermissionTranslationDto {
  @IsString()
  label: string;

  @IsString()
  description: string;

  @IsNumber()
  languageId: number;
}
