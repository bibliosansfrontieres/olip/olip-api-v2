import { IsEnum } from 'class-validator';
import { PermissionEnum } from '../enums/permission.enum';

export class CreatePermissionDto {
  @IsEnum(PermissionEnum)
  name: PermissionEnum;
}
