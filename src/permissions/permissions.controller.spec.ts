import { Test, TestingModule } from '@nestjs/testing';
import { PermissionsController } from './permissions.controller';
import { PermissionsService } from './permissions.service';
import { Repository } from 'typeorm';
import { Permission } from './entities/permission.entity';
import { PermissionTranslation } from './entities/permission-translation.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LanguagesService } from 'src/languages/languages.service';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';

describe('PermissionsController', () => {
  let controller: PermissionsController;
  let permissionRepository: Repository<Permission>;
  let permissionTranslationRepository: Repository<PermissionTranslation>;

  const PERMISSION_REPOSITORY_TOKEN = getRepositoryToken(Permission);
  const PERMISSION_TRANSLATION_REPOSITORY_TOKEN = getRepositoryToken(
    PermissionTranslation,
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PermissionsController],
      providers: [
        PermissionsService,
        {
          provide: PERMISSION_REPOSITORY_TOKEN,
          useClass: Repository<Permission>,
        },
        {
          provide: PERMISSION_TRANSLATION_REPOSITORY_TOKEN,
          useClass: Repository<PermissionTranslation>,
        },
        LanguagesService,
      ],
    })
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .compile();

    controller = module.get<PermissionsController>(PermissionsController);
    permissionRepository = module.get<Repository<Permission>>(
      PERMISSION_REPOSITORY_TOKEN,
    );
    permissionTranslationRepository = module.get<
      Repository<PermissionTranslation>
    >(PERMISSION_TRANSLATION_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('GET /', () => {
    it('no DTO to test here', () => {
      expect(true).toBe(true);
    });
  });
});
