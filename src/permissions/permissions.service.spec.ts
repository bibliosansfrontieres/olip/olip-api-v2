import { Test, TestingModule } from '@nestjs/testing';
import { PermissionsService } from './permissions.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Permission } from './entities/permission.entity';
import { Repository } from 'typeorm';
import { PermissionTranslation } from './entities/permission-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';
import { permissionMock } from 'src/tests/mocks/objects/permission.mock';
import { permissionTranslationMock } from 'src/tests/mocks/objects/permission-translation.mock';
import { NotFoundException } from '@nestjs/common';

describe('PermissionsService', () => {
  let service: PermissionsService;
  let permissionRepository: Repository<Permission>;
  let permissionTranslationRepository: Repository<PermissionTranslation>;
  let languagesService: LanguagesService;

  const PERMISSION_REPOSITORY_TOKEN = getRepositoryToken(Permission);
  const PERMISSION_TRANSLATION_REPOSITORY_TOKEN = getRepositoryToken(
    PermissionTranslation,
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PermissionsService,
        {
          provide: PERMISSION_REPOSITORY_TOKEN,
          useClass: Repository<Permission>,
        },
        {
          provide: PERMISSION_TRANSLATION_REPOSITORY_TOKEN,
          useClass: Repository<PermissionTranslation>,
        },
        LanguagesService,
      ],
    })
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .compile();

    service = module.get<PermissionsService>(PermissionsService);
    permissionRepository = module.get<Repository<Permission>>(
      PERMISSION_REPOSITORY_TOKEN,
    );
    permissionTranslationRepository = module.get<
      Repository<PermissionTranslation>
    >(PERMISSION_TRANSLATION_REPOSITORY_TOKEN);
    languagesService = module.get<LanguagesService>(LanguagesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getPermissions()', () => {
    const permissionsArray = [permissionMock(), permissionMock()];
    it('should return an array of roles', async () => {
      jest
        .spyOn(permissionRepository, 'find')
        .mockResolvedValue(permissionsArray);
      jest
        .spyOn(languagesService, 'getTranslation')
        .mockResolvedValue(permissionTranslationMock());
      const result = await service.getPermissions('fra');
      expect(result).toBeDefined();
      expect(result).toEqual(permissionsArray);
    });
    it('should throw error if not language found', async () => {
      jest
        .spyOn(permissionRepository, 'find')
        .mockResolvedValue(permissionsArray);
      jest
        .spyOn(languagesService, 'getTranslation')
        .mockRejectedValue(new NotFoundException());
      await expect(service.getPermissions('fra')).rejects.toThrowError();
    });
  });
});
