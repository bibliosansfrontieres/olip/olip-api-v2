import { Controller, Get, Query } from '@nestjs/common';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { PermissionsService } from './permissions.service';
import { PermissionDto } from './dto/permission.dto';
import { LanguageNotFound } from 'src/languages/responses/errors/language-not-found';
import { cachedMethod } from 'src/utils/caching.util';
import { Permission } from './entities/permission.entity';

@ApiTags('permissions')
@Controller('api/permissions')
export class PermissionsController {
  constructor(private permissionsService: PermissionsService) {}

  @Get()
  @ApiOperation({ summary: 'Get all permissions' })
  @ApiOkResponse({ type: PermissionDto, isArray: true })
  @ApiNotFoundResponse({ type: LanguageNotFound })
  @ApiQuery({ name: 'languageIdentifier', required: true })
  getPermissions(@Query('languageIdentifier') languageIdentifier: string) {
    return cachedMethod(
      'permissionsController',
      'getPermissions',
      [{ languageIdentifier }],
      async () => {
        return this.permissionsService.getPermissions(languageIdentifier);
      },
      [Permission],
    );
  }
}
