import { Test, TestingModule } from '@nestjs/testing';
import { DublinCoreLanguagesService } from './dublin-core-languages.service';
import { Repository } from 'typeorm';
import { DublinCoreLanguage } from './entities/dublin-core-language.entity';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('DublinCoreLanguagesService', () => {
  let service: DublinCoreLanguagesService;

  beforeEach(async () => {
    let dublinCoreLanguageRepository: Repository<DublinCoreLanguage>;
    const DUBLIN_CORE_LANGUAGE_REPOSITORY_TOKEN =
      getRepositoryToken(DublinCoreLanguage);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: DUBLIN_CORE_LANGUAGE_REPOSITORY_TOKEN,
          useClass: Repository<DublinCoreLanguage>,
        },
        DublinCoreLanguagesService,
      ],
    }).compile();

    service = module.get<DublinCoreLanguagesService>(
      DublinCoreLanguagesService,
    );
    dublinCoreLanguageRepository = module.get<Repository<DublinCoreLanguage>>(
      DUBLIN_CORE_LANGUAGE_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
