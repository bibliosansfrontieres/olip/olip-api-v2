import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Language } from 'src/languages/entities/language.entity';
import { LanguageType } from '../enums/language-type.enum';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';

@Entity()
export class DublinCoreLanguage {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: LanguageType;

  @ManyToOne(
    () => DublinCoreItem,
    (dublinCoreItem) => dublinCoreItem.dublinCoreLanguages,
    { onDelete: 'CASCADE' },
  )
  dublinCoreItem: DublinCoreItem;

  @ManyToOne(() => Language, (language) => language.dublinCoreLanguages)
  language: Language;

  constructor(
    dublinCoreItem: DublinCoreItem,
    type: LanguageType,
    language: Language,
  ) {
    if (!dublinCoreItem || !language || !type) return;
    this.dublinCoreItem = dublinCoreItem;
    this.language = language;
    this.type = type;
  }
}
