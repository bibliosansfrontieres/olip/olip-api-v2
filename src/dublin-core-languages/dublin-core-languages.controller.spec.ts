import { Test, TestingModule } from '@nestjs/testing';
import { DublinCoreLanguagesController } from './dublin-core-languages.controller';
import { DublinCoreLanguagesService } from './dublin-core-languages.service';
import { Repository } from 'typeorm';
import { DublinCoreLanguage } from './entities/dublin-core-language.entity';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('DublinCoreLanguagesController', () => {
  let controller: DublinCoreLanguagesController;

  beforeEach(async () => {
    let dublinCoreLanguageRepository: Repository<DublinCoreLanguage>;
    const DUBLIN_CORE_LANGUAGE_REPOSITORY_TOKEN =
      getRepositoryToken(DublinCoreLanguage);

    const module: TestingModule = await Test.createTestingModule({
      controllers: [DublinCoreLanguagesController],
      providers: [
        {
          provide: DUBLIN_CORE_LANGUAGE_REPOSITORY_TOKEN,
          useClass: Repository<DublinCoreLanguage>,
        },
        DublinCoreLanguagesService,
      ],
    }).compile();

    controller = module.get<DublinCoreLanguagesController>(
      DublinCoreLanguagesController,
    );
    dublinCoreLanguageRepository = module.get<Repository<DublinCoreLanguage>>(
      DUBLIN_CORE_LANGUAGE_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
