import { Module } from '@nestjs/common';
import { DublinCoreLanguagesService } from './dublin-core-languages.service';
import { DublinCoreLanguagesController } from './dublin-core-languages.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DublinCoreLanguage } from './entities/dublin-core-language.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DublinCoreLanguage])],
  controllers: [DublinCoreLanguagesController],
  providers: [DublinCoreLanguagesService],
  exports: [DublinCoreLanguagesService],
})
export class DublinCoreLanguagesModule {}
