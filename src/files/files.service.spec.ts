import { Test, TestingModule } from '@nestjs/testing';
import { FilesService } from './files.service';
import { Repository } from 'typeorm';
import { File } from './entities/file.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ItemsService } from 'src/items/items.service';
import { itemsServiceMock } from 'src/tests/mocks/providers/items-service.mock';

describe('FilesService', () => {
  let service: FilesService;
  let filesRepository: Repository<File>;
  const FILES_REPOSITORY_TOKEN = getRepositoryToken(File);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: FILES_REPOSITORY_TOKEN, useClass: Repository<File> },
        FilesService,
        ItemsService,
      ],
    })
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .compile();

    service = module.get<FilesService>(FilesService);
    filesRepository = module.get<Repository<File>>(FILES_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
