import { Injectable } from '@nestjs/common';
import { File } from './entities/file.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class FilesService {
  constructor(
    @InjectRepository(File) private filesRepository: Repository<File>,
  ) {}

  async save(file: File) {
    return this.filesRepository.save(file);
  }

  async getFilesTotalSize(): Promise<number> {
    const files = await this.filesRepository.find({ select: { size: true } });
    return files.reduce<number>((sum, file) => sum + parseInt(file.size), 0);
  }
}
