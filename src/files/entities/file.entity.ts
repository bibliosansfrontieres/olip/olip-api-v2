import { Item } from 'src/items/entities/item.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { IMaestroItem } from 'src/maestro/interfaces/maestro-item.interface';

@Entity()
export class File {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  path: string;

  @Column()
  size: string;

  @Column()
  duration: string;

  @Column()
  pages: string;

  @Column()
  extension: string;

  @Column()
  mimeType: string;

  @OneToOne(() => Item, (item) => item.file, { onDelete: 'CASCADE' })
  @JoinColumn()
  item: Item;

  constructor(maestroItem: IMaestroItem) {
    if (!maestroItem) return;
    this.name = maestroItem.file.name;
    this.path = maestroItem.file.path;
    this.size = maestroItem.file.size;
    this.duration = maestroItem.file.duration;
    this.pages = maestroItem.file.pages;
    this.extension = maestroItem.file.extension;
    this.mimeType = maestroItem.file.mimeType;
  }
}
