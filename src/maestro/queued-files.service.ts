import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { QueuedFile } from './entities/queued-file.entity';
import { FindManyOptions, Repository } from 'typeorm';
import { existsSync, rmSync } from 'fs';
import { CreateQueuedFileDto } from './dto/create-queued-file.dto';

@Injectable()
export class QueuedFilesService {
  constructor(
    @InjectRepository(QueuedFile)
    private queueFileRepository: Repository<QueuedFile>,
  ) {}

  create(createQueuedFileDto: CreateQueuedFileDto) {
    const fileQueue = new QueuedFile(createQueuedFileDto);
    return this.queueFileRepository.save(fileQueue);
  }

  async findAll(options?: FindManyOptions<QueuedFile>) {
    return this.queueFileRepository.find(options);
  }

  delete(queuedFile: QueuedFile, deleteFiles: boolean = false) {
    const isFileDownloaded = existsSync(queuedFile.filePath);
    if (deleteFiles) {
      if (isFileDownloaded) {
        rmSync(queuedFile.filePath);
      }
      if (queuedFile.thumbnailUrl) {
        const isThumbnailDownloaded = existsSync(queuedFile.imagePath);
        if (isThumbnailDownloaded) {
          rmSync(queuedFile.imagePath);
        }
      }
    }
    return this.queueFileRepository.remove(queuedFile);
  }

  save(queuedFile: QueuedFile) {
    return this.queueFileRepository.save(queuedFile);
  }
}
