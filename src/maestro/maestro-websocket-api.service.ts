import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';
import { DeployStatusEnum } from './enums/deploy-status.enum';
import { getVmSaveId } from 'src/utils/get-vm-save-id.util';

@Injectable()
export class MaestroWebsocketApiService {
  private maestroWebsocketApiUrl: string;

  constructor(private configService: ConfigService) {
    this.maestroWebsocketApiUrl = configService.get<string>(
      'MAESTRO_SOCKET_API_URL',
    );
  }

  async postDeploy(status: DeployStatusEnum) {
    try {
      const vmSaveId = getVmSaveId();
      if (!vmSaveId) {
        console.error('no virtual machine save id found');
        return;
      }
      const result = await axios.post(`${this.maestroWebsocketApiUrl}/deploy`, {
        status,
        virtualMachineSaveId: vmSaveId,
      });
      if (result.status !== 200) {
        console.error(result.data);
        throw new BadRequestException('failed to deploy');
      } else {
        Logger.log(
          'Sent deploy informations to websocket-client',
          'MaestroWebsocketApiService',
        );
      }
      return;
    } catch (e) {
      console.error(e);
      return;
    }
  }
}
