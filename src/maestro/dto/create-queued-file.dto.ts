export class CreateQueuedFileDto {
  thumbnailUrl: string;
  fileUrl: string;
  itemId: string;
  fileExtension: string;
  thumbnailExtension: string;
  isPlaylist?: boolean;
}
