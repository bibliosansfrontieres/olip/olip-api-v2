import { Inject, Injectable, Logger, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { QueuedFile } from './entities/queued-file.entity';
import { Repository } from 'typeorm';
import { join } from 'path';
import { ItemsService } from 'src/items/items.service';
import { DockerService } from 'src/docker/docker.service';
import { copyFileSync, existsSync, rmSync } from 'fs';
import { ApplicationsService } from 'src/applications/applications.service';
import { Item } from 'src/items/entities/item.entity';
import { SocketGateway } from 'src/socket/socket.gateway';
import { QueuedFilesService } from './queued-files.service';
import { sleep } from 'src/utils/sleep.util';

@Injectable()
export class InstallService {
  private isQueueRunning: boolean;
  private isQueueNeedsRestart: boolean;

  constructor(
    @InjectRepository(QueuedFile)
    private queueFileRepository: Repository<QueuedFile>,
    @Inject(forwardRef(() => ItemsService))
    private itemsService: ItemsService,
    private dockerService: DockerService,
    @Inject(forwardRef(() => ApplicationsService))
    private applicationsService: ApplicationsService,
    private socketGateway: SocketGateway,
    private queuedFilesService: QueuedFilesService,
  ) {
    this.isQueueRunning = false;
    this.isQueueNeedsRestart = false;
  }

  onModuleInit() {
    this.runQueue();
  }

  private async restartQueue(): Promise<void> {
    const restartTimeout = 5000;
    Logger.warn(
      `Restarting install queue in ${Math.round(
        restartTimeout / 1000,
      )} seconds...`,
      'InstallService',
    );
    await sleep(restartTimeout);
    return this.processQueue();
  }

  runQueue(): Promise<void> {
    if (this.isQueueRunning) {
      this.isQueueNeedsRestart = true;
      return;
    }
    this.isQueueRunning = true;
    return this.processQueue();
  }

  private stopQueue(): Promise<void> {
    this.isQueueRunning = false;
    if (this.isQueueNeedsRestart) {
      this.isQueueNeedsRestart = false;
      return this.runQueue();
    }
    return;
  }

  private async processQueue(): Promise<void> {
    const queuedFiles = await this.queueFileRepository.find({
      where: { isInstallationNeeded: true },
    });

    // If no files to install found, stop queue
    if (queuedFiles.length === 0) {
      return this.stopQueue();
    }

    Logger.log(
      'Install queue started (installing applications contents)...',
      'InstallService',
    );

    await this.startInstallations(queuedFiles);

    const queuedFilesRemaining = await this.queuedFilesService.findAll({
      where: { isInstallationNeeded: true },
    });

    if (queuedFilesRemaining.length > 0) {
      return this.restartQueue();
    }

    Logger.log('Install queue finished !', 'InstallService');
    return this.stopQueue();
  }

  private async startInstallations(queuedFiles: QueuedFile[]) {
    const totalFiles = +queuedFiles.length;
    while (queuedFiles.length > 0) {
      Logger.log(
        `${
          queuedFiles.length
        }/${totalFiles} files remaining to finish install queue (${
          100 - Math.round((queuedFiles.length * 100) / totalFiles)
        }%)`,
        'InstallService',
      );
      const queuedFile = queuedFiles.shift();
      await this.installQueuedFile(queuedFile);
    }
  }

  private async installQueuedFile(queuedFile: QueuedFile) {
    const item = await this.itemsService.findOne({
      relations: { application: true, file: true },
      where: { id: queuedFile.itemId },
    });

    if (!item) {
      Logger.error(
        `Item ${queuedFile.itemId} not found, deleting QueuedFile...`,
        'InstallService',
      );
      await this.queuedFilesService.delete(queuedFile, true);
      return;
    }

    if (item.application.id === 'olip') {
      Logger.error(
        `Tried to install item from OLIP, deleting QueuedFile...`,
        'InstallService',
      );
      await this.queuedFilesService.delete(queuedFile, true);
      return;
    }

    Logger.log(`Installing ${queuedFile.fileUrl}...`, 'InstallService');
    const isInstalled = await this.installFile(queuedFile, item);
    if (isInstalled) {
      Logger.log(`Installed ${queuedFile.fileUrl} !`, 'InstallService');
    } else {
      Logger.log(`Installed ${queuedFile.fileUrl} !`, 'InstallService');
    }
  }

  private async installFile(queuedFile: QueuedFile, item: Item) {
    if (item.application.id === 'olip') return true;
    const application = item.application;
    try {
      const applicationPath =
        this.dockerService.getApplicationPath(application);
      const contentsPath = join(applicationPath, 'data/content');
      const newFilePath = join(contentsPath, queuedFile.fileName);
      if (!existsSync(newFilePath)) {
        copyFileSync(queuedFile.filePath, newFilePath);
        rmSync(queuedFile.filePath);
      }
      // TODO : every file name should be filename without extension
      item.file.name = queuedFile.fileName;

      const isInstalled = await this.applicationsService.installContent(
        item,
        item.application,
      );
      if (!isInstalled) {
        Logger.error(
          `Error while installing content ${item.file.name} in ${item.application.name}`,
          'InstallService',
        );
        return false;
      }

      item.isInstalled = true;
      item.url = await this.applicationsService.getContentUrl(
        application,
        item,
      );

      await this.itemsService.save(item);
      await this.queuedFilesService.delete(queuedFile);
      await this.socketGateway.emitStorage();
    } catch (error) {
      console.error(error);
      Logger.error(
        `Error while installing content ${item.file.name} in ${item.application.name}`,
        'InstallService',
      );
    }
  }
}
