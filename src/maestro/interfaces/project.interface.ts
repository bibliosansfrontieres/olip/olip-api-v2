export interface IProject {
  id: number;
  url: string;
  createdAt: string;
  updatedAt: string;
  title: string;
  date: string;
  languages: string;
  startDate: string;
  endDate: string;
  partners: string;
  location: string;
  device: string;
  projectManager: string;
}
