import { ApplicationTypeNameEnum } from '../enums/application-type-name.enum';
import { IMaestroApplicationTypeTranslation } from './maestro-application-type-translation.interface';

export interface IMaestroApplicationType {
  id: string;
  name: ApplicationTypeNameEnum;
  md5: string;
  applicationTypeTranslations: IMaestroApplicationTypeTranslation[];
}
