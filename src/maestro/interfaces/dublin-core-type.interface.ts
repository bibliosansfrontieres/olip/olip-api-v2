import { IDublinCoreTranslation } from './dublin-core-type-translation.interface';

export interface IDublinCoreType {
  id: number;
  image: string;
  dublinCoreTypeTranslations: IDublinCoreTranslation[];
}
