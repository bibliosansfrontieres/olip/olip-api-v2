import { IMaestroApplication } from './maestro-application.interface';
import { IMaestroCategory } from './maestro-category.interface';
import { IMaestroItem } from './maestro-item.interface';
import { IMaestroPlaylist } from './maestro-playlist.interface';
import { IMaestroProject } from './maestro-project.interface';

export interface IMaestroFullProject {
  project: IMaestroProject;
  applications: IMaestroApplication[];
  categories: IMaestroCategory[];
  playlists: IMaestroPlaylist[];
  items: IMaestroItem[];
}
