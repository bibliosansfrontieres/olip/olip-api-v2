export interface IMaestroCategoryTranslation {
  id: number;
  title: string;
  description: string | null;
  languageIdentifier: string;
}
