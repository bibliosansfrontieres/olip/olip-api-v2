export interface IDublinCoreTranslation {
  id: number;
  label: string;
  description: string;
  languageIdentifier: string;
}
