import { IMaestroApplicationTranslation } from './maestro-application-translation.interface';
import { IMaestroApplicationType } from './maestro-application-type.interface';

export interface IMaestroApplication {
  id: string;
  name: string;
  displayName: string;
  port: string;
  size: number;
  version: string;
  logo: string;
  image: string;
  compose: string;
  hooks: string;
  md5: string;
  exposeServices?: string;
  applicationType: IMaestroApplicationType;
  applicationTranslations: IMaestroApplicationTranslation[];
}
