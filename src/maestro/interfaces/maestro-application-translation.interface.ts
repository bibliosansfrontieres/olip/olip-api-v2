export interface IMaestroApplicationTranslation {
  id: number;
  shortDescription: string;
  longDescription: string;
  languageIdentifier: string;
}
