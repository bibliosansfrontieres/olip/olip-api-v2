export interface IMaestroApplicationTypeTranslation {
  id: number;
  label: string;
  description: string;
  languageIdentifier: string;
}
