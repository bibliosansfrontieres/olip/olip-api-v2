import { ICategoryTranslation } from './category-translation.interface';

export interface ICategory {
  id: number;
  pictogram: string;
  image: string;
  categoryTranslations: ICategoryTranslation[];
}
