import { ItemSourceEnum } from '../enums/item-source.enum';
import { IMaestroCategoryTranslation } from './maestro-category-translation.interface';

export interface IMaestroCategory {
  id: number;
  source: ItemSourceEnum;
  pictogram: string;
  categoryTranslations: IMaestroCategoryTranslation[];
}
