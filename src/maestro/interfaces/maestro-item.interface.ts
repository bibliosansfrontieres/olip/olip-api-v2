import { ItemSourceEnum } from '../enums/item-source.enum';
import { IMaestroDublinCoreItemTranslation } from './maestro-dublin-core-item-translation.interface';

export interface IMaestroItem {
  id: string;
  thumbnail: string;
  createdCountry: string;
  version?: string;
  source: ItemSourceEnum;
  dublinCoreItemTranslations: IMaestroDublinCoreItemTranslation[];
  file: {
    name: string;
    path: string;
    size: string;
    duration: string;
    pages: string;
    extension: string;
    mimeType: string;
  };
  applicationId: string;
  dublinCoreTypeId: number;
}
