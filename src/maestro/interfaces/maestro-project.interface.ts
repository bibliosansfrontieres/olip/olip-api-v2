import { ItemSourceEnum } from '../enums/item-source.enum';

export interface IMaestroProject {
  id: number;
  source: ItemSourceEnum;
  url: string;
  title: string;
  description: string;
  date: string;
  languages: string;
  startDate: string;
  endDate: string;
  partners: string;
  location: string;
  device: string;
  projectManager: string;
  playlistIds: number[];
}
