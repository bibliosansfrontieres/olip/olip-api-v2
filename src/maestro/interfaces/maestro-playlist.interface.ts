import { ItemSourceEnum } from '../enums/item-source.enum';
import { IMaestroPlaylistTranslation } from './maestro-playlist-translation.interface';

export interface IMaestroPlaylist {
  id: string;
  source: ItemSourceEnum;
  image: string;
  version?: string;
  applicationId: string;
  categoryId?: number;
  size: number;
  itemIds: string[];
  playlistTranslations: IMaestroPlaylistTranslation[];
}
