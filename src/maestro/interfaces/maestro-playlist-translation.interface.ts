export interface IMaestroPlaylistTranslation {
  title: string;
  description: string;
  languageIdentifier: string;
}
