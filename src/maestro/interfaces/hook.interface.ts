import { HookNamesEnum } from '../enums/hook-names.enum';

export interface IHook {
  name: HookNamesEnum;
  actions?: IHookAction[];
  returnValue?: string;
  returnExec?: string;
}

export interface IHookAction {
  exec?: string;
  docker?: string;
}
