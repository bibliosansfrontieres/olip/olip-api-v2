export interface ICategoryTranslation {
  id: number;
  title: string;
  description: string;
  languageIdentifier: string;
}
