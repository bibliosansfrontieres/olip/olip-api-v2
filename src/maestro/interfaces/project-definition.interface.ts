import { IDublinCoreType } from './dublin-core-type.interface';
import { IProject } from './project.interface';
import { IMaestroPlaylist } from './maestro-playlist.interface';
import { IMaestroItem } from './maestro-item.interface';
import { ICategory } from './category.interface';

export interface IProjectDefinition {
  olipProject: IProject;
  olipPlaylists: IMaestroPlaylist[];
  olipItems: IMaestroItem[];
  dublinCoreTypes: IDublinCoreType[];
  categories: ICategory[];
}
