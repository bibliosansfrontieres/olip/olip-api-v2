import { Inject, Injectable, Logger, forwardRef } from '@nestjs/common';
import { QueuedFile } from './entities/queued-file.entity';
import { join } from 'path';
import { downloadFile as download } from 'src/utils/download-file.util';
import { ItemsService } from 'src/items/items.service';
import { FilesService } from 'src/files/files.service';
import { Item } from 'src/items/entities/item.entity';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { ConfigService } from '@nestjs/config';
import { SocketGateway } from 'src/socket/socket.gateway';
import { QueuedFilesService } from './queued-files.service';
import { InstallService } from './install.service';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { v4 as uuid } from 'uuid';
import { DeployStatusEnum } from './enums/deploy-status.enum';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import { MaestroWebsocketApiService } from './maestro-websocket-api.service';
import { SettingsService } from 'src/settings/settings.service';
import { copyFileSync, rmSync } from 'fs';
import { DockerService } from 'src/docker/docker.service';

@Injectable()
export class DownloadService {
  private isQueueRunning: boolean;
  private isQueueNeedsRestart: boolean;

  constructor(
    @Inject(forwardRef(() => ItemsService))
    private itemsService: ItemsService,
    private filesService: FilesService,
    @Inject(forwardRef(() => PlaylistsService))
    private playlistsService: PlaylistsService,
    private configService: ConfigService,
    private socketGateway: SocketGateway,
    private queuedFilesService: QueuedFilesService,
    private installService: InstallService,
    private maestroWebsocketApiService: MaestroWebsocketApiService,
    private settingsService: SettingsService,
    private dockerService: DockerService,
  ) {
    this.isQueueRunning = false;
    this.isQueueNeedsRestart = false;
  }

  onModuleInit() {
    this.runQueue();
  }

  private stopQueue(): Promise<void> {
    this.isQueueRunning = false;
    if (this.isQueueNeedsRestart) {
      this.isQueueNeedsRestart = false;
      return this.runQueue();
    }

    return;
  }

  runQueue(): Promise<void> {
    if (this.isQueueRunning) {
      this.isQueueNeedsRestart = true;
      return;
    }
    this.isQueueRunning = true;
    return this.processQueue();
  }

  private async startDownloadingThreads(queuedFiles: QueuedFile[]) {
    const maxThreads = 10;
    const threads = [];
    const totalFiles = +queuedFiles.length;

    for (
      let threadIndex = 0;
      threadIndex < maxThreads && threadIndex < queuedFiles.length;
      threadIndex++
    ) {
      threads.push(this.downloadThread(threadIndex, totalFiles, queuedFiles));
    }

    await Promise.all(threads);
  }

  private async processQueue(): Promise<void> {
    const queuedFiles = await this.queuedFilesService.findAll({
      where: { isInstallationNeeded: false },
    });

    // If no files found, stop queue
    if (queuedFiles.length === 0) {
      return this.stopQueue();
    }

    Logger.log(
      'Download queue started (downloading files and thumbnails)...',
      'DownloadService',
    );

    await this.startDownloadingThreads(queuedFiles);

    const queuedFilesLeft = await this.queuedFilesService.findAll({
      where: { isInstallationNeeded: false },
    });
    if (queuedFilesLeft.length > 0) {
      Logger.warn(
        'Some files missing, restarting download queue...',
        'DownloadService',
      );
      return this.processQueue();
    }

    await this.checkForInstalledPlaylists();

    this.installService.runQueue();

    Logger.log('Download queue finished !', 'DownloadService');

    const vmId = this.configService.get('VM_ID');
    if (vmId === 'olip') {
      const isOlipInstalled = await this.settingsService.findOne(
        SettingKey.IS_OLIP_INSTALLED,
      );
      if (!isOlipInstalled || isOlipInstalled.value === 'false') {
        await this.settingsService.update(SettingKey.IS_OLIP_INSTALLED, {
          value: 'true',
        });
        await this.maestroWebsocketApiService.postDeploy(
          DeployStatusEnum.DEPLOYED,
        );
        Logger.log('OLIP deploy is finished !', 'MaestroService');
      }
    }

    return this.stopQueue();
  }

  private async checkForInstalledPlaylists() {
    const notInstalledPlaylists = await this.playlistsService.find({
      where: { isInstalled: false },
      relations: { items: true },
    });
    for (const playlist of notInstalledPlaylists) {
      let allInstalled = true;
      for (const item of playlist.items) {
        if (item.isInstalled === false) {
          allInstalled = false;
          break;
        }
      }
      if (allInstalled === true) {
        playlist.isInstalled = true;
        await this.playlistsService.save(playlist);
      }
    }
  }

  private async downloadThread(
    threadIndex: number,
    totalFiles: number,
    queuedFiles: QueuedFile[],
  ) {
    while (queuedFiles.length > 0) {
      Logger.log(
        `${
          queuedFiles.length
        }/${totalFiles} files remaining to finish download queue (${
          100 - Math.round((queuedFiles.length * 100) / totalFiles)
        }%)`,
        'DownloadService',
      );
      const queuedFile = queuedFiles.shift();
      Logger.log(
        `Thread [${threadIndex + 1}] - Downloading ${queuedFile.fileUrl}...`,
        'DownloadService',
      );
      await this.downloadFile(queuedFile);
      Logger.log(
        `Thread [${threadIndex + 1}] - Downloaded ${queuedFile.fileUrl} !`,
        'DownloadService',
      );
    }
  }

  private getPlaylistUrls(queuedFile: QueuedFile, playlist: Playlist) {
    let thumbnailUrl = queuedFile.thumbnailUrl;
    if (playlist.application.id !== 'olip') {
    } else {
      // If application is OLIP, download thumbnails from S3
      if (thumbnailUrl) {
        thumbnailUrl = `${this.configService.get(
          'S3_BUCKET_URL',
        )}/${thumbnailUrl}`;
      }
    }

    return {
      thumbnailUrl,
    };
  }

  private getFileUrls(queuedFile: QueuedFile, item: Item) {
    let fileUrl = queuedFile.fileUrl;
    let thumbnailUrl = queuedFile.thumbnailUrl;

    if (item.application.id !== 'olip') {
    } else {
      // If application is OLIP, download thumbnails from S3
      if (thumbnailUrl) {
        thumbnailUrl = `${this.configService.get(
          'S3_BUCKET_URL',
        )}/${thumbnailUrl}`;
      }
      fileUrl = `${this.configService.get('S3_BUCKET_URL')}/${fileUrl}`;
    }

    return { fileUrl, thumbnailUrl };
  }

  private getFileName(queuedFile: QueuedFile) {
    const fileNameSplit = queuedFile.fileUrl.split('/');
    let fileName = fileNameSplit[fileNameSplit.length - 1];
    if (!fileName.includes('.')) {
      fileName = fileName + `.${queuedFile.fileExtension}`;
    }
    return fileName;
  }

  private getFilePaths(queuedFile: QueuedFile) {
    const fileName = this.getFileName(queuedFile);
    const staticPath = join(__dirname, '../../data/static');
    const filePath = join(staticPath, 'items', fileName);
    let imagePath = '';
    let imageName = '';
    if (queuedFile.thumbnailUrl) {
      const thumbnailName = uuid();
      imageName = `${thumbnailName}.${queuedFile.thumbnailExtension}`;
      imagePath = join(staticPath, 'item_thumbnails', imageName);
    }
    return { filePath, fileName, imagePath, imageName };
  }

  private getPlaylistPaths(queuedFile: QueuedFile) {
    const staticPath = join(__dirname, '../../data/static');
    let imagePath = '';
    let imageName = '';
    if (queuedFile.thumbnailUrl) {
      const thumbnailName = uuid();
      imageName = `${thumbnailName}.${queuedFile.thumbnailExtension}`;
      imagePath = join(staticPath, 'playlist_thumbnails', imageName);
    }
    return { imagePath, imageName };
  }

  private async downloadFile(queuedFile: QueuedFile) {
    if (queuedFile.isPlaylist) {
      const playlist = await this.playlistsService.findOne({
        where: { id: queuedFile.itemId },
        relations: {
          application: true,
        },
      });

      if (!playlist) {
        console.error(queuedFile);
        Logger.error(
          `Playlist ${queuedFile.itemId} not found, deleting QueuedFile...`,
          'DownloadService',
        );
        await this.queuedFilesService.delete(queuedFile, true);
        return;
      }

      const { thumbnailUrl } = this.getPlaylistUrls(queuedFile, playlist);
      const { imagePath, imageName } = this.getPlaylistPaths(queuedFile);

      if (thumbnailUrl) {
        const isThumbnailDownloaded = await download(thumbnailUrl, imagePath);

        if (!isThumbnailDownloaded) return;
        playlist.image = `playlist_thumbnails/${imageName}`;
      }

      await this.playlistsService.save(playlist);
      await this.queuedFilesService.delete(queuedFile);
      await this.socketGateway.emitStorage();
      return;
    }
    const item = await this.itemsService.findOne({
      where: { id: queuedFile.itemId },
      relations: {
        application: true,
        file: true,
        dublinCoreItem: {
          dublinCoreType: { dublinCoreTypeTranslations: true },
        },
      },
    });

    if (!item) {
      console.error(queuedFile);
      Logger.error(
        `Item ${queuedFile.itemId} not found, deleting QueuedFile...`,
        'DownloadService',
      );
      await this.queuedFilesService.delete(queuedFile, true);
      return;
    }

    const { fileUrl, thumbnailUrl } = this.getFileUrls(queuedFile, item);
    const { filePath, fileName, imagePath, imageName } =
      this.getFilePaths(queuedFile);

    const isFileDownloaded = await download(fileUrl, filePath);

    if (!isFileDownloaded) return;
    item.file.path = `items/${fileName}`;

    if (thumbnailUrl) {
      const isThumbnailDownloaded = await download(thumbnailUrl, imagePath);

      if (!isThumbnailDownloaded) return;
      item.thumbnail = `item_thumbnails/${imageName}`;
    }

    item.isDownloading = false;

    await this.filesService.save(item.file);
    if (item.application.id !== 'olip') {
      queuedFile.filePath = filePath;
      queuedFile.fileName = fileName;
      queuedFile.imagePath = imagePath;
      queuedFile.imageName = imageName;
      queuedFile.isInstallationNeeded = true;
      await this.queuedFilesService.save(queuedFile);

      copyFileSync(
        filePath,
        join(
          this.dockerService.getApplicationPath(item.application),
          'data/content',
          fileName,
        ),
      );
      rmSync(filePath);
    } else {
      item.isInstalled = true;
      await this.queuedFilesService.delete(queuedFile);
    }
    await this.itemsService.save(item);
    await this.socketGateway.emitStorage();
  }
}
