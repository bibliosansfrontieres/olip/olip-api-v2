import {
  Inject,
  Injectable,
  Logger,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { ItemsService } from 'src/items/items.service';
import { QueuedFilesService } from './queued-files.service';
import mime from 'mime-types';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { MaestroApiService } from './maestro-api.service';
import { CategoriesService } from 'src/categories/categories.service';
import { CategoryPlaylistsService } from 'src/category-playlists/category-playlists.service';
import { SearchService } from 'src/search/search.service';
import { IMaestroPlaylist } from './interfaces/maestro-playlist.interface';
import { IMaestroItem } from './interfaces/maestro-item.interface';
import { IMaestroCategory } from './interfaces/maestro-category.interface';

@Injectable()
export class CreateService {
  constructor(
    @Inject(forwardRef(() => ItemsService))
    private itemsService: ItemsService,
    private queuedFilesService: QueuedFilesService,
    @Inject(forwardRef(() => PlaylistsService))
    private playlistsService: PlaylistsService,
    private maestroApiService: MaestroApiService,
    private categoriesService: CategoriesService,
    private categoryPlaylistsService: CategoryPlaylistsService,
    private searchService: SearchService,
  ) {}

  async createQueuedFiles() {
    const items = await this.itemsService.find({ relations: { file: true } });
    const playlists = await this.playlistsService.find();
    // TODO : manage applications items
    for (const item of items) {
      const thumbnailExtensionSplit = item.thumbnail.split('.');
      const thumbnailExtension =
        thumbnailExtensionSplit[thumbnailExtensionSplit.length - 1];
      await this.queuedFilesService.create({
        thumbnailUrl: `${item.thumbnail}`,
        fileUrl: `${item.file.path}`,
        itemId: item.id,
        fileExtension: item.file.extension,
        thumbnailExtension: thumbnailExtension,
        isPlaylist: false,
      });
    }
    for (const playlist of playlists) {
      const thumbnailExtensionSplit = playlist.image.split('.');
      const thumbnailExtension =
        thumbnailExtensionSplit[thumbnailExtensionSplit.length - 1];
      await this.queuedFilesService.create({
        thumbnailUrl: `${playlist.image}`,
        fileUrl: undefined,
        itemId: playlist.id,
        fileExtension: undefined,
        thumbnailExtension: thumbnailExtension,
        isPlaylist: true,
      });
    }
  }

  async createCategories(maestroCategories: IMaestroCategory[]) {
    for (const maestroCategory of maestroCategories) {
      const category = await this.categoriesService.create({
        sourceId: maestroCategory.id,
        createCategoryTranslationsDto: [
          ...maestroCategory.categoryTranslations,
        ],
      });
      this.searchService.indexCategory(category.id);
    }
  }
  // async installPlaylists(
  //   options: {
  //     maestroFullProject?: IMaestroFullProject;
  //     playlistIds?: number[];
  //   },
  //   isDownloading = false,
  // ) {
  //   if (!options.maestroFullProject && !options.playlistIds) return;

  //   let maestroPlaylists: IMaestroPlaylist[] = [];
  //   let maestroItems: IMaestroItem[] = [];
  //   const applicationsNotInstalled = [];

  //   if (options.maestroFullProject) {
  //     maestroPlaylists = options.maestroFullProject.playlists;
  //     maestroItems = options.maestroFullProject.items;
  //   } else {
  //     maestroPlaylists = await this.getPlaylistsMetadatas(options.playlistIds);
  //     maestroItems = await this.getPlaylistsItemsMetadatas(options.playlistIds);
  //   }

  //   Logger.log(`Checking applications to install...`, 'CreateService');
  //   for (const maestroPlaylist of maestroPlaylists) {
  //     const application = await this.applicationsService.findOne({
  //       where: { id: maestroPlaylist.applicationId },
  //     });
  //     if (!application) {
  //       throw new InternalServerErrorException(
  //         `application ${maestroPlaylist.applicationId} not found`,
  //       );
  //     }
  //     if (!application.isInstalled) {
  //       applicationsNotInstalled.push(application);
  //     }
  //   }

  //   const notInstalledApplications = [];

  //   for (let i = 0; i < applicationsNotInstalled.length; i++) {
  //     const application = applicationsNotInstalled[i];
  //     const result = await this.applicationsService.install(application.id);
  //     if (!result) {
  //       notInstalledApplications.push(application.id);
  //     }
  //   }

  //   if (notInstalledApplications.length > 0) {
  //     Logger.warn(
  //       `Failed to install those applications : ${notInstalledApplications.join(
  //         ', ',
  //       )}`,
  //       'CreateService',
  //     );
  //     Logger.warn(
  //       'Continue installation without playlists that uses those applications...',
  //       'CreateService',
  //     );
  //   }

  //   maestroPlaylists = await this.getNotInstalledPlaylists(maestroPlaylists);

  //   Logger.log(
  //     `Creating ${maestroPlaylists.length} new playlists...`,
  //     'CreateService',
  //   );

  //   await this.createPlaylists(maestroPlaylists, isDownloading);

  //   Logger.log(
  //     `${maestroPlaylists.length} playlists successfully created !`,
  //     'CreateService',
  //   );

  //   maestroItems = await this.getNotInstalledItems(maestroItems);

  //   Logger.log(`Creating ${maestroItems.length} new items...`, 'CreateService');

  //   await this.createItems(maestroItems, isDownloading);

  //   Logger.log(
  //     `${maestroPlaylists.length} items successfully created !`,
  //     'CreateService',
  //   );

  //   Logger.log(
  //     `Linking items to ${maestroPlaylists.length} playlists...`,
  //     'CreateService',
  //   );

  //   await this.linkItemsToPlaylists(maestroPlaylists);

  //   Logger.log(
  //     `Linked items to${maestroPlaylists.length} playlists successfully !`,
  //     'CreateService',
  //   );

  //   if (options?.maestroFullProject) {
  //     Logger.log(
  //       `Creating ${maestroPlaylists.length} categoryPlaylists...`,
  //       'CreateService',
  //     );
  //     await this.createCategoryPlaylists(options.maestroFullProject.playlists);
  //     Logger.log(
  //       `Created ${maestroPlaylists.length} categoryPlaylists successfully !`,
  //       'CreateService',
  //     );
  //   }

  //   await invalidateCache([Playlist, Item, PlaylistTranslation]);

  //   this.downloadService.runQueue();
  // }

  async createCategoryPlaylists(maestroPlaylists: IMaestroPlaylist[]) {
    for (const maestroPlaylist of maestroPlaylists) {
      await this.categoryPlaylistsService.create({
        playlistId: maestroPlaylist.id,
        categoryId: maestroPlaylist.categoryId,
      });
    }
  }

  private async linkItemsToPlaylists(maestroPlaylists: IMaestroPlaylist[]) {
    for (const maestroPlaylist of maestroPlaylists) {
      await this.linkItemsToPlaylist(maestroPlaylist);
    }
  }

  private async linkItemsToPlaylist(maestroPlaylist: IMaestroPlaylist) {
    const foundPlaylist = await this.playlistsService.findOne({
      where: { id: maestroPlaylist.id },
    });
    if (!foundPlaylist) return;
    const items = [];
    for (const itemId of maestroPlaylist.itemIds) {
      const foundItem = await this.itemsService.findOne({
        where: { id: itemId },
      });
      if (!foundItem) continue;
      items.push(foundItem);
    }
    foundPlaylist.items = items;
    await this.playlistsService.save(foundPlaylist);
  }

  // private async createPlaylists(
  //   maestroPlaylists: IMaestroPlaylist[],
  //   isDownloading: boolean,
  // ) {
  //   for (const maestroPlaylist of maestroPlaylists) {
  //     await this.createPlaylist(maestroPlaylist, isDownloading);
  //   }
  // }

  // private async createPlaylist(
  //   maestroPlaylist: IMaestroPlaylist,
  //   isDownloading: boolean,
  // ) {
  //   const playlist =
  //     await this.playlistsService.createPlaylist(maestroPlaylist);
  //   // TODO : should not create queued file for vm installation
  //   if (isDownloading) {
  //     await this.createQueuedFilePlaylist(maestroPlaylist);
  //   }
  //   this.searchService.indexPlaylist(playlist.id);
  //   return playlist;
  // }

  private async createItems(
    maestroItems: IMaestroItem[],
    isDownloading: boolean,
  ) {
    for (const maestroItem of maestroItems) {
      await this.createItem(maestroItem, isDownloading);
    }
  }

  async createItem(maestroItem: IMaestroItem, isDownloading: boolean) {
    const dublinCoreitem = await this.itemsService.create(maestroItem);
    // TODO : should not create queued file for vm installation
    if (isDownloading) {
      await this.createQueuedFile(maestroItem);
    }
    this.searchService.indexItem(dublinCoreitem.item.id);
    return dublinCoreitem;
  }

  private async createQueuedFile(maestroItem: IMaestroItem) {
    let extension = mime.extension(maestroItem.file.mimeType);
    if (!extension) {
      extension = '';
    }
    if (extension === null) {
      const extensionSplit = maestroItem.file.path.split('.');
      extension = extensionSplit[extensionSplit.length - 1];
    }
    let thumbnailUrl = undefined;
    let thumbnailExtension = undefined;
    if (
      maestroItem.thumbnail !== undefined &&
      maestroItem.thumbnail !== null &&
      maestroItem.thumbnail !== ''
    ) {
      thumbnailUrl = maestroItem.thumbnail;
      const thumbnailExtensionFind = maestroItem.thumbnail.split('.');
      thumbnailExtension =
        thumbnailExtensionFind[thumbnailExtensionFind.length - 1];
      if (thumbnailExtension !== 'png' && thumbnailExtension !== 'jpg') {
        thumbnailExtension = 'png';
      }
    }
    await this.queuedFilesService.create({
      thumbnailUrl,
      fileUrl: maestroItem.file.path,
      itemId: maestroItem.id,
      fileExtension: extension,
      thumbnailExtension,
    });
  }

  private async createQueuedFilePlaylist(maestroPlaylist: IMaestroPlaylist) {
    let thumbnailUrl = undefined;
    let thumbnailExtension = undefined;
    if (
      maestroPlaylist.image !== undefined &&
      maestroPlaylist.image !== null &&
      maestroPlaylist.image !== ''
    ) {
      thumbnailUrl = maestroPlaylist.image;
      const thumbnailExtensionFind = maestroPlaylist.image.split('.');
      thumbnailExtension =
        thumbnailExtensionFind[thumbnailExtensionFind.length - 1];
      if (thumbnailExtension !== 'png' && thumbnailExtension !== 'jpg') {
        thumbnailExtension = 'png';
      }
    }
    await this.queuedFilesService.create({
      thumbnailUrl,
      fileUrl: undefined,
      itemId: maestroPlaylist.id,
      fileExtension: undefined,
      thumbnailExtension,
      isPlaylist: true,
    });
  }

  private async getNotInstalledPlaylists(maestroPlaylists: IMaestroPlaylist[]) {
    const notInstalledPlaylists = [];

    for (const maestroPlaylist of maestroPlaylists) {
      const playlist = await this.playlistsService.findOne({
        where: { id: maestroPlaylist.id },
      });

      if (!playlist) {
        notInstalledPlaylists.push(maestroPlaylist);
      }
    }

    Logger.warn(
      `Removed ${
        maestroPlaylists.length - notInstalledPlaylists.length
      } playlists already installed`,
      'CreateService',
    );

    return notInstalledPlaylists;
  }

  private async getNotInstalledItems(maestroItems: IMaestroItem[]) {
    const notInstalledItems = [];

    for (const maestroItem of maestroItems) {
      const item = await this.itemsService.findOne({
        where: { id: maestroItem.id },
      });

      if (!item) {
        notInstalledItems.push(maestroItem);
      }
    }

    Logger.warn(
      `Removed ${
        maestroItems.length - notInstalledItems.length
      } items already installed`,
      'CreateService',
    );

    return notInstalledItems;
  }

  private async getPlaylistsMetadatas(playlistIds: number[]) {
    Logger.log(
      `Fetching ${playlistIds.length} playlists metadatas...`,
      'CreateService',
    );

    const maestroPlaylists: IMaestroPlaylist[] = [];

    for (const playlistId of playlistIds) {
      const maestroPlaylist =
        await this.maestroApiService.getPlaylist(playlistId);
      if (!maestroPlaylist) throw new NotFoundException('playlist not found');

      maestroPlaylists.push(maestroPlaylist);
    }

    Logger.log(
      `Retrieved ${maestroPlaylists.length} playlists metadatas !`,
      'CreateService',
    );

    return maestroPlaylists;
  }

  private async getPlaylistsItemsMetadatas(playlistIds: number[]) {
    Logger.log(
      `Fetching ${playlistIds.length} playlists items metadatas...`,
      'CreateService',
    );

    const maestroItems: IMaestroItem[] = [];

    for (const playlistId of playlistIds) {
      const maestroPlaylistItems =
        await this.maestroApiService.getPlaylistItems(playlistId);
      if (!maestroPlaylistItems)
        throw new NotFoundException('playlist not found');
      maestroItems.push(...maestroPlaylistItems.items);
    }

    const filteredMaestroItems = [];
    const maestroItemsIds = [];
    for (const maestroItem of maestroItems) {
      if (!maestroItemsIds.includes(maestroItem.id)) {
        maestroItemsIds.push(maestroItem.id);
        filteredMaestroItems.push(maestroItem);
      }
    }

    Logger.log(
      `Retrieved ${maestroItems.length} playlists items metadatas !`,
      'CreateService',
    );

    return filteredMaestroItems;
  }
  /*

  async getPlaylistsMetadatas(playlistIds: number[]) {
    Logger.log(
      `Getting ${playlistIds.length} playlists metadatas...`,
      'CreateService',
    );

    const maestroPlaylists: IPlaylist[] = [];

    for (const playlistId of playlistIds) {
      const maestroPlaylist =
        await this.maestroApiService.getPlaylist(playlistId);
      if (!maestroPlaylist) throw new NotFoundException('playlist not found');

      maestroPlaylists.push(maestroPlaylist);
    }

    Logger.log(
      `Retrieved ${maestroPlaylists.length} playlists metadatas !`,
      'CreateService',
    );

    return maestroPlaylists;
  }

  private async installApplications(
    maestroPlaylists: IPlaylist[],
    applicationsNotInstalled: string[],
  ) {
    const filteredMaestroPlaylists: IPlaylist[] = [];

    for (const maestroPlaylist of maestroPlaylists) {
      const application = await this.applicationsService.findOne({
        where: { id: maestroPlaylist.applicationId },
      });
      if (!application) {
        applicationsNotInstalled.push(`? (${application.id})`);
        continue;
      }
      if (!application.isInstalled) {
        const isInstalled = await this.applicationsService.install(
          application.id,
        );
        if (!isInstalled) {
          applicationsNotInstalled.push(
            `${application.name} (${application.id})`,
          );
          continue;
        }
      }
      filteredMaestroPlaylists.push(maestroPlaylist);
    }

    return filteredMaestroPlaylists;
  }

  private async createItems(
    playlist: Playlist,
    maestroPlaylist: IPlaylist,
    application: Application,
  ) {
    for (const maestroItem of maestroPlaylist.items) {
      await this.createItem(playlist, maestroItem, application);
    }
  }

 
  private async createPlaylists(
    maestroPlaylists: IPlaylist[],
    linkToCategories?: boolean,
  ) {
    for (const maestroPlaylist of maestroPlaylists) {
      const application = await this.applicationsService.findOne({
        where: { id: maestroPlaylist.applicationId },
        relations: { applicationType: true },
      });
      if (!application) return;
      await this.createPlaylistAndItems(
        maestroPlaylist,
        application,
        linkToCategories,
      );
    }
  }

  

  private async createItem(
    playlist: Playlist,
    maestroItem: IItem,
    application: Application,
  ) {
    const item = await this.itemsService.findOne({
      where: { contentId: maestroItem.id.toString() },
    });
    if (item) {
      await this.playlistsService.addItemToPlaylist(playlist.id, item);
      return;
    }
    let extension = mime.getExtension(maestroItem.mimeType);
    if (extension === null) {
      const extensionSplit = maestroItem.url.split('.');
      extension = extensionSplit[extensionSplit.length - 1];
    }

    const dublinCoreTypeText = await this.dublinCoreTypesService.findOne({
      where: { dublinCoreTypeTranslations: { label: 'Text' } },
    });
    const dublinCoreTypeMovingImage = await this.dublinCoreTypesService.findOne(
      {
        where: { dublinCoreTypeTranslations: { label: 'Moving Image' } },
      },
    );
    const dublinCoreTypeSound = await this.dublinCoreTypesService.findOne({
      where: { dublinCoreTypeTranslations: { label: 'Sound' } },
    });
    const dublinCoreTypeImage = await this.dublinCoreTypesService.findOne({
      where: { dublinCoreTypeTranslations: { label: 'Still Image' } },
    });
    const dublinCoreTypeOther = await this.dublinCoreTypesService.findOne({
      where: { dublinCoreTypeTranslations: { label: 'Other' } },
    });
    let dublinCoreType = dublinCoreTypeOther;
    if (maestroItem.mimeType !== '') {
      if (maestroItem.mimeType.includes('pdf')) {
        dublinCoreType = dublinCoreTypeText;
      } else if (maestroItem.mimeType.includes('audio/')) {
        dublinCoreType = dublinCoreTypeSound;
      } else if (maestroItem.mimeType.includes('video/')) {
        dublinCoreType = dublinCoreTypeMovingImage;
      } else if (maestroItem.mimeType.includes('image/')) {
        dublinCoreType = dublinCoreTypeImage;
      } else {
        dublinCoreType = dublinCoreTypeOther;
        if (application.id === 'olip') {
          Logger.warn(
            `No mimetype found for ${maestroItem.id}`,
            'PlaylistsService',
          );
        }
      }
    }
    let applicationId = 'olip';

    if (application) {
      applicationId = application.id;
    }
    // TODO : Some missing fields
    const createItemDto: CreateItemDto = {
      thumbnail: '',
      createdCountry: 'string',
      isInstalled: false,
      isUpdateNeeded: false,
      isBroken: false,
      isManuallyInstalled: false,
      version: maestroItem.updatedAt,
      contentId: maestroItem.id.toString(),
      dublinCoreTypeId: dublinCoreType.id,
      name: '',
      path: '',
      size: maestroItem.size.toString(),
      duration: '',
      pages: '',
      applicationId,
      extension,
      mimeType: maestroItem.mimeType,
    };
    const dublinCoreItem = await this.itemsService.create(createItemDto);
    for (const maestroItemTranslation of maestroItem.itemTranslations) {
      // TODO : hard coded language identifier
      const language = await this.languagesService.findOneByIdentifier('eng');
      const createDublinCoreItemTranslationDto: CreateDublinCoreItemTranslationDto =
        {
          title: maestroItemTranslation.title,
          description: maestroItemTranslation.description,
          creator: maestroItemTranslation.creator,
          publisher: maestroItemTranslation.publisher,
          source: maestroItemTranslation.source,
          extent: maestroItemTranslation.extent,
          subject: maestroItemTranslation.subject,
          dateCreated: maestroItemTranslation.dateCreated,
          dublinCoreItemId: dublinCoreItem.id,
          languageId: language.id,
        };
      this.dublinCoreItemsService.createTranslation(
        createDublinCoreItemTranslationDto,
      );
    }
    await this.playlistsService.addItemToPlaylist(
      playlist.id,
      dublinCoreItem.item,
    );
    let thumbnailUrl = undefined;
    let thumbnailExtension = undefined;
    if (maestroItem.thumbnail) {
      thumbnailUrl = maestroItem.thumbnail;
      const thumbnailExtensionFind = maestroItem.thumbnail.split('.');
      thumbnailExtension =
        thumbnailExtensionFind[thumbnailExtensionFind.length - 1];
    }
    await this.queuedFilesService.create({
      thumbnailUrl,
      fileUrl: maestroItem.url,
      itemId: dublinCoreItem.item.id,
      fileExtension: extension,
      thumbnailExtension,
    });

    this.searchService.indexItem(dublinCoreItem.item.id);
  }*/
}
