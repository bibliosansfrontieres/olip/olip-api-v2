import {
  Inject,
  Injectable,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { MaestroApiService } from './maestro-api.service';
import { ItemsService } from 'src/items/items.service';
import { Item } from 'src/items/entities/item.entity';
import { QueuedFilesService } from './queued-files.service';
import { ConfigService } from '@nestjs/config';
import { IMaestroPlaylist } from './interfaces/maestro-playlist.interface';
import { IMaestroItem } from './interfaces/maestro-item.interface';
import { CategoryPlaylistsService } from 'src/category-playlists/category-playlists.service';
import { CategoryPlaylistItemsService } from 'src/category-playlist-items/category-playlist-items.service';

@Injectable()
export class ContentService {
  private isVm: boolean | undefined = undefined;

  constructor(
    @Inject(forwardRef(() => PlaylistsService))
    private playlistsService: PlaylistsService,
    @Inject(forwardRef(() => ItemsService))
    private itemsService: ItemsService,
    private maestroApiService: MaestroApiService,
    private queuedFilesService: QueuedFilesService,
    private configService: ConfigService,
    @Inject(forwardRef(() => CategoryPlaylistsService))
    private categoryPlaylistsService: CategoryPlaylistsService,
    @Inject(forwardRef(() => CategoryPlaylistItemsService))
    private categoryPlaylistItemsService: CategoryPlaylistItemsService,
  ) {}

  private async checkIsVm() {
    if (this.isVm === undefined) {
      this.isVm = (await this.configService.get('VM_ID')) !== 'olip';
    }
  }

  /**
   * Install or update a Maestro playlist
   * @param id Maestro playlist id
   * @returns boolean true if needs to start download queue
   */
  async installOrUpdatePlaylist(
    maestroPlaylistOrId: string | IMaestroPlaylist,
    maestroItems?: IMaestroItem[],
  ): Promise<boolean> {
    await this.checkIsVm();

    let isDownloadQueueNeeded = false;

    let maestroPlaylist: IMaestroPlaylist;

    // Get maestroPlaylist and maestroItems from Maestro API
    if (typeof maestroPlaylistOrId === 'string') {
      const id = maestroPlaylistOrId;
      maestroPlaylist = await this.maestroApiService.getPlaylist(id);
      if (!maestroPlaylist)
        throw new NotFoundException('playlist not found in Maestro');
      const maestroPlaylistItems =
        await this.maestroApiService.getPlaylistItems(id);
      if (!maestroPlaylistItems)
        throw new NotFoundException('items not found in Maestro');
      maestroItems = maestroPlaylistItems.items;
    } else {
      maestroPlaylist = maestroPlaylistOrId;
      if (!maestroItems) {
        throw new NotFoundException('items not found in function');
      }
    }

    // Try to get playlist from database
    let playlist = await this.playlistsService.findOne({
      where: { id: maestroPlaylist.id },
      relations: { items: { playlists: true }, application: true },
    });

    // Download playlist thumbnail if playlist doesn't exists or need an update (and not a VM)
    const isPlaylistDownloadNeeded =
      (!playlist ||
        playlist.isUpdateNeeded ||
        playlist.version !== maestroPlaylist.version) &&
      (!this.isVm || maestroPlaylist.applicationId !== 'olip');

    if (
      isPlaylistDownloadNeeded &&
      (!this.isVm || maestroPlaylist.applicationId !== 'olip')
    ) {
      isDownloadQueueNeeded = true;
      await this.queuedFilesService.create({
        itemId: maestroPlaylist.id,
        thumbnailExtension: 'png',
        thumbnailUrl: maestroPlaylist.image,
        isPlaylist: true,
        fileUrl: '',
        fileExtension: '',
      });
    }

    // Create or update playlist's metadatas
    await this.playlistsService.createOrUpdate(maestroPlaylist);
    playlist = await this.playlistsService.findOne({
      where: { id: maestroPlaylist.id },
      relations: { items: { playlists: true } },
    });

    const itemsToLinkToPlaylist: Item[] = [];

    // Manage items to add/update
    for (const maestroItem of maestroItems) {
      // Try to get item from database
      let item = await this.itemsService.findOne({
        where: { id: maestroItem.id },
      });

      // Download item file/thumbnail if item doesn't exists or need an update (and not a VM)
      const itemDownloadNeeded =
        (!item ||
          item.version !== maestroItem.version ||
          item.isUpdateNeeded) &&
        (!this.isVm || maestroItem.applicationId !== 'olip');

      if (itemDownloadNeeded) {
        isDownloadQueueNeeded = true;
        await this.queuedFilesService.create({
          itemId: maestroItem.id,
          thumbnailExtension: 'png',
          thumbnailUrl: maestroItem.thumbnail,
          isPlaylist: false,
          fileUrl: maestroItem.file.path,
          fileExtension: maestroItem.file.extension,
        });
      }

      // Create or update item's metadatas
      await this.itemsService.createOrUpdate(maestroItem);
      item = await this.itemsService.findOne({
        where: { id: maestroItem.id },
      });

      // Need to link item to playlist
      itemsToLinkToPlaylist.push(item);
    }

    // Manage items to delete
    for (const item of playlist.items) {
      // Try to find item in itemsToLinkToPlaylist
      const itemToLinkToPlaylist = itemsToLinkToPlaylist.find(
        (i) => i.id === item.id,
      );

      if (!itemToLinkToPlaylist) {
        // Item is not needed anymore in this playlist

        // Get playlists linked to this item, except the current one
        const otherPlaylists = item.playlists
          ? item.playlists.filter((p) => p.id !== playlist.id)
          : [];

        if (otherPlaylists.length === 0) {
          // Item not found in any other playlist we don't need it anymore
          // TODO : DELETE FILE/ITEM ETC
        }
      }
    }

    // Link items and save playlist
    playlist.items = itemsToLinkToPlaylist;

    await this.playlistsService.save(playlist);

    // If we are in a VM, we need to remove isInstalled=false flag because we won't download them
    if (this.isVm) {
      for (const item of playlist.items) {
        item.isInstalled = true;
      }
      await this.itemsService.save(playlist.items);
    }

    // Manage CategoryPlaylists and CategoryPlaylistItems
    const categoryPlaylists = await this.categoryPlaylistsService.find({
      where: { playlist: { id: playlist.id } },
      relations: { categoryPlaylistItems: { item: true } },
    });
    for (const categoryPlaylist of categoryPlaylists) {
      for (const item of playlist.items) {
        const categoryPlaylistItem =
          categoryPlaylist.categoryPlaylistItems.find(
            (cpi) => cpi.item.id === item.id,
          );
        if (!categoryPlaylistItem) {
          await this.categoryPlaylistItemsService.create({
            itemId: item.id,
            categoryPlaylistId: categoryPlaylist.id,
          });
        }
      }
      for (const categoryPlaylistItem of categoryPlaylist.categoryPlaylistItems) {
        const item = playlist.items.find(
          (i) => i.id === categoryPlaylistItem.item.id,
        );
        if (!item) {
          await this.categoryPlaylistItemsService.delete(categoryPlaylistItem);
        }
      }
    }

    return isDownloadQueueNeeded;
  }
}
