import { Inject, Injectable, Logger, forwardRef } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { PageOptions } from 'src/utils/classes/page-options';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { GetUninstalledPlaylistsDto } from 'src/playlists/dto/get-uninstalled-playlists.dto';
import { ItemsService } from 'src/items/items.service';
import { IMaestroApplication } from './interfaces/maestro-application.interface';
import { IMaestroApplicationType } from './interfaces/maestro-application-type.interface';
import { IMaestroFullProject } from './interfaces/maestro-full-project.interface';
import { IMaestroPlaylist } from './interfaces/maestro-playlist.interface';
import { IMaestroItem } from './interfaces/maestro-item.interface';

@Injectable()
export class MaestroApiService {
  private maestroUrl: string;

  constructor(
    private configService: ConfigService,
    @Inject(forwardRef(() => PlaylistsService))
    private playlistsService: PlaylistsService,
    @Inject(forwardRef(() => ItemsService))
    private itemsService: ItemsService,
  ) {
    this.maestroUrl = configService.get<string>('MAESTRO_API_URL');
  }

  async getUninstalledPlaylists(
    getUninstalledPlaylistsDto: GetUninstalledPlaylistsDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const pageOptions = new PageOptions(pageOptionsDto);
    const query = `&languageIdentifier=${
      getUninstalledPlaylistsDto.languageIdentifier
    }&page=${pageOptions.page}&order=${pageOptions.order}&skip=${
      pageOptions.skip
    }&take=${pageOptions.take}&query=${
      getUninstalledPlaylistsDto.query || ''
    }&applicationIds=${getUninstalledPlaylistsDto.applicationIds || ''}`;
    const playlists = await this.playlistsService.find({
      where: { isInstalled: true },
    });
    const playlistIds = playlists.map((playlist) => playlist.id);
    try {
      const result = await axios.get(
        `${
          this.maestroUrl
        }/playlists/uninstalled?playlistIds=${playlistIds.join(',')}${query}`,
      );
      return result.data;
    } catch (e) {
      console.error(e);
      Logger.error(
        'Error while fetching uninstalled playlists',
        'MaestroService',
      );
      return null;
    }
  }

  async getPlaylist(id: string | number) {
    try {
      const result = await axios.get(`${this.maestroUrl}/playlists/${id}`);
      return result.data as IMaestroPlaylist;
    } catch {
      return undefined;
    }
  }

  async getPlaylistItems(id: string | number) {
    try {
      const result = await axios.get(
        `${this.maestroUrl}/playlists/${id}/items`,
      );
      return result.data as { items: IMaestroItem[] };
    } catch {
      return undefined;
    }
  }

  async getProject(id: number) {
    try {
      const result = await axios.get(`${this.maestroUrl}/projects/full/${id}`);
      return result.data as IMaestroFullProject;
    } catch {
      return undefined;
    }
  }

  async getUpdates() {
    const installedPlaylists = await this.playlistsService.find({
      where: { isInstalled: true, isUpdateNeeded: false },
      relations: { items: true },
    });
    const installedItems = await this.itemsService.find({
      where: { isInstalled: true, isUpdateNeeded: false },
    });
    const playlists = installedPlaylists.map((playlist) => {
      return {
        id: playlist.id,
        version: playlist.version,
        length: playlist.items.length,
      };
    });
    const items = installedItems.map((item) => {
      return { id: item.id, version: item.version };
    });
    try {
      const result = await axios.post(`${this.maestroUrl}/omeka/updates`, {
        playlists,
        items,
      });
      return result.data as {
        playlistIds: string[];
      };
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }

  async getApplications(): Promise<
    { data: IMaestroApplication[] } | undefined
  > {
    try {
      // TODO : take to 5000 ? Maybe we can have more than 5000 apps ???
      const result = await axios.get(
        `${this.maestroUrl}/applications?take=5000`,
      );
      return result.data as { data: IMaestroApplication[] };
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }

  async patchVirtualMachineDeployStatus(isDeployed: boolean) {
    if (!this.configService.get('MAESTRO_API_TOKEN')) {
      return;
    }
    if (this.configService.get('VM_ID') === 'olip') {
      return;
    }
    try {
      await axios.patch(
        `${this.maestroUrl}/virtual-machines/deploy-status`,
        { isDeployed },
        {
          headers: {
            Authorization: `Bearer ${this.configService.get(
              'MAESTRO_API_TOKEN',
            )}`,
          },
        },
      );
    } catch (e) {
      console.error(e);
    }
  }

  async getApplicationTypes(): Promise<IMaestroApplicationType[] | undefined> {
    try {
      const result = await axios.get(`${this.maestroUrl}/applications/types`);
      return result.data as IMaestroApplicationType[];
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }
}
