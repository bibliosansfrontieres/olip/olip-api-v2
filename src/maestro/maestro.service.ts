import {
  Inject,
  Injectable,
  InternalServerErrorException,
  Logger,
  forwardRef,
} from '@nestjs/common';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { ItemsService } from 'src/items/items.service';
import { MaestroApiService } from './maestro-api.service';
import { CreateService } from './create.service';
import { DownloadService } from './download.service';
import { SettingsService } from 'src/settings/settings.service';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import { SearchService } from 'src/search/search.service';
import { ConfigService } from '@nestjs/config';
import { ApplicationsService } from 'src/applications/applications.service';
import { QueuedFilesService } from './queued-files.service';
import { ContentService } from './content.service';
import { CategoriesService } from 'src/categories/categories.service';

@Injectable()
export class MaestroService {
  constructor(
    @Inject(forwardRef(() => PlaylistsService))
    private playlistsService: PlaylistsService,
    @Inject(forwardRef(() => ItemsService))
    private itemsService: ItemsService,
    private maestroApiService: MaestroApiService,
    private createService: CreateService,
    private downloadService: DownloadService,
    private settingsService: SettingsService,
    private searchService: SearchService,
    private configService: ConfigService,
    private applicationsService: ApplicationsService,
    private queuedFilesService: QueuedFilesService,
    private contentService: ContentService,
    private categoriesService: CategoriesService,
  ) {}

  async deploy(isOlip: boolean) {
    // Wait for elasticsearch
    await this.searchService.isReady();

    if (!isOlip) {
      // We are installing a VM here
      const omekaProjectId = this.configService.get<string>('OMEKA_PROJECT_ID');
      Logger.log(`Deploying project ${omekaProjectId}...`, 'MaestroService');

      Logger.log('Getting project from Maestro...', 'MaestroService');
      // Get project definition from Maestro
      const project = await this.maestroApiService.getProject(+omekaProjectId);
      if (project === undefined) {
        await this.maestroApiService.patchVirtualMachineDeployStatus(false);
        throw new InternalServerErrorException(
          `could not fetch project ${omekaProjectId} from Maestro`,
        );
      }

      await this.createService.createCategories(project.categories);

      for (const maestroPlaylist of project.playlists) {
        const maestroItems = project.items.filter((item) =>
          maestroPlaylist.itemIds.includes(item.id),
        );
        await this.contentService.installOrUpdatePlaylist(
          maestroPlaylist,
          maestroItems,
        );
      }

      await this.createService.createCategoryPlaylists(project.playlists);

      // TODO : check what's needed below
      await this.settingsService.update(SettingKey.PROJECT_VERSION, {
        value: Date.now().toString(),
      });

      await this.settingsService.update(SettingKey.IS_PROJECT_UPDATE_NEEDED, {
        value: 'false',
      });

      Logger.log(
        `Omeka project ${omekaProjectId} deployed !`,
        'MaestroService',
      );

      await this.settingsService.update(SettingKey.IS_VM_INSTALLED, {
        value: 'true',
      });

      await this.maestroApiService.patchVirtualMachineDeployStatus(true);
    } else {
      const queuedFiles = await this.queuedFilesService.findAll();
      if (queuedFiles.length > 0) {
        Logger.log('Files in queue, continue deploy...', 'MaestroService');
        return;
      }
      Logger.log('OLIP deploy in progress...', 'MaestroService');
      await this.itemsService.resetAllIsInstalled();

      const applications = await this.applicationsService.resetAllIsInstalled();
      for (const application of applications) {
        if (application.id === 'olip') continue;
        await this.applicationsService.checkAndInstall(application.id, true);
      }

      await this.itemsService.indexAll();
      await this.playlistsService.indexAll();
      await this.applicationsService.indexAll();
      await this.categoriesService.indexAll();

      await this.createService.createQueuedFiles();
      await this.downloadService.runQueue();

      Logger.log('OLIP deploy done !', 'MaestroService');
      // TODO : install applications's contents
    }
  }
}
