import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { CreateQueuedFileDto } from '../dto/create-queued-file.dto';

@Entity()
export class QueuedFile {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  thumbnailUrl: string;

  @Column({ nullable: true })
  fileUrl: string;

  @Column({ nullable: true })
  fileExtension: string;

  @Column({ nullable: true })
  thumbnailExtension: string;

  @Column()
  itemId: string;

  @Column()
  isPlaylist: boolean;

  @Column()
  isInstallationNeeded: boolean;

  @Column({ nullable: true })
  filePath: string;

  @Column({ nullable: true })
  fileName: string;

  @Column({ nullable: true })
  imagePath: string;

  @Column({ nullable: true })
  imageName: string;

  constructor(createQueuedFileDto: CreateQueuedFileDto) {
    if (!createQueuedFileDto) return;
    this.thumbnailUrl = createQueuedFileDto.thumbnailUrl;
    this.fileUrl = createQueuedFileDto.fileUrl;
    this.itemId = createQueuedFileDto.itemId;
    this.fileExtension = createQueuedFileDto.fileExtension;
    this.thumbnailExtension = createQueuedFileDto.thumbnailExtension;
    this.isInstallationNeeded = false;
    this.isPlaylist = createQueuedFileDto.isPlaylist === true;
  }
}
