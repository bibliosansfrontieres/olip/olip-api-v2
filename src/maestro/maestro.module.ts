import { Module, forwardRef } from '@nestjs/common';
import { MaestroService } from './maestro.service';
import { ConfigModule } from '@nestjs/config';
import { PlaylistsModule } from 'src/playlists/playlists.module';
import { DownloadService } from './download.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ItemsModule } from 'src/items/items.module';
import { HardwareModule } from 'src/hardware/hardware.module';
import { DockerModule } from 'src/docker/docker.module';
import { ApplicationsModule } from 'src/applications/applications.module';
import { UploadModule } from 'src/upload/upload.module';
import { CategoriesModule } from 'src/categories/categories.module';
import { LanguagesModule } from 'src/languages/languages.module';
import { DublinCoreItemsModule } from 'src/dublin-core-items/dublin-core-items.module';
import { DublinCoreTypesModule } from 'src/dublin-core-types/dublin-core-types.module';
import { CategoryPlaylistsModule } from 'src/category-playlists/category-playlists.module';
import { FilesModule } from 'src/files/files.module';
import { MaestroApiService } from './maestro-api.service';
import { SocketModule } from 'src/socket/socket.module';
import { QueuedFile } from './entities/queued-file.entity';
import { InstallService } from './install.service';
import { QueuedFilesService } from './queued-files.service';
import { CreateService } from './create.service';
import { SettingsModule } from 'src/settings/settings.module';
import { SearchModule } from 'src/search/search.module';
import { MaestroWebsocketApiService } from './maestro-websocket-api.service';
import { ContentService } from './content.service';
import { CategoryPlaylistItemsModule } from 'src/category-playlist-items/category-playlist-items.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([QueuedFile]),
    ConfigModule,
    forwardRef(() => PlaylistsModule),
    forwardRef(() => ItemsModule),
    HardwareModule,
    DockerModule,
    ApplicationsModule,
    UploadModule,
    CategoriesModule,
    LanguagesModule,
    DublinCoreItemsModule,
    DublinCoreTypesModule,
    CategoryPlaylistsModule,
    FilesModule,
    SocketModule,
    SettingsModule,
    SearchModule,
    CategoryPlaylistItemsModule,
  ],
  providers: [
    MaestroService,
    DownloadService,
    MaestroApiService,
    InstallService,
    QueuedFilesService,
    CreateService,
    MaestroWebsocketApiService,
    ContentService,
  ],
  exports: [
    MaestroService,
    DownloadService,
    MaestroApiService,
    InstallService,
    QueuedFilesService,
    CreateService,
    MaestroWebsocketApiService,
    ContentService,
  ],
})
export class MaestroModule {}
