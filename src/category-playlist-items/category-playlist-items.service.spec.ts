import { Test, TestingModule } from '@nestjs/testing';
import { CategoryPlaylistItemsService } from './category-playlist-items.service';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { CategoryPlaylistItem } from './entities/category-playlist-item.entity';
import { ItemsService } from 'src/items/items.service';
import { itemsServiceMock } from 'src/tests/mocks/providers/items-service.mock';
import { CategoryPlaylistsService } from 'src/category-playlists/category-playlists.service';
import { categoryPlaylistsServiceMock } from 'src/tests/mocks/providers/category-playlists-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';

describe('CategoryPlaylistItemsService', () => {
  let service: CategoryPlaylistItemsService;
  let categoryPlaylistItemRepository: Repository<CategoryPlaylistItem>;

  const CATEGORY_PLAYLIST_ITEM_REPOSITORY_TOKEN =
    getRepositoryToken(CategoryPlaylistItem);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: CATEGORY_PLAYLIST_ITEM_REPOSITORY_TOKEN,
          useClass: Repository<CategoryPlaylistItem>,
        },
        CategoryPlaylistItemsService,
        ItemsService,
        CategoryPlaylistsService,
        ApplicationsService,
      ],
    })
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .overrideProvider(CategoryPlaylistsService)
      .useValue(categoryPlaylistsServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .compile();

    service = module.get<CategoryPlaylistItemsService>(
      CategoryPlaylistItemsService,
    );
    categoryPlaylistItemRepository = module.get<
      Repository<CategoryPlaylistItem>
    >(CATEGORY_PLAYLIST_ITEM_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(categoryPlaylistItemRepository).toBeDefined();
  });
});
