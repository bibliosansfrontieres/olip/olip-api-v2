import { Body, Controller, Param, Patch } from '@nestjs/common';
import { CategoryPlaylistItemsService } from './category-playlist-items.service';
import { UpdateCategoryPlaylistItemDto } from './dto/update-category-playlist-item.dto';
import {
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { UpdateCategoryPlaylistItemResponse } from './responses/update-category-playlist-item-response';
import { UpdateCategoryPlaylistItemBadRequest } from './responses/update-category-playlist-item-bad-request';

@Controller('api/category-playlist-items')
@ApiTags('category-playlist-items')
export class CategoryPlaylistItemsController {
  constructor(
    private categoryPlaylistItemsService: CategoryPlaylistItemsService,
  ) {}

  @Patch(':id')
  @ApiOperation({ summary: 'Update category playlist item' })
  @ApiOkResponse({ type: UpdateCategoryPlaylistItemResponse })
  @ApiNotFoundResponse()
  @ApiBadRequestResponse({ type: UpdateCategoryPlaylistItemBadRequest })
  updateCategoryPlaylistItem(
    @Param('id') id: string,
    @Body() updateCategoryPlaylistItemDto: UpdateCategoryPlaylistItemDto,
  ) {
    return this.categoryPlaylistItemsService.update(
      +id,
      updateCategoryPlaylistItemDto,
    );
  }
}
