import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsOptional } from 'class-validator';

export class UpdateCategoryPlaylistItemDto {
  @ApiPropertyOptional({ example: true })
  @IsOptional()
  @IsBoolean()
  isDisplayed: boolean;

  @ApiPropertyOptional({ example: true })
  @IsOptional()
  @IsBoolean()
  isHighlightable: boolean;
}
