import { IsNumber } from 'class-validator';

export class CreateCategoryPlaylistItemDto {
  @IsNumber()
  itemId: string;

  @IsNumber()
  categoryPlaylistId: number;
}
