import {
  Inject,
  Injectable,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { CreateCategoryPlaylistItemDto } from './dto/create-category-playlist-item.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryPlaylistItem } from './entities/category-playlist-item.entity';
import { FindManyOptions, In, Repository } from 'typeorm';
import { ItemsService } from 'src/items/items.service';
import { CategoryPlaylistsService } from 'src/category-playlists/category-playlists.service';
import { UpdateCategoryPlaylistItemDto } from './dto/update-category-playlist-item.dto';
import { UpdateCategoryPlaylistItemResponse } from './responses/update-category-playlist-item-response';
import { invalidateCache } from 'src/utils/caching.util';
import { sortCategoryPlaylistItemsTranslations } from 'src/utils/language/sort-translations/sort-category-playlist-items-translations.util';
import { getApplicationIdsFromCategoryPlaylistItems } from 'src/utils/application/get-application-ids.util';
import { sortApplicationsTranslations } from 'src/utils/language/sort-translations/sort-applications-translations.util';
import { ApplicationsService } from 'src/applications/applications.service';
import { Application } from 'src/applications/entities/application.entity';

@Injectable()
export class CategoryPlaylistItemsService {
  constructor(
    @InjectRepository(CategoryPlaylistItem)
    private categoryPlaylistItemRepository: Repository<CategoryPlaylistItem>,
    @Inject(forwardRef(() => ItemsService)) private itemsService: ItemsService,
    private categoryPlaylistsService: CategoryPlaylistsService,
    private applicationsService: ApplicationsService,
  ) {}

  getQueryBuilder() {
    return this.categoryPlaylistItemRepository.createQueryBuilder(
      'categoryPlaylistItem',
    );
  }

  /**
   * Retrieve all categoryPlaylistItems from a given categoryPlaylist, with item data and translations.
   * The results are ordered by the order of the categoryPlaylistItems.
   * The item translations are sorted by the given languageIdentifier first, then by the identifier of the language.
   * @param {number} categoryPlaylistId the id of the categoryPlaylist
   * @param {string} languageIdentifier the identifier of the language to prioritize the translations
   * @return {Promise<CategoryPlaylistItem[]>} an array of categoryPlaylistItems with item data and translations
   */
  async getCategoryPlaylistCategoryPlaylistItems(
    categoryPlaylistId: number,
    languageIdentifier: string,
  ): Promise<CategoryPlaylistItem[]> {
    const categoryPlaylistItemsQueryBuilder =
      this.categoryPlaylistItemRepository
        .createQueryBuilder('categoryPlaylistItem')
        .leftJoinAndSelect('categoryPlaylistItem.item', 'item')
        .leftJoinAndSelect('item.dublinCoreItem', 'dublinCoreItem')
        .leftJoin(
          'dublinCoreItem.dublinCoreItemTranslations',
          'dublinCoreItemTranslations',
        )
        .addSelect([
          'dublinCoreItemTranslations.id',
          'dublinCoreItemTranslations.title',
          'dublinCoreItemTranslations.description',
          'dublinCoreItemTranslations.creator',
          'dublinCoreItemTranslations.publisher',
          'dublinCoreItemTranslations.source',
          'dublinCoreItemTranslations.extent',
          'dublinCoreItemTranslations.subject',
          'dublinCoreItemTranslations.dateCreated',
        ])
        .leftJoin(
          'dublinCoreItemTranslations.language',
          'dublinCoreItemTranslationsLanguage',
        )
        .addSelect([
          'dublinCoreItemTranslationsLanguage.identifier',
          'dublinCoreItemTranslationsLanguage.label',
        ])
        .leftJoin('dublinCoreItem.dublinCoreType', 'dublinCoreType')
        .addSelect('dublinCoreType.id')
        .leftJoin('item.application', 'application')
        .addSelect('application.id')
        .leftJoin('categoryPlaylistItem.categoryPlaylist', 'categoryPlaylist')
        .where('categoryPlaylist.id = :categoryPlaylistId', {
          categoryPlaylistId,
        });

    const categoryPlaylistItems =
      await categoryPlaylistItemsQueryBuilder.getMany();

    sortCategoryPlaylistItemsTranslations(
      categoryPlaylistItems,
      languageIdentifier,
    );

    return categoryPlaylistItems;
  }

  /**
   * Retrieves all applications associated with the given categoryPlaylistItems, with translations.
   * The translations are sorted by the specified languageIdentifier.
   *
   * @param {CategoryPlaylistItem[]} categoryPlaylistItems - The categoryPlaylistItems to retrieve applications from.
   * @param {string} languageIdentifier - The identifier of the language to prioritize the translations.
   * @return {Promise<Application[]>} A promise that resolves to an array of applications with translations.
   */
  async getCategoryPlaylistItemsApplications(
    categoryPlaylistItems: CategoryPlaylistItem[],
    languageIdentifier: string,
  ): Promise<Application[]> {
    const applicationIds = getApplicationIdsFromCategoryPlaylistItems(
      categoryPlaylistItems,
    );

    const applications = await this.applicationsService.find({
      where: { id: In(applicationIds) },
      relations: {
        applicationTranslations: { language: true },
        applicationType: { applicationTypeTranslations: { language: true } },
      },
      select: {
        id: true,
        name: true,
        displayName: true,
        version: true,
        logo: true,
        image: true,
        url: true,
        size: true,
        isInstalled: true,
        isDownloading: true,
        isInstalling: true,
        isUp: true,
        isUpdateNeeded: true,
        isUpdating: true,
        isOlip: true,
        applicationType: {
          id: true,
          name: true,
          applicationTypeTranslations: {
            id: true,
            label: true,
            description: true,
            language: { identifier: true, label: true },
          },
        },
        applicationTranslations: {
          id: true,
          longDescription: true,
          shortDescription: true,
          language: {
            identifier: true,
            label: true,
          },
        },
      },
    });

    sortApplicationsTranslations(applications, languageIdentifier);

    return applications;
  }

  async create(createCategoryPlaylistItemDto: CreateCategoryPlaylistItemDto) {
    const item = await this.itemsService.findOne({
      where: { id: createCategoryPlaylistItemDto.itemId },
    });
    if (item === null) return null;
    const categoryPlaylist = await this.categoryPlaylistsService.findOne({
      where: { id: createCategoryPlaylistItemDto.categoryPlaylistId },
    });
    if (categoryPlaylist === null) return null;
    const categoryPlaylistItem = new CategoryPlaylistItem(
      item,
      categoryPlaylist,
    );
    return this.categoryPlaylistItemRepository.save(categoryPlaylistItem);
  }

  delete(categoryPlaylistItem: CategoryPlaylistItem) {
    return this.categoryPlaylistItemRepository.remove(categoryPlaylistItem);
  }

  async findAll(options?: FindManyOptions<CategoryPlaylistItem>) {
    return this.categoryPlaylistItemRepository.find(options);
  }

  async update(
    id: number,
    updateCategoryPlaylistItemDto: UpdateCategoryPlaylistItemDto,
  ) {
    let categoryPlaylistItem =
      await this.categoryPlaylistItemRepository.findOne({ where: { id } });

    if (categoryPlaylistItem === null) {
      throw new NotFoundException('categoryPlaylistItem not found');
    }

    if (updateCategoryPlaylistItemDto === undefined) {
      return new UpdateCategoryPlaylistItemResponse(categoryPlaylistItem);
    }

    if (updateCategoryPlaylistItemDto.isDisplayed !== undefined) {
      categoryPlaylistItem.isDisplayed =
        updateCategoryPlaylistItemDto.isDisplayed;
    }

    categoryPlaylistItem =
      await this.categoryPlaylistItemRepository.save(categoryPlaylistItem);
    await invalidateCache([CategoryPlaylistItem]);
    return new UpdateCategoryPlaylistItemResponse(categoryPlaylistItem);
  }
}
