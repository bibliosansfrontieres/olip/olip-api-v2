import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateCategoryPlaylistItemBadRequest {
  @ApiProperty({ example: 400 })
  statusCode: number;

  @ApiProperty({
    example: [
      'isDisplayed must be a boolean',
      'isHighlightable must be a boolean',
    ],
  })
  message: string | string[];

  @ApiPropertyOptional({ example: 'Bad Request' })
  error?: string;
}
