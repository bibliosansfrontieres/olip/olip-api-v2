import { ApiProperty } from '@nestjs/swagger';
import { CategoryPlaylistItem } from '../entities/category-playlist-item.entity';

export class UpdateCategoryPlaylistItemResponse {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: true })
  isHighlightable: boolean;

  @ApiProperty({ example: true })
  isDisplayed: boolean;

  constructor(categoryPlaylistItem: CategoryPlaylistItem) {
    this.id = categoryPlaylistItem.id;
    this.isHighlightable = categoryPlaylistItem.isHighlightable;
    this.isDisplayed = categoryPlaylistItem.isDisplayed;
  }
}
