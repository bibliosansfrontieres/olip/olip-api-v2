import { Test, TestingModule } from '@nestjs/testing';
import { CategoryPlaylistItemsController } from './category-playlist-items.controller';
import { CategoryPlaylistItemsService } from './category-playlist-items.service';
import { CategoryPlaylistItem } from './entities/category-playlist-item.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ItemsService } from 'src/items/items.service';
import { itemsServiceMock } from 'src/tests/mocks/providers/items-service.mock';
import { CategoryPlaylistsService } from 'src/category-playlists/category-playlists.service';
import { categoryPlaylistsServiceMock } from 'src/tests/mocks/providers/category-playlists-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';

describe('CategoryPlaylistItemsController', () => {
  let controller: CategoryPlaylistItemsController;
  let categoryPlaylistItemRepository: Repository<CategoryPlaylistItem>;

  const CATEGORY_PLAYLIST_ITEM_REPOSITORY_TOKEN =
    getRepositoryToken(CategoryPlaylistItem);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CategoryPlaylistItemsController],
      providers: [
        {
          provide: CATEGORY_PLAYLIST_ITEM_REPOSITORY_TOKEN,
          useClass: Repository<CategoryPlaylistItem>,
        },
        CategoryPlaylistItemsService,
        ItemsService,
        CategoryPlaylistsService,
        ApplicationsService,
      ],
    })
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .overrideProvider(CategoryPlaylistsService)
      .useValue(categoryPlaylistsServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .compile();

    controller = module.get<CategoryPlaylistItemsController>(
      CategoryPlaylistItemsController,
    );
    categoryPlaylistItemRepository = module.get<
      Repository<CategoryPlaylistItem>
    >(CATEGORY_PLAYLIST_ITEM_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(categoryPlaylistItemRepository).toBeDefined();
  });
});
