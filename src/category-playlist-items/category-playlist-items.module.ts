import { Module, forwardRef } from '@nestjs/common';
import { CategoryPlaylistItemsService } from './category-playlist-items.service';
import { CategoryPlaylistItemsController } from './category-playlist-items.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryPlaylistItem } from './entities/category-playlist-item.entity';
import { ItemsModule } from 'src/items/items.module';
import { CategoryPlaylistsModule } from 'src/category-playlists/category-playlists.module';
import { ApplicationsModule } from 'src/applications/applications.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([CategoryPlaylistItem]),
    forwardRef(() => ItemsModule),
    CategoryPlaylistsModule,
    forwardRef(() => ApplicationsModule),
  ],
  controllers: [CategoryPlaylistItemsController],
  providers: [CategoryPlaylistItemsService],
  exports: [CategoryPlaylistItemsService],
})
export class CategoryPlaylistItemsModule {}
