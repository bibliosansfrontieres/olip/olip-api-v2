import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { Item } from 'src/items/entities/item.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class CategoryPlaylistItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  isHighlightable: boolean;

  @Column()
  isDisplayed: boolean;

  @ManyToOne(() => Item, (item) => item.categoryPlaylistItems, {
    onDelete: 'CASCADE',
  })
  item: Item;

  @ManyToOne(
    () => CategoryPlaylist,
    (categoryPlaylist) => categoryPlaylist.categoryPlaylistItems,
    { onDelete: 'CASCADE' },
  )
  categoryPlaylist: CategoryPlaylist;

  constructor(item: Item, categoryPlaylist: CategoryPlaylist) {
    this.item = item;
    this.categoryPlaylist = categoryPlaylist;
    this.isHighlightable = false;
    this.isDisplayed = true;
  }
}
