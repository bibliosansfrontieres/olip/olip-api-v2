import { Test, TestingModule } from '@nestjs/testing';
import { EditosService } from './editos.service';
import { EditoParagraphsService } from 'src/edito-paragraphs/edito-paragraphs.service';
import { editoParagraphsServiceMock } from 'src/tests/mocks/providers/edito-paragraphs-service.mock';
import { Repository } from 'typeorm';
import { Edito } from './entities/edito.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { EditoTranslation } from './entities/edito-translation.entity';
import { UploadService } from 'src/upload/upload.service';
import { LanguagesService } from 'src/languages/languages.service';
import { uploadServiceMock } from 'src/tests/mocks/providers/upload-service.mock';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';

describe('EditosService', () => {
  let service: EditosService;
  let editosRepository: Repository<Edito>;
  const EDITOS_REPOSITORY_TOKEN = getRepositoryToken(Edito);
  let editoTranslationsRepository: Repository<EditoTranslation>;
  const EDITO_TRANSLATIONS_REPOSITORY_TOKEN =
    getRepositoryToken(EditoTranslation);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EditosService,
        { provide: EDITOS_REPOSITORY_TOKEN, useClass: Repository<Edito> },
        {
          provide: EDITO_TRANSLATIONS_REPOSITORY_TOKEN,
          useClass: Repository<EditoTranslation>,
        },
        EditoParagraphsService,
        UploadService,
        LanguagesService,
      ],
    })
      .overrideProvider(EditoParagraphsService)
      .useValue(editoParagraphsServiceMock)
      .overrideProvider(UploadService)
      .useValue(uploadServiceMock)
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .compile();

    service = module.get<EditosService>(EditosService);
    editosRepository = module.get<Repository<Edito>>(EDITOS_REPOSITORY_TOKEN);
    editoTranslationsRepository = module.get<Repository<EditoTranslation>>(
      EDITO_TRANSLATIONS_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
