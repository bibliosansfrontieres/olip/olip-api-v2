import { Test, TestingModule } from '@nestjs/testing';
import { EditosController } from './editos.controller';
import { EditosService } from './editos.service';
import { Repository } from 'typeorm';
import { Edito } from './entities/edito.entity';
import { EditoTranslation } from './entities/edito-translation.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { EditoParagraphsService } from 'src/edito-paragraphs/edito-paragraphs.service';
import { UploadService } from 'src/upload/upload.service';
import { LanguagesService } from 'src/languages/languages.service';
import { editoParagraphsServiceMock } from 'src/tests/mocks/providers/edito-paragraphs-service.mock';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';
import { uploadServiceMock } from 'src/tests/mocks/providers/upload-service.mock';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';

describe('EditosController', () => {
  let controller: EditosController;
  let editosRepository: Repository<Edito>;
  const EDITOS_REPOSITORY_TOKEN = getRepositoryToken(Edito);
  let editoTranslationsRepository: Repository<EditoTranslation>;
  const EDITO_TRANSLATIONS_REPOSITORY_TOKEN =
    getRepositoryToken(EditoTranslation);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EditosController],
      providers: [
        EditosService,
        { provide: EDITOS_REPOSITORY_TOKEN, useClass: Repository<Edito> },
        {
          provide: EDITO_TRANSLATIONS_REPOSITORY_TOKEN,
          useClass: Repository<EditoTranslation>,
        },
        EditoParagraphsService,
        UploadService,
        LanguagesService,
        JwtService,
        UsersService,
        ConfigService,
      ],
    })
      .overrideProvider(EditoParagraphsService)
      .useValue(editoParagraphsServiceMock)
      .overrideProvider(UploadService)
      .useValue(uploadServiceMock)
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    controller = module.get<EditosController>(EditosController);
    editosRepository = module.get<Repository<Edito>>(EDITOS_REPOSITORY_TOKEN);
    editoTranslationsRepository = module.get<Repository<EditoTranslation>>(
      EDITO_TRANSLATIONS_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
