import { Type } from 'class-transformer';
import {
  ArrayMaxSize,
  ArrayMinSize,
  IsArray,
  IsString,
  ValidateNested,
} from 'class-validator';
import { CreateEditoTranslationDto } from './create-edito-translation.dto';
import { CreateEditoParagraphDto } from 'src/edito-paragraphs/dto/create-edito-paragraph.dto';

export class CreateEditoDto {
  @IsString()
  image: string;

  @IsArray()
  @ValidateNested({ each: true })
  @ArrayMinSize(1)
  @Type(() => CreateEditoTranslationDto)
  createEditoTranslationsDto: CreateEditoTranslationDto[];

  @IsArray()
  @ValidateNested({ each: true })
  @ArrayMaxSize(9)
  @Type(() => CreateEditoParagraphDto)
  createEditoParagraphsDto: CreateEditoParagraphDto[];
}
