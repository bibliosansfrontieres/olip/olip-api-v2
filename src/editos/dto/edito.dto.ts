import { ApiProperty } from '@nestjs/swagger';
import { Edito } from '../entities/edito.entity';
import { EditoParagraph } from 'src/edito-paragraphs/entities/edito-paragraph.entity';
import { EditoTranslation } from '../entities/edito-translation.entity';

export class EditoDto {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: 'images/sdf65sd4f6sdf65sdf.png' })
  image: string;

  @ApiProperty({ example: 1695807884215 })
  createdAt: number;

  @ApiProperty({ example: 1695807884215 })
  updatedAt: number;

  @ApiProperty({
    example: [
      {
        id: 1,
        title: 'My title',
        description: 'My description',
        language: {
          id: 1,
          languageIdentifier: 'eng',
          oldIdentifier: 'en',
          label: 'English',
        },
      },
    ],
  })
  editoTranslations: EditoTranslation[];

  @ApiProperty({
    example: [
      {
        id: 1,
        editoParagraphTranslations: [
          {
            id: 1,
            text: 'My paragraph 1',
            language: {
              id: 1,
              languageIdentifier: 'eng',
              oldIdentifier: 'en',
              label: 'English',
            },
          },
          {
            id: 1,
            text: 'Mon paragraphe 1',
            language: {
              id: 1,
              languageIdentifier: 'fra',
              oldIdentifier: 'fr',
              label: 'French',
            },
          },
        ],
      },
      {
        id: 2,
        editoParagraphTranslations: [
          {
            id: 1,
            text: 'My paragraph 2',
            language: {
              id: 1,
              languageIdentifier: 'eng',
              oldIdentifier: 'en',
              label: 'English',
            },
          },
          {
            id: 1,
            text: 'Mon paragraphe 2',
            language: {
              id: 1,
              languageIdentifier: 'fra',
              oldIdentifier: 'fr',
              label: 'French',
            },
          },
        ],
      },
    ],
  })
  editoParagraphs: EditoParagraph[];

  constructor(edito: Edito) {
    this.id = edito.id;
    this.image = edito.image;
    this.createdAt = edito.createdAt;
    this.updatedAt = edito.updatedAt;
    this.editoTranslations = edito.editoTranslations;
    this.editoParagraphs = edito.editoParagraphs;
  }
}
