import { IsString, MaxLength } from 'class-validator';

export class CreateEditoTranslationDto {
  @IsString()
  languageIdentifier: string;

  @IsString()
  @MaxLength(100)
  title: string;

  @IsString()
  @MaxLength(500)
  description: string;
}
