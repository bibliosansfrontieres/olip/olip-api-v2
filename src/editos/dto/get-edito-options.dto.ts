import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetEditoOptions {
  @ApiPropertyOptional({ example: 'eng' })
  @IsString()
  @IsOptional()
  languageIdentifier: string;
}
