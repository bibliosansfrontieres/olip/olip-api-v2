import {
  Injectable,
  NotAcceptableException,
  NotFoundException,
} from '@nestjs/common';
import { CreateEditoDto } from './dto/create-edito.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Edito } from './entities/edito.entity';
import { Repository } from 'typeorm';
import { UploadService } from 'src/upload/upload.service';
import { EditoTranslation } from './entities/edito-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';
import { EditoParagraphsService } from 'src/edito-paragraphs/edito-paragraphs.service';
import { Language } from 'src/languages/entities/language.entity';
import { CreateEditoTranslationDto } from './dto/create-edito-translation.dto';
import { CreateEditoParagraphTranslationDto } from 'src/edito-paragraphs/dto/create-edito-paragraph-translation.dto';
import { invalidateCache } from 'src/utils/caching.util';
import { EditoParagraph } from 'src/edito-paragraphs/entities/edito-paragraph.entity';
import { GetEditoOptions } from './dto/get-edito-options.dto';
import { join } from 'path';
import { existsSync } from 'fs';

@Injectable()
export class EditosService {
  constructor(
    @InjectRepository(Edito) private editosRepository: Repository<Edito>,
    @InjectRepository(EditoTranslation)
    private editoTranslationsRepository: Repository<EditoTranslation>,
    private editoParagraphsService: EditoParagraphsService,
    private uploadService: UploadService,
    private languagesService: LanguagesService,
  ) {}

  async create(createEditoDto: CreateEditoDto) {
    const { editoTranslationLanguages, editoTranslationsToCreate } =
      await this.extractEditoTranslationLanguages(createEditoDto);
    const paragraphsToCreate =
      await this.extractParagraphsToCreate(createEditoDto);
    let image = '';
    if (createEditoDto.image.startsWith('images/')) {
      const imagePath = join(
        __dirname,
        '../../data/static',
        createEditoDto.image,
      );
      if (existsSync(imagePath)) {
        image = createEditoDto.image;
      } else {
        throw new NotAcceptableException(
          `image ${createEditoDto.image} not found`,
        );
      }
    } else {
      image = await this.uploadService.image(createEditoDto.image);
    }
    let edito = new Edito(createEditoDto, image);
    edito = await this.editosRepository.save(edito);
    for (let i = 0; i < editoTranslationsToCreate.length; i++) {
      const createEditoTranslationDto = editoTranslationsToCreate[i];
      const language = editoTranslationLanguages[i];
      const editoTranslation = new EditoTranslation(
        createEditoTranslationDto,
        edito,
        language,
      );
      await this.editoTranslationsRepository.save(editoTranslation);
    }
    for (const paragraphToCreate of paragraphsToCreate) {
      const paragraphTranslations = paragraphToCreate.translations;
      const paragraphLanguages = paragraphToCreate.languages;
      await this.editoParagraphsService.create(
        edito,
        paragraphTranslations,
        paragraphLanguages,
      );
    }
    await this.deleteOldEditos();
    await invalidateCache([Edito, EditoTranslation, EditoParagraph]);
    return this.editosRepository.findOne({
      where: { id: edito.id },
      relations: {
        editoTranslations: { language: true },
        editoParagraphs: { editoParagraphTranslations: { language: true } },
      },
    });
  }

  private async deleteOldEditos() {
    const countEditos = await this.editosRepository.count();
    if (countEditos > 30) {
      const editosToDelete = await this.editosRepository.find({
        skip: 30,
        order: { updatedAt: 'DESC' },
      });
      await this.editosRepository.remove(editosToDelete);
    }
  }

  private async extractEditoTranslationLanguages(
    createEditoDto: CreateEditoDto,
  ) {
    const editoTranslationLanguages: Language[] = [];
    const editoTranslationsToCreate: CreateEditoTranslationDto[] = [];
    for (const createEditoTranslationDto of createEditoDto.createEditoTranslationsDto) {
      const language = await this.languagesService.findOneByIdentifier(
        createEditoTranslationDto.languageIdentifier,
      );
      if (language === null)
        throw new NotFoundException(
          `language ${createEditoTranslationDto.languageIdentifier} not found`,
        );
      editoTranslationsToCreate.push(createEditoTranslationDto);
      editoTranslationLanguages.push(language);
    }
    return { editoTranslationLanguages, editoTranslationsToCreate };
  }

  private async extractParagraphsToCreate(createEditoDto: CreateEditoDto) {
    const paragraphsToCreate: {
      languages: Language[];
      translations: CreateEditoParagraphTranslationDto[];
    }[] = [];
    for (const createEditoParagraphDto of createEditoDto.createEditoParagraphsDto) {
      const createEditoParagraphTranslationsDto =
        createEditoParagraphDto.createEditoParagraphTranslationsDto;
      const languages = [];
      const translations = [];
      for (const createEditoParagraphTranslation of createEditoParagraphTranslationsDto) {
        const language = await this.languagesService.findOneByIdentifier(
          createEditoParagraphTranslation.languageIdentifier,
        );
        if (language === null)
          throw new NotFoundException(
            `language ${createEditoParagraphTranslation.languageIdentifier} not found`,
          );
        languages.push(language);
        translations.push(createEditoParagraphTranslation);
      }
      paragraphsToCreate.push({ languages, translations });
    }
    return paragraphsToCreate;
  }

  async get(id: number) {
    const edito = await this.editosRepository.findOne({
      where: { id },
      relations: {
        editoTranslations: { language: true },
        editoParagraphs: { editoParagraphTranslations: { language: true } },
      },
    });
    if (edito === null) throw new NotFoundException(`edito not found`);
    return edito;
  }

  async getLast(getEditoOptions: GetEditoOptions) {
    if (getEditoOptions.languageIdentifier !== undefined) {
      const editos = await this.editosRepository.find({
        order: { updatedAt: 'DESC' },
        relations: { editoParagraphs: true },
        take: 1,
      });
      let edito = editos[0];
      if (edito === undefined)
        throw new NotFoundException('no last edito found');
      edito = await this.getTranslation(
        edito,
        getEditoOptions.languageIdentifier,
      );
      edito = await this.getEditoParagraphsTranslations(
        edito,
        getEditoOptions.languageIdentifier,
        false,
      );
      return edito;
    } else {
      const editos = await this.editosRepository.find({
        order: { updatedAt: 'DESC' },
        relations: {
          editoParagraphs: { editoParagraphTranslations: { language: true } },
          editoTranslations: { language: true },
        },
        take: 1,
      });
      const edito = editos[0];
      if (edito === undefined)
        throw new NotFoundException('no last edito found');
      return edito;
    }
  }

  async getAll() {
    const editos = await this.editosRepository.find({
      order: { updatedAt: 'DESC' },
      relations: {
        editoParagraphs: { editoParagraphTranslations: { language: true } },
        editoTranslations: { language: true },
      },
    });
    return editos;
  }

  private async getTranslation(
    edito: Edito,
    languageIdentifier: string,
  ): Promise<Edito> {
    const translation = await this.languagesService.getTranslation<
      Edito,
      EditoTranslation
    >('edito', edito, this.editoTranslationsRepository, languageIdentifier);
    edito.editoTranslations = [translation];
    return edito;
  }

  private async getEditoParagraphsTranslations(
    edito: Edito,
    languageIdentifier: string,
    withFallback = true,
  ) {
    for (let editoParagraph of edito.editoParagraphs) {
      const editoParagraphTranslated =
        await this.editoParagraphsService.getTranslation(
          editoParagraph,
          languageIdentifier,
          withFallback,
        );
      editoParagraph = editoParagraphTranslated;
    }
    edito.editoParagraphs = edito.editoParagraphs.filter(
      (editoParagraph) => editoParagraph.editoParagraphTranslations[0] !== null,
    );
    return edito;
  }

  async delete(id: number) {
    const edito = await this.editosRepository.findOne({ where: { id } });
    if (edito === null) throw new NotFoundException('edito not found');
    await invalidateCache([Edito, EditoTranslation, EditoParagraph]);
    await this.editosRepository.remove(edito);
  }
}
