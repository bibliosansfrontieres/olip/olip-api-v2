import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class CreateEditoBadRequest {
  @ApiProperty({ example: 400 })
  statusCode: number;

  @ApiProperty({
    example: [
      'image must be a string',
      'createEditoTranslationsDto must contain at least 1 elements',
      'createEditoTranslationsDto must be an array',
      'createEditoTranslationsDto.0.languageIdentifier must be a string',
      'createEditoTranslationsDto.0.title must be a string',
      'createEditoTranslationsDto.0.description must be a string',
      'createEditoParagraphsDto must be an array',
      'createEditoParagraphsDto must contain at least 1 elements',
      'createEditoParagraphsDto must contain no more than 9 elements',
      'createEditoParagraphsDto.0.createEditoParagraphTranslationsDto must contain at least 1 elements',
      'createEditoParagraphsDto.0.createEditoParagraphTranslationsDto must be an array',
      'createEditoParagraphsDto.0.createEditoParagraphTranslationsDto.0.languageIdentifier must be a string',
      'createEditoParagraphsDto.0.createEditoParagraphTranslationsDto.0.text must be a string',
    ],
  })
  message: string | string[];

  @ApiPropertyOptional({ example: 'Bad Request' })
  error?: string;
}
