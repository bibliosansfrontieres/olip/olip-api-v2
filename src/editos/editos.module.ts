import { Module } from '@nestjs/common';
import { EditosService } from './editos.service';
import { EditosController } from './editos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Edito } from './entities/edito.entity';
import { EditoTranslation } from './entities/edito-translation.entity';
import { EditoParagraphsModule } from 'src/edito-paragraphs/edito-paragraphs.module';
import { UploadModule } from 'src/upload/upload.module';
import { LanguagesModule } from 'src/languages/languages.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    TypeOrmModule.forFeature([Edito, EditoTranslation]),
    EditoParagraphsModule,
    UploadModule,
    LanguagesModule,
    JwtModule,
    UsersModule,
    ConfigModule,
  ],
  controllers: [EditosController],
  providers: [EditosService],
  exports: [EditosService],
})
export class EditosModule {}
