import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { EditoTranslation } from './edito-translation.entity';
import { EditoParagraph } from 'src/edito-paragraphs/entities/edito-paragraph.entity';
import { CreateEditoDto } from '../dto/create-edito.dto';

@Entity()
export class Edito {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  image: string;

  @CreateDateColumn({
    default: () => Date.now(),
  })
  createdAt: number;

  @UpdateDateColumn({
    default: () => Date.now(),
    onUpdate: Date.now().toString(),
  })
  updatedAt: number;

  @OneToMany(
    () => EditoTranslation,
    (editoTranslation) => editoTranslation.edito,
  )
  editoTranslations: EditoTranslation[];

  @OneToMany(() => EditoParagraph, (editoParagraph) => editoParagraph.edito)
  editoParagraphs: EditoParagraph[];

  constructor(createEditoDto: CreateEditoDto, image: string) {
    if (!createEditoDto) return;
    this.image = image;
  }
}
