import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Edito } from './edito.entity';
import { Language } from 'src/languages/entities/language.entity';
import { CreateEditoTranslationDto } from '../dto/create-edito-translation.dto';

@Entity()
export class EditoTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @ManyToOne(() => Edito, (edito) => edito.editoTranslations, {
    onDelete: 'CASCADE',
  })
  edito: Edito;

  @ManyToOne(() => Language, (language) => language.editoTranslations)
  language: Language;

  constructor(
    createEditoTranslationDto: CreateEditoTranslationDto,
    edito: Edito,
    language: Language,
  ) {
    if (!createEditoTranslationDto) return;
    this.title = createEditoTranslationDto.title;
    this.description = createEditoTranslationDto.description;
    this.edito = edito;
    this.language = language;
  }
}
