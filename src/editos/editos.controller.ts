import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { EditosService } from './editos.service';
import { Auth } from 'src/auth/decorators/auth.decorator';
import {
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { EditoDto } from './dto/edito.dto';
import { CreateEditoBadRequest } from './responses/errors/create-edito-bad-request';
import { CreateEditoDto } from './dto/create-edito.dto';
import { cachedMethod } from 'src/utils/caching.util';
import { Edito } from './entities/edito.entity';
import { EditoTranslation } from './entities/edito-translation.entity';
import { EditoParagraph } from 'src/edito-paragraphs/entities/edito-paragraph.entity';
import { GetEditoOptions } from './dto/get-edito-options.dto';
import { LastEditoDto } from './dto/last-edito.dto';

@ApiTags('editos')
@Controller('api/editos')
export class EditosController {
  constructor(private editosService: EditosService) {}

  @Auth(PermissionEnum.CREATE_EDITO)
  @Post()
  @ApiOperation({
    summary: 'Create edito',
    description: 'Needs authentification',
  })
  @ApiOkResponse({ type: EditoDto })
  @ApiBadRequestResponse({ type: CreateEditoBadRequest })
  @ApiUnauthorizedResponse()
  create(@Body() createEditoDto: CreateEditoDto) {
    return this.editosService.create(createEditoDto);
  }

  @Get()
  @ApiOperation({
    summary: 'Get all editos',
  })
  @ApiOkResponse({ type: LastEditoDto, isArray: true })
  getAll() {
    return cachedMethod(
      'editosService',
      'getAll',
      [],
      async () => {
        const editos = await this.editosService.getAll();
        return editos.map((edito) => new LastEditoDto(edito));
      },
      [Edito, EditoTranslation, EditoParagraph],
    );
  }

  @Get('last')
  @ApiOperation({
    summary: 'Get last edito',
  })
  @ApiOkResponse({ type: LastEditoDto })
  @ApiNotFoundResponse()
  getLast(@Query() getEditoOptions: GetEditoOptions) {
    return cachedMethod(
      'editosService',
      'getLast',
      [getEditoOptions],
      async () => {
        const edito = await this.editosService.getLast(getEditoOptions);
        return new LastEditoDto(edito);
      },
      [Edito, EditoTranslation, EditoParagraph],
    );
  }

  @Auth(PermissionEnum.CREATE_EDITO)
  @Get(':id')
  @ApiOperation({
    summary: 'Get an edito by ID',
    description: 'Needs authentification',
  })
  @ApiOkResponse({ type: EditoDto })
  @ApiNotFoundResponse()
  get(@Param('id') id: number) {
    return cachedMethod(
      'editosService',
      'get',
      [{ id }],
      async () => {
        const edito = await this.editosService.get(+id);
        return new EditoDto(edito);
      },
      [Edito, EditoTranslation, EditoParagraph],
    );
  }

  @Auth(PermissionEnum.DELETE_EDITO)
  @Delete(':id')
  @ApiOperation({
    summary: 'Delete an edito by ID',
    description: 'Needs authentification',
  })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  async delete(@Param('id') id: number) {
    await this.editosService.delete(+id);
  }
}
