import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';
import { CreateIndexDto } from './dto/create-index.dto';
import { sleep } from 'src/utils/sleep.util';
import { ISearch } from './interfaces/search.interface';
import { ISearchOptions } from './interfaces/search-options.interface';
import { replaceAll } from 'src/utils/replace-all.util';

@Injectable()
export class ElasticsearchService {
  private elasticsearchApiUrl: string;
  private isInitialized: boolean;

  async onModuleInit() {
    await this.isReady(true);
    await this.createMappings();
    this.isInitialized = true;
  }

  constructor(private configService: ConfigService) {
    this.elasticsearchApiUrl = this.configService.get('ELASTICSEARCH_API_URL');
    this.isInitialized = false;
  }

  private async createMappings() {
    const vmId = this.configService.get<string>('VM_ID');
    await this.api(
      `${vmId}-item`,
      'put',
      {
        mappings: {
          properties: {
            id: {
              type: 'text',
            },
            title: {
              type: 'text',
            },
            content: {
              type: 'text',
            },
            description: {
              type: 'text',
            },
            languageIdentifier: {
              type: 'keyword',
            },
            suggest: {
              type: 'completion',
            },
          },
        },
      },
      false,
    );
    await this.api(
      `${vmId}-playlist`,
      'put',
      {
        mappings: {
          properties: {
            id: {
              type: 'text',
            },
            title: {
              type: 'text',
            },
            description: {
              type: 'text',
            },
            languageIdentifier: {
              type: 'keyword',
            },
            suggest: {
              type: 'completion',
            },
          },
        },
      },
      false,
    );
    await this.api(
      `${vmId}-category`,
      'put',
      {
        mappings: {
          properties: {
            id: {
              type: 'integer',
            },
            title: {
              type: 'text',
            },
            description: {
              type: 'text',
            },
            languageIdentifier: {
              type: 'keyword',
            },
            suggest: {
              type: 'completion',
            },
          },
        },
      },
      false,
    );
    await this.api(
      `${vmId}-application`,
      'put',
      {
        mappings: {
          properties: {
            id: {
              type: 'text',
            },
            title: {
              type: 'text',
            },
            shortDescription: {
              type: 'text',
            },
            longDescription: {
              type: 'text',
            },
            languageIdentifier: {
              type: 'keyword',
            },
            suggest: {
              type: 'completion',
            },
          },
        },
      },
      false,
    );
  }

  private async api(
    url: string,
    method: 'post' | 'get' | 'put' | 'delete',
    data?: object,
    log: boolean = true,
  ) {
    try {
      const result = await axios(`${this.elasticsearchApiUrl}/${url}`, {
        method,
        data,
      });
      return result.data;
    } catch (e) {
      if (log) console.error(e);
      return undefined;
    }
  }

  async index(createIndexDto: CreateIndexDto) {
    const result = await this.api(
      `${createIndexDto.index}/_doc`,
      'post',
      createIndexDto.data,
    );
    return result;
  }

  async removeIndex(index: string, id: string) {
    const result = await this.api(
      `${index}/_doc/${id}`,
      'delete',
      undefined,
      true,
    );
    return result;
  }

  async getSuggestions(vmId: string, query: string, take?: string) {
    const result = await this.api(
      `${[
        `${vmId}-application`,
        `${vmId}-item`,
        `${vmId}-playlist`,
        `${vmId}-category`,
      ].join(',')}/_search`,
      'post',
      {
        suggest: {
          suggestion: {
            prefix: query,
            completion: {
              field: 'suggest',
              size: parseInt(take || '5'),
            },
          },
        },
      },
    );
    return result;
  }

  async isReady(fromElasticsearch = false) {
    let ready = false;
    const isInitialized = fromElasticsearch ? true : this.isInitialized;
    do {
      Logger.warn(
        'Waiting for Elasticsearch to be ready...',
        'ElasticsearchService',
      );
      const result = await this.api('', 'get', undefined, false);
      if (result) {
        ready = true;
      } else {
        await sleep(5000);
      }
    } while (!ready || !isInitialized);
    Logger.log('Elasticsearch is ready !', 'ElasticsearchService');
    return ready;
  }

  async search(searchOptions: ISearchOptions) {
    const indexUrl = searchOptions?.index ? `${searchOptions.index}/` : '';
    const queryObject: { query?: object; size?: number; from?: number } = {};

    if (searchOptions.query) {
      searchOptions.query = replaceAll(searchOptions.query, '/', ' ');
      queryObject.query = {
        query_string: {
          query: `${searchOptions.query}~`,
          fuzziness: 2,
        },
      };
    }

    if (searchOptions.size) {
      queryObject.size = searchOptions.size;
    }

    if (searchOptions.from) {
      queryObject.from = searchOptions.from;
    }

    const result = await this.api(`${indexUrl}_search`, 'post', queryObject);

    return result as ISearch;
  }
}
