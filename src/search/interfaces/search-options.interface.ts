export interface ISearchOptions {
  size?: number;
  query: string;
  index?: string;
  from?: number;
}
