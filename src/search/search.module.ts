import { Module, forwardRef } from '@nestjs/common';
import { SearchService } from './search.service';
import { SearchController } from './search.controller';
import { ElasticsearchService } from './elasticsearch.service';
import { ItemsModule } from 'src/items/items.module';
import { CategoriesModule } from 'src/categories/categories.module';
import { PlaylistsModule } from 'src/playlists/playlists.module';
import { ConfigModule } from '@nestjs/config';
import { ApplicationsModule } from 'src/applications/applications.module';
import { CategoryPlaylistsModule } from 'src/category-playlists/category-playlists.module';

@Module({
  imports: [
    forwardRef(() => ItemsModule),
    forwardRef(() => CategoriesModule),
    forwardRef(() => PlaylistsModule),
    forwardRef(() => ApplicationsModule),
    ConfigModule,
    forwardRef(() => CategoryPlaylistsModule),
  ],
  controllers: [SearchController],
  providers: [SearchService, ElasticsearchService],
  exports: [SearchService, ElasticsearchService],
})
export class SearchModule {}
