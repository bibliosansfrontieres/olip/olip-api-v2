import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { CreateItemIndexDto } from './dto/create-item-index.dto';
import { ElasticsearchService } from './elasticsearch.service';
import { CreatePlaylistIndexDto } from './dto/create-playlist-index.dto';
import { CreateCategoryIndexDto } from './dto/create-category-index.dto';
import { ItemsService } from 'src/items/items.service';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { CategoriesService } from 'src/categories/categories.service';
import { Item } from 'src/items/entities/item.entity';
import { Category } from 'src/categories/entities/category.entity';
import { GetSearchDto } from './dto/get-search.dto';
import { PageDto } from 'src/utils/classes/page.dto';
import { PageOptions } from 'src/utils/classes/page-options';
import { SearchDto } from 'src/utils/classes/search.dto';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { ApplicationsService } from 'src/applications/applications.service';
import { CreateApplicationIndexDto } from './dto/create-application-index.dto';
import { Application } from 'src/applications/entities/application.entity';
import { CategoryPlaylistsService } from 'src/category-playlists/category-playlists.service';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class SearchService {
  private vmId: string;

  constructor(
    private elasticsearchService: ElasticsearchService,
    @Inject(forwardRef(() => ItemsService))
    private itemsService: ItemsService,
    @Inject(forwardRef(() => PlaylistsService))
    private playlistsService: PlaylistsService,
    @Inject(forwardRef(() => ApplicationsService))
    private applicationsService: ApplicationsService,
    @Inject(forwardRef(() => CategoriesService))
    private categoriesService: CategoriesService,
    @Inject(forwardRef(() => CategoryPlaylistsService))
    private categoryPlaylistsService: CategoryPlaylistsService,
    private configService: ConfigService,
  ) {
    this.vmId = configService.get('VM_ID');
  }

  async isReady() {
    return this.elasticsearchService.isReady();
  }

  async indexItem(itemId: string) {
    const item = await this.itemsService.findOne({
      where: { id: itemId },
      relations: {
        dublinCoreItem: { dublinCoreItemTranslations: { language: true } },
      },
    });
    for (const dublinCoreItemTranslation of item.dublinCoreItem
      .dublinCoreItemTranslations) {
      const createItemIndexDto: CreateItemIndexDto = new CreateItemIndexDto(
        this.vmId,
        {
          id: item.id,
          title: dublinCoreItemTranslation.title,
          suggest: dublinCoreItemTranslation.title.split(' '),
          description: dublinCoreItemTranslation.description,
          // TODO : content doesn't exists for now, using title
          content: dublinCoreItemTranslation.title,
          languageIdentifier: dublinCoreItemTranslation.language.identifier,
        },
      );
      const index = await this.elasticsearchService.index(createItemIndexDto);
      dublinCoreItemTranslation.indexId = index._id;
      await this.itemsService.saveTranslation(dublinCoreItemTranslation);
    }
  }

  async removeItemIndex(itemId: string) {
    const item = await this.itemsService.findOne({
      where: { id: itemId },
      relations: {
        dublinCoreItem: { dublinCoreItemTranslations: { language: true } },
      },
    });
    const index = `${this.vmId}-item`;
    for (const dublinCoreItemTranslation of item.dublinCoreItem
      .dublinCoreItemTranslations) {
      await this.elasticsearchService.removeIndex(
        index,
        dublinCoreItemTranslation.indexId,
      );
      dublinCoreItemTranslation.indexId = undefined;
      await this.itemsService.saveTranslation(dublinCoreItemTranslation);
    }
  }

  async indexPlaylist(playlistId: string) {
    const playlist = await this.playlistsService.findOne({
      where: { id: playlistId },
      relations: { playlistTranslations: { language: true } },
    });
    for (const playlistTranslation of playlist.playlistTranslations) {
      const createPlaylistIndexDto: CreatePlaylistIndexDto =
        new CreatePlaylistIndexDto(this.vmId, {
          id: playlist.id,
          title: playlistTranslation.title,
          description: playlistTranslation.description,
          languageIdentifier: playlistTranslation.language.identifier,
          suggest: playlistTranslation.title.split(' '),
        });
      const index = await this.elasticsearchService.index(
        createPlaylistIndexDto,
      );
      playlistTranslation.indexId = index._id;
      await this.playlistsService.saveTranslation(playlistTranslation);
    }
    return playlist;
  }

  async removePlaylistIndex(playlistId: string) {
    const playlist = await this.playlistsService.findOne({
      where: { id: playlistId },
      relations: {
        playlistTranslations: true,
      },
    });
    const index = `${this.vmId}-playlist`;
    for (const playlistTranslation of playlist.playlistTranslations) {
      await this.elasticsearchService.removeIndex(
        index,
        playlistTranslation.indexId,
      );
      playlistTranslation.indexId = undefined;
      await this.playlistsService.saveTranslation(playlistTranslation);
    }
  }

  async indexCategory(categoryId: number) {
    const category = await this.categoriesService.findOne({
      where: { id: categoryId },
      relations: { categoryTranslations: { language: true } },
    });
    for (const categoryTranslation of category.categoryTranslations) {
      const createCategoryIndexDto: CreateCategoryIndexDto =
        new CreateCategoryIndexDto(this.vmId, {
          id: category.id,
          title: categoryTranslation.title,
          suggest: categoryTranslation.title.split(' '),
          description: categoryTranslation.description,
          languageIdentifier: categoryTranslation.language.identifier,
        });
      const index = await this.elasticsearchService.index(
        createCategoryIndexDto,
      );
      categoryTranslation.indexId = index._id;
      await this.categoriesService.saveTranslation(categoryTranslation);
    }
  }

  async indexApplication(applicationId: string) {
    const application = await this.applicationsService.findOne({
      where: { id: applicationId },
      relations: { applicationTranslations: { language: true } },
    });
    for (const applicationTranslation of application.applicationTranslations) {
      const createApplicationIndexDto: CreateApplicationIndexDto =
        new CreateApplicationIndexDto(this.vmId, {
          id: application.id,
          title: application.name,
          suggest: application.name.split(' '),
          shortDescription: applicationTranslation.shortDescription,
          longDescription: applicationTranslation.longDescription,
          languageIdentifier: applicationTranslation.language.identifier,
        });
      const index = await this.elasticsearchService.index(
        createApplicationIndexDto,
      );
      applicationTranslation.indexId = index._id;
      await this.applicationsService.saveTranslation(applicationTranslation);
    }
  }

  private async searchItems(
    getSearchDto: GetSearchDto,
    pageOptions: PageOptions,
  ) {
    const itemResults = await this.elasticsearchService.search({
      query: getSearchDto.query,
      index: `${this.vmId}-item`,
      size: pageOptions.take,
      from: pageOptions.skip,
    });
    const items: Item[] = [];
    for (const hit of itemResults.hits.hits) {
      const item = await this.itemsService.findOne({
        where: { id: hit._source.id },
        relations: {
          dublinCoreItem: { dublinCoreType: true },
          application: { applicationType: true },
        },
      });
      if (item) {
        items.push(item);
      }
    }
    await this.itemsService.getDublinCoreItemsTranslations(
      items,
      getSearchDto.languageIdentifier,
    );
    return { items, itemResults };
  }

  private async searchPlaylists(
    getSearchDto: GetSearchDto,
    pageOptions: PageOptions,
  ) {
    const playlistResults = await this.elasticsearchService.search({
      query: getSearchDto.query,
      index: `${this.vmId}-playlist`,
      size: pageOptions.take,
    });
    const categoryPlaylists: CategoryPlaylist[] = [];
    for (const hit of playlistResults.hits.hits) {
      const categoryPlaylist = await this.categoryPlaylistsService.findOne({
        where: { playlist: { id: hit._source.id } },
        relations: { playlist: { items: true }, category: true },
      });
      if (categoryPlaylist && categoryPlaylist.category) {
        categoryPlaylists.push(categoryPlaylist);
      }
    }

    await this.categoryPlaylistsService.getPlaylistsTranslations(
      getSearchDto.languageIdentifier,
      categoryPlaylists,
    );

    for (const categoryPlaylist of categoryPlaylists) {
      categoryPlaylist['totalItems'] = categoryPlaylist.playlist.items.length;
      delete categoryPlaylist.playlist.items;
    }

    return { categoryPlaylists, playlistResults };
  }

  private async searchCategories(
    getSearchDto: GetSearchDto,
    pageOptions: PageOptions,
  ) {
    const categoryResults = await this.elasticsearchService.search({
      query: getSearchDto.query,
      index: `${this.vmId}-category`,
      size: pageOptions.take,
    });
    const categories: Category[] = [];
    for (const hit of categoryResults.hits.hits) {
      const category = await this.categoriesService.findOne({
        where: { id: hit._source.id },
        relations: { categoryTranslations: true },
      });
      if (category) {
        categories.push(category);
      }
    }

    await this.categoriesService.getTranslations(
      categories,
      getSearchDto.languageIdentifier,
    );

    return { categories, categoryResults };
  }

  private async searchApplications(
    getSearchDto: GetSearchDto,
    pageOptions: PageOptions,
  ) {
    const applicationResults = await this.elasticsearchService.search({
      query: getSearchDto.query,
      index: `${this.vmId}-application`,
      size: pageOptions.take,
    });
    const applications: Application[] = [];
    for (const hit of applicationResults.hits.hits) {
      const application = await this.applicationsService.findOne({
        where: { id: hit._source.id },
        relations: { applicationType: true },
      });
      if (application) {
        applications.push(application);
      }
    }

    await this.applicationsService.getApplicationsTranslations(
      applications,
      getSearchDto.languageIdentifier,
    );

    return { applications, applicationResults };
  }

  private async getItemsGlobalResult(
    getSearchDto: GetSearchDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const pageOptions = new PageOptions(pageOptionsDto);
    const { items, itemResults } = await this.searchItems(
      getSearchDto,
      pageOptions,
    );
    const search = new SearchDto(items, itemResults.hits.total.value);
    return search;
  }

  async getItemsResult(
    getSearchDto: GetSearchDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const pageOptions = new PageOptions(pageOptionsDto);
    const { items, itemResults } = await this.searchItems(
      getSearchDto,
      pageOptions,
    );
    const page = new PageDto(items, itemResults.hits.total.value, pageOptions);
    return page;
  }

  private async getPlaylistsGlobalResult(
    getSearchDto: GetSearchDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const pageOptions = new PageOptions(pageOptionsDto);
    const { categoryPlaylists, playlistResults } = await this.searchPlaylists(
      getSearchDto,
      pageOptions,
    );
    const search = new SearchDto(
      categoryPlaylists,
      playlistResults.hits.total.value,
    );
    return search;
  }

  async getPlaylistsResult(
    getSearchDto: GetSearchDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const pageOptions = new PageOptions(pageOptionsDto);
    const { categoryPlaylists, playlistResults } = await this.searchPlaylists(
      getSearchDto,
      pageOptions,
    );
    const page = new PageDto(
      categoryPlaylists,
      playlistResults.hits.total.value,
      pageOptions,
    );
    return page;
  }

  private async getCategoriesGlobalResult(
    getSearchDto: GetSearchDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const pageOptions = new PageOptions(pageOptionsDto);
    const { categories, categoryResults } = await this.searchCategories(
      getSearchDto,
      pageOptions,
    );
    const search = new SearchDto(categories, categoryResults.hits.total.value);
    return search;
  }

  async getCategoriesResult(
    getSearchDto: GetSearchDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const pageOptions = new PageOptions(pageOptionsDto);
    const { categories, categoryResults } = await this.searchCategories(
      getSearchDto,
      pageOptions,
    );
    const page = new PageDto(
      categories,
      categoryResults.hits.total.value,
      pageOptions,
    );
    return page;
  }

  private async getApplicationsGlobalResult(
    getSearchDto: GetSearchDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const pageOptions = new PageOptions(pageOptionsDto);
    const { applications, applicationResults } = await this.searchApplications(
      getSearchDto,
      pageOptions,
    );
    const search = new SearchDto(
      applications,
      applicationResults.hits.total.value,
    );
    return search;
  }

  async getApplicationsResult(
    getSearchDto: GetSearchDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const pageOptions = new PageOptions(pageOptionsDto);
    const { applications, applicationResults } = await this.searchApplications(
      getSearchDto,
      pageOptions,
    );
    const page = new PageDto(
      applications,
      applicationResults.hits.total.value,
      pageOptions,
    );
    return page;
  }

  async indexAll() {
    await this.applicationsService.indexAll();
    await this.playlistsService.indexAll();
    await this.itemsService.indexAll();
    await this.categoriesService.indexAll();
  }

  async search(getSearchDto: GetSearchDto, pageOptionsDto: PageOptionsDto) {
    return {
      items: await this.getItemsGlobalResult(getSearchDto, pageOptionsDto),
      playlists: await this.getPlaylistsGlobalResult(
        getSearchDto,
        pageOptionsDto,
      ),
      categories: await this.getCategoriesGlobalResult(
        getSearchDto,
        pageOptionsDto,
      ),
      applications: await this.getApplicationsGlobalResult(
        getSearchDto,
        pageOptionsDto,
      ),
    };
  }

  async getSuggestions(query: string, take?: string) {
    try {
      const suggestionsResult = await this.elasticsearchService.getSuggestions(
        this.vmId,
        query,
        take,
      );
      const suggestions = suggestionsResult.suggest.suggestion;
      const lastSuggestion = suggestions[suggestions.length - 1];
      const suggestionsList = lastSuggestion.options;
      const suggestionsArray: { id: string; suggestion: string }[] = [];
      for (const suggestion of suggestionsList) {
        suggestionsArray.push({
          id: suggestion._id,
          suggestion: suggestion._source.title,
        });
      }
      return suggestionsArray;
    } catch (e) {
      console.error(e);
      return [];
    }
  }
}
