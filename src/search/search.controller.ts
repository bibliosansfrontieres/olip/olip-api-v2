import { Controller, Get, Query } from '@nestjs/common';
import { SearchService } from './search.service';
import { GetSearchDto } from './dto/get-search.dto';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';

@Controller('api/search')
export class SearchController {
  constructor(private readonly searchService: SearchService) {}

  @Get()
  search(
    @Query() getSearchDto: GetSearchDto,
    @Query() pageOptionsDto: PageOptionsDto,
  ) {
    return this.searchService.search(getSearchDto, pageOptionsDto);
  }

  @Get('index')
  index() {
    return this.searchService.indexAll();
  }

  @Get('items')
  searchItems(
    @Query() getSearchDto: GetSearchDto,
    @Query() pageOptionsDto: PageOptionsDto,
  ) {
    return this.searchService.getItemsResult(getSearchDto, pageOptionsDto);
  }

  @Get('playlists')
  searchPlaylists(
    @Query() getSearchDto: GetSearchDto,
    @Query() pageOptionsDto: PageOptionsDto,
  ) {
    return this.searchService.getPlaylistsResult(getSearchDto, pageOptionsDto);
  }

  @Get('categories')
  searchCategories(
    @Query() getSearchDto: GetSearchDto,
    @Query() pageOptionsDto: PageOptionsDto,
  ) {
    return this.searchService.getCategoriesResult(getSearchDto, pageOptionsDto);
  }

  @Get('applications')
  searchApplications(
    @Query() getSearchDto: GetSearchDto,
    @Query() pageOptionsDto: PageOptionsDto,
  ) {
    return this.searchService.getApplicationsResult(
      getSearchDto,
      pageOptionsDto,
    );
  }

  @Get('suggestions')
  getSuggestions(@Query('query') query: string, @Query('take') take?: string) {
    return this.searchService.getSuggestions(query, take);
  }
}
