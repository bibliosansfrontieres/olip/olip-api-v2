import { CreateIndexDto } from './create-index.dto';

export class CreateCategoryIndexDto extends CreateIndexDto {
  index: string;
  data: {
    id: number;
    title: string;
    description?: string;
    languageIdentifier: string;
    suggest: string[];
  };

  constructor(
    vmId: string,
    data: {
      id: number;
      title: string;
      description?: string;
      suggest: string[];
      languageIdentifier: string;
    },
  ) {
    super();
    this.index = `${vmId}-category`;
    this.data = data;
  }
}
