import { CreateIndexDto } from './create-index.dto';

export class CreateItemIndexDto extends CreateIndexDto {
  index: string;
  data: {
    id: string;
    title: string;
    description: string;
    content: string;
    languageIdentifier: string;
    suggest: string[];
  };

  constructor(
    vmId: string,
    data: {
      id: string;
      title: string;
      description: string;
      content: string;
      languageIdentifier: string;
      suggest: string[];
    },
  ) {
    super();
    this.index = `${vmId}-item`;
    this.data = data;
  }
}
