import { CreateIndexDto } from './create-index.dto';

export class CreatePlaylistIndexDto extends CreateIndexDto {
  index: string;
  data: {
    id: string;
    title: string;
    description: string;
    languageIdentifier: string;
    suggest: string[];
  };

  constructor(
    vmId: string,
    data: {
      id: string;
      title: string;
      description: string;
      languageIdentifier: string;
      suggest: string[];
    },
  ) {
    super();
    this.index = `${vmId}-playlist`;
    this.data = data;
  }
}
