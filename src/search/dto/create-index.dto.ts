export class CreateIndexDto {
  index: string;
  data: object;
}
