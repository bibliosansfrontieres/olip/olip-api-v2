import { IsString } from 'class-validator';

export class GetSearchDto {
  @IsString()
  query: string;

  @IsString()
  languageIdentifier: string;
}
