import { CreateIndexDto } from './create-index.dto';

export class CreateApplicationIndexDto extends CreateIndexDto {
  index: string;
  data: {
    id: string;
    title: string;
    shortDescription?: string;
    longDescription?: string;
    languageIdentifier: string;
    suggest: string[];
  };

  constructor(
    vmId: string,
    data: {
      id: string;
      title: string;
      shortDescription?: string;
      longDescription?: string;
      suggest: string[];
      languageIdentifier: string;
    },
  ) {
    super();
    this.index = `${vmId}-application`;
    this.data = data;
  }
}
