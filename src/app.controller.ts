import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiProperty } from '@nestjs/swagger';

export class IsAliveDto {
  @ApiProperty({ example: true })
  isAlive = true;
}

@Controller('api')
export class AppController {
  @Get('healthcheck')
  @ApiOperation({ summary: 'Get the status of the app' })
  @ApiOkResponse({ type: IsAliveDto })
  isAlive(): IsAliveDto {
    return new IsAliveDto();
  }
}
