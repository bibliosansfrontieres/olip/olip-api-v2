import { SelectQueryBuilder } from 'typeorm';

export async function getManyAndCount<T>(
  qb: SelectQueryBuilder<any>,
  groupColumns: string[],
  join: boolean = true,
): Promise<[number, T[]]> {
  return Promise.all([
    new Promise<number>((resolve, reject) => {
      const cQb = qb.clone();
      cQb.skip(0);
      cQb.select(`COUNT(DISTINCT (${groupColumns.join(', ')})) as c`);
      cQb.orderBy(null);
      cQb
        .getRawOne()
        .then((value) => resolve(value.c ? Number(value.c) : 0))
        .catch((e) => reject(e));
    }),
    new Promise<T[]>((resolve, reject) => {
      const vQb = qb.clone();
      if (join) {
        vQb.groupBy(groupColumns.join(', '));
      }
      vQb
        .getMany()
        .then((values) => resolve(values))
        .catch((e) => reject(e));
    }),
  ]);
}
