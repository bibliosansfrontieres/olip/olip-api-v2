import { Request } from 'express';

/**
 * Extracts a Bearer token from the header of a request.
 *
 * @param {Request} request - The request object.
 * @return {string | undefined} The extracted token, or undefined if not found.
 */
export function extractTokenFromHeaders(request: Request): string | undefined {
  const [type, token] = request.headers.authorization?.split(' ') ?? [];
  return type === 'Bearer' ? token : undefined;
}
