import { existsSync, rmSync, writeFileSync } from 'fs';

export function writeEnvFile(envPath: string, envValues: Map<string, string>) {
  if (existsSync(envPath)) {
    rmSync(envPath);
  }

  let envContent = '';

  for (const [key, value] of envValues) {
    envContent += `${key}=${value}\n`;
  }

  writeFileSync(envPath, envContent);
}
