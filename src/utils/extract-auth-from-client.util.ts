import { Socket } from 'socket.io';

/**
 * Extracts the authentication information from the client object.
 *
 * @param {Socket} client - The client object representing a socket connection.
 * @return {{ token: string | undefined, clientId: string | undefined }} - An object containing the extracted authentication information, including the token and client ID.
 */
export function extractAuthFromClient(client: Socket): {
  token: string | undefined;
  clientId: string | undefined;
} {
  const token = client.handshake.auth.token ?? undefined;
  const clientId = client.handshake.auth.clientId ?? undefined;
  return { token, clientId };
}
