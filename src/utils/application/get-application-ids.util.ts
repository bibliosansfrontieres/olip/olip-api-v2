import { Category } from 'src/categories/entities/category.entity';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';

/**
 * Get application ids from categories playlists
 *
 * @param {Category[]} categories The categories to get the application ids from
 * @return {string[]}
 */
export function getApplicationIdsFromCategories(
  categories: Category[],
): string[] {
  return [
    ...new Set(
      categories.flatMap((category) =>
        category.categoryPlaylists.map(
          (categoryPlaylist) => categoryPlaylist.playlist.application.id,
        ),
      ),
    ),
  ];
}

/**
 * Get application ids from categoryPlaylists playlists
 *
 * @param {CategoryPlaylist[]} categoryPlaylists The categoryPlaylists to get the application ids from
 * @return {string[]}
 */
export function getApplicationIdsFromCategoryPlaylists(
  categoryPlaylists: CategoryPlaylist[],
): string[] {
  return [
    ...new Set(
      categoryPlaylists.map(
        (categoryPlaylist) => categoryPlaylist.playlist.application.id,
      ),
    ),
  ];
}

/**
 * Get application ids from categoryPlaylistItems items
 *
 * @param {CategoryPlaylistItem[]} categoryPlaylistItems The categoryPlaylistItems to get the application ids from
 * @return {string[]}
 */
export function getApplicationIdsFromCategoryPlaylistItems(
  categoryPlaylistItems: CategoryPlaylistItem[],
): string[] {
  return [
    ...new Set(
      categoryPlaylistItems.map(
        (categoryPlaylistItem) => categoryPlaylistItem.item.application.id,
      ),
    ),
  ];
}
