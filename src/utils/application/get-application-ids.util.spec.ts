import { categoryMock } from 'src/tests/mocks/objects/category.mock';
import { getApplicationIdsFromCategories } from './get-application-ids.util';
import { categoryPlaylistMock } from 'src/tests/mocks/objects/category-playlist.mock';
import { playlistMock } from 'src/tests/mocks/objects/playlist.mock';
import { applicationMock } from 'src/tests/mocks/objects/application.mock';

describe('GetApplicationIdsUtil', () => {
  describe('getApplicationIdsFromCategories', () => {
    const categoriesMock = [
      categoryMock({
        categoryPlaylists: [
          categoryPlaylistMock({
            playlist: playlistMock({
              application: applicationMock({ id: '1' }),
            }),
          }),
          categoryPlaylistMock({
            playlist: playlistMock({
              application: applicationMock({ id: '2' }),
            }),
          }),
        ],
      }),
      categoryMock({
        categoryPlaylists: [
          categoryPlaylistMock({
            playlist: playlistMock({
              application: applicationMock({ id: '1' }),
            }),
          }),
          categoryPlaylistMock({
            playlist: playlistMock({
              application: applicationMock({ id: '3' }),
            }),
          }),
        ],
      }),
    ];

    it('should be defined', () => {
      expect(getApplicationIdsFromCategories).toBeDefined();
    });

    it('should return an array', () => {
      const result = getApplicationIdsFromCategories(categoriesMock);
      expect(Array.isArray(result)).toBe(true);
    });

    it('should return not return empty array', () => {
      const result = getApplicationIdsFromCategories(categoriesMock);
      expect(result.length).toBeGreaterThan(0);
    });

    it('should return an array of strings', () => {
      const result = getApplicationIdsFromCategories(categoriesMock);
      expect(typeof result[0]).toBe('string');
    });

    it('should return unique ids', () => {
      const result = getApplicationIdsFromCategories(categoriesMock);
      expect(new Set(result).size).toBe(result.length);
    });
  });
});
