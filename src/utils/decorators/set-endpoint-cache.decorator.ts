import { applyDecorators } from '@nestjs/common';
import { EntityClassOrSchema } from '@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type';
import { SetCache } from './set-cache.decorator';

export function SetEndpointCache(
  controllerName: string,
  functionName: string,
  invalidatingEntities: EntityClassOrSchema[] | EntityClassOrSchema,
  ttl?: number,
) {
  const key = `controller:${controllerName}:${functionName}`;
  return applyDecorators(SetCache(key, invalidatingEntities, ttl));
}
