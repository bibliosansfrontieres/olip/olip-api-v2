import { applyDecorators, UseInterceptors } from '@nestjs/common';
import { CacheInterceptor, CacheKey, CacheTTL } from '@nestjs/cache-manager';
import { EntityClassOrSchema } from '@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type';
import { registerCache } from '../caching.util';
import { toArray } from '../to-array.util';

export function SetCache(
  key: string,
  invalidatingEntities: EntityClassOrSchema[] | EntityClassOrSchema,
  ttl?: number,
) {
  const entities: EntityClassOrSchema[] = toArray(invalidatingEntities);
  registerCache(key, entities);
  const decorators = [UseInterceptors(CacheInterceptor), CacheKey(key)];
  if (ttl) {
    decorators.push(CacheTTL(ttl));
  }
  return applyDecorators(...decorators);
}
