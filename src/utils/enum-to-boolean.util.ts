import { TrueFalseEnum } from './enums/true-false.enum';

export function enumToBoolean(
  value: TrueFalseEnum | undefined,
  undefinedDefaultValue: boolean = false,
): boolean {
  if (value === TrueFalseEnum.TRUE) return true;
  if (value === TrueFalseEnum.FALSE) return false;
  return undefinedDefaultValue;
}
