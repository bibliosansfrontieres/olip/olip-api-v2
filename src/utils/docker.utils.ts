import { NotFoundException } from '@nestjs/common';
import { existsSync } from 'fs';
import { exec } from 'child_process';

async function dockerExec(dockerCommand: string) {
  return new Promise((resolve) => {
    exec(`docker ${dockerCommand}`, (error, stdout, stderr) => {
      if (error || stderr) {
        resolve({ raw: stdout || stderr });
        return;
      }

      resolve({ raw: stdout || stderr });
    });
  });
}

export class Docker {
  private log: boolean;
  constructor(log?: boolean) {
    if (log !== undefined) {
      this.log = log;
    } else {
      this.log = true;
    }
  }

  login(username: string, password: string, registry?: string) {
    registry = registry || 'docker.io';
    return this.exec(`login -u ${username} -p ${password} ${registry}`);
  }

  pull(image: string) {
    return this.exec(`pull ${image}`);
  }

  run(
    image: string,
    options?: {
      volumes?: string | string[];
      name?: string;
      rm?: boolean;
      network?: string;
      environment?: string[];
    },
  ) {
    const runOptions = [];
    if (options) {
      if (options.volumes) {
        if (typeof options.volumes === 'string') {
          runOptions.push(`--volume ${options.volumes}`);
        } else {
          for (const volume of options.volumes) {
            runOptions.push(`--volume ${volume}`);
          }
        }
      }
      if (options.name) {
        runOptions.push(`--name ${options.name}`);
      }
      if (options.rm === true) {
        runOptions.push('--rm');
      }
      if (options.network) {
        runOptions.push(`--network ${options.network}`);
      }
      if (options.environment) {
        for (const variable of options.environment) {
          runOptions.push(`-e ${variable}`);
        }
      }
    }
    const runOptionsString =
      runOptions.length > 0 ? runOptions.join(' ') + ' ' : '';
    return this.exec(`run ${runOptionsString}${image}`);
  }

  inspect(image: string) {
    return this.exec(`inspect ${image}`);
  }

  manifest(action: 'inspect', image: string, verbose?: boolean) {
    const options = [];
    if (verbose === true) {
      options.push('v');
    }
    return this.exec(
      `manifest ${action} ${image}${options.map((option) => ' -' + option)}`,
    );
  }

  async compose(
    {
      composePath,
      composeOverridePath,
    }: { composePath: string; composeOverridePath: string },
    command: 'up' | 'pull' | 'down' | 'restart' | 'build',
    options?: string[],
  ) {
    try {
      if (!existsSync(composePath)) {
        throw new NotFoundException('compose file not found');
      }
      if (!existsSync(composeOverridePath)) {
        throw new NotFoundException('compose override file not found');
      }

      if (!options) options = [];

      await this.exec(
        `compose -f ${composePath} -f ${composeOverridePath} ${command} ${options.join(
          ' ',
        )}`,
      );
      return true;
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  async exec(command: string): Promise<any> {
    const result = await dockerExec(command);
    if (this.log) {
      console.log(result);
    }
    return result;
  }
}
