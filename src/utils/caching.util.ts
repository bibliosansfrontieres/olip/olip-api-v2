import { EntityClassOrSchema } from '@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type';
import { toArray } from './to-array.util';
import { Cache } from 'cache-manager';

interface RegisteredCache {
  key: string;
  entities: EntityClassOrSchema[];
}

/////////////////////
//                 //
//  Cache Manager  //
//                 //
/////////////////////

let cacheManager: Cache | undefined = undefined;

const registeredCaches: RegisteredCache[] = [];

/**
 * Sets the cache manager to the specified cache.
 * This function is used only once, at AppModule initialization.
 *
 * @param {Cache} cache - The cache to be set as the cache manager.
 * @return {void} This function does not return anything.
 */
export function setCacheManager(cache: Cache): void {
  cacheManager = cache;
}

/**
 * Registers a cache with the specified key and entities.
 * This function is used when you want a cacheKey being invalidated by editing entities.
 *
 * @param {string} key - The key of the cache.
 * @param {EntityClassOrSchema[]} entities - An array of entity classes or schemas to be associated with the cache.
 * @return {void} This function does not return anything.
 */
export function registerCache(
  key: string,
  entities: EntityClassOrSchema[],
): void {
  registeredCaches.push({ key, entities });
}

/////////////////////
//                 //
//  Cache Methods  //
//                 //
/////////////////////

/**
 * Sets a method result in the cache using the specified key.
 *
 * @param {string} key - The key to use for storing the value in the cache.
 * @param {T} result - The value to be stored in the cache.
 * @param {number} [ttl] - The time-to-live (TTL) for the cached value (optional).
 * @return {Promise<boolean>} Returns true if the value was successfully set in the cache, false otherwise.
 */
async function setMethodCache<T>(
  key: string,
  result: T,
  ttl?: number,
): Promise<boolean> {
  if (cacheManager === undefined) return false;
  await cacheManager.set(key, result, ttl);
  return true;
}

/**
 * Retrieves a method result from the cache based on the provided key.
 *
 * @param {string} key - The key used to retrieve the value from the cache.
 * @returns {Promise<T | undefined>} A promise that resolves to the value retrieved from the cache, or undefined if the cache manager is not defined.
 */
async function getMethodCache<T>(key: string): Promise<T | undefined> {
  if (cacheManager === undefined) return undefined;
  return cacheManager.get<T>(key);
}

/**
 * Returns an array of cache keys generated from the provided `keyObjects`.
 *
 * @param {object | object[]} keyObjects - The object or array of objects from which to generate the cache keys.
 * @return {string[]} An array of cache keys.
 */
function getCacheKeys(keyObjects: object | object[]): string[] {
  const objects: object[] = toArray(keyObjects);

  const keys: string[] = [];
  for (const object of objects) {
    // Sort each object property in the same order everytime
    const objectProperties = Object.keys(object).sort();

    // Get all the object values sorted by ordered object properties
    const objectValues: string[] = objectProperties.map((objectProperty) =>
      String(object[objectProperty]),
    );

    keys.push(...objectValues);
  }

  return keys;
}

/**
 * Retrieves a cached value for a given method, or caches the value if it does not exist.
 *
 * @param {string} serviceName - The name of the service.
 * @param {string} methodName - The name of the method.
 * @param {object | object[]} keyObjects - The objects used to generate the cache key.
 * @param {() => Promise<T> | T} getValue - A function that retrieves the value to be cached.
 * @param {EntityClassOrSchema[] | EntityClassOrSchema} [invalidatingEntities] - Optional. The entities that invalidate the cache.
 * @returns {Promise<T>} - The cached value.
 */
export async function cachedMethod<T>(
  serviceName: string,
  methodName: string,
  keyObjects: object | object[],
  getValue: () => Promise<T> | T,
  invalidatingEntities?: EntityClassOrSchema[] | EntityClassOrSchema,
): Promise<T> {
  const objects: object[] = toArray(keyObjects);
  const keys = getCacheKeys(objects);
  const cacheKey = `service:${serviceName}:${methodName}:${keys.join(':')}`;

  // Try to retrieve cached method
  let cache: T = await getMethodCache(cacheKey);
  if (cache) {
    return cache;
  }

  // If we didn't retrieve cached method we cache it
  cache = await getValue();
  await setMethodCache(cacheKey, cache);

  // If there are invalidating entities dependency, we register cacheKey with entities
  if (invalidatingEntities) {
    const entities = toArray(invalidatingEntities);
    registerCache(cacheKey, entities);
  }

  return cache;
}

//////////////////////////
//                      //
//  Cache Invalidation  //
//                      //
//////////////////////////

/**
 * Invalidates the cache for the given entities.
 *
 * @param {EntityClassOrSchema[] | EntityClassOrSchema} entitiesToInvalidate - The entities to invalidate the cache for.
 * @return {Promise<void>} - A promise that resolves when the cache is invalidated.
 */
export async function invalidateCache(
  entitiesToInvalidate: EntityClassOrSchema[] | EntityClassOrSchema,
) {
  if (cacheManager === undefined) return;
  const entities: EntityClassOrSchema[] = toArray(entitiesToInvalidate);

  // For each entity to invalidate
  for (const entity of entities) {
    // We look if a key is registered, linked to this entity
    const registeredCachesFound = registeredCaches.filter((registeredCache) =>
      registeredCache.entities.includes(entity),
    );
    // For each keys registered linked to entity to invalidate
    for (const registeredCacheFound of registeredCachesFound) {
      // We delete the key that was found
      await cacheManager.del(registeredCacheFound.key);
    }
  }
}
