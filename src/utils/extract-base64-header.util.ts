export function extractBase64Header(base64Data: string) {
  const dataSplit = base64Data.split('base64,');
  if (dataSplit.length === 1) return { header: undefined, data: base64Data };
  return { header: dataSplit[0], data: dataSplit[1] };
}
