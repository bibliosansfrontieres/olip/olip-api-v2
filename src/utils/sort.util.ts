export function sortByObjectPropertyLength<T, K extends keyof T>(
  array: T[],
  propertyName: K & (T[K] extends any[] ? K : never),
) {
  return array.sort((a: T, b: T) => {
    let aProperty = a[propertyName] as Array<unknown>;
    let bProperty = b[propertyName] as Array<unknown>;

    if (aProperty === undefined) aProperty = [];
    if (bProperty === undefined) bProperty = [];

    const countA = aProperty.length;
    const countB = bProperty.length;

    if (countA === countB) {
      return 0; // No change in order if both categories have the same playlist count
    }

    if (countA === 0) {
      return 1; // Category 'a' has no playlists, so it should come after category 'b'
    }

    if (countB === 0) {
      return -1; // Category 'b' has no playlists, so it should come after category 'a'
    }

    return countB - countA;
  });
}
