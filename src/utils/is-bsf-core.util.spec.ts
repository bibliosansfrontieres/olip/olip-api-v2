import { isBsfCore } from './is-bsf-core.util';
import { existsSync } from 'fs';

jest.mock('fs');

describe('IsBsfCoreUtil', () => {
  describe('isBsfCore', () => {
    it('should be defined', () => {
      expect(isBsfCore).toBeDefined();
    });

    it('should return false if bsf-core file exists', async () => {
      (existsSync as jest.Mock).mockReturnValue(false);
      const result = isBsfCore();
      expect(result).toBe(false);
    });

    it('should return true if bsf-core file exists', async () => {
      (existsSync as jest.Mock).mockReturnValue(true);
      const result = isBsfCore();
      expect(result).toBe(true);
    });
  });
});
