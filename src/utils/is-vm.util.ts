import { ConfigService } from '@nestjs/config';

export function isVm(configService: ConfigService): boolean {
  const vmId = configService.get<string>('VM_ID');
  return vmId !== 'olip';
}
