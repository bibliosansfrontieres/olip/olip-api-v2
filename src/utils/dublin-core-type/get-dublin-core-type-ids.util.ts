import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';

/**
 * Get dublinCoreType ids from categoryPlaylistItems
 *
 * @param {CategoryPlaylistItem[]} categoryPlaylistItems The categoryPlaylistItems to get the dublinCoreType ids from
 * @return {string[]}
 */
export function getDublinCoreIdsFromCategoryPlaylistItems(
  categoryPlaylistItems: CategoryPlaylistItem[],
): number[] {
  return [
    ...new Set(
      categoryPlaylistItems.map(
        (categoryPlaylistItem) =>
          categoryPlaylistItem.item.dublinCoreItem.dublinCoreType.id,
      ),
    ),
  ];
}
