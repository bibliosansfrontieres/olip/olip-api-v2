import { getDublinCoreIdsFromCategoryPlaylistItems } from './get-dublin-core-type-ids.util';
import { categoryPlaylistItemMock } from 'src/tests/mocks/objects/category-playlist-item.mock';
import { itemMock } from 'src/tests/mocks/objects/item.mock';
import { dublinCoreItemMock } from 'src/tests/mocks/objects/dublin-core-item.mock';
import { dublinCoreTypeMock } from 'src/tests/mocks/objects/dublin-core-type.mock';

describe('GetApplicationIdsUtil', () => {
  describe('getApplicationIdsFromCategories', () => {
    const categoryPlaylistItems = [
      categoryPlaylistItemMock({
        item: itemMock({
          dublinCoreItem: dublinCoreItemMock({
            dublinCoreType: dublinCoreTypeMock({ id: 1 }),
          }),
        }),
      }),
      categoryPlaylistItemMock({
        item: itemMock({
          dublinCoreItem: dublinCoreItemMock({
            dublinCoreType: dublinCoreTypeMock({ id: 2 }),
          }),
        }),
      }),
      categoryPlaylistItemMock({
        item: itemMock({
          dublinCoreItem: dublinCoreItemMock({
            dublinCoreType: dublinCoreTypeMock({ id: 2 }),
          }),
        }),
      }),
    ];

    it('should be defined', () => {
      expect(getDublinCoreIdsFromCategoryPlaylistItems).toBeDefined();
    });

    it('should return an array', () => {
      const result = getDublinCoreIdsFromCategoryPlaylistItems(
        categoryPlaylistItems,
      );
      expect(Array.isArray(result)).toBe(true);
    });

    it('should return not return empty array', () => {
      const result = getDublinCoreIdsFromCategoryPlaylistItems(
        categoryPlaylistItems,
      );
      expect(result.length).toBeGreaterThan(0);
    });

    it('should return an array of number', () => {
      const result = getDublinCoreIdsFromCategoryPlaylistItems(
        categoryPlaylistItems,
      );
      expect(typeof result[0]).toBe('number');
    });

    it('should return unique ids', () => {
      const result = getDublinCoreIdsFromCategoryPlaylistItems(
        categoryPlaylistItems,
      );
      expect(new Set(result).size).toBe(result.length);
    });
  });
});
