import { OrderEnum } from './order.enum';
import { PageOptionsDto } from './page-options.dto';

export class PageOptions {
  order: OrderEnum;
  take: number;
  page: number;
  skip: number;

  constructor(pageOptionsDto: PageOptionsDto) {
    this.order = pageOptionsDto.order || OrderEnum.ASC;
    this.take = +pageOptionsDto.take || 12;
    this.page = +pageOptionsDto.page || 1;
    this.skip = (this.page - 1) * this.take;
  }
}
