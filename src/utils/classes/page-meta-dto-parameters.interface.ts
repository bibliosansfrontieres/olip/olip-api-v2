import { PageOptions } from './page-options';

export interface IPageMetaDtoParameters {
  pageOptions: PageOptions;
  totalItemsCount: number;
}
