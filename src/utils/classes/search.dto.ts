import { ApiProperty } from '@nestjs/swagger';
import { SearchMetaDto } from './search-meta.dto';

export class SearchDto<T> {
  @ApiProperty({ isArray: true })
  data: T[];

  @ApiProperty({ type: () => SearchMetaDto })
  meta: SearchMetaDto;

  constructor(data: T[], totalItemsCount: number) {
    const meta = new SearchMetaDto({ totalItemsCount });
    this.data = data;
    this.meta = meta;
  }
}
