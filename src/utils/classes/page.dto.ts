import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsDefined } from 'class-validator';
import { PageMetaDto } from './page-meta.dto';
import { PageOptions } from './page-options';
import { Type } from 'class-transformer';

export class PageDto<T> {
  @IsArray()
  @ApiProperty({ isArray: true })
  data: T[];

  @ApiProperty({ type: () => PageMetaDto })
  @IsDefined()
  @Type(() => PageMetaDto)
  meta: PageMetaDto;

  constructor(data: T[], totalItemsCount: number, pageOptions: PageOptions) {
    const meta = new PageMetaDto({ totalItemsCount, pageOptions });
    this.data = data;
    this.meta = meta;
  }
}
