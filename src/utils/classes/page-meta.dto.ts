import { ApiProperty } from '@nestjs/swagger';
import { IPageMetaDtoParameters } from './page-meta-dto-parameters.interface';

export class PageMetaDto {
  @ApiProperty()
  page: number;

  @ApiProperty()
  take: number;

  @ApiProperty()
  totalItemsCount: number;

  @ApiProperty()
  pageCount: number;

  @ApiProperty()
  hasPreviousPage: boolean;

  @ApiProperty()
  hasNextPage: boolean;

  constructor({ pageOptions, totalItemsCount }: IPageMetaDtoParameters) {
    this.page = pageOptions.page;
    this.take = pageOptions.take;
    this.totalItemsCount = totalItemsCount;
    this.pageCount = Math.ceil(this.totalItemsCount / this.take);
    this.hasPreviousPage = this.page > 1;
    this.hasNextPage = this.page < this.pageCount;
  }
}
