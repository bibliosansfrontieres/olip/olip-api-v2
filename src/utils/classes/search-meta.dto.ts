import { ApiProperty } from '@nestjs/swagger';
import { ISearchMetaDtoParameters } from './search-meta-dto-parameters.interface';

export class SearchMetaDto {
  @ApiProperty()
  totalItemsCount: number;

  constructor({ totalItemsCount }: ISearchMetaDtoParameters) {
    this.totalItemsCount = totalItemsCount;
  }
}
