import { existsSync, readFileSync } from 'fs';
import { Logger } from '@nestjs/common';
import { IContext } from './interfaces/context.interface';

export function getContext(): IContext | null {
  try {
    if (existsSync('/olip-files/deploy/context')) {
      const contextFileContext = readFileSync(
        '/olip-files/deploy/context',
      ).toString();
      const context = JSON.parse(contextFileContext);
      return context as IContext;
    } else {
      Logger.error(
        'Could not find context (/olip-files/deploy/context)',
        'Utils',
      );
      return null;
    }
  } catch (e) {
    console.error(e);
    Logger.error('Context parsing error', 'Utils');
    return null;
  }
}
