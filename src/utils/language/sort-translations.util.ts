import { Language } from 'src/languages/entities/language.entity';

/**
 * Sorts an array of translations by language priority.
 * Translations that match the primary language identifier are prioritized first,
 * followed by translations that match the fallback language identifier,
 * and all other translations are ranked last.
 *
 * @template T Extends { language: Language } that every translation entities use
 * @param {T[]} translations The translations to evaluate
 * @param {string} languageIdentifier The language identifier to match
 * @param {string} [languageIdentifierFallback='eng'] The fallback language identifier to match
 * @return {T[]} The sorted translations
 */
export function sortTranslations<T extends { language: Language }>(
  translations: T[],
  languageIdentifier: string,
  languageIdentifierFallback: string = 'eng',
): T[] {
  return translations.sort((a, b) => {
    const priorityA = getTranslationPriority(
      a,
      languageIdentifier,
      languageIdentifierFallback,
    );
    const priorityB = getTranslationPriority(
      b,
      languageIdentifier,
      languageIdentifierFallback,
    );

    return priorityA - priorityB;
  });
}

/**
 * Returns a priority for a given translation.
 * Higher priority will result in translation being displayed first.
 * Priority 1 is given to translation that match the given language identifier.
 * Priority 2 is given to translation that match the given fallback language identifier.
 * Priority 3 is given to all other translations.
 *
 * @template T Extends { language: Language } that every translation entities use
 * @param {T} translation The translation to evaluate
 * @param {string} languageIdentifier The language identifier to match
 * @param {string} languageIdentifierFallback The fallback language identifier to match
 * @return {number} The priority of the translation
 */
export function getTranslationPriority<T extends { language: Language }>(
  translation: T,
  languageIdentifier: string,
  languageIdentifierFallback: string,
): number {
  if (isSameLanguage(translation, languageIdentifier)) {
    return 1;
  }
  if (isSameLanguage(translation, languageIdentifierFallback)) {
    return 2;
  }
  return 3;
}

/**
 * Returns true if the given translation matches the given language identifier.
 *
 * @template T Extends { language: Language } that every translation entities use
 * @param {T} translation The translation to evaluate
 * @param {string} languageIdentifier The language identifier to match
 * @return {boolean} True if the translation matches the given language identifier
 */
export function isSameLanguage<T extends { language: Language }>(
  translation: T,
  languageIdentifier: string,
): boolean {
  return translation.language.identifier === languageIdentifier;
}
