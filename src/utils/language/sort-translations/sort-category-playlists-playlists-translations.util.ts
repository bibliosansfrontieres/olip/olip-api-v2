import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { sortTranslations } from '../sort-translations.util';

/**
 * Sorts the playlists's translations of the given categoryPlaylists according to the given language identifier.
 *
 * @param {CategoryPlaylist[]} categoryPlaylists The categoryPlaylists to sort their playlists's translations
 * @param {string} languageIdentifier The language identifier to sort the translations with
 */
export function sortCategoryPlaylistsPlaylistsTranslations(
  categoryPlaylists: CategoryPlaylist[],
  languageIdentifier: string,
): void {
  for (const categoryPlaylist of categoryPlaylists) {
    categoryPlaylist.playlist.playlistTranslations = sortTranslations(
      categoryPlaylist.playlist.playlistTranslations,
      languageIdentifier,
    );
  }
}
