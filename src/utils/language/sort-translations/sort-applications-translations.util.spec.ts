import { languageMock } from 'src/tests/mocks/objects/language.mock';
import { applicationMock } from 'src/tests/mocks/objects/application.mock';
import { applicationTranslationMock } from 'src/tests/mocks/objects/application-translation.mock';
import { sortApplicationsTranslations } from './sort-applications-translations.util';
import * as SortTranslationsUtil from '../sort-translations.util';

describe('SortApplicationsTranslationsUtil', () => {
  describe('sortApplicationsTranslations', () => {
    const languageIdentifier = 'fra';
    const applicationsMock = [
      applicationMock({
        applicationTranslations: [
          applicationTranslationMock({ language: languageMock() }),
        ],
      }),
      applicationMock({
        applicationTranslations: [
          applicationTranslationMock({ language: languageMock() }),
        ],
      }),
    ];

    it('should be defined', () => {
      expect(sortApplicationsTranslations).toBeDefined();
    });

    it('should return undefined', () => {
      const result = sortApplicationsTranslations(
        applicationsMock,
        languageIdentifier,
      );
      expect(result).toBe(undefined);
    });

    it('should call sortTranslations with right arguments', () => {
      const sortTranslationsSpy = jest.spyOn(
        SortTranslationsUtil,
        'sortTranslations',
      );
      sortApplicationsTranslations(applicationsMock, languageIdentifier);
      for (const application of applicationsMock) {
        expect(sortTranslationsSpy).toHaveBeenCalledWith(
          application.applicationTranslations,
          languageIdentifier,
        );
      }
    });
  });
});
