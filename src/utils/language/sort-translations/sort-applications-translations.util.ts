import { Application } from 'src/applications/entities/application.entity';
import { sortTranslations } from '../sort-translations.util';

/**
 * Sorts the translations of the given applications according to the given language identifier.
 *
 * @param {Application[]} applications The applications to sort their translations
 * @param {string} languageIdentifier The language identifier to sort the translations with
 */
export function sortApplicationsTranslations(
  applications: Application[],
  languageIdentifier: string,
) {
  for (const application of applications) {
    application.applicationTranslations = sortTranslations(
      application.applicationTranslations,
      languageIdentifier,
    );
  }
}
