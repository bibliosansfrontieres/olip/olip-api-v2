import { Category } from 'src/categories/entities/category.entity';
import { sortTranslations } from '../sort-translations.util';

/**
 * Sorts the playlists's translations of the given categories according to the given language identifier.
 *
 * @param {Category[]} categories The categories to sort their playlists's translations
 * @param {string} languageIdentifier The language identifier to sort the translations with
 */
export function sortCategoriesPlaylistsTranslations(
  categories: Category[],
  languageIdentifier: string,
): void {
  for (const category of categories) {
    for (const categoryPlaylist of category.categoryPlaylists) {
      categoryPlaylist.playlist.playlistTranslations = sortTranslations(
        categoryPlaylist.playlist.playlistTranslations,
        languageIdentifier,
      );
    }
  }
}
