import { languageMock } from 'src/tests/mocks/objects/language.mock';
import * as SortTranslationsUtil from '../sort-translations.util';
import { dublinCoreTypeMock } from 'src/tests/mocks/objects/dublin-core-type.mock';
import { dublinCoreTypeTranslationMock } from 'src/tests/mocks/objects/dublin-core-type-translation.mock';
import { sortDublinCoreTypesTranslations } from './sort-dublin-core-types-translations.util';

describe('SortDublinCoreTypesTranslations', () => {
  describe('sortDublinCoreTypesTranslations', () => {
    const languageIdentifier = 'fra';
    const dublinCoreTypesMock = [
      dublinCoreTypeMock({
        dublinCoreTypeTranslations: [
          dublinCoreTypeTranslationMock({ language: languageMock() }),
          dublinCoreTypeTranslationMock({ language: languageMock() }),
        ],
      }),
      dublinCoreTypeMock({
        dublinCoreTypeTranslations: [
          dublinCoreTypeTranslationMock({ language: languageMock() }),
          dublinCoreTypeTranslationMock({ language: languageMock() }),
        ],
      }),
    ];

    it('should be defined', () => {
      expect(sortDublinCoreTypesTranslations).toBeDefined();
    });

    it('should return undefined', () => {
      const result = sortDublinCoreTypesTranslations(
        dublinCoreTypesMock,
        languageIdentifier,
      );
      expect(result).toBe(undefined);
    });

    it('should call sortTranslations with right arguments', () => {
      const sortTranslationsSpy = jest.spyOn(
        SortTranslationsUtil,
        'sortTranslations',
      );
      sortDublinCoreTypesTranslations(dublinCoreTypesMock, languageIdentifier);
      for (const dublinCoreType of dublinCoreTypesMock) {
        expect(sortTranslationsSpy).toHaveBeenCalledWith(
          dublinCoreType.dublinCoreTypeTranslations,
          languageIdentifier,
        );
      }
    });
  });
});
