import { categoryPlaylistItemMock } from 'src/tests/mocks/objects/category-playlist-item.mock';
import * as SortTranslationsUtil from '../sort-translations.util';
import { sortCategoryPlaylistItemsTranslations } from './sort-category-playlist-items-translations.util';
import { itemMock } from 'src/tests/mocks/objects/item.mock';
import { dublinCoreItemMock } from 'src/tests/mocks/objects/dublin-core-item.mock';
import { dublinCoreItemTranslationMock } from 'src/tests/mocks/objects/dublin-core-item-translation.mock';
import { languageMock } from 'src/tests/mocks/objects/language.mock';
import { dublinCoreTypeMock } from 'src/tests/mocks/objects/dublin-core-type.mock';
import { dublinCoreTypeTranslationMock } from 'src/tests/mocks/objects/dublin-core-type-translation.mock';

describe('SortCategoryPlaylistItemsTranslations', () => {
  describe('sortCategoryPlaylistItemsTranslations', () => {
    const languageIdentifier = 'fra';
    const categoryPlaylistItemsMock = [
      categoryPlaylistItemMock({
        item: itemMock({
          dublinCoreItem: dublinCoreItemMock({
            dublinCoreItemTranslations: [
              dublinCoreItemTranslationMock({ language: languageMock() }),
            ],
            dublinCoreType: dublinCoreTypeMock({
              dublinCoreTypeTranslations: [
                dublinCoreTypeTranslationMock({ language: languageMock() }),
              ],
            }),
          }),
        }),
      }),
      categoryPlaylistItemMock({
        item: itemMock({
          dublinCoreItem: dublinCoreItemMock({
            dublinCoreItemTranslations: [
              dublinCoreItemTranslationMock({ language: languageMock() }),
            ],
            dublinCoreType: dublinCoreTypeMock({
              dublinCoreTypeTranslations: [
                dublinCoreTypeTranslationMock({ language: languageMock() }),
              ],
            }),
          }),
        }),
      }),
    ];

    it('should be defined', () => {
      expect(sortCategoryPlaylistItemsTranslations).toBeDefined();
    });

    it('should return undefined', () => {
      const result = sortCategoryPlaylistItemsTranslations(
        categoryPlaylistItemsMock,
        languageIdentifier,
      );
      expect(result).toBe(undefined);
    });

    it('should call sortTranslations with right arguments', () => {
      const sortTranslationsSpy = jest.spyOn(
        SortTranslationsUtil,
        'sortTranslations',
      );
      sortCategoryPlaylistItemsTranslations(
        categoryPlaylistItemsMock,
        languageIdentifier,
      );
      for (const categoryPlaylistItem of categoryPlaylistItemsMock) {
        expect(sortTranslationsSpy).toHaveBeenCalledWith(
          categoryPlaylistItem.item.dublinCoreItem.dublinCoreItemTranslations,
          languageIdentifier,
        );
        expect(sortTranslationsSpy).toHaveBeenCalledWith(
          categoryPlaylistItem.item.dublinCoreItem.dublinCoreType
            .dublinCoreTypeTranslations,
          languageIdentifier,
        );
      }
    });
  });
});
