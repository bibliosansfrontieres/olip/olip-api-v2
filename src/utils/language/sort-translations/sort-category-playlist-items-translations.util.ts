import { sortTranslations } from '../sort-translations.util';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';

/**
 * Sorts the translations of the given categoryPlaylistItems according to the given language identifier.
 *
 * @param {CategoryPlaylistItem[]} categoryPlaylistItems The categoryPlaylistItems to sort their translations
 * @param {string} languageIdentifier The language identifier to sort the translations with
 */
export function sortCategoryPlaylistItemsTranslations(
  categoryPlaylistItems: CategoryPlaylistItem[],
  languageIdentifier: string,
): void {
  for (const categoryPlaylistItem of categoryPlaylistItems) {
    if (
      categoryPlaylistItem?.item?.dublinCoreItem?.dublinCoreItemTranslations
    ) {
      categoryPlaylistItem.item.dublinCoreItem.dublinCoreItemTranslations =
        sortTranslations(
          categoryPlaylistItem.item.dublinCoreItem.dublinCoreItemTranslations,
          languageIdentifier,
        );
    }

    if (
      categoryPlaylistItem?.item?.dublinCoreItem?.dublinCoreType
        ?.dublinCoreTypeTranslations
    ) {
      categoryPlaylistItem.item.dublinCoreItem.dublinCoreType.dublinCoreTypeTranslations =
        sortTranslations(
          categoryPlaylistItem.item.dublinCoreItem.dublinCoreType
            .dublinCoreTypeTranslations,
          languageIdentifier,
        );
    }
  }
}
