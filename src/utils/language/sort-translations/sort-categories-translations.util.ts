import { Category } from 'src/categories/entities/category.entity';
import { sortTranslations } from '../sort-translations.util';

/**
 * Sorts the translations of the given categories according to the given language identifier.
 *
 * @param {Category[]} categories The categories to sort their translations
 * @param {string} languageIdentifier The language identifier to sort the translations with
 */
export function sortCategoriesTranslations(
  categories: Category[],
  languageIdentifier: string,
) {
  for (const category of categories) {
    category.categoryTranslations = sortTranslations(
      category.categoryTranslations,
      languageIdentifier,
    );
  }
}
