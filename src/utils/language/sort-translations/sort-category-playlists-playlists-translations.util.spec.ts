import { languageMock } from 'src/tests/mocks/objects/language.mock';
import * as SortTranslationsUtil from '../sort-translations.util';
import { categoryPlaylistMock } from 'src/tests/mocks/objects/category-playlist.mock';
import { playlistMock } from 'src/tests/mocks/objects/playlist.mock';
import { playlistTranslationMock } from 'src/tests/mocks/objects/playlist-translation.mock';
import { sortCategoryPlaylistsPlaylistsTranslations } from './sort-category-playlists-playlists-translations.util';

describe('SortCategoryPlaylistsPlaylistsTranslations', () => {
  describe('sortCategoryPlaylistsPlaylistsTranslations', () => {
    const languageIdentifier = 'fra';
    const categoryPlaylistsMock = [
      categoryPlaylistMock({
        playlist: playlistMock({
          playlistTranslations: [
            playlistTranslationMock({ language: languageMock() }),
            playlistTranslationMock({ language: languageMock() }),
          ],
        }),
      }),
      categoryPlaylistMock({
        playlist: playlistMock({
          playlistTranslations: [
            playlistTranslationMock({ language: languageMock() }),
            playlistTranslationMock({ language: languageMock() }),
          ],
        }),
      }),
    ];

    it('should be defined', () => {
      expect(sortCategoryPlaylistsPlaylistsTranslations).toBeDefined();
    });

    it('should return undefined', () => {
      const result = sortCategoryPlaylistsPlaylistsTranslations(
        categoryPlaylistsMock,
        languageIdentifier,
      );
      expect(result).toBe(undefined);
    });

    it('should call sortTranslations with right arguments', () => {
      const sortTranslationsSpy = jest.spyOn(
        SortTranslationsUtil,
        'sortTranslations',
      );
      sortCategoryPlaylistsPlaylistsTranslations(
        categoryPlaylistsMock,
        languageIdentifier,
      );
      for (const categoryPlaylist of categoryPlaylistsMock) {
        expect(sortTranslationsSpy).toHaveBeenCalledWith(
          categoryPlaylist.playlist.playlistTranslations,
          languageIdentifier,
        );
      }
    });
  });
});
