import { sortTranslations } from '../sort-translations.util';
import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';

/**
 * Sorts the dublinCoreTypeTranslations of the given dublinCoreTypes according to the given language identifier.
 *
 * @param {DublinCoreType[]} dublinCoreTypes The dublinCoreTypes to sort their translations
 * @param {string} languageIdentifier The language identifier to sort the translations with
 */
export function sortDublinCoreTypesTranslations(
  dublinCoreTypes: DublinCoreType[],
  languageIdentifier: string,
): void {
  for (const dublinCoreType of dublinCoreTypes) {
    dublinCoreType.dublinCoreTypeTranslations = sortTranslations(
      dublinCoreType.dublinCoreTypeTranslations,
      languageIdentifier,
    );
  }
}
