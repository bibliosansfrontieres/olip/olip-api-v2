import { languageMock } from 'src/tests/mocks/objects/language.mock';
import * as SortTranslationsUtil from '../sort-translations.util';
import { categoryMock } from 'src/tests/mocks/objects/category.mock';
import { categoryTranslationMock } from 'src/tests/mocks/objects/category-translation.mock';
import { sortCategoriesTranslations } from './sort-categories-translations.util';

describe('SortCategoriesTranslationsUtil', () => {
  describe('sortCategoriesTranslations', () => {
    const languageIdentifier = 'fra';
    const categoriesMock = [
      categoryMock({
        categoryTranslations: [
          categoryTranslationMock({ language: languageMock() }),
        ],
      }),
      categoryMock({
        categoryTranslations: [
          categoryTranslationMock({ language: languageMock() }),
        ],
      }),
    ];

    it('should be defined', () => {
      expect(sortCategoriesTranslations).toBeDefined();
    });

    it('should return undefined', () => {
      const result = sortCategoriesTranslations(
        categoriesMock,
        languageIdentifier,
      );
      expect(result).toBe(undefined);
    });

    it('should call sortTranslations with right arguments', () => {
      const sortTranslationsSpy = jest.spyOn(
        SortTranslationsUtil,
        'sortTranslations',
      );
      sortCategoriesTranslations(categoriesMock, languageIdentifier);
      for (const category of categoriesMock) {
        expect(sortTranslationsSpy).toHaveBeenCalledWith(
          category.categoryTranslations,
          languageIdentifier,
        );
      }
    });
  });
});
