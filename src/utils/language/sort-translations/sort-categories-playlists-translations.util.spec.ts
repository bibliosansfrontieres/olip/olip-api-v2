import { languageMock } from 'src/tests/mocks/objects/language.mock';
import * as SortTranslationsUtil from '../sort-translations.util';
import { categoryMock } from 'src/tests/mocks/objects/category.mock';
import { sortCategoriesPlaylistsTranslations } from './sort-categories-playlists-translations.util';
import { categoryPlaylistMock } from 'src/tests/mocks/objects/category-playlist.mock';
import { playlistMock } from 'src/tests/mocks/objects/playlist.mock';
import { playlistTranslationMock } from 'src/tests/mocks/objects/playlist-translation.mock';

describe('SortCategoriesPlaylistsTranslations', () => {
  describe('sortCategoriesPlaylistsTranslations', () => {
    const languageIdentifier = 'fra';
    const categoriesMock = [
      categoryMock({
        categoryPlaylists: [
          categoryPlaylistMock({
            playlist: playlistMock({
              playlistTranslations: [
                playlistTranslationMock({ language: languageMock() }),
              ],
            }),
          }),
          categoryPlaylistMock({
            playlist: playlistMock({
              playlistTranslations: [
                playlistTranslationMock({ language: languageMock() }),
              ],
            }),
          }),
        ],
      }),
      categoryMock({
        categoryPlaylists: [
          categoryPlaylistMock({
            playlist: playlistMock({
              playlistTranslations: [
                playlistTranslationMock({ language: languageMock() }),
              ],
            }),
          }),
          categoryPlaylistMock({
            playlist: playlistMock({
              playlistTranslations: [
                playlistTranslationMock({ language: languageMock() }),
              ],
            }),
          }),
        ],
      }),
    ];

    it('should be defined', () => {
      expect(sortCategoriesPlaylistsTranslations).toBeDefined();
    });

    it('should return undefined', () => {
      const result = sortCategoriesPlaylistsTranslations(
        categoriesMock,
        languageIdentifier,
      );
      expect(result).toBe(undefined);
    });

    it('should call sortTranslations with right arguments', () => {
      const sortTranslationsSpy = jest.spyOn(
        SortTranslationsUtil,
        'sortTranslations',
      );
      sortCategoriesPlaylistsTranslations(categoriesMock, languageIdentifier);
      for (const category of categoriesMock) {
        for (const categoryPlaylist of category.categoryPlaylists) {
          expect(sortTranslationsSpy).toHaveBeenCalledWith(
            categoryPlaylist.playlist.playlistTranslations,
            languageIdentifier,
          );
        }
      }
    });
  });
});
