import { categoryTranslationMock } from 'src/tests/mocks/objects/category-translation.mock';
import {
  getTranslationPriority,
  isSameLanguage,
  sortTranslations,
} from './sort-translations.util';
import { languageMock } from 'src/tests/mocks/objects/language.mock';

describe('SortTranslationsUtil', () => {
  const engTranslation = categoryTranslationMock({
    language: languageMock({ identifier: 'eng' }),
  });
  const fraTranslation = categoryTranslationMock({
    language: languageMock({ identifier: 'fra' }),
  });
  const rcfTranslation = categoryTranslationMock({
    language: languageMock({ identifier: 'rcf' }),
  });
  const translationsMock = [engTranslation, fraTranslation, rcfTranslation];

  const primaryLanguage = 'fra';
  const notFoundPrimaryLanguage = 'acf';
  const fallbackLanguage = 'eng';

  describe('sortTranslations', () => {
    it('should be defined', () => {
      expect(sortTranslations).toBeDefined();
    });

    it('should return translations', () => {
      const result = sortTranslations(
        translationsMock,
        primaryLanguage,
        fallbackLanguage,
      );
      expect(result.length).toBe(3);
    });

    it('should return sorted translations with primary language first', () => {
      const result = sortTranslations(
        translationsMock,
        primaryLanguage,
        fallbackLanguage,
      );
      expect(result[0]).toEqual(fraTranslation);
    });

    it('should return sorted translations with fallback language first if no primary language found', () => {
      const result = sortTranslations(
        translationsMock,
        notFoundPrimaryLanguage,
        fallbackLanguage,
      );
      expect(result[0]).toEqual(engTranslation);
    });

    it('should not sort translations if no primary language and no fallback found', () => {
      const notSortableTranslations = [fraTranslation, rcfTranslation];
      const result = sortTranslations(
        notSortableTranslations,
        notFoundPrimaryLanguage,
        fallbackLanguage,
      );
      expect(result).toEqual(notSortableTranslations);
    });
  });

  describe('getTranslationPriority', () => {
    it('should be defined', () => {
      expect(getTranslationPriority).toBeDefined();
    });

    it('should return 1 if translation is primary language', () => {
      const result = getTranslationPriority(
        fraTranslation,
        primaryLanguage,
        fallbackLanguage,
      );
      expect(result).toBe(1);
    });

    it('should return 2 if translation is fallback language', () => {
      const result = getTranslationPriority(
        engTranslation,
        primaryLanguage,
        fallbackLanguage,
      );
      expect(result).toBe(2);
    });

    it('should return 3 if translation is not primary or fallback language', () => {
      const result = getTranslationPriority(
        rcfTranslation,
        primaryLanguage,
        fallbackLanguage,
      );
      expect(result).toBe(3);
    });
  });

  describe('isSameLanguage', () => {
    it('should be defined', () => {
      expect(isSameLanguage).toBeDefined();
    });

    it('should return true if translation has same language as identifier sent', () => {
      const result = isSameLanguage(fraTranslation, primaryLanguage);
      expect(result).toBe(true);
    });

    it('should return true if translation has not same language as identifier sent', () => {
      const result = isSameLanguage(fraTranslation, fallbackLanguage);
      expect(result).toBe(false);
    });
  });
});
