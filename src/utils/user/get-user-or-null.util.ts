import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';

/**
 * Retrieves a user by their ID, including their role information.
 *
 * @param {UsersService} usersService The service used to interact with user data.
 * @param {number} [userId] The ID of the user to retrieve.
 * @returns {Promise<User | null>} A promise that resolves to the user object if found, otherwise null.
 */
export function getUserOrNull(
  usersService: UsersService,
  userId?: number,
): Promise<User | null> {
  if (!userId) return null;
  return usersService.findOne({
    where: { id: userId },
    relations: { role: true },
  });
}
