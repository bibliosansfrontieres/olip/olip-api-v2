import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { getUserOrNull } from './get-user-or-null.util';
import { UsersService } from 'src/users/users.service';
import { userMock } from 'src/tests/mocks/objects/user.mock';
import { roleMock } from 'src/tests/mocks/objects/role.mock';

describe('GetUserOrNullUtil', () => {
  describe('getUserOrNull', () => {
    const user = userMock({ id: 1, role: roleMock({ id: 1 }) });
    const usersService = usersServiceMock as unknown as UsersService;

    it('should be defined', () => {
      expect(getUserOrNull).toBeDefined();
    });

    it('should return null if no userId sent', async () => {
      const result = await getUserOrNull(usersService, null);
      expect(result).toBe(null);
    });

    it('should return null if userId sent but user not found', async () => {
      jest.spyOn(usersService, 'findOne').mockResolvedValue(null);
      const result = await getUserOrNull(usersService, user.id);
      expect(result).toBe(null);
    });

    it("should get user's relations needed", async () => {
      const usersServiceSpy = jest
        .spyOn(usersService, 'findOne')
        .mockResolvedValue(user);
      await getUserOrNull(usersService, user.id);
      expect(usersServiceSpy).toHaveBeenCalledWith({
        where: { id: user.id },
        relations: { role: true },
      });
    });

    it('should return a user if userId sent and user found', async () => {
      jest.spyOn(usersService, 'findOne').mockResolvedValue(user);
      const result = await getUserOrNull(usersService, user.id);
      expect(result).toEqual(user);
    });
  });
});
