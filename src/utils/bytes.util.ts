export function bytesToKb(bytes: number) {
  return parseFloat((bytes / 1024).toFixed(2));
}
