import { IMaestroApplication } from 'src/maestro/interfaces/maestro-application.interface';

export interface IContext {
  id: number;
  isOutsideProjectDownloadsAuthorized: boolean;
  isDefaultContext: boolean;
  originalContext: string;
  applications: IMaestroApplication[];
}
