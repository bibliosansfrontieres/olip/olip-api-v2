import { ConnectedUser } from 'src/stats/entities/connected-user.entity';

export const filterDuplicatedClientId = (connectedUsers: ConnectedUser[]) => {
  const users = [...connectedUsers];
  const uniqueClientIds = new Set();
  const filteredArray = users.filter((user) => {
    if (!uniqueClientIds.has(user.clientId)) {
      uniqueClientIds.add(user.clientId);
      return true;
    }
    return false;
  });
  return filteredArray;
};

export const filterConnectedUsers = (connectedUsers: ConnectedUser[]) => {
  const users = [...connectedUsers];
  const filteredArrayWithUsers = users.filter(
    (user) => user.users.length !== 0,
  );
  return filterDuplicatedClientId(filteredArrayWithUsers);
};
