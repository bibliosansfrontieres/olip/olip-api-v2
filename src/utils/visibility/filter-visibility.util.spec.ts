import { visibilityMock } from 'src/tests/mocks/objects/visibility.mock';
import {
  filterVisibility,
  isRoleAuthorized,
  isRoleInVisibility,
  isUserAuthorized,
  isUserInVisibility,
  isVisibilityByRole,
  isVisibilityByUser,
  isVisibilityPublic,
} from './filter-visibility.util';
import { userMock } from 'src/tests/mocks/objects/user.mock';
import { roleMock } from 'src/tests/mocks/objects/role.mock';

describe('FilterVisibilityUtil', () => {
  describe('filterVisibility', () => {
    it('should be defined', () => {
      expect(filterVisibility).toBeDefined();
    });

    it('should return true if visibility is public and not logged in', () => {
      const visibility = visibilityMock({ roles: [], users: [] });
      const result = filterVisibility(visibility, undefined);
      expect(result).toBe(true);
    });

    it('should return true if visibility is public and logged in', () => {
      const visibility = visibilityMock({ roles: [], users: [] });
      const result = filterVisibility(visibility, userMock());
      expect(result).toBe(true);
    });

    it('should return false if visibility is by user and not logged in', () => {
      const visibility = visibilityMock({ users: [userMock()], roles: [] });
      const result = filterVisibility(visibility, undefined);
      expect(result).toBe(false);
    });

    it('should return false if visibility is by role and not logged in', () => {
      const visibility = visibilityMock({ roles: [roleMock()], users: [] });
      const result = filterVisibility(visibility, undefined);
      expect(result).toBe(false);
    });

    it('should return false if visibility is by user and logged in but not authorized', () => {
      const visibility = visibilityMock({
        users: [userMock({ id: 1 })],
        roles: [],
      });
      const result = filterVisibility(visibility, userMock());
      expect(result).toBe(false);
    });

    it('should return false if visibility is by role and logged in but not authorized', () => {
      const visibility = visibilityMock({
        roles: [roleMock({ id: 1 })],
        users: [],
      });
      const result = filterVisibility(
        visibility,
        userMock({ role: roleMock() }),
      );
      expect(result).toBe(false);
    });

    it('should return true if visibility is by user, logged in and authorized', () => {
      const user = userMock({ id: 1 });
      const visibility = visibilityMock({
        users: [user],
        roles: [],
      });
      const result = filterVisibility(visibility, user);
      expect(result).toBe(true);
    });

    it('should return true if visibility is by role, logged in and authorized', () => {
      const role = roleMock({ id: 1 });
      const visibility = visibilityMock({ roles: [role], users: [] });
      const result = filterVisibility(visibility, userMock({ role }));
      expect(result).toBe(true);
    });
  });

  describe('isUserAuthorized', () => {
    it('should be defined', () => {
      expect(isUserAuthorized).toBeDefined();
    });

    it('should return false if visibility is not by user', () => {
      const user = userMock();
      const visibility = visibilityMock({ roles: [], users: [] });
      const result = isUserAuthorized(visibility, user);
      expect(result).toBe(false);
    });

    it('should return false if visibility is by role', () => {
      const user = userMock();
      const visibility = visibilityMock({ roles: [roleMock()], users: [] });
      const result = isUserAuthorized(visibility, user);
      expect(result).toBe(false);
    });

    it('should return false if visibility is by user and user is not authorized', () => {
      const user = userMock();
      const visibility = visibilityMock({ users: [userMock({ id: 1 })] });
      const result = isUserAuthorized(visibility, user);
      expect(result).toBe(false);
    });

    it('should return true if visibility is by user and user is authorized', () => {
      const user = userMock();
      const visibility = visibilityMock({ users: [user] });
      const result = isUserAuthorized(visibility, user);
      expect(result).toBe(true);
    });
  });

  describe('isRoleAuthorized', () => {
    it('should be defined', () => {
      expect(isRoleAuthorized).toBeDefined();
    });

    it('should return false if visibility is not by role', () => {
      const role = roleMock();
      const visibility = visibilityMock({ roles: [], users: [] });
      const result = isRoleAuthorized(visibility, role);
      expect(result).toBe(false);
    });

    it('should return false if visibility is by user', () => {
      const role = roleMock();
      const visibility = visibilityMock({
        users: [userMock({ id: 1 })],
        roles: [],
      });
      const result = isRoleAuthorized(visibility, role);
      expect(result).toBe(false);
    });

    it('should return false if visibility is by role and user is not authorized', () => {
      const role = roleMock();
      const visibility = visibilityMock({
        roles: [roleMock({ id: 1 })],
      });
      const result = isRoleAuthorized(visibility, role);
      expect(result).toBe(false);
    });

    it('should return true if visibility is by role and user is authorized', () => {
      const role = roleMock({ id: 1 });
      const visibility = visibilityMock({ roles: [role] });
      const result = isRoleAuthorized(visibility, role);
      expect(result).toBe(true);
    });
  });

  describe('isUserInVisibility', () => {
    it('should be defined', () => {
      expect(isUserInVisibility).toBeDefined();
    });

    it('should return false if user is not in visibility', () => {
      const user = userMock();
      const visibility = visibilityMock({ roles: [], users: [] });
      const result = isUserInVisibility(visibility, user);
      expect(result).toBe(false);
    });

    it('should return true if user is in visibility', () => {
      const user = userMock();
      const visibility = visibilityMock({ users: [user] });
      const result = isUserInVisibility(visibility, user);
      expect(result).toBe(true);
    });
  });

  describe('isRoleInVisibility', () => {
    it('should be defined', () => {
      expect(isRoleInVisibility).toBeDefined();
    });

    it('should return false if role is not in visibility', () => {
      const role = roleMock();
      const visibility = visibilityMock({ roles: [] });
      const result = isRoleInVisibility(visibility, role);
      expect(result).toBe(false);
    });

    it('should return true if role is in visibility', () => {
      const role = roleMock();
      const visibility = visibilityMock({ roles: [role] });
      const result = isRoleInVisibility(visibility, role);
      expect(result).toBe(true);
    });
  });

  describe('isVisibilityByUser', () => {
    it('should be defined', () => {
      expect(isVisibilityByUser).toBeDefined();
    });

    it('should return false if visibility is public', () => {
      const visibility = visibilityMock({ roles: [], users: [] });
      const result = isVisibilityByUser(visibility);
      expect(result).toBe(false);
    });

    it('should return false if visibility is by role', () => {
      const visibility = visibilityMock({ roles: [roleMock()], users: [] });
      const result = isVisibilityByUser(visibility);
      expect(result).toBe(false);
    });

    it('should return true if visibility is by user', () => {
      const visibility = visibilityMock({ users: [userMock()] });
      const result = isVisibilityByUser(visibility);
      expect(result).toBe(true);
    });
  });

  describe('isVisibilityByRole', () => {
    it('should be defined', () => {
      expect(isVisibilityByRole).toBeDefined();
    });

    it('should return false if visibility is public', () => {
      const visibility = visibilityMock({ roles: [], users: [] });
      const result = isVisibilityByRole(visibility);
      expect(result).toBe(false);
    });

    it('should return false if visibility is by user', () => {
      const visibility = visibilityMock({ users: [userMock()], roles: [] });
      const result = isVisibilityByRole(visibility);
      expect(result).toBe(false);
    });

    it('should return true if visibility is by role', () => {
      const visibility = visibilityMock({ roles: [roleMock()] });
      const result = isVisibilityByRole(visibility);
      expect(result).toBe(true);
    });
  });

  describe('isVisibilityPublic', () => {
    it('should be defined', () => {
      expect(isVisibilityPublic).toBeDefined();
    });

    it('should return false if visibility is by user', () => {
      const visibility = visibilityMock({ users: [userMock()] });
      const result = isVisibilityPublic(visibility);
      expect(result).toBe(false);
    });

    it('should return false if visibility is by role', () => {
      const visibility = visibilityMock({ roles: [roleMock()], users: [] });
      const result = isVisibilityPublic(visibility);
      expect(result).toBe(false);
    });

    it('should return true if visibility is public', () => {
      const visibility = visibilityMock({ roles: [], users: [] });
      const result = isVisibilityPublic(visibility);
      expect(result).toBe(true);
    });
  });
});
