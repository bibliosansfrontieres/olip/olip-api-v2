import { Visibility } from 'src/visibilities/entities/visibility.entity';
import { User } from 'src/users/entities/user.entity';
import { Role } from 'src/roles/entities/role.entity';

export function filterVisibility(visibility: Visibility, user?: User) {
  if (isVisibilityPublic(visibility)) {
    return true;
  }

  if (
    user &&
    (isUserAuthorized(visibility, user) ||
      isRoleAuthorized(visibility, user.role))
  ) {
    return true;
  }

  return false;
}

export function isUserAuthorized(visibility: Visibility, user: User) {
  return isVisibilityByUser(visibility) && isUserInVisibility(visibility, user);
}

export function isRoleAuthorized(visibility: Visibility, role: Role) {
  return isVisibilityByRole(visibility) && isRoleInVisibility(visibility, role);
}

export function isUserInVisibility(visibility: Visibility, user: User) {
  return visibility.users.find((u) => u.id === user.id) !== undefined;
}

export function isRoleInVisibility(visibility: Visibility, role: Role) {
  return visibility.roles.find((r) => r.id === role.id) !== undefined;
}

export function isVisibilityByUser(visibility: Visibility) {
  return visibility.users.length !== 0;
}

export function isVisibilityByRole(visibility: Visibility) {
  return visibility.roles.length !== 0;
}

export function isVisibilityPublic(visibility: Visibility) {
  return visibility.users.length === 0 && visibility.roles.length === 0;
}
