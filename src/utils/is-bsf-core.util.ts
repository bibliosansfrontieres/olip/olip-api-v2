import { existsSync } from 'fs';

export function isBsfCore(): boolean {
  return existsSync('/olip-files/bsf-core');
}
