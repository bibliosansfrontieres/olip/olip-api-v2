import { readFileSync } from 'fs';

export function readIsoTabFile(filePath: string) {
  const content = readFileSync(filePath, 'utf8');
  const json = content.split('\r\n').map((profile) => {
    const [Id, Part2B, Part2T, Part1, Scope, Language_Type, Ref_Name, Comment] =
      profile.split('\t');
    return {
      Id,
      Part2B,
      Part2T,
      Part1,
      Scope,
      Language_Type,
      Ref_Name,
      Comment,
    };
  });
  return json;
}
