import { existsSync, readFileSync } from 'fs';
import { Logger } from '@nestjs/common';

export function getVmSaveId(): string | null {
  if (existsSync('/olip-files/deploy/virtual-machine-save-id')) {
    return readFileSync('/olip-files/deploy/virtual-machine-save-id')
      .toString()
      .trim();
  } else {
    Logger.error(
      'Could not find vm save id (/olip-files/deploy/virtual-machine-save-id)',
      'Utils',
    );
    return null;
  }
}
