import { enumToBoolean } from './enum-to-boolean.util';
import { TrueFalseEnum } from './enums/true-false.enum';

describe('EnumToBooleanUtil', () => {
  describe('enumToBoolean', () => {
    it('should be defined', () => {
      expect(enumToBoolean).toBeDefined();
    });

    it('should return true if value is TRUE', async () => {
      const result = enumToBoolean(TrueFalseEnum.TRUE);
      expect(result).toBe(true);
    });

    it('should return false if value is FALSE', async () => {
      const result = enumToBoolean(TrueFalseEnum.FALSE);
      expect(result).toBe(false);
    });

    it('should return undefinedDefaultValue if value is undefined', async () => {
      const resultTrue = enumToBoolean(undefined, true);
      expect(resultTrue).toBe(true);

      const resultFalse = enumToBoolean(undefined, false);
      expect(resultFalse).toBe(false);
    });

    it('should return false if value is undefined and undefinedDefaultValue is undefined', async () => {
      const result = enumToBoolean(undefined);
      expect(result).toBe(false);
    });
  });
});
