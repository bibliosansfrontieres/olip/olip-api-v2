import { ValidationError } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { UpdateProfileDto } from 'src/users/dto/update-profile.dto';

export const validateUpdateProfileDto = async (
  updateProfileDto: UpdateProfileDto,
): Promise<ValidationError[]> => {
  const of = plainToInstance(UpdateProfileDto, updateProfileDto);
  return validate(of);
};
