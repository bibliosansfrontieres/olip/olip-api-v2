import { ValidationError } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { UpdateUserDto } from 'src/users/dto/update-user.dto';

export const validateUpdateUserDto = async (
  updateUserDto: UpdateUserDto,
): Promise<ValidationError[]> => {
  const of = plainToInstance(UpdateUserDto, updateUserDto);
  return validate(of);
};
