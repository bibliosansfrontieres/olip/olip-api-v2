import { ValidationError } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';

export const validatePageOptionsDto = async (
  pageOptionsDto: PageOptionsDto,
): Promise<ValidationError[]> => {
  const of = plainToInstance(PageOptionsDto, pageOptionsDto);
  return validate(of);
};
