import { ValidationError } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { SigninDto } from 'src/auth/dto/signin.dto';

export const validateSigninDto = async (
  signinDto: SigninDto,
): Promise<ValidationError[]> => {
  const of = plainToInstance(SigninDto, signinDto);
  return validate(of);
};
