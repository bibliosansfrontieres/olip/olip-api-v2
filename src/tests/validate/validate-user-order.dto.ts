import { ValidationError } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { UserOrderDto } from 'src/users/dto/user-order.dto';

export const validateUserOrderDto = async (
  userOrderDto: UserOrderDto,
): Promise<ValidationError[]> => {
  const of = plainToInstance(UserOrderDto, userOrderDto);
  return validate(of);
};
