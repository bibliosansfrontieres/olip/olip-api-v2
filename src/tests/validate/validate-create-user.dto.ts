import { ValidationError } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { CreateUserDto } from 'src/users/dto/create-user.dto';

export const validateCreateUserDto = async (
  createUserDto: CreateUserDto,
): Promise<ValidationError[]> => {
  const of = plainToInstance(CreateUserDto, createUserDto);
  return validate(of);
};
