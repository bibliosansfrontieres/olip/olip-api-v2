export const applicationsServiceMock = {
  find: jest.fn(),
  findOne: jest.fn(),
  create: jest.fn(),
  needUpdate: jest.fn(),
  updateMetadata: jest.fn(),
  save: jest.fn(),
  uninstallContent: jest.fn(),
};
