export const categoryPlaylistItemsServiceMock = {
  getCategoryPlaylistCategoryPlaylistItems: jest.fn(),
  getCategoryPlaylistItemsApplications: jest.fn(),
  getQueryBuilder: jest.fn(() => {
    return {
      leftJoinAndSelect: jest.fn().mockReturnThis(),
      innerJoinAndSelect: jest.fn().mockReturnThis(),
      leftJoin: jest.fn().mockReturnThis(),
      where: jest.fn().mockReturnThis(),
      andWhere: jest.fn().mockReturnThis(),
      addSelect: jest.fn().mockReturnThis(),
      loadRelationCountAndMap: jest.fn().mockReturnThis(),
      orWhere: jest.fn().mockReturnThis(),
      orderBy: jest.fn().mockReturnThis(),
      getMany: jest.fn().mockResolvedValue([]),
    };
  }),
};
