import { roleMock } from '../objects/role.mock';

export const rolesServiceMock = {
  findRoleById: jest.fn().mockResolvedValue(roleMock()),
};
