export const maestroApiServiceMock = {
  getUpdates: jest.fn(),
  getApplications: jest.fn(),
};
