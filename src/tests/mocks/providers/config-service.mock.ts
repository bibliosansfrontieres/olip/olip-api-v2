export const configServiceMock = {
  get: jest.fn((key: string) => {
    let value = '';
    switch (key) {
      case 'TM_URL':
        value = 'https://front.tm.bsf-intranet.org';
        break;
      case 'CATALOG_EXPIRES_IN':
        value = '1m';
        break;
    }
    return value;
  }),
};
