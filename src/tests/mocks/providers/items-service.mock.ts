export const itemsServiceMock = {
  removeAllItems: jest.fn(() => {
    return;
  }),
  removeAllPlaylists: jest.fn(() => {
    return;
  }),
  remove: jest.fn(),
  uninstall: jest.fn(),
};
