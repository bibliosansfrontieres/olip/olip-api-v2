export const categoriesServiceMock = {
  getCategoriesCatalog: jest.fn(() => {}),
  getCategoriesList: jest.fn(() => {}),
  getCategoriesBackOfficeList: jest.fn(() => {}),
  getCategoriesApplications: jest.fn(() => []),
  getCategoryBackOfficeCategoryPlaylists: jest.fn(() => {}),
  getCategoryBackOffice: jest.fn(() => {}),
  save: jest.fn(),
  findAll: jest.fn(() => []),
  getTranslation: jest.fn(),
  getTranslations: jest.fn(),
};
