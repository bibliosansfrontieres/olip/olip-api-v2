export const playlistsServiceMock = {
  removeAllItems: jest.fn(() => {
    return;
  }),
  removeAllPlaylists: jest.fn(() => {
    return;
  }),
  findOne: jest.fn(),
  save: jest.fn(),
};
