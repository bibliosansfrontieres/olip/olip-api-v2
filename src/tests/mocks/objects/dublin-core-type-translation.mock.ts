import { DublinCoreTypeTranslation } from 'src/dublin-core-types/entities/dublin-core-type-translation.entity';
import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';
import { Language } from 'src/languages/entities/language.entity';

export interface IDublinCoreTypeTranslationOptions {
  id?: number;
  label?: string;
  description?: string;
  dublinCoreType?: DublinCoreType;
  language?: Language;
}

export function dublinCoreTypeTranslationMock(
  options?: IDublinCoreTypeTranslationOptions,
): DublinCoreTypeTranslation {
  return {
    id: options?.id ?? Date.now(),
    label: options?.label ?? 'label',
    description: options?.description ?? 'description',
    dublinCoreType: options?.dublinCoreType ?? undefined,
    language: options?.language ?? undefined,
  };
}
