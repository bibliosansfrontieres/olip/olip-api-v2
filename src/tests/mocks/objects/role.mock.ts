import { Role } from 'src/roles/entities/role.entity';
import { RoleEnum } from 'src/roles/enums/role.enum';
import { RoleTranslation } from 'src/roles/entities/role-translation.entity';
import { Visibility } from 'src/visibilities/entities/visibility.entity';
import { User } from 'src/users/entities/user.entity';
import { Permission } from 'src/permissions/entities/permission.entity';

export interface IRoleMockOptions {
  id?: number;
  name?: RoleEnum;
  roleTranslations?: RoleTranslation[];
  permissions?: Permission[];
  users?: User[];
  visibilities?: Visibility[];
}

export function roleMock(options?: IRoleMockOptions): Role {
  return {
    id: options?.id || Date.now(),
    name: options?.name || RoleEnum.USER,
    roleTranslations: options?.roleTranslations || undefined,
    permissions: options?.permissions || undefined,
    users: options?.users || undefined,
    visibilities: options?.visibilities || undefined,
  };
}
