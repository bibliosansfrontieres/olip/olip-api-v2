import { Language } from 'src/languages/entities/language.entity';

export interface ISimplifiedLanguageMockOptions {
  identifier?: string;
  label?: string;
}

export function simplifiedLanguageMock(
  options?: ISimplifiedLanguageMockOptions,
): Language {
  return {
    identifier: options?.identifier || 'fra',
    label: options?.label || 'Français',
  } as Language;
}
