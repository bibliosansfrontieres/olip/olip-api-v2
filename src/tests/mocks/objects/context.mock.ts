import { IMaestroApplication } from 'src/maestro/interfaces/maestro-application.interface';
import { IContext } from 'src/utils/interfaces/context.interface';

export interface IContextMockOptions {
  id?: number;
  isOutsideProjectDownloadsAuthorized?: boolean;
  isDefaultContext?: boolean;
  originalContext?: string;
  applications?: IMaestroApplication[];
}

export function contextMock(options?: IContextMockOptions): IContext {
  return {
    id: options?.id ?? Date.now(),
    isOutsideProjectDownloadsAuthorized:
      options?.isOutsideProjectDownloadsAuthorized ?? true,
    isDefaultContext: options?.isDefaultContext ?? true,
    originalContext: options?.originalContext ?? '',
    applications: options?.applications ?? [],
  };
}
