import { Application } from 'src/applications/entities/application.entity';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';
import { File } from 'src/files/entities/file.entity';
import { HighlightContent } from 'src/highlight-contents/entities/highlight-content.entity';
import { ItemDocumentType } from 'src/item-document-types/entities/item-document-type.entity';
import { ItemLanguageLevel } from 'src/item-language-levels/entities/item-language-level.entity';
import { Item } from 'src/items/entities/item.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';

export interface IItemMockOptions {
  id?: string;
  thumbnail?: string;
  createdCountry?: string;
  isInstalled?: boolean;
  isDownloading?: boolean;
  isUpdateNeeded?: boolean;
  isBroken?: boolean;
  isManuallyInstalled?: boolean;
  version?: string | null;
  url?: string | null;
  dublinCoreItem?: DublinCoreItem;
  itemDocumentType?: ItemDocumentType;
  itemLanguageLevel?: ItemLanguageLevel;
  playlists?: Playlist[];
  categoryPlaylistItems?: CategoryPlaylistItem[];
  file?: File;
  application?: Application;
  highlightContents?: HighlightContent[];
  createdAt?: number;
  updatedAt?: number;
}

export function itemMock(options?: IItemMockOptions): Item {
  return {
    id: options?.id ?? Date.now().toString(),
    thumbnail: options?.thumbnail ?? 'thumbnail',
    createdCountry: options?.createdCountry ?? 'createdCountry',
    isInstalled: options?.isInstalled ?? true,
    isDownloading: options?.isDownloading ?? false,
    isUpdateNeeded: options?.isUpdateNeeded ?? false,
    isBroken: options?.isBroken ?? false,
    isManuallyInstalled: options?.isManuallyInstalled ?? false,
    version: options?.version ?? undefined,
    url: options?.url ?? undefined,
    dublinCoreItem: options?.dublinCoreItem ?? undefined,
    itemDocumentType: options?.itemDocumentType ?? undefined,
    itemLanguageLevel: options?.itemLanguageLevel ?? undefined,
    playlists: options?.playlists ?? undefined,
    categoryPlaylistItems: options?.categoryPlaylistItems ?? undefined,
    file: options?.file ?? undefined,
    application: options?.application ?? undefined,
    highlightContents: options?.highlightContents ?? undefined,
    createdAt: options?.createdAt ?? Date.now(),
    updatedAt: options?.updatedAt ?? Date.now(),
  };
}
