import { RoleTranslation } from 'src/roles/entities/role-translation.entity';
import { Language } from 'src/languages/entities/language.entity';
import { Role } from 'src/roles/entities/role.entity';

export interface IRoleTranslationMockOptions {
  id?: number;
  label?: string;
  role?: Role;
  language?: Language;
}

export function roleTranslationMock(
  options?: IRoleTranslationMockOptions,
): RoleTranslation {
  return {
    id: options?.id || Date.now(),
    label: options?.label || 'label',
    role: options?.role || undefined,
    language: options?.language || undefined,
  };
}
