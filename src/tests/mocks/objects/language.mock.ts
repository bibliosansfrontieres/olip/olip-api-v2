import { ApplicationTypeTranslation } from 'src/application-types/entities/application-type-translation.entity';
import { ApplicationTranslation } from 'src/applications/entities/application-translation.entity';
import { CategoryTranslation } from 'src/categories/entities/category-translation.entity';
import { DublinCoreAudienceTranslation } from 'src/dublin-core-audiences/entities/dublin-core-audience-translation.entity';
import { DublinCoreItemTranslation } from 'src/dublin-core-items/entities/dublin-core-item-translation';
import { DublinCoreLanguage } from 'src/dublin-core-languages/entities/dublin-core-language.entity';
import { DublinCoreLicenseTranslation } from 'src/dublin-core-licenses/entities/dublin-core-license-translation.entity';
import { DublinCoreTypeTranslation } from 'src/dublin-core-types/entities/dublin-core-type-translation.entity';
import { EditoParagraphTranslation } from 'src/edito-paragraphs/entities/edito-paragraph-translation.entity';
import { EditoTranslation } from 'src/editos/entities/edito-translation.entity';
import { HighlightTranslation } from 'src/highlights/entities/highlight-translation.entity';
import { ItemDocumentTypeTranslation } from 'src/item-document-types/entities/item-document-type-translation.entity';
import { ItemLanguageLevelTranslation } from 'src/item-language-levels/entities/item-language-level-translation.entity';
import { Language } from 'src/languages/entities/language.entity';
import { PermissionTranslation } from 'src/permissions/entities/permission-translation.entity';
import { PlaylistTranslation } from 'src/playlists/entities/playlist-translation.entity';
import { RoleTranslation } from 'src/roles/entities/role-translation.entity';

export interface ILanguageMockOptions {
  id?: number;
  identifier?: string;
  oldIdentifier?: string;
  label?: string;
  itemLanguageLevelTranslations?: ItemLanguageLevelTranslation[];
  itemDocumentTypeTranslations?: ItemDocumentTypeTranslation[];
  dublinCoreItemTranslations?: DublinCoreItemTranslation[];
  dublinCoreTypeTranslations?: DublinCoreTypeTranslation[];
  dublinCoreAudienceTranslations?: DublinCoreAudienceTranslation[];
  dublinCoreLicenseTranslations?: DublinCoreLicenseTranslation[];
  dublinCoreLanguages?: DublinCoreLanguage[];
  playlistTranslations?: PlaylistTranslation[];
  categoryTranslations?: CategoryTranslation[];
  applicationTranslations?: ApplicationTranslation[];
  applicationTypeTranslations?: ApplicationTypeTranslation[];
  editoTranslation?: EditoTranslation[];
  editoParagraphTranslation?: EditoParagraphTranslation[];
  permissionTranslations?: PermissionTranslation[];
  roleTranslations?: RoleTranslation[];
  highlightTranslations?: HighlightTranslation[];
}

export function languageMock(options?: ILanguageMockOptions): Language {
  return {
    id: options?.id || Date.now(),
    identifier: options?.identifier || 'fra',
    oldIdentifier: options?.oldIdentifier || 'fr',
    label: options?.label || 'Français',
    itemLanguageLevelTranslations:
      options?.itemLanguageLevelTranslations || undefined,
    itemDocumentTypeTranslations:
      options?.itemDocumentTypeTranslations || undefined,
    dublinCoreItemTranslations:
      options?.dublinCoreItemTranslations || undefined,
    dublinCoreTypeTranslations:
      options?.dublinCoreTypeTranslations || undefined,
    dublinCoreAudienceTranslations:
      options?.dublinCoreAudienceTranslations || undefined,
    dublinCoreLicenseTranslations:
      options?.dublinCoreLicenseTranslations || undefined,
    dublinCoreLanguages: options?.dublinCoreLanguages || undefined,
    playlistTranslations: options?.playlistTranslations || undefined,
    categoryTranslations: options?.categoryTranslations || undefined,
    applicationTranslations: options?.applicationTranslations || undefined,
    applicationTypeTranslations:
      options?.applicationTypeTranslations || undefined,
    editoTranslations: options?.editoTranslation || undefined,
    editoParagraphTranslations: options?.editoParagraphTranslation || undefined,
    permissionTranslations: options?.permissionTranslations || undefined,
    roleTranslations: options?.roleTranslations || undefined,
    highlightTranslations: options?.highlightTranslations || undefined,
  };
}
