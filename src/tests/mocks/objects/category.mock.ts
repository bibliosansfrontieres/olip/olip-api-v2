import { CategoryTranslation } from 'src/categories/entities/category-translation.entity';
import { Category } from 'src/categories/entities/category.entity';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { Visibility } from 'src/visibilities/entities/visibility.entity';
import { User } from 'src/users/entities/user.entity';

export interface ICategoryMockOptions {
  id?: number;
  sourceId?: number;
  pictogram?: string;
  isHomepageDisplayed?: boolean;
  order?: number;
  visibility?: Visibility;
  playlists?: Playlist[];
  categoryPlaylists?: CategoryPlaylist[];
  categoryTranslations?: CategoryTranslation[];
  users?: User[];
  deletedAt?: Date;
  isUpdateNeeded?: boolean;
  updatedAt?: number;
}

export function categoryMock(options?: ICategoryMockOptions): Category {
  return {
    id: options?.id || Date.now(),
    sourceId: options?.sourceId || null,
    pictogram: options?.pictogram || 'pictogram',
    isHomepageDisplayed: options?.isHomepageDisplayed || true,
    order: options?.order || 0,
    visibility: options?.visibility || undefined,
    playlists: options?.playlists || undefined,
    categoryPlaylists: options?.categoryPlaylists || undefined,
    categoryTranslations: options?.categoryTranslations || undefined,
    users: options?.users || undefined,
    deletedAt: options?.deletedAt || null,
    isUpdateNeeded: options?.isUpdateNeeded || false,
    updatedAt: options?.updatedAt ?? Date.now(),
  };
}
