import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { Item } from 'src/items/entities/item.entity';

export interface ICategoryPlaylistItemMockOptions {
  id?: number;
  isHighlightable?: boolean;
  isDisplayed?: boolean;
  item?: Item;
  categoryPlaylist?: CategoryPlaylist;
}

export function categoryPlaylistItemMock(
  options?: ICategoryPlaylistItemMockOptions,
): CategoryPlaylistItem {
  return {
    id: options?.id ?? Date.now(),
    isHighlightable: options?.isHighlightable ?? false,
    isDisplayed: options?.isDisplayed ?? true,
    item: options?.item ?? undefined,
    categoryPlaylist: options?.categoryPlaylist ?? undefined,
  };
}
