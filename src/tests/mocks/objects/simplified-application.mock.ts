import { Application } from 'src/applications/entities/application.entity';

export interface ISimplifiedApplicationMockOptions {
  id?: string;
}

export function simplifiedApplicationMock(
  options?: ISimplifiedApplicationMockOptions,
): Application {
  return {
    id: options?.id ?? Date.now().toString(),
  } as Application;
}
