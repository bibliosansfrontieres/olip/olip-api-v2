import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';

export interface ISimplifiedDublinCoreTypeMockOptions {
  id?: string;
}

export function simplifiedDublinCoreTypeMock(
  options?: ISimplifiedDublinCoreTypeMockOptions,
): DublinCoreType {
  return {
    id: options?.id ?? Date.now().toString(),
  } as unknown as DublinCoreType;
}
