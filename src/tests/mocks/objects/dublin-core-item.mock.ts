import { DublinCoreAudience } from 'src/dublin-core-audiences/entities/dublin-core-audience.entity';
import { DublinCoreItemTranslation } from 'src/dublin-core-items/entities/dublin-core-item-translation';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';
import { DublinCoreLanguage } from 'src/dublin-core-languages/entities/dublin-core-language.entity';
import { DublinCoreLicense } from 'src/dublin-core-licenses/entities/dublin-core-license.entity';
import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';
import { Item } from 'src/items/entities/item.entity';

export interface IDublinCoreItemMockOptions {
  id?: number;
  dublinCoreLanguages?: DublinCoreLanguage[];
  dublinCoreLicenses?: DublinCoreLicense[];
  dublinCoreAudiences?: DublinCoreAudience[];
  dublinCoreType?: DublinCoreType;
  dublinCoreItemTranslations?: DublinCoreItemTranslation[];
  item?: Item;
}

export function dublinCoreItemMock(
  options?: IDublinCoreItemMockOptions,
): DublinCoreItem {
  return {
    id: options?.id ?? Date.now(),
    dublinCoreLanguages: options?.dublinCoreLanguages ?? [],
    dublinCoreLicenses: options?.dublinCoreLicenses ?? [],
    dublinCoreAudiences: options?.dublinCoreAudiences ?? [],
    dublinCoreType: options?.dublinCoreType ?? undefined,
    dublinCoreItemTranslations: options?.dublinCoreItemTranslations ?? [],
    item: options?.item ?? undefined,
  };
}
