import { User } from 'src/users/entities/user.entity';

export interface ISimplifiedUserMockOptions {
  id?: number;
  username?: string;
}

export function simplifiedUserMock(options?: ISimplifiedUserMockOptions): User {
  return {
    id: options?.id || Date.now(),
    username: options?.username || 'username',
  } as User;
}
