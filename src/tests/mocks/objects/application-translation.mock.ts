import { ApplicationTranslation } from 'src/applications/entities/application-translation.entity';
import { Application } from 'src/applications/entities/application.entity';
import { Language } from 'src/languages/entities/language.entity';

export interface IApplicationTranslationMockOptions {
  id?: number;
  shortDescription?: string;
  longDescription?: string;
  indexId?: string | null;
  application?: Application;
  language?: Language;
}

export function applicationTranslationMock(
  options?: IApplicationTranslationMockOptions,
): ApplicationTranslation {
  return {
    id: options?.id || Date.now(),
    shortDescription: options?.shortDescription || 'shortDescription',
    longDescription: options?.longDescription || 'longDescription',
    indexId: options?.indexId || 'indexId',
    application: options?.application || undefined,
    language: options?.language || undefined,
  };
}
