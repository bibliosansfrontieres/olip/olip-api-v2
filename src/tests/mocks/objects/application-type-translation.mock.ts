import { ApplicationTypeTranslation } from 'src/application-types/entities/application-type-translation.entity';
import { ApplicationType } from 'src/application-types/entities/application-type.entity';
import { Language } from 'src/languages/entities/language.entity';

export interface IApplicationTypeTranslationMockOptions {
  id?: number;
  label?: string;
  description?: string;
  applicationType?: ApplicationType;
  language?: Language;
}

export function applicationTypeTranslationMock(
  options?: IApplicationTypeTranslationMockOptions,
): ApplicationTypeTranslation {
  return {
    id: options?.id ?? Date.now(),
    label: options?.label ?? 'label',
    description: options?.description ?? 'description',
    applicationType: options?.applicationType ?? undefined,
    language: options?.language ?? undefined,
  };
}
