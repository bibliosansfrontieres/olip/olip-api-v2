import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';
import { DublinCoreTypeTranslation } from 'src/dublin-core-types/entities/dublin-core-type-translation.entity';
import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';

export interface IDublinCoreTypeMockOptions {
  id?: number;
  image?: string;
  dublinCoreItems?: DublinCoreItem[];
  dublinCoreTypeTranslations?: DublinCoreTypeTranslation[];
}

export function dublinCoreTypeMock(
  options?: IDublinCoreTypeMockOptions,
): DublinCoreType {
  return {
    id: options?.id ?? Date.now(),
    image: options?.image ?? 'image',
    dublinCoreItems: options?.dublinCoreItems ?? undefined,
    dublinCoreTypeTranslations:
      options?.dublinCoreTypeTranslations ?? undefined,
  };
}
