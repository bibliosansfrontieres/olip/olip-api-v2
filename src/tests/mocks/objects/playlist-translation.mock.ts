import { Language } from 'src/languages/entities/language.entity';
import { PlaylistTranslation } from 'src/playlists/entities/playlist-translation.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';

export interface IPlaylistTranslationMockOptions {
  id?: number;
  title?: string;
  description?: string;
  indexId?: string | null;
  playlist?: Playlist;
  language?: Language;
}

export function playlistTranslationMock(
  options?: IPlaylistTranslationMockOptions,
): PlaylistTranslation {
  return {
    id: options?.id || Date.now(),
    title: options?.title || 'title',
    description: options?.description || 'description',
    indexId: options?.indexId || 'indexId',
    playlist: options?.playlist || undefined,
    language: options?.language || undefined,
  };
}
