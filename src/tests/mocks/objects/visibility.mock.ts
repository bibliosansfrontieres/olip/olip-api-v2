import { Application } from 'src/applications/entities/application.entity';
import { Category } from 'src/categories/entities/category.entity';
import { Role } from 'src/roles/entities/role.entity';
import { User } from 'src/users/entities/user.entity';
import { Visibility } from 'src/visibilities/entities/visibility.entity';

export interface IVisibilityMockOptions {
  id?: number;
  roles?: Role[];
  users?: User[];
  category?: Category;
  application?: Application;
}

export function visibilityMock(options?: IVisibilityMockOptions): Visibility {
  return {
    id: options?.id || Date.now(),
    roles: options?.roles || undefined,
    users: options?.users || undefined,
    category: options?.category || undefined,
    application: options?.application || undefined,
  };
}
