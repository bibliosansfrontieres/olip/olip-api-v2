import { DublinCoreItemTranslation } from 'src/dublin-core-items/entities/dublin-core-item-translation';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';
import { Language } from 'src/languages/entities/language.entity';

export interface IDublinCoreItemTranslationMockOptions {
  id?: number;
  title?: string;
  description?: string;
  creator?: string;
  publisher?: string;
  source?: string;
  extent?: string;
  subject?: string;
  dateCreated?: string;
  indexId?: string;
  dublinCoreItem?: DublinCoreItem;
  language?: Language;
}

export function dublinCoreItemTranslationMock(
  options?: IDublinCoreItemTranslationMockOptions,
): DublinCoreItemTranslation {
  return {
    id: options?.id ?? Date.now(),
    title: options?.title ?? 'title',
    description: options?.description ?? 'description',
    creator: options?.creator ?? 'creator',
    publisher: options?.publisher ?? 'publisher',
    source: options?.source ?? 'source',
    extent: options?.extent ?? 'extent',
    subject: options?.subject ?? 'subject',
    dateCreated: options?.dateCreated ?? 'dateCreated',
    indexId: options?.indexId ?? 'indexId',
    dublinCoreItem: options?.dublinCoreItem ?? undefined,
    language: options?.language ?? undefined,
  };
}
