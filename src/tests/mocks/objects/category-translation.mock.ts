import { CategoryTranslation } from 'src/categories/entities/category-translation.entity';
import { Category } from 'src/categories/entities/category.entity';
import { Language } from 'src/languages/entities/language.entity';

export interface ICategoryTranslationMockOptions {
  id?: number;
  title?: string;
  description?: string;
  indexId?: string;
  category?: Category;
  language?: Language;
}

export function categoryTranslationMock(
  options?: ICategoryTranslationMockOptions,
): CategoryTranslation {
  return {
    id: options?.id || Date.now(),
    title: options?.title || 'title',
    description: options?.description || 'description',
    indexId: options?.indexId || 'indexId',
    category: options?.category || undefined,
    language: options?.language || undefined,
  };
}
