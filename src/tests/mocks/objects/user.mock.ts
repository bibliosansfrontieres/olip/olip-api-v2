import { Role } from 'src/roles/entities/role.entity';
import { User } from 'src/users/entities/user.entity';
import { Visibility } from 'src/visibilities/entities/visibility.entity';
import { ConnectedUser } from 'src/stats/entities/connected-user.entity';
import { Category } from 'src/categories/entities/category.entity';

export interface IUserMockOptions {
  id?: number;
  username?: string;
  password?: string;
  language?: string;
  role?: Role;
  photo?: string;
  visibilities?: Visibility[];
  connectedUsers?: ConnectedUser[];
  categories?: Category[];
}

export function userMock(options?: IUserMockOptions): User {
  return {
    id: options?.id || Date.now(),
    username: options?.username || 'username',
    password: options?.password || null,
    photo: null,
    language: options?.language || 'eng',
    visibilities: options?.visibilities || undefined,
    connectedUsers: options?.connectedUsers || undefined,
    categories: options?.categories || undefined,
    role: options?.role || undefined,
  };
}
