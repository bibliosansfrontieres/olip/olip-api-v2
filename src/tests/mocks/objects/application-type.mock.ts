import { ApplicationTypeTranslation } from 'src/application-types/entities/application-type-translation.entity';
import { ApplicationType } from 'src/application-types/entities/application-type.entity';
import { Application } from 'src/applications/entities/application.entity';

export interface IApplicationTypeMockOptions {
  id?: string;
  name?: string;
  md5?: string;
  applications?: Application[];
  applicationTypeTranslations?: ApplicationTypeTranslation[];
}

export function applicationTypeMock(
  options?: IApplicationTypeMockOptions,
): ApplicationType {
  return {
    id: options?.id ?? Date.now().toString(),
    name: options?.name ?? 'name',
    md5: options?.md5 ?? 'md5',
    applications: options?.applications ?? undefined,
    applicationTypeTranslations:
      options?.applicationTypeTranslations ?? undefined,
  };
}
