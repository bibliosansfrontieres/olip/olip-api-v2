import { Language } from 'src/languages/entities/language.entity';
import { PermissionTranslation } from 'src/permissions/entities/permission-translation.entity';
import { Permission } from 'src/permissions/entities/permission.entity';

export interface IPermissionTranslationMockOptions {
  id?: number;
  label?: string;
  description?: string;
  permission?: Permission;
  language?: Language;
}

export function permissionTranslationMock(
  options?: IPermissionTranslationMockOptions,
): PermissionTranslation {
  return {
    id: options?.id || Date.now(),
    label: options?.label || 'label',
    description: options?.description || 'description',
    permission: options?.permission || undefined,
    language: options?.language || undefined,
  };
}
