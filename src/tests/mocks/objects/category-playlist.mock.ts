import { Category } from 'src/categories/entities/category.entity';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';

export interface ICategoryPlaylistMockOptions {
  id?: number;
  order?: number;
  playlist?: Playlist;
  category?: Category;
  categoryPlaylistItems?: CategoryPlaylistItem[];
}

export function categoryPlaylistMock(
  options?: ICategoryPlaylistMockOptions,
): CategoryPlaylist {
  return {
    id: options?.id ?? Date.now(),
    order: options?.order ?? 0,
    playlist: options?.playlist ?? undefined,
    category: options?.category ?? undefined,
    categoryPlaylistItems: options?.categoryPlaylistItems ?? undefined,
  };
}
