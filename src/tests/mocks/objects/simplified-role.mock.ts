import { Role } from 'src/roles/entities/role.entity';
import { RoleEnum } from 'src/roles/enums/role.enum';

export interface ISimplifiedRoleMockOptions {
  id?: number;
  name?: RoleEnum;
}

export function simplifiedRoleMock(options?: ISimplifiedRoleMockOptions): Role {
  return {
    id: options?.id ?? Date.now(),
    name: options?.name ?? RoleEnum.USER,
  } as Role;
}
