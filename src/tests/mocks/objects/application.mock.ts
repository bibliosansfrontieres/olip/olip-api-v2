import { ApplicationType } from 'src/application-types/entities/application-type.entity';
import { ApplicationTranslation } from 'src/applications/entities/application-translation.entity';
import { Application } from 'src/applications/entities/application.entity';
import { Item } from 'src/items/entities/item.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { Visibility } from 'src/visibilities/entities/visibility.entity';

export interface IApplicationMockOptions {
  id?: string;
  name?: string;
  displayName?: string;
  port?: string;
  exposeServices?: string | null;
  version?: string;
  logo?: string;
  image?: string;
  url?: string;
  size?: number;
  compose?: string;
  isInstalled?: boolean;
  isDownloading?: boolean;
  isInstalling?: boolean;
  isUp?: boolean;
  isUpdateNeeded?: boolean;
  isUpdating?: boolean;
  isOlip?: boolean;
  hooks?: string | null;
  applicationTranslations?: ApplicationTranslation[];
  applicationType?: ApplicationType;
  items?: Item[];
  playlists?: Playlist[];
  visibility?: Visibility;
}

export function applicationMock(
  options?: IApplicationMockOptions,
): Application {
  return {
    id: options?.id ?? Date.now().toString(),
    name: options?.name ?? 'name',
    displayName: options?.displayName ?? 'displayName',
    port: options?.port ?? '80',
    exposeServices: options?.exposeServices ?? null,
    version: options?.version ?? '1.0.0',
    logo: options?.logo ?? 'logo',
    image: options?.image ?? 'image',
    url: options?.url ?? 'url',
    size: options?.size ?? 0,
    compose: options?.compose ?? 'compose',
    isInstalled: options?.isInstalled ?? true,
    isDownloading: options?.isDownloading ?? false,
    isInstalling: options?.isInstalling ?? false,
    isUp: options?.isUp ?? false,
    isUpdateNeeded: options?.isUpdateNeeded ?? false,
    isUpdating: options?.isUpdating ?? false,
    isOlip: options?.isOlip ?? false,
    hooks: options?.hooks ?? null,
    applicationType: options?.applicationType ?? undefined,
    applicationTranslations: options?.applicationTranslations ?? undefined,
    items: options?.items ?? undefined,
    playlists: options?.playlists ?? undefined,
    visibility: options?.visibility ?? undefined,
  };
}
