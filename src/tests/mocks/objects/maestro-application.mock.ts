import { IMaestroApplicationTranslation } from 'src/maestro/interfaces/maestro-application-translation.interface';
import { IMaestroApplicationType } from 'src/maestro/interfaces/maestro-application-type.interface';
import { IMaestroApplication } from 'src/maestro/interfaces/maestro-application.interface';

export interface IMaestroApplicationMockOptions {
  id?: string;
  name?: string;
  displayName?: string;
  port?: string;
  size?: number;
  version?: string;
  logo?: string;
  image?: string;
  compose?: string;
  hooks?: string;
  md5?: string;
  exposeServices?: string;
  applicationType?: IMaestroApplicationType;
  applicationTranslations?: IMaestroApplicationTranslation[];
}

export function maestroApplicationMock(
  options?: IMaestroApplicationMockOptions,
): IMaestroApplication {
  return {
    id: options?.id ?? Date.now().toString(),
    name: options?.name ?? 'name',
    displayName: options?.displayName ?? 'displayName',
    port: options?.port ?? '80',
    size: options?.size ?? 0,
    version: options?.version ?? '1.0.0',
    logo: options?.logo ?? 'logo',
    image: options?.image ?? 'image',
    compose: options?.compose ?? 'compose',
    hooks: options?.hooks ?? 'hooks',
    md5: options?.md5 ?? 'md5',
    exposeServices: options?.exposeServices ?? 'exposeServices',
    applicationType: options?.applicationType ?? undefined,
    applicationTranslations: options?.applicationTranslations ?? [],
  };
}
