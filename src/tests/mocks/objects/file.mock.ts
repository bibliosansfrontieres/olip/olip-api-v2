import { File } from 'src/files/entities/file.entity';
import { Item } from 'src/items/entities/item.entity';

export interface IFileMockOptions {
  id?: number;
  name?: string;
  path?: string;
  size?: string;
  duration?: string;
  pages?: string;
  extension?: string;
  mimeType?: string;
  item?: Item;
}

export function fileMock(options?: IFileMockOptions): File {
  return {
    id: options?.id ?? Number(Date.now()),
    name: options?.name ?? 'name',
    path: options?.path ?? 'path',
    size: options?.size ?? 'size',
    duration: options?.duration ?? 'duration',
    pages: options?.pages ?? 'pages',
    extension: options?.extension ?? 'extension',
    mimeType: options?.mimeType ?? 'mimeType',
    item: options?.item ?? undefined,
  };
}
