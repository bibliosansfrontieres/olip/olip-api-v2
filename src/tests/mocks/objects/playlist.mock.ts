import { Application } from 'src/applications/entities/application.entity';
import { Category } from 'src/categories/entities/category.entity';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { Item } from 'src/items/entities/item.entity';
import { PlaylistTranslation } from 'src/playlists/entities/playlist-translation.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';

export interface IPlaylistMockOptions {
  id?: string;
  image?: string;
  isInstalled?: boolean;
  isUpdateNeeded?: boolean;
  isBroken?: boolean;
  isManuallyInstalled?: boolean;
  version?: string | null;
  size?: number;
  application?: Application;
  items?: Item[];
  playlistTranslations?: PlaylistTranslation[];
  categoryPlaylists?: CategoryPlaylist[];
  categories?: Category[];
}

export function playlistMock(options?: IPlaylistMockOptions): Playlist {
  return {
    id: options?.id ?? Date.now().toString(),
    image: options?.image ?? 'image',
    isInstalled: options?.isInstalled ?? true,
    isUpdateNeeded: options?.isUpdateNeeded ?? false,
    isBroken: options?.isBroken ?? false,
    isManuallyInstalled: options?.isManuallyInstalled ?? false,
    version: options?.version ?? undefined,
    size: options?.size ?? 0,
    application: options?.application ?? undefined,
    items: options?.items ?? undefined,
    playlistTranslations: options?.playlistTranslations ?? undefined,
    categoryPlaylists: options?.categoryPlaylists ?? undefined,
    categories: options?.categories ?? undefined,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  } as Playlist;
}
