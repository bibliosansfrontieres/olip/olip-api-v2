import { Setting } from 'src/settings/entities/setting.entity';
import { SettingKey } from 'src/settings/enums/setting-key.enum';

export interface ISettingMockOptions {
  id?: number;
  key: SettingKey;
  value: string;
}

export function settingMock(options: ISettingMockOptions): Setting {
  return {
    id: options?.id ?? Date.now(),
    key: options.key,
    value: options.value,
  };
}
