import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { Permission } from 'src/permissions/entities/permission.entity';
import { PermissionTranslation } from 'src/permissions/entities/permission-translation.entity';
import { Role } from 'src/roles/entities/role.entity';

export interface IPermissionMockOptions {
  id?: number;
  name?: PermissionEnum;
  roles?: Role[];
  permissionTranslations?: PermissionTranslation[];
}

export function permissionMock(options?: IPermissionMockOptions): Permission {
  return {
    id: options?.id || Date.now(),
    name: options?.name || PermissionEnum.CREATE_USER,
    roles: options?.roles || undefined,
    permissionTranslations: options?.permissionTranslations || undefined,
  };
}
