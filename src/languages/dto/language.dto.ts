import { ApiProperty } from '@nestjs/swagger';

export class LanguageDto {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: 'eng' })
  identifier: string;

  @ApiProperty({ example: 'en' })
  oldIdentifier: string;

  @ApiProperty({ example: 'English' })
  label: string;
}
