import { IsOptional, IsString } from 'class-validator';

export class CreateLanguageDto {
  @IsString()
  identifier: string;

  @IsString()
  label: string;

  @IsOptional()
  @IsString()
  oldIdentifier?: string;
}
