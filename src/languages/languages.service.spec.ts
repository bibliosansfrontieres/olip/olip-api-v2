import { Test, TestingModule } from '@nestjs/testing';
import { LanguagesService } from './languages.service';
import { Repository } from 'typeorm';
import { Language } from './entities/language.entity';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('LanguagesService', () => {
  let service: LanguagesService;
  let languagesRepository: Repository<Language>;
  const LANGUAGES_REPOSITORY_TOKEN = getRepositoryToken(Language);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: LANGUAGES_REPOSITORY_TOKEN, useClass: Repository<Language> },
        LanguagesService,
      ],
    }).compile();

    service = module.get<LanguagesService>(LanguagesService);
    languagesRepository = module.get<Repository<Language>>(
      LANGUAGES_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
