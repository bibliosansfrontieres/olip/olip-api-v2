import { ApiProperty } from '@nestjs/swagger';

export class LanguageNotFound {
  @ApiProperty({ example: 404 })
  statusCode: number;

  @ApiProperty({
    example: 'language not found',
  })
  message: string | string[];
}
