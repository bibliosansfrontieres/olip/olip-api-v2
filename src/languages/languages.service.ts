import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Language } from './entities/language.entity';
import { DeepPartial, FindOneOptions, Repository } from 'typeorm';
import { CreateLanguageDto } from './dto/create-language.dto';
import { invalidateCache } from 'src/utils/caching.util';

@Injectable()
export class LanguagesService {
  constructor(
    @InjectRepository(Language)
    private languageRepository: Repository<Language>,
  ) {}

  create(createLanguageDto: CreateLanguageDto) {
    const language = new Language(createLanguageDto);
    return this.languageRepository.create(language);
  }

  async insert(entities: DeepPartial<Language> | DeepPartial<Language>[]) {
    const result = await this.languageRepository.insert(entities);
    await invalidateCache(Language);
    return result;
  }

  count() {
    return this.languageRepository.count();
  }

  findOne(options?: FindOneOptions<Language>) {
    return this.languageRepository.findOne(options);
  }

  findOneByIdentifier(identifier: string): Promise<Language | null> {
    return this.languageRepository.findOne({ where: { identifier } });
  }

  async getTranslation<T, U>(
    entityName: string,
    entity: T,
    entityRepository: Repository<U>,
    languageIdentifier: string,
    withFallback = true,
  ): Promise<U | null> {
    const language = await this.findOneByIdentifier(languageIdentifier);
    if (language === null) {
      throw new NotFoundException('language not found');
    }
    const where1 = {};
    where1['language'] = language;
    where1[`${entityName}`] = { id: entity['id'] };
    const where2 = {};
    where2['language'] = { identifier: 'eng' };
    where2[`${entityName}`] = { id: entity['id'] };
    const where3 = {};
    where3[`${entityName}`] = { id: entity['id'] };

    let translation = await entityRepository.findOne({
      where: where1,
      relations: ['language'],
    });
    if (translation === null) {
      if (withFallback === false) {
        return null;
      }
      translation = await entityRepository.findOne({
        where: where2,
        relations: ['language'],
      });
      if (translation === null) {
        translation = await entityRepository.findOne({
          where: where3,
          relations: ['language'],
        });
      }
    }
    return translation;
  }
}
