import { ApplicationTranslation } from 'src/applications/entities/application-translation.entity';
import { CategoryTranslation } from 'src/categories/entities/category-translation.entity';
import { EditoTranslation } from 'src/editos/entities/edito-translation.entity';
import { HighlightTranslation } from 'src/highlights/entities/highlight-translation.entity';
import { PermissionTranslation } from 'src/permissions/entities/permission-translation.entity';
import { PlaylistTranslation } from 'src/playlists/entities/playlist-translation.entity';
import { RoleTranslation } from 'src/roles/entities/role-translation.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CreateLanguageDto } from '../dto/create-language.dto';
import { ApiProperty } from '@nestjs/swagger';
import { ApplicationTypeTranslation } from 'src/application-types/entities/application-type-translation.entity';
import { DublinCoreAudienceTranslation } from 'src/dublin-core-audiences/entities/dublin-core-audience-translation.entity';
import { DublinCoreLicenseTranslation } from 'src/dublin-core-licenses/entities/dublin-core-license-translation.entity';
import { DublinCoreLanguage } from 'src/dublin-core-languages/entities/dublin-core-language.entity';
import { DublinCoreTypeTranslation } from 'src/dublin-core-types/entities/dublin-core-type-translation.entity';
import { DublinCoreItemTranslation } from 'src/dublin-core-items/entities/dublin-core-item-translation';
import { ItemLanguageLevelTranslation } from 'src/item-language-levels/entities/item-language-level-translation.entity';
import { ItemDocumentTypeTranslation } from 'src/item-document-types/entities/item-document-type-translation.entity';
import { EditoParagraphTranslation } from 'src/edito-paragraphs/entities/edito-paragraph-translation.entity';

@Entity()
export class Language {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  identifier: string;

  @Column({ nullable: true })
  oldIdentifier: string | null;

  @Column()
  label: string;

  @OneToMany(
    () => ItemLanguageLevelTranslation,
    (itemLanguageLevelTranslation) => itemLanguageLevelTranslation.language,
  )
  itemLanguageLevelTranslations: ItemLanguageLevelTranslation[];

  @OneToMany(
    () => ItemDocumentTypeTranslation,
    (itemDocumentTypeTranslation) => itemDocumentTypeTranslation.language,
  )
  itemDocumentTypeTranslations: ItemDocumentTypeTranslation[];

  @OneToMany(
    () => DublinCoreItemTranslation,
    (dublinCoreItemTranslation) => dublinCoreItemTranslation.language,
  )
  dublinCoreItemTranslations: DublinCoreItemTranslation[];

  @OneToMany(
    () => DublinCoreTypeTranslation,
    (dublinCoreTypeTranslation) => dublinCoreTypeTranslation.language,
  )
  dublinCoreTypeTranslations: DublinCoreTypeTranslation[];

  @OneToMany(
    () => DublinCoreAudienceTranslation,
    (dublinCoreAudienceTranslation) => dublinCoreAudienceTranslation.language,
  )
  dublinCoreAudienceTranslations: DublinCoreAudienceTranslation[];

  @OneToMany(
    () => DublinCoreLicenseTranslation,
    (dublinCoreLicenseTranslation) => dublinCoreLicenseTranslation.language,
  )
  dublinCoreLicenseTranslations: DublinCoreLicenseTranslation[];

  @OneToMany(
    () => DublinCoreLanguage,
    (dublinCoreLanguage) => dublinCoreLanguage.language,
  )
  dublinCoreLanguages: DublinCoreLanguage[];

  @OneToMany(
    () => PlaylistTranslation,
    (playlistTranslation) => playlistTranslation.language,
  )
  playlistTranslations: PlaylistTranslation[];

  @OneToMany(
    () => CategoryTranslation,
    (categoryTranslation) => categoryTranslation.language,
  )
  categoryTranslations: CategoryTranslation[];

  @OneToMany(
    () => ApplicationTranslation,
    (applicationTranslation) => applicationTranslation.language,
  )
  applicationTranslations: ApplicationTranslation[];

  @OneToMany(
    () => ApplicationTypeTranslation,
    (applicationTypeTranslation) => applicationTypeTranslation.language,
  )
  applicationTypeTranslations: ApplicationTypeTranslation[];

  @OneToMany(
    () => EditoTranslation,
    (editoTranslation) => editoTranslation.language,
  )
  editoTranslations: EditoTranslation[];

  @OneToMany(
    () => EditoParagraphTranslation,
    (editoParagraphTranslation) => editoParagraphTranslation.language,
  )
  editoParagraphTranslations: EditoParagraphTranslation[];

  @OneToMany(
    () => PermissionTranslation,
    (permissionTranslation) => permissionTranslation.language,
  )
  permissionTranslations: PermissionTranslation[];

  @OneToMany(
    () => RoleTranslation,
    (roleTranslation) => roleTranslation.language,
  )
  roleTranslations: RoleTranslation[];

  @OneToMany(
    () => HighlightTranslation,
    (highlightTranslation) => highlightTranslation.language,
  )
  highlightTranslations: HighlightTranslation[];

  constructor(createLanguageDto: CreateLanguageDto) {
    if (!createLanguageDto) return;
    this.identifier = createLanguageDto.identifier;
    this.label = createLanguageDto.label;
    this.oldIdentifier = createLanguageDto.oldIdentifier || null;
  }
}
