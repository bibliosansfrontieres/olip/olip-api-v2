import * as Joi from 'joi';

export const configurationSchema = Joi.object({
  // App configuration
  VM_ID: Joi.string().required(),
  PORT: Joi.number().required(),
  JWT_SECRET: Joi.string().required(),
  JWT_EXPIRES_IN: Joi.string().required(),
  GLOBAL_CACHE_EXPIRES_IN: Joi.string().required(),
  MAX_IMAGE_SIZE_UPLOAD_IN_KB: Joi.number().required(),
  CHECK_INTERNET_INTERVAL: Joi.string().required(),
  EMIT_BATTERY_INFORMATION_INTERVAL: Joi.string().default('3s'),
  LOW_BATTERY_PERCENTAGE: Joi.number().default(5),
  CHECK_UPDATES_AT_STARTUP: Joi.string().required().valid('true', 'false'),
  // Applications configuration
  DOCKER_PRIVATE_APPS_REPOSITORY_TOKEN: Joi.string().required(),
  DOCKER_PRIVATE_APPS_REGISTRY: Joi.string().required(),
  APPS_IMAGES_TAG: Joi.string().required().valid('latest', 'staging'),
  APPLICATION_INTEGRATION: Joi.string().required().valid('true', 'false'),
  // Updates configuration
  DEPLOY_REPOSITORY_VOLUME: Joi.string().required(),
  CHECK_UPDATE_INTERVAL: Joi.string().required(),
  OLIP_STACK_IMAGES: Joi.string().required(),
  BSF_CORE_STACK_IMAGES: Joi.string().required(),
  OLIP_STACK_IMAGES_REGISTRY: Joi.string().required(),
  BSF_CORE_STACK_IMAGES_REGISTRY: Joi.string().required(),
  STACK_IMAGES_TAG: Joi.string().required(),
  // Threads configuration
  DOCKER_DIGEST_CHECKING_TIMEOUT: Joi.string().required(),
  DOCKER_DIGEST_CHECKING_MAX_THREADS: Joi.number().required(),
  DOCKER_DIGEST_CHECKING_MAX_THREADS_RESTART: Joi.number().required(),
  // Deploy configuration
  OMEKA_PROJECT_ID: Joi.string().required(),
  S3_BUCKET_URL: Joi.string().required(),
  // APIs configuration
  HAO_URL: Joi.string().required(),
  ELASTICSEARCH_API_URL: Joi.string().required(),
  MAESTRO_API_URL: Joi.string().required(),
  MAESTRO_SOCKET_API_URL: Joi.string().required(),
  // Docker configuration
  OLIP_VOLUME: Joi.string().required(),
  OLIP_NETWORK: Joi.string().required(),
  OLIP_RESOLVER_ID: Joi.string().required(),
  TLS_ENABLED: Joi.string().required(),
  OLIP_HOST: Joi.string().required(),
});
