import { forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import { SettingsService } from 'src/settings/settings.service';
import { ApplicationsService } from 'src/applications/applications.service';
import { MaestroApiService } from 'src/maestro/maestro-api.service';
import { IMaestroApplication } from 'src/maestro/interfaces/maestro-application.interface';

@Injectable()
export class ApplicationsUpdateService {
  constructor(
    private settingsService: SettingsService,
    @Inject(forwardRef(() => ApplicationsService))
    private applicationsService: ApplicationsService,
    private maestroApiService: MaestroApiService,
  ) {}

  async check() {
    Logger.log('Checking applications updates...', 'UpdateService');
    const result = await this.maestroApiService.getApplications();
    if (!result) {
      Logger.debug('Could not get applications from Maestro', 'UpdateService');
      return;
    }

    for (const maestroApplication of result.data) {
      await this.checkApplicationUpdate(maestroApplication);
    }

    await this.settingsService.update(SettingKey.LAST_APPLICATIONS_UPDATE, {
      value: Date.now().toString(),
    });
    Logger.log('Applications updates checked !', 'UpdateService');
  }

  async checkApplicationUpdate(maestroApplication: IMaestroApplication) {
    const application = await this.applicationsService.findOne({
      where: { id: maestroApplication.id },
    });

    if (application) {
      if (application.version !== maestroApplication.version) {
        if (application.isInstalled) {
          await this.applicationsService.needUpdate(application);
        } else {
          await this.applicationsService.updateMetadata(
            application,
            maestroApplication,
          );
        }
      }
    } else {
      await this.applicationsService.create(maestroApplication);
    }
  }
}
