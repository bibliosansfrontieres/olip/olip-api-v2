import { Test, TestingModule } from '@nestjs/testing';
import { UpdateService } from './update.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { HardwareService } from 'src/hardware/hardware.service';
import { SettingsService } from 'src/settings/settings.service';
import { ApplicationsService } from 'src/applications/applications.service';
import { MaestroService } from 'src/maestro/maestro.service';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { CategoriesService } from 'src/categories/categories.service';
import { settingsServiceMock } from 'src/tests/mocks/providers/settings-service.mock';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';
import { maestroServiceMock } from 'src/tests/mocks/providers/maestro-service.mock';
import { playlistsServiceMock } from 'src/tests/mocks/providers/playlists-service.mock';
import { categoriesServiceMock } from 'src/tests/mocks/providers/categories-service.mock';
import { hardwareServiceMock } from 'src/tests/mocks/providers/hardware-service.mock';
import { ApplicationsUpdateService } from './applications-update.service';
import { DockerUpdateService } from './docker-update.service';
import { PlaylistsUpdateService } from './playlists-update.service';
import { playlistsUpdateServiceMock } from 'src/tests/mocks/providers/playlists-update-service.mock';
import { dockerUpdateServiceMock } from 'src/tests/mocks/providers/docker-update-service.mock';
import { applicationsUpdateServiceMock } from 'src/tests/mocks/providers/applications-update-service.mock';
import { settingMock } from 'src/tests/mocks/objects/setting.mock';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import { Logger } from '@nestjs/common';

describe('UpdateService', () => {
  let service: UpdateService;
  let settingsService: SettingsService;
  let configService: ConfigService;
  let hardwareService: HardwareService;
  let dockerUpdateService: DockerUpdateService;
  let applicationsUpdateService: ApplicationsUpdateService;
  let playlistsUpdateService: PlaylistsUpdateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UpdateService,
        ConfigService,
        HardwareService,
        SettingsService,
        ApplicationsService,
        MaestroService,
        PlaylistsService,
        CategoriesService,
        ApplicationsUpdateService,
        DockerUpdateService,
        PlaylistsUpdateService,
      ],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(HardwareService)
      .useValue(hardwareServiceMock)
      .overrideProvider(SettingsService)
      .useValue(settingsServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(MaestroService)
      .useValue(maestroServiceMock)
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(ApplicationsUpdateService)
      .useValue(applicationsUpdateServiceMock)
      .overrideProvider(DockerUpdateService)
      .useValue(dockerUpdateServiceMock)
      .overrideProvider(PlaylistsUpdateService)
      .useValue(playlistsUpdateServiceMock)
      .compile();

    service = module.get<UpdateService>(UpdateService);
    settingsService = module.get<SettingsService>(SettingsService);
    configService = module.get<ConfigService>(ConfigService);
    hardwareService = module.get<HardwareService>(HardwareService);
    dockerUpdateService = module.get<DockerUpdateService>(DockerUpdateService);
    applicationsUpdateService = module.get<ApplicationsUpdateService>(
      ApplicationsUpdateService,
    );
    playlistsUpdateService = module.get<PlaylistsUpdateService>(
      PlaylistsUpdateService,
    );
    jest.spyOn(Logger, 'debug').mockImplementation(() => {});
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.clearAllTimers();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('isAlreadyCheckingUpdates', () => {
    it('should be defined', () => {
      expect(service.isAlreadyCheckingUpdates).toBeDefined();
    });

    it('should return true if already checking updates', () => {
      service.isCheckingUpdates = true;
      expect(service.isAlreadyCheckingUpdates()).toBe(true);
    });

    it('should return false if not already checking updates', () => {
      service.isCheckingUpdates = false;
      expect(service.isAlreadyCheckingUpdates()).toBe(false);
    });
  });

  describe('runCheckUpdates', () => {
    it('should be defined', () => {
      expect(service.runCheckUpdates).toBeDefined();
    });

    it('should return if already checking updates', async () => {
      jest.spyOn(service, 'isAlreadyCheckingUpdates').mockReturnValue(true);
      jest.spyOn(hardwareService, 'hasInternet');

      await service.runCheckUpdates();

      expect(hardwareService.hasInternet).not.toHaveBeenCalled();
    });

    it('should return if no internet connection', async () => {
      jest.spyOn(service, 'isAlreadyCheckingUpdates').mockReturnValue(false);
      jest.spyOn(hardwareService, 'hasInternet').mockResolvedValue(false);
      jest.spyOn(service, 'checkUpdates');

      await service.runCheckUpdates();

      expect(service.checkUpdates).not.toHaveBeenCalled();
    });

    it('should call checkUpdates', async () => {
      jest.spyOn(service, 'isAlreadyCheckingUpdates').mockReturnValue(false);
      jest.spyOn(hardwareService, 'hasInternet').mockResolvedValue(true);
      jest.spyOn(service, 'checkUpdates').mockResolvedValue(undefined);

      await service.runCheckUpdates();

      expect(service.checkUpdates).toHaveBeenCalled();
    });

    it('should manage isCheckingUpdates', async () => {
      jest.spyOn(service, 'isAlreadyCheckingUpdates').mockReturnValue(false);
      jest.spyOn(hardwareService, 'hasInternet').mockResolvedValue(true);
      jest.spyOn(service, 'checkUpdates').mockResolvedValue(undefined);

      expect(service.isCheckingUpdates).toBe(false);

      await service.runCheckUpdates();

      expect(service.isCheckingUpdates).toBe(false);
    });
  });

  describe('checkUpdates', () => {
    it('should be defined', () => {
      expect(service.checkUpdates).toBeDefined();
    });

    it('should skip Docker updates if not needed', async () => {
      service.isCheckingUpdates = false;
      jest.spyOn(hardwareService, 'hasInternet').mockResolvedValue(true);

      jest
        .spyOn(service, 'isUpdateCheckingNeeded')
        .mockResolvedValueOnce(false)
        .mockResolvedValueOnce(false)
        .mockResolvedValueOnce(false);

      await service.checkUpdates();

      expect(Logger.debug).toHaveBeenCalledWith(
        'No docker image updates checking needed',
        'UpdateService',
      );
    });

    it('should check Docker updates if needed', async () => {
      service.isCheckingUpdates = false;
      jest.spyOn(hardwareService, 'hasInternet').mockResolvedValue(true);

      jest
        .spyOn(service, 'isUpdateCheckingNeeded')
        .mockResolvedValueOnce(true) // Docker update check needed
        .mockResolvedValueOnce(false) // Applications update check not needed
        .mockResolvedValueOnce(false); // Playlists update check not needed

      await service.checkUpdates();

      expect(dockerUpdateService.check).toHaveBeenCalled();
    });

    it('should skip applications updates if not needed', async () => {
      service.isCheckingUpdates = false;
      jest.spyOn(hardwareService, 'hasInternet').mockResolvedValue(true);

      jest
        .spyOn(service, 'isUpdateCheckingNeeded')
        .mockResolvedValueOnce(false)
        .mockResolvedValueOnce(false)
        .mockResolvedValueOnce(false);

      await service.checkUpdates();

      expect(Logger.debug).toHaveBeenCalledWith(
        'No applications updates checking needed',
        'UpdateService',
      );
    });

    it('should check applications updates if needed', async () => {
      service.isCheckingUpdates = false;
      jest.spyOn(hardwareService, 'hasInternet').mockResolvedValue(true);

      jest
        .spyOn(service, 'isUpdateCheckingNeeded')
        .mockResolvedValueOnce(false) // Docker update check needed
        .mockResolvedValueOnce(true) // Applications update check not needed
        .mockResolvedValueOnce(false); // Playlists update check not needed

      await service.checkUpdates();

      expect(applicationsUpdateService.check).toHaveBeenCalled();
    });

    it('should skip playlists updates if not needed', async () => {
      service.isCheckingUpdates = false;
      jest.spyOn(hardwareService, 'hasInternet').mockResolvedValue(true);

      jest
        .spyOn(service, 'isUpdateCheckingNeeded')
        .mockResolvedValueOnce(false)
        .mockResolvedValueOnce(false)
        .mockResolvedValueOnce(false);

      await service.checkUpdates();

      expect(Logger.debug).toHaveBeenCalledWith(
        'No playlists updates checking needed',
        'UpdateService',
      );
    });

    it('should check playlists updates if needed', async () => {
      service.isCheckingUpdates = false;
      jest.spyOn(hardwareService, 'hasInternet').mockResolvedValue(true);

      jest
        .spyOn(service, 'isUpdateCheckingNeeded')
        .mockResolvedValueOnce(false) // Docker update check needed
        .mockResolvedValueOnce(false) // Applications update check not needed
        .mockResolvedValueOnce(true); // Playlists update check not needed

      await service.checkUpdates();

      expect(playlistsUpdateService.check).toHaveBeenCalled();
    });
  });

  describe('isUpdateCheckingNeeded', () => {
    it('should be defined', () => {
      expect(service.isUpdateCheckingNeeded).toBeDefined();
    });

    it('should return false if no update checking needed', async () => {
      const lastUpdateSetting = settingMock({
        key: SettingKey.LAST_PLAYLISTS_UPDATE,
        value: Date.now().toString(),
      });
      jest
        .spyOn(settingsService, 'findOne')
        .mockResolvedValueOnce(lastUpdateSetting);
      jest.spyOn(configService, 'get').mockReturnValueOnce('10m');
      const result = await service.isUpdateCheckingNeeded(
        lastUpdateSetting.key,
      );
      expect(result).toBe(false);
    });

    it('should return true if update checking needed', async () => {
      const lastUpdateSetting = settingMock({
        key: SettingKey.LAST_PLAYLISTS_UPDATE,
        value: (Date.now() - 10000).toString(),
      });
      jest
        .spyOn(settingsService, 'findOne')
        .mockResolvedValueOnce(lastUpdateSetting);
      jest.spyOn(configService, 'get').mockReturnValueOnce('1s');
      const result = await service.isUpdateCheckingNeeded(
        lastUpdateSetting.key,
      );
      expect(result).toBe(true);
    });
  });
});
