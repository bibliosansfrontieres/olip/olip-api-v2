import { Controller, Get } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { UpdateService } from './update.service';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';

@ApiTags('update')
@Controller('api/update')
export class UpdateController {
  constructor(private readonly updateService: UpdateService) {}

  @Auth(PermissionEnum.CHECK_UPDATES)
  @Get('check')
  @ApiOperation({ summary: 'Check for updates' })
  getUpdateCheck() {
    this.updateService.runCheckUpdates();
  }
}
