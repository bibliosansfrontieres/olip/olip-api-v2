import { forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import { SettingsService } from 'src/settings/settings.service';
import { threadsFunction } from 'src/utils/thread-function.util';
import { Docker } from 'src/utils/docker.utils';
import { ApplicationsService } from 'src/applications/applications.service';
import { isBsfCore } from 'src/utils/is-bsf-core.util';
import { ConfigService } from '@nestjs/config';
import ms, { StringValue } from 'ms';

@Injectable()
export class DockerUpdateService {
  constructor(
    private settingsService: SettingsService,
    @Inject(forwardRef(() => ApplicationsService))
    private applicationsService: ApplicationsService,
    private configService: ConfigService,
  ) {}

  getImages(registry: string, stackImages: string, tag: string) {
    const images = stackImages.split(';');
    return images.map((image) => `${registry}/${image}:${tag}`);
  }

  getOlipStackImages() {
    const stackImagesTag = this.configService.get<string>('STACK_IMAGES_TAG');
    const olipStackImagesRegistry = this.configService.get<string>(
      'OLIP_STACK_IMAGES_REGISTRY',
    );
    const olipStackImages = this.configService.get<string>('OLIP_STACK_IMAGES');

    return this.getImages(
      olipStackImagesRegistry,
      olipStackImages,
      stackImagesTag,
    );
  }

  getBsfCoreStackImages() {
    const stackImagesTag = this.configService.get<string>('STACK_IMAGES_TAG');
    const bsfCoreStackImagesRegistry = this.configService.get<string>(
      'BSF_CORE_STACK_IMAGES_REGISTRY',
    );
    const bsfCoreStackImages = this.configService.get<string>(
      'BSF_CORE_STACK_IMAGES',
    );

    return this.getImages(
      bsfCoreStackImagesRegistry,
      bsfCoreStackImages,
      stackImagesTag,
    );
  }

  async check() {
    Logger.log('Checking docker images updates...', 'UpdateService');

    const dockerImages = this.getOlipStackImages();

    if (isBsfCore()) {
      dockerImages.push(...this.getBsfCoreStackImages());
    }

    const areUpdatesCheckingSuccessful =
      await this.runCheckDockerImagesUpdatesThreads(dockerImages);

    if (!areUpdatesCheckingSuccessful) return;

    await this.settingsService.update(SettingKey.LAST_DOCKER_IMAGES_UPDATE, {
      value: Date.now().toString(),
    });
  }

  async runCheckDockerImagesUpdatesThreads(dockerImages: string[]) {
    const dockerDigestCheckingTimeout = ms(
      this.configService.get<StringValue>('DOCKER_DIGEST_CHECKING_TIMEOUT'),
    );
    const dockerDigestCheckingMaxThreads = this.configService.get<number>(
      'DOCKER_DIGEST_CHECKING_MAX_THREADS',
    );

    const results = await threadsFunction<string, boolean | 'error'>({
      iterationArray: dockerImages,
      timeout: dockerDigestCheckingTimeout,
      maxThreads: dockerDigestCheckingMaxThreads,
      thread: this.checkDockerImageUpdate.bind(this),
      maxRestart: 0,
      options: {
        sleepBeforeRestart: 1000,
        service: 'DockerUpdateService',
        name: 'check docker image update',
      },
    });

    if (!results) return false;

    const isUpdateNeeded = results.includes(true);

    const olipApp = await this.applicationsService.findOne({
      where: { id: 'olip' },
    });

    if (!olipApp) {
      throw new Error(
        'OLIP application not found, this is not normal my friend, we have a huuuuuuge problem.',
      );
    }

    olipApp.isUpdateNeeded = isUpdateNeeded;
    await this.applicationsService.save(olipApp);

    if (isUpdateNeeded) {
      Logger.debug('OLIP update needed', 'UpdateService');
    } else {
      Logger.debug('No OLIP update needed', 'UpdateService');
    }

    return !results.includes('error');
  }

  async checkDockerImageUpdate(image: string): Promise<boolean | 'error'> {
    Logger.debug(`Checking image ${image}`, 'UpdateService');

    const docker = new Docker(false);

    try {
      const localDigest = await this.getLocalImageDigest(docker, image);
      const distDigests = await this.getRemoteImageDigests(docker, image);

      if (localDigest && distDigests.length > 0) {
        return !distDigests.includes(localDigest);
      }

      return false;
    } catch (e) {
      console.error(e);
      return 'error';
    }
  }

  async getLocalImageDigest(
    docker: Docker,
    image: string,
  ): Promise<string | null> {
    try {
      const dockerInspectResult = await docker.inspect(image);

      if (!dockerInspectResult?.raw) {
        throw new Error('Could not get raw docker inspect result');
      }

      const dockerInspectParsedResult = JSON.parse(dockerInspectResult.raw);

      return dockerInspectParsedResult[0]?.Id || null;
    } catch (e) {
      console.error(`Error fetching local image digest: ${e}`);
      return null;
    }
  }

  async getRemoteImageDigests(
    docker: Docker,
    image: string,
  ): Promise<string[]> {
    const digests: string[] = [];

    try {
      const dockerInspectResult = await docker.manifest('inspect', image);

      if (!dockerInspectResult?.raw) {
        throw new Error('Could not get raw docker manifest inspect result');
      }

      const dockerInspectParsedResult = JSON.parse(dockerInspectResult.raw);

      for (const dockerManifest of dockerInspectParsedResult?.manifests) {
        const digest = dockerManifest?.digest;
        if (!digest) continue;
        digests.push(digest);
      }
    } catch (e) {
      console.error(`Error fetching remote image digests: ${e}`);
    }

    return digests;
  }
}
