import { Test, TestingModule } from '@nestjs/testing';
import { UpdateController } from './update.controller';
import { UpdateService } from './update.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { HardwareService } from 'src/hardware/hardware.service';
import { hardwareServiceMock } from 'src/tests/mocks/providers/hardware-service.mock';
import { SettingsService } from 'src/settings/settings.service';
import { settingsServiceMock } from 'src/tests/mocks/providers/settings-service.mock';
import { ApplicationsUpdateService } from './applications-update.service';
import { DockerUpdateService } from './docker-update.service';
import { PlaylistsUpdateService } from './playlists-update.service';
import { applicationsUpdateServiceMock } from 'src/tests/mocks/providers/applications-update-service.mock';
import { dockerUpdateServiceMock } from 'src/tests/mocks/providers/docker-update-service.mock';
import { playlistsUpdateServiceMock } from 'src/tests/mocks/providers/playlists-update-service.mock';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';

describe('UpdateController', () => {
  let controller: UpdateController;
  let updateService: UpdateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UpdateController],
      providers: [
        UpdateService,
        ConfigService,
        HardwareService,
        SettingsService,
        ApplicationsUpdateService,
        DockerUpdateService,
        PlaylistsUpdateService,
        JwtService,
        UsersService,
      ],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(HardwareService)
      .useValue(hardwareServiceMock)
      .overrideProvider(SettingsService)
      .useValue(settingsServiceMock)
      .overrideProvider(ApplicationsUpdateService)
      .useValue(applicationsUpdateServiceMock)
      .overrideProvider(DockerUpdateService)
      .useValue(dockerUpdateServiceMock)
      .overrideProvider(PlaylistsUpdateService)
      .useValue(playlistsUpdateServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .compile();

    controller = module.get<UpdateController>(UpdateController);
    updateService = module.get<UpdateService>(UpdateService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getUpdateCheck', () => {
    it('should be defined', () => {
      expect(controller.getUpdateCheck).toBeDefined();
    });

    it('should have @Get("check") route', () => {
      const routeMetadata = Reflect.getMetadata(
        'path',
        controller.getUpdateCheck,
      );
      expect(routeMetadata).toBe('check');
    });

    it('should have @Auth() decorator', () => {
      const optionalAuthDecorator = Reflect.getMetadata(
        'auth',
        Object.getPrototypeOf(controller),
        'getUpdateCheck',
      );
      expect(optionalAuthDecorator).toBe(true);
    });

    it('should call updateService.runCheckUpdates', async () => {
      jest.spyOn(updateService, 'runCheckUpdates');

      controller.getUpdateCheck();

      expect(updateService.runCheckUpdates).toHaveBeenCalled();
    });
  });
});
