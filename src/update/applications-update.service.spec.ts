import { Test, TestingModule } from '@nestjs/testing';
import { ApplicationsUpdateService } from './applications-update.service';
import { SettingsService } from 'src/settings/settings.service';
import { settingsServiceMock } from 'src/tests/mocks/providers/settings-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';
import { MaestroApiService } from 'src/maestro/maestro-api.service';
import { maestroApiServiceMock } from 'src/tests/mocks/providers/maestro-api-service.mock';
import { maestroApplicationMock } from 'src/tests/mocks/objects/maestro-application.mock';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import { applicationMock } from 'src/tests/mocks/objects/application.mock';

describe('ApplicationsUpdateService', () => {
  let service: ApplicationsUpdateService;
  let maestroApiService: MaestroApiService;
  let settingsService: SettingsService;
  let applicationsService: ApplicationsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ApplicationsUpdateService,
        SettingsService,
        ApplicationsService,
        MaestroApiService,
      ],
    })
      .overrideProvider(SettingsService)
      .useValue(settingsServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(MaestroApiService)
      .useValue(maestroApiServiceMock)
      .compile();

    service = module.get<ApplicationsUpdateService>(ApplicationsUpdateService);
    maestroApiService = module.get<MaestroApiService>(MaestroApiService);
    settingsService = module.get<SettingsService>(SettingsService);
    applicationsService = module.get<ApplicationsService>(ApplicationsService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('check', () => {
    const mockMaestroApplicationsResult = {
      data: [maestroApplicationMock(), maestroApplicationMock()],
    };
    it('should be defined', () => {
      expect(service.check).toBeDefined();
    });

    it('should call maestroApiService.getApplications', async () => {
      jest.spyOn(maestroApiService, 'getApplications');
      await service.check();
      expect(maestroApiService.getApplications).toHaveBeenCalled();
    });

    it('should skip if maestroApiService.getApplications return undefined', async () => {
      jest
        .spyOn(maestroApiService, 'getApplications')
        .mockResolvedValue(undefined);
      jest.spyOn(service, 'checkApplicationUpdate');

      await service.check();

      expect(service.checkApplicationUpdate).not.toHaveBeenCalled();
    });

    it('should call checkApplicationUpdate for each maestroApplication received', async () => {
      jest
        .spyOn(maestroApiService, 'getApplications')
        .mockResolvedValue(mockMaestroApplicationsResult);

      jest.spyOn(service, 'checkApplicationUpdate');

      await service.check();

      expect(service.checkApplicationUpdate).toHaveBeenCalledTimes(
        mockMaestroApplicationsResult.data.length,
      );
    });

    it('should update LAST_APPLICATIONS_UPDATE setting', async () => {
      const fakeDate = new Date();
      const fakeTimestamp = fakeDate.getTime();
      jest.useFakeTimers().setSystemTime(fakeDate);

      jest
        .spyOn(maestroApiService, 'getApplications')
        .mockResolvedValue({ data: [] });
      jest.spyOn(settingsService, 'update');

      await service.check();

      expect(settingsService.update).toHaveBeenCalledWith(
        SettingKey.LAST_APPLICATIONS_UPDATE,
        { value: fakeTimestamp.toString() },
      );
    });
  });

  describe('checkApplicationUpdate', () => {
    const mockMaestroApplication = maestroApplicationMock({
      id: 'sameAppId',
      version: '1',
    });
    const mockApplication = applicationMock({
      id: 'sameAppId',
      version: '0',
      isInstalled: false,
    });

    it('should be defined', () => {
      expect(service.checkApplicationUpdate).toBeDefined();
    });

    it('should create application if does not exists', async () => {
      jest
        .spyOn(applicationsService, 'findOne')
        .mockResolvedValueOnce(undefined);

      await service.checkApplicationUpdate(mockMaestroApplication);

      expect(applicationsService.create).toHaveBeenCalledWith(
        mockMaestroApplication,
      );
    });

    it('should switch application to isUpdateNeeded = true if application is installed', async () => {
      const mockInstalledApplication = {
        ...mockApplication,
        isInstalled: true,
      };

      jest
        .spyOn(applicationsService, 'findOne')
        .mockResolvedValueOnce(mockInstalledApplication);

      jest.spyOn(applicationsService, 'needUpdate');

      await service.checkApplicationUpdate(mockMaestroApplication);

      expect(applicationsService.needUpdate).toHaveBeenCalledWith(
        mockInstalledApplication,
      );
    });

    it('should update application metadata if application is not installed', async () => {
      jest
        .spyOn(applicationsService, 'findOne')
        .mockResolvedValueOnce(mockApplication);

      jest.spyOn(applicationsService, 'updateMetadata');

      await service.checkApplicationUpdate(mockMaestroApplication);

      expect(applicationsService.updateMetadata).toHaveBeenCalledWith(
        mockApplication,
        mockMaestroApplication,
      );
    });
  });
});
