import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import ms, { StringValue } from 'ms';
import { HardwareService } from 'src/hardware/hardware.service';
import { OnEvent } from '@nestjs/event-emitter';
import { ApplicationsUpdateService } from './applications-update.service';
import { PlaylistsUpdateService } from './playlists-update.service';
import { DockerUpdateService } from './docker-update.service';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import { SettingsService } from 'src/settings/settings.service';
import prettyMilliseconds from 'pretty-ms';

@Injectable()
export class UpdateService {
  private intervalId: NodeJS.Timeout;
  isCheckingUpdates = false;

  constructor(
    private configService: ConfigService,
    private hardwareService: HardwareService,
    private settingsService: SettingsService,
    private applicationsUpdateService: ApplicationsUpdateService,
    private playlistsUpdateService: PlaylistsUpdateService,
    private dockerUpdateService: DockerUpdateService,
  ) {}

  /**
   * Called when the application is starting. Registers an interval to check updates
   * at the interval specified by the CHECK_UPDATE_INTERVAL environment variable.
   */
  onApplicationBootstrap() {
    const checkUpdatesInterval = this.configService.get<StringValue>(
      'CHECK_UPDATE_INTERVAL',
    );
    const interval = ms(checkUpdatesInterval);
    Logger.log(
      `Registering check updates interval every ${prettyMilliseconds(interval)}`,
      'UpdateService',
    );
    this.intervalId = setInterval(this.runCheckUpdates.bind(this), interval);
  }

  onModuleDestroy() {
    if (this.intervalId) {
      clearInterval(this.intervalId);
    }
  }

  isAlreadyCheckingUpdates() {
    if (this.isCheckingUpdates) {
      Logger.debug('Already checking updates, skipping...', 'UpdateService');
      return true;
    }
    return false;
  }

  @OnEvent('internet.connected')
  async runCheckUpdates() {
    if (this.isAlreadyCheckingUpdates()) return false;
    this.isCheckingUpdates = true;

    const hasInternet = await this.hardwareService.hasInternet();

    if (!hasInternet) {
      Logger.debug(
        'No internet, skipping updates checking...',
        'UpdateService',
      );
      this.isCheckingUpdates = false;
      return false;
    }

    Logger.debug('Checking updates...', 'UpdateService');

    await this.checkUpdates();

    Logger.debug('Updates checked !', 'UpdateService');
    this.isCheckingUpdates = false;
    return true;
  }

  /**
   * Checks for updates.
   * If the device has no internet connection, it simply skips the check.
   * Otherwise, it checks for each update type, if updates check is needed.
   */
  async checkUpdates() {
    const isDockerCheckingNeeded = await this.isUpdateCheckingNeeded(
      SettingKey.LAST_DOCKER_IMAGES_UPDATE,
    );
    if (!isDockerCheckingNeeded) {
      Logger.debug('No docker image updates checking needed', 'UpdateService');
    } else {
      await this.dockerUpdateService.check();
    }

    const isApplicationsCheckingNeeded = await this.isUpdateCheckingNeeded(
      SettingKey.LAST_APPLICATIONS_UPDATE,
    );
    if (!isApplicationsCheckingNeeded) {
      Logger.debug('No applications updates checking needed', 'UpdateService');
    } else {
      await this.applicationsUpdateService.check();
    }

    const isPlaylistsCheckingNeeded = await this.isUpdateCheckingNeeded(
      SettingKey.LAST_PLAYLISTS_UPDATE,
    );
    if (!isPlaylistsCheckingNeeded) {
      Logger.debug('No playlists updates checking needed', 'UpdateService');
    } else {
      await this.playlistsUpdateService.check();
    }
  }

  async isUpdateCheckingNeeded(key: SettingKey) {
    const { value } = await this.settingsService.findOne(key);
    const lastUpdateTimestamp = parseInt(value);

    return (
      lastUpdateTimestamp +
        ms(this.configService.get<StringValue>('CHECK_UPDATE_INTERVAL')) <=
      Date.now()
    );
  }
}
