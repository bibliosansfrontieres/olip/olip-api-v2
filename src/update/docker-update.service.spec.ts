import { Test, TestingModule } from '@nestjs/testing';
import { DockerUpdateService } from './docker-update.service';
import { SettingsService } from 'src/settings/settings.service';
import { settingsServiceMock } from 'src/tests/mocks/providers/settings-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { ConfigService } from '@nestjs/config';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import * as isBsfCoreUtil from 'src/utils/is-bsf-core.util';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import * as threadFunctionUtil from 'src/utils/thread-function.util';
import { applicationMock } from 'src/tests/mocks/objects/application.mock';
import { Docker } from 'src/utils/docker.utils';

describe('DockerUpdateService', () => {
  let service: DockerUpdateService;
  let configService: ConfigService;
  let settingsService: SettingsService;
  let applicationsService: ApplicationsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DockerUpdateService,
        SettingsService,
        ApplicationsService,
        ConfigService,
      ],
    })
      .overrideProvider(SettingsService)
      .useValue(settingsServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    service = module.get<DockerUpdateService>(DockerUpdateService);
    configService = module.get<ConfigService>(ConfigService);
    settingsService = module.get<SettingsService>(SettingsService);
    applicationsService = module.get<ApplicationsService>(ApplicationsService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  const registry = 'registry.io';
  const images = 'olip;service';
  const tag = 'latest';

  describe('getImages', () => {
    it('should be defined', () => {
      expect(service.getImages).toBeDefined();
    });

    it('should return an array of images', () => {
      const result = service.getImages(registry, images, tag);

      expect(result).toBeInstanceOf(Array);
      expect(result).toEqual([
        `${registry}/olip:${tag}`,
        `${registry}/service:${tag}`,
      ]);
    });
  });

  describe('getOlipStackImages', () => {
    it('should be defined', () => {
      expect(service.getOlipStackImages).toBeDefined();
    });

    it('should call getImages', () => {
      jest.spyOn(service, 'getImages');

      service.getOlipStackImages();

      expect(service.getImages).toHaveBeenCalled();
    });

    it('should call getImages with right arguments', () => {
      jest.spyOn(configService, 'get').mockReturnValueOnce(tag);
      jest.spyOn(configService, 'get').mockReturnValueOnce(registry);
      jest.spyOn(configService, 'get').mockReturnValueOnce(images);

      jest.spyOn(service, 'getImages');

      service.getOlipStackImages();

      expect(service.getImages).toHaveBeenCalledWith(registry, images, tag);
    });

    it('should return an array', () => {
      jest.spyOn(configService, 'get').mockReturnValueOnce(tag);
      jest.spyOn(configService, 'get').mockReturnValueOnce(registry);
      jest.spyOn(configService, 'get').mockReturnValueOnce(images);

      jest.spyOn(service, 'getImages');

      const result = service.getOlipStackImages();

      expect(Array.isArray(result)).toBeTruthy();
    });
  });

  describe('getBsfCoreStackImages', () => {
    it('should be defined', () => {
      expect(service.getBsfCoreStackImages).toBeDefined();
    });

    it('should call getImages', () => {
      jest.spyOn(service, 'getImages');

      service.getBsfCoreStackImages();

      expect(service.getImages).toHaveBeenCalled();
    });

    it('should call getImages with right arguments', () => {
      jest.spyOn(configService, 'get').mockReturnValueOnce(tag);
      jest.spyOn(configService, 'get').mockReturnValueOnce(registry);
      jest.spyOn(configService, 'get').mockReturnValueOnce(images);

      jest.spyOn(service, 'getImages');

      service.getBsfCoreStackImages();

      expect(service.getImages).toHaveBeenCalledWith(registry, images, tag);
    });

    it('should return an array', () => {
      jest.spyOn(configService, 'get').mockReturnValueOnce(tag);
      jest.spyOn(configService, 'get').mockReturnValueOnce(registry);
      jest.spyOn(configService, 'get').mockReturnValueOnce(images);

      jest.spyOn(service, 'getImages');

      const result = service.getBsfCoreStackImages();

      expect(Array.isArray(result)).toBeTruthy();
    });
  });

  describe('check', () => {
    it('should be defined', () => {
      expect(service.check).toBeDefined();
    });

    it('should call getOlipStackImages', async () => {
      jest.spyOn(service, 'getOlipStackImages');
      jest
        .spyOn(service, 'runCheckDockerImagesUpdatesThreads')
        .mockResolvedValue(false);

      await service.check();

      expect(service.getOlipStackImages).toHaveBeenCalled();
    });

    it('should not call getBsfCoreStackImages if not bsf-core deployed', async () => {
      jest.spyOn(service, 'getBsfCoreStackImages');
      jest.spyOn(isBsfCoreUtil, 'isBsfCore').mockReturnValue(false);
      jest
        .spyOn(service, 'runCheckDockerImagesUpdatesThreads')
        .mockResolvedValue(false);

      await service.check();

      expect(service.getBsfCoreStackImages).not.toHaveBeenCalled();
    });

    it('should call getBsfCoreStackImages if bsf-core deployed', async () => {
      jest.spyOn(service, 'getBsfCoreStackImages');
      jest.spyOn(isBsfCoreUtil, 'isBsfCore').mockReturnValue(true);
      jest
        .spyOn(service, 'runCheckDockerImagesUpdatesThreads')
        .mockResolvedValue(false);

      await service.check();

      expect(service.getBsfCoreStackImages).toHaveBeenCalled();
    });

    it('should call runCheckDockerImagesUpdatesThreads', async () => {
      jest
        .spyOn(service, 'runCheckDockerImagesUpdatesThreads')
        .mockResolvedValue(false);

      await service.check();

      expect(service.runCheckDockerImagesUpdatesThreads).toHaveBeenCalled();
    });

    it('should not update LAST_DOCKER_IMAGES_UPDATE if runCheckDockerImagesUpdatesThreads returns false', async () => {
      jest
        .spyOn(service, 'runCheckDockerImagesUpdatesThreads')
        .mockResolvedValue(false);

      await service.check();

      expect(settingsService.update).not.toHaveBeenCalled();
    });

    it('should update LAST_DOCKER_IMAGES_UPDATE if runCheckDockerImagesUpdatesThreads returns true', async () => {
      const fakeDate = new Date();
      const fakeTimestamp = fakeDate.getTime();
      jest.useFakeTimers().setSystemTime(fakeDate);
      jest.spyOn(settingsService, 'update');

      jest
        .spyOn(service, 'runCheckDockerImagesUpdatesThreads')
        .mockResolvedValue(true);

      await service.check();

      expect(settingsService.update).toHaveBeenCalledWith(
        SettingKey.LAST_DOCKER_IMAGES_UPDATE,
        { value: fakeTimestamp.toString() },
      );
    });
  });

  describe('runCheckDockerImagesUpdatesThreads', () => {
    const timeout = '1m';
    const maxThreads = 5;
    const mockApplication = applicationMock({ id: 'olip' });

    it('should be defined', () => {
      expect(service.runCheckDockerImagesUpdatesThreads).toBeDefined();
    });

    it('should call threadsFunction', async () => {
      jest.spyOn(configService, 'get').mockReturnValueOnce(timeout);
      jest.spyOn(configService, 'get').mockReturnValueOnce(maxThreads);
      jest.spyOn(threadFunctionUtil, 'threadsFunction');
      jest
        .spyOn(applicationsService, 'findOne')
        .mockResolvedValue(mockApplication);
      await service.runCheckDockerImagesUpdatesThreads([]);
      expect(threadFunctionUtil.threadsFunction).toHaveBeenCalled();
    });

    it('should call checkDockerImageUpdate', async () => {
      jest.spyOn(configService, 'get').mockReturnValueOnce(timeout);
      jest.spyOn(configService, 'get').mockReturnValueOnce(maxThreads);
      jest.spyOn(service, 'checkDockerImageUpdate').mockResolvedValue(true);
      await service.runCheckDockerImagesUpdatesThreads(['image']);
      expect(service.checkDockerImageUpdate).toHaveBeenCalled();
    });

    it('should return false if threadsFunction result is undefined', async () => {
      jest.spyOn(configService, 'get').mockReturnValueOnce(timeout);
      jest.spyOn(configService, 'get').mockReturnValueOnce(maxThreads);
      jest.spyOn(service, 'checkDockerImageUpdate').mockResolvedValue(true);
      jest
        .spyOn(threadFunctionUtil, 'threadsFunction')
        .mockResolvedValue(undefined);

      const result = await service.runCheckDockerImagesUpdatesThreads([]);

      expect(result).toBeFalsy();
    });

    it('should set OLIP app isUpdateNeeded = true if threadsFunction result includes true', async () => {
      jest.spyOn(configService, 'get').mockReturnValueOnce(timeout);
      jest.spyOn(configService, 'get').mockReturnValueOnce(maxThreads);
      jest.spyOn(service, 'checkDockerImageUpdate').mockResolvedValue(true);
      jest
        .spyOn(threadFunctionUtil, 'threadsFunction')
        .mockResolvedValue([true]);
      jest
        .spyOn(applicationsService, 'findOne')
        .mockResolvedValue(mockApplication);

      await service.runCheckDockerImagesUpdatesThreads([]);

      expect(mockApplication.isUpdateNeeded).toBe(true);
      expect(applicationsService.save).toHaveBeenCalled();
    });

    it('should set OLIP app isUpdateNeeded = false if threadsFunction result does not include true', async () => {
      jest.spyOn(configService, 'get').mockReturnValueOnce(timeout);
      jest.spyOn(configService, 'get').mockReturnValueOnce(maxThreads);
      jest.spyOn(service, 'checkDockerImageUpdate').mockResolvedValue(true);
      jest
        .spyOn(threadFunctionUtil, 'threadsFunction')
        .mockResolvedValue([false, false]);
      jest
        .spyOn(applicationsService, 'findOne')
        .mockResolvedValue(mockApplication);

      await service.runCheckDockerImagesUpdatesThreads([]);

      expect(mockApplication.isUpdateNeeded).toBe(false);
      expect(applicationsService.save).toHaveBeenCalled();
    });

    it('should return false if results includes error', async () => {
      jest.spyOn(configService, 'get').mockReturnValueOnce(timeout);
      jest.spyOn(configService, 'get').mockReturnValueOnce(maxThreads);
      jest.spyOn(service, 'checkDockerImageUpdate').mockResolvedValue(true);
      jest
        .spyOn(threadFunctionUtil, 'threadsFunction')
        .mockResolvedValue(['error', false]);
      jest
        .spyOn(applicationsService, 'findOne')
        .mockResolvedValue(mockApplication);

      const result = await service.runCheckDockerImagesUpdatesThreads([]);

      expect(result).toBe(false);
    });

    it('should return true if results does not includes error', async () => {
      jest.spyOn(configService, 'get').mockReturnValueOnce(timeout);
      jest.spyOn(configService, 'get').mockReturnValueOnce(maxThreads);
      jest.spyOn(service, 'checkDockerImageUpdate').mockResolvedValue(true);
      jest
        .spyOn(threadFunctionUtil, 'threadsFunction')
        .mockResolvedValue([true, false]);
      jest
        .spyOn(applicationsService, 'findOne')
        .mockResolvedValue(mockApplication);

      const result = await service.runCheckDockerImagesUpdatesThreads([]);

      expect(result).toBe(true);
    });
  });

  describe('checkDockerImageUpdate', () => {
    it('should be defined', () => {
      expect(service.checkDockerImageUpdate).toBeDefined();
    });

    it('should call getLocalImageDigest', async () => {
      jest.spyOn(service, 'getLocalImageDigest').mockResolvedValue(null);
      jest.spyOn(service, 'getRemoteImageDigests').mockResolvedValue([]);

      await service.checkDockerImageUpdate('image');

      expect(service.getLocalImageDigest).toHaveBeenCalled();
    });

    it('should call getRemoteImageDigests', async () => {
      jest.spyOn(service, 'getLocalImageDigest').mockResolvedValue(null);
      jest.spyOn(service, 'getRemoteImageDigests').mockResolvedValue([]);

      await service.checkDockerImageUpdate('image');

      expect(service.getRemoteImageDigests).toHaveBeenCalled();
    });

    it('should return "error" if getLocalImageDigest fails', async () => {
      jest.spyOn(service, 'getLocalImageDigest').mockRejectedValue(undefined);
      jest.spyOn(service, 'getRemoteImageDigests').mockResolvedValue([]);

      const result = await service.checkDockerImageUpdate('image');

      expect(result).toBe('error');
    });

    it('should return "error" if getRemoteImageDigests fails', async () => {
      jest.spyOn(service, 'getLocalImageDigest').mockResolvedValue(null);
      jest.spyOn(service, 'getRemoteImageDigests').mockRejectedValue(undefined);

      const result = await service.checkDockerImageUpdate('image');

      expect(result).toBe('error');
    });

    it('should return false if no local digest found', async () => {
      jest.spyOn(service, 'getLocalImageDigest').mockResolvedValue(null);
      jest.spyOn(service, 'getRemoteImageDigests').mockResolvedValue(['00000']);

      const result = await service.checkDockerImageUpdate('image');

      expect(result).toBe(false);
    });

    it('should return false if no remote digests found', async () => {
      jest.spyOn(service, 'getLocalImageDigest').mockResolvedValue('00000');
      jest.spyOn(service, 'getRemoteImageDigests').mockResolvedValue([]);

      const result = await service.checkDockerImageUpdate('image');

      expect(result).toBe(false);
    });

    it('should return false if a remote digest matches local digest', async () => {
      jest.spyOn(service, 'getLocalImageDigest').mockResolvedValue('00000');
      jest
        .spyOn(service, 'getRemoteImageDigests')
        .mockResolvedValue(['00000', '11111']);

      const result = await service.checkDockerImageUpdate('image');

      expect(result).toBe(false);
    });

    it('should return true if no remote digest matches local digest', async () => {
      jest.spyOn(service, 'getLocalImageDigest').mockResolvedValue('00000');
      jest
        .spyOn(service, 'getRemoteImageDigests')
        .mockResolvedValue(['11111', '22222']);

      const result = await service.checkDockerImageUpdate('image');

      expect(result).toBe(true);
    });
  });

  describe('getLocalImageDigest', () => {
    const mockDocker: Docker = {
      inspect: jest.fn().mockResolvedValue({ raw: undefined }),
    } as unknown as Docker;

    it('should be defined', () => {
      expect(service.getLocalImageDigest).toBeDefined();
    });

    it('should return null if no raw result from Docker command', async () => {
      jest.spyOn(mockDocker, 'inspect').mockResolvedValue({ raw: undefined });
      const result = await service.getLocalImageDigest(mockDocker, 'image');
      expect(result).toBe(null);
    });

    it('should return null if parsedResult[0].Id is undefined', async () => {
      jest.spyOn(mockDocker, 'inspect').mockResolvedValue({ raw: '[]' });
      const result = await service.getLocalImageDigest(mockDocker, 'image');
      expect(result).toBe(null);
    });

    it('should return parsedResult[0].Id', async () => {
      jest
        .spyOn(mockDocker, 'inspect')
        .mockResolvedValue({ raw: '[{"Id":"123"}]' });
      const result = await service.getLocalImageDigest(mockDocker, 'image');
      expect(result).toBe('123');
    });
  });

  describe('getRemoteImageDigests', () => {
    const mockDocker: Docker = {
      manifest: jest.fn().mockResolvedValue({ raw: undefined }),
    } as unknown as Docker;

    it('should be defined', () => {
      expect(service.getRemoteImageDigests).toBeDefined();
    });

    it('should return empty array if no raw result from Docker command', async () => {
      jest.spyOn(mockDocker, 'manifest').mockResolvedValue({ raw: undefined });
      const result = await service.getRemoteImageDigests(mockDocker, 'image');
      expect(Array.isArray(result)).toBe(true);
      expect(result.length).toBe(0);
    });

    it('should return each digest found', async () => {
      jest.spyOn(mockDocker, 'manifest').mockResolvedValue({
        raw: '{"manifests":[{"digest":"123"},{"digest":"456"}]}',
      });
      const result = await service.getRemoteImageDigests(mockDocker, 'image');
      expect(Array.isArray(result)).toBe(true);
      expect(result.length).toBe(2);
    });
  });
});
