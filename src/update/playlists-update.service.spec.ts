import { Test, TestingModule } from '@nestjs/testing';
import { SettingsService } from 'src/settings/settings.service';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { CategoriesService } from 'src/categories/categories.service';
import { settingsServiceMock } from 'src/tests/mocks/providers/settings-service.mock';
import { playlistsServiceMock } from 'src/tests/mocks/providers/playlists-service.mock';
import { categoriesServiceMock } from 'src/tests/mocks/providers/categories-service.mock';
import { PlaylistsUpdateService } from './playlists-update.service';
import { MaestroApiService } from 'src/maestro/maestro-api.service';
import { maestroApiServiceMock } from 'src/tests/mocks/providers/maestro-api-service.mock';
import * as cachingUtil from 'src/utils/caching.util';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { Item } from 'src/items/entities/item.entity';
import { Category } from 'src/categories/entities/category.entity';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import { playlistMock } from 'src/tests/mocks/objects/playlist.mock';
import { ContentService } from 'src/maestro/content.service';
import { contentServiceMock } from 'src/tests/mocks/providers/content-service.mock';
import { DownloadService } from 'src/maestro/download.service';
import { downloadServiceMock } from 'src/tests/mocks/providers/download-service.mock';
import { categoryPlaylistMock } from 'src/tests/mocks/objects/category-playlist.mock';
import { categoryMock } from 'src/tests/mocks/objects/category.mock';

describe('PlaylistsUpdateService', () => {
  let service: PlaylistsUpdateService;
  let maestroApiService: MaestroApiService;
  let settingsService: SettingsService;
  let playlistsService: PlaylistsService;
  let categoriesService: CategoriesService;
  let contentService: ContentService;
  let downloadService: DownloadService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PlaylistsUpdateService,
        MaestroApiService,
        SettingsService,
        PlaylistsService,
        CategoriesService,
        ContentService,
        DownloadService,
      ],
    })
      .overrideProvider(MaestroApiService)
      .useValue(maestroApiServiceMock)
      .overrideProvider(SettingsService)
      .useValue(settingsServiceMock)
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(ContentService)
      .useValue(contentServiceMock)
      .overrideProvider(DownloadService)
      .useValue(downloadServiceMock)
      .compile();

    service = module.get<PlaylistsUpdateService>(PlaylistsUpdateService);
    maestroApiService = module.get<MaestroApiService>(MaestroApiService);
    settingsService = module.get<SettingsService>(SettingsService);
    playlistsService = module.get<PlaylistsService>(PlaylistsService);
    categoriesService = module.get<CategoriesService>(CategoriesService);
    contentService = module.get<ContentService>(ContentService);
    downloadService = module.get<DownloadService>(DownloadService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('check', () => {
    it('should be defined', () => {
      expect(service.check).toBeDefined();
    });

    it('should return if Maestro API call error', async () => {
      jest.spyOn(maestroApiService, 'getUpdates').mockResolvedValue(undefined);
      jest.spyOn(service, 'checkPlaylistUpdate');

      await service.check();

      expect(service.checkPlaylistUpdate).not.toHaveBeenCalled();
    });

    it('should not use checkPlaylistUpdate method if not update needed', async () => {
      jest
        .spyOn(maestroApiService, 'getUpdates')
        .mockResolvedValue({ playlistIds: [] });
      jest.spyOn(service, 'checkPlaylistUpdate');

      await service.check();

      expect(service.checkPlaylistUpdate).not.toHaveBeenCalled();
    });

    it('should not invalidate cache if not update needed', async () => {
      jest
        .spyOn(maestroApiService, 'getUpdates')
        .mockResolvedValue({ playlistIds: [] });
      jest.spyOn(cachingUtil, 'invalidateCache');

      await service.check();

      expect(cachingUtil.invalidateCache).not.toHaveBeenCalled();
    });

    it('should update LAST_PLAYLISTS_UPDATE setting', async () => {
      const fakeDate = new Date();
      const fakeTimestamp = fakeDate.getTime();
      jest.useFakeTimers().setSystemTime(fakeDate);

      jest
        .spyOn(maestroApiService, 'getUpdates')
        .mockResolvedValue({ playlistIds: [] });
      jest.spyOn(settingsService, 'update');

      await service.check();

      expect(settingsService.update).toHaveBeenCalledWith(
        SettingKey.LAST_PLAYLISTS_UPDATE,
        { value: fakeTimestamp.toString() },
      );
    });
  });

  describe('checkPlaylistUpdate', () => {
    const mockPlaylist = playlistMock({
      isUpdateNeeded: false,
      categoryPlaylists: [
        categoryPlaylistMock({
          category: categoryMock({ isUpdateNeeded: false }),
        }),
        categoryPlaylistMock({
          category: categoryMock({ isUpdateNeeded: false }),
        }),
      ],
    });

    it('should be defined', () => {
      expect(service.checkPlaylistUpdate).toBeDefined();
    });

    it('should return if playlist not found', async () => {
      jest.spyOn(playlistsService, 'findOne').mockResolvedValue(null);
      jest.spyOn(playlistsService, 'save');

      await service.checkPlaylistUpdate(mockPlaylist.id);

      expect(playlistsService.save).not.toHaveBeenCalled();
    });

    it('should get playlist with its relations', async () => {
      jest.spyOn(playlistsService, 'findOne').mockResolvedValue(mockPlaylist);

      await service.checkPlaylistUpdate(mockPlaylist.id);

      expect(playlistsService.findOne).toHaveBeenCalledWith({
        where: { id: mockPlaylist.id },
        relations: { categoryPlaylists: { category: true } },
      });
    });

    it('should set playlist.isUpdateNeeded to true and save it', async () => {
      jest.spyOn(playlistsService, 'findOne').mockResolvedValue(mockPlaylist);
      jest.spyOn(playlistsService, 'save');

      await service.checkPlaylistUpdate(mockPlaylist.id);

      expect(mockPlaylist.isUpdateNeeded).toBe(true);
      expect(playlistsService.save).toHaveBeenCalledWith(mockPlaylist);
    });

    it('should set playlist.categoryPlaylists.category.isUpdateNeeded to true and save them', async () => {
      jest.spyOn(playlistsService, 'findOne').mockResolvedValue(mockPlaylist);
      jest.spyOn(categoriesService, 'save');

      await service.checkPlaylistUpdate(mockPlaylist.id);

      expect(categoriesService.save).toHaveBeenCalledTimes(
        mockPlaylist.categoryPlaylists.length,
      );

      mockPlaylist.categoryPlaylists.forEach((categoryPlaylist) => {
        expect(categoryPlaylist.category.isUpdateNeeded).toBe(true);
        expect(categoriesService.save).toHaveBeenCalledWith(
          categoryPlaylist.category,
        );
      });
    });
  });

  describe('updatePlaylists', () => {
    const playlistIds = ['1', '2'];
    it('should be defined', () => {
      expect(service.updatePlaylists).toBeDefined();
    });

    it('should call installOrUpdatePlaylist for each playlist', async () => {
      await service.updatePlaylists(playlistIds);

      expect(contentService.installOrUpdatePlaylist).toHaveBeenCalledTimes(
        playlistIds.length,
      );

      playlistIds.forEach((playlistId) => {
        expect(contentService.installOrUpdatePlaylist).toHaveBeenCalledWith(
          playlistId,
        );
      });
    });

    it('should run downloads if any installOrUpdatePlaylist returns true', async () => {
      jest
        .spyOn(contentService, 'installOrUpdatePlaylist')
        .mockResolvedValue(true);

      await service.updatePlaylists(playlistIds);

      expect(downloadService.runQueue).toHaveBeenCalled();
    });

    it('should invalidate cache', async () => {
      await service.updatePlaylists(playlistIds);

      expect(cachingUtil.invalidateCache).toHaveBeenCalledWith([
        Playlist,
        CategoryPlaylist,
        Item,
        CategoryPlaylistItem,
        Category,
      ]);
    });
  });
});
