import { forwardRef, Module } from '@nestjs/common';
import { UpdateService } from './update.service';
import { HardwareModule } from 'src/hardware/hardware.module';
import { SettingsModule } from 'src/settings/settings.module';
import { ApplicationsModule } from 'src/applications/applications.module';
import { MaestroModule } from 'src/maestro/maestro.module';
import { PlaylistsModule } from 'src/playlists/playlists.module';
import { CategoriesModule } from 'src/categories/categories.module';
import { ApplicationsUpdateService } from './applications-update.service';
import { DockerUpdateService } from './docker-update.service';
import { PlaylistsUpdateService } from './playlists-update.service';
import { UpdateController } from './update.controller';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [
    forwardRef(() => HardwareModule),
    SettingsModule,
    forwardRef(() => ApplicationsModule),
    forwardRef(() => MaestroModule),
    forwardRef(() => PlaylistsModule),
    forwardRef(() => CategoriesModule),
    JwtModule,
    UsersModule,
  ],
  providers: [
    UpdateService,
    ApplicationsUpdateService,
    DockerUpdateService,
    PlaylistsUpdateService,
  ],
  controllers: [UpdateController],
  exports: [UpdateService, PlaylistsUpdateService],
})
export class UpdateModule {}
