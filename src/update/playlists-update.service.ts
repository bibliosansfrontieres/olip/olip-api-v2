import { forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import { SettingsService } from 'src/settings/settings.service';
import { MaestroApiService } from 'src/maestro/maestro-api.service';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { invalidateCache } from 'src/utils/caching.util';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { Item } from 'src/items/entities/item.entity';
import { Category } from 'src/categories/entities/category.entity';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { CategoriesService } from 'src/categories/categories.service';
import { ContentService } from 'src/maestro/content.service';
import { DownloadService } from 'src/maestro/download.service';

@Injectable()
export class PlaylistsUpdateService {
  constructor(
    private settingsService: SettingsService,
    private maestroApiService: MaestroApiService,
    @Inject(forwardRef(() => PlaylistsService))
    private playlistsService: PlaylistsService,
    @Inject(forwardRef(() => CategoriesService))
    private categoriesService: CategoriesService,
    private contentService: ContentService,
    private downloadService: DownloadService,
  ) {}

  async check() {
    Logger.log('Checking playlists updates...', 'UpdateService');
    const updates = await this.maestroApiService.getUpdates();
    if (!updates) return;

    for (const playlistId of updates.playlistIds) {
      await this.checkPlaylistUpdate(playlistId);
    }

    if (updates.playlistIds.length > 0) {
      await invalidateCache([
        Playlist,
        Item,
        Category,
        CategoryPlaylist,
        CategoryPlaylistItem,
      ]);
    }

    await this.settingsService.update(SettingKey.LAST_PLAYLISTS_UPDATE, {
      value: Date.now().toString(),
    });

    Logger.log('Playlists updates checked !', 'UpdateService');
  }

  async checkPlaylistUpdate(playlistId: string) {
    const playlist = await this.playlistsService.findOne({
      where: { id: playlistId },
      relations: { categoryPlaylists: { category: true } },
    });
    if (!playlist) return;

    playlist.isUpdateNeeded = true;
    await this.playlistsService.save(playlist);

    for (const categoryPlaylist of playlist.categoryPlaylists) {
      categoryPlaylist.category.isUpdateNeeded = true;
      await this.categoriesService.save(categoryPlaylist.category);
    }
  }

  async updatePlaylists(ids: string[]) {
    let runDownloads = false;
    for (const id of ids) {
      const result = await this.contentService.installOrUpdatePlaylist(id);
      if (result) {
        runDownloads = true;
      }
    }
    if (runDownloads) {
      await this.downloadService.runQueue();
    }
    await invalidateCache([
      Playlist,
      CategoryPlaylist,
      Item,
      CategoryPlaylistItem,
      Category,
    ]);
  }
}
