import { Module, MiddlewareConsumer, Global } from '@nestjs/common';
import { AppController } from './app.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { configurationSchema } from './config/configuration';
import { CacheModule } from '@nestjs/cache-manager';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { LanguagesModule } from './languages/languages.module';
import { HighlightsModule } from './highlights/highlights.module';
import { EditosModule } from './editos/editos.module';
import { ApplicationsModule } from './applications/applications.module';
import { ItemsModule } from './items/items.module';
import { FilesModule } from './files/files.module';
import { PlaylistsModule } from './playlists/playlists.module';
import { CategoriesModule } from './categories/categories.module';
import { SettingsModule } from './settings/settings.module';
import { Item } from './items/entities/item.entity';
import { File } from './files/entities/file.entity';
import { Playlist } from './playlists/entities/playlist.entity';
import { PlaylistTranslation } from './playlists/entities/playlist-translation.entity';
import { Category } from './categories/entities/category.entity';
import { CategoryTranslation } from './categories/entities/category-translation.entity';
import { Application } from './applications/entities/application.entity';
import { ApplicationTranslation } from './applications/entities/application-translation.entity';
import { Edito } from './editos/entities/edito.entity';
import { EditoTranslation } from './editos/entities/edito-translation.entity';
import { User } from './users/entities/user.entity';
import { Setting } from './settings/entities/setting.entity';
import { Language } from './languages/entities/language.entity';
import { Highlight } from './highlights/entities/highlight.entity';
import { HighlightTranslation } from './highlights/entities/highlight-translation.entity';
import { PermissionsModule } from './permissions/permissions.module';
import { RolesModule } from './roles/roles.module';
import { Role } from './roles/entities/role.entity';
import { RoleTranslation } from './roles/entities/role-translation.entity';
import { Permission } from './permissions/entities/permission.entity';
import { PermissionTranslation } from './permissions/entities/permission-translation.entity';
import { InitDatabaseModule } from './init-database/init-database.module';
import { AuthModule } from './auth/auth.module';
import { CategoryPlaylistsModule } from './category-playlists/category-playlists.module';
import { CategoryPlaylistItemsModule } from './category-playlist-items/category-playlist-items.module';
import { CategoryPlaylist } from './category-playlists/entities/category-playlist.entity';
import { CategoryPlaylistItem } from './category-playlist-items/entities/category-playlist-item.entity';
import { ApplicationTypesModule } from './application-types/application-types.module';
import { ApplicationTypeTranslation } from './application-types/entities/application-type-translation.entity';
import { ApplicationType } from './application-types/entities/application-type.entity';
import { DublinCoreAudiencesModule } from './dublin-core-audiences/dublin-core-audiences.module';
import { DublinCoreAudience } from './dublin-core-audiences/entities/dublin-core-audience.entity';
import { DublinCoreAudienceTranslation } from './dublin-core-audiences/entities/dublin-core-audience-translation.entity';
import { DublinCoreLicensesModule } from './dublin-core-licenses/dublin-core-licenses.module';
import { DublinCoreLicense } from './dublin-core-licenses/entities/dublin-core-license.entity';
import { DublinCoreLicenseTranslation } from './dublin-core-licenses/entities/dublin-core-license-translation.entity';
import { DublinCoreLanguagesModule } from './dublin-core-languages/dublin-core-languages.module';
import { DublinCoreLanguage } from './dublin-core-languages/entities/dublin-core-language.entity';
import { DublinCoreTypesModule } from './dublin-core-types/dublin-core-types.module';
import { DublinCoreType } from './dublin-core-types/entities/dublin-core-type.entity';
import { DublinCoreTypeTranslation } from './dublin-core-types/entities/dublin-core-type-translation.entity';
import { DublinCoreItem } from './dublin-core-items/entities/dublin-core-item.entity';
import { DublinCoreItemTranslation } from './dublin-core-items/entities/dublin-core-item-translation';
import { DublinCoreItemsModule } from './dublin-core-items/dublin-core-items.module';
import { ItemLanguageLevelsModule } from './item-language-levels/item-language-levels.module';
import { ItemLanguageLevel } from './item-language-levels/entities/item-language-level.entity';
import { ItemLanguageLevelTranslation } from './item-language-levels/entities/item-language-level-translation.entity';
import { ItemDocumentTypesModule } from './item-document-types/item-document-types.module';
import { ItemDocumentType } from './item-document-types/entities/item-document-type.entity';
import { ItemDocumentTypeTranslation } from './item-document-types/entities/item-document-type-translation.entity';
import { EditoParagraphsModule } from './edito-paragraphs/edito-paragraphs.module';
import { EditoParagraph } from './edito-paragraphs/entities/edito-paragraph.entity';
import { EditoParagraphTranslation } from './edito-paragraphs/entities/edito-paragraph-translation.entity';
import { HighlightContentsModule } from './highlight-contents/highlight-contents.module';
import { HighlightContent } from './highlight-contents/entities/highlight-content.entity';
import { AppService } from './app.service';
import ms, { StringValue } from 'ms';
import { UploadModule } from './upload/upload.module';
import { VisibilitiesModule } from './visibilities/visibilities.module';
import { Visibility } from './visibilities/entities/visibility.entity';
import { HardwareModule } from './hardware/hardware.module';
import { JwtModule } from '@nestjs/jwt';
import { ConnectedUser } from './stats/entities/connected-user.entity';
import { SocketModule } from './socket/socket.module';
import { StatsModule } from './stats/stats.module';
import { MaestroModule } from './maestro/maestro.module';
import { DockerModule } from './docker/docker.module';
import { QueuedFile } from './maestro/entities/queued-file.entity';
import { SearchModule } from './search/search.module';
import { FilesMiddleware } from './files.middleware';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { UpdateModule } from './update/update.module';

@Global()
@Module({
  imports: [
    EventEmitterModule.forRoot(),
    ConfigModule.forRoot({
      validationSchema: configurationSchema,
      isGlobal: true,
    }),
    CacheModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        ttl: ms(configService.get<StringValue>('GLOBAL_CACHE_EXPIRES_IN')),
        isGlobal: true,
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'data/databases/olip-api-v2.db',
      entities: [
        DublinCoreType,
        DublinCoreTypeTranslation,
        DublinCoreAudience,
        DublinCoreAudienceTranslation,
        DublinCoreLicense,
        DublinCoreLicenseTranslation,
        DublinCoreLanguage,
        DublinCoreItem,
        DublinCoreItemTranslation,
        Item,
        ItemDocumentType,
        ItemDocumentTypeTranslation,
        ItemLanguageLevel,
        ItemLanguageLevelTranslation,
        File,
        Playlist,
        PlaylistTranslation,
        Category,
        CategoryTranslation,
        CategoryPlaylist,
        CategoryPlaylistItem,
        Application,
        ApplicationTranslation,
        ApplicationType,
        ApplicationTypeTranslation,
        Edito,
        EditoTranslation,
        EditoParagraph,
        EditoParagraphTranslation,
        User,
        Role,
        RoleTranslation,
        Permission,
        PermissionTranslation,
        Highlight,
        HighlightTranslation,
        HighlightContent,
        Setting,
        Language,
        Visibility,
        ConnectedUser,
        QueuedFile,
      ],
      synchronize: true,
    }),
    UsersModule,
    LanguagesModule,
    HighlightsModule,
    EditosModule,
    ApplicationsModule,
    ItemsModule,
    FilesModule,
    PlaylistsModule,
    CategoriesModule,
    SettingsModule,
    PermissionsModule,
    RolesModule,
    DublinCoreItemsModule,
    InitDatabaseModule,
    AuthModule,
    CategoryPlaylistsModule,
    CategoryPlaylistItemsModule,
    ApplicationTypesModule,
    DublinCoreAudiencesModule,
    DublinCoreLicensesModule,
    DublinCoreLanguagesModule,
    DublinCoreTypesModule,
    ItemLanguageLevelsModule,
    ItemDocumentTypesModule,
    EditoParagraphsModule,
    HighlightContentsModule,
    UploadModule,
    VisibilitiesModule,
    HardwareModule,
    JwtModule,
    SocketModule,
    StatsModule,
    MaestroModule,
    DockerModule,
    SearchModule,
    UpdateModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [CacheModule],
})
export class AppModule {
  // unused-class-members-ignore-next
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(FilesMiddleware).exclude('api/(.*)').forRoutes('/*');
  }
}
