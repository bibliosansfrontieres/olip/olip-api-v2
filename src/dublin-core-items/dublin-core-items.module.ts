import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LanguagesModule } from 'src/languages/languages.module';
import { DublinCoreItem } from './entities/dublin-core-item.entity';
import { DublinCoreItemTranslation } from './entities/dublin-core-item-translation';
import { DublinCoreItemsController } from './dublin-core-items.controller';
import { DublinCoreItemsService } from './dublin-core-items.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([DublinCoreItem, DublinCoreItemTranslation]),
    LanguagesModule,
  ],
  controllers: [DublinCoreItemsController],
  providers: [DublinCoreItemsService],
  exports: [DublinCoreItemsService],
})
export class DublinCoreItemsModule {}
