import {
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Item } from 'src/items/entities/item.entity';
import { DublinCoreAudience } from 'src/dublin-core-audiences/entities/dublin-core-audience.entity';
import { DublinCoreLicense } from 'src/dublin-core-licenses/entities/dublin-core-license.entity';
import { DublinCoreLanguage } from 'src/dublin-core-languages/entities/dublin-core-language.entity';
import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';
import { DublinCoreItemTranslation } from './dublin-core-item-translation';

interface IOptionalFields {
  dublinCoreLicenses?: DublinCoreLicense[];
  dublinCoreLanguages?: DublinCoreLanguage[];
}

@Entity()
export class DublinCoreItem {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany(
    () => DublinCoreLanguage,
    (dublinCoreLanguage) => dublinCoreLanguage.dublinCoreItem,
  )
  dublinCoreLanguages: DublinCoreLanguage[];

  @ManyToMany(
    () => DublinCoreLicense,
    (dublinCoreLicense) => dublinCoreLicense.dublinCoreItems,
  )
  @JoinTable()
  dublinCoreLicenses: DublinCoreLicense[];

  @ManyToMany(
    () => DublinCoreAudience,
    (dublinCoreAudience) => dublinCoreAudience.dublinCoreItems,
  )
  @JoinTable()
  dublinCoreAudiences: DublinCoreAudience[];

  @ManyToOne(
    () => DublinCoreType,
    (dublinCoreType) => dublinCoreType.dublinCoreItems,
  )
  dublinCoreType: DublinCoreType;

  @OneToMany(
    () => DublinCoreItemTranslation,
    (dublinCoreItemTranslation) => dublinCoreItemTranslation.dublinCoreItem,
    { cascade: true },
  )
  dublinCoreItemTranslations: DublinCoreItemTranslation[];

  @OneToOne(() => Item, (item) => item.dublinCoreItem, { onDelete: 'CASCADE' })
  @JoinColumn()
  item: Item;

  constructor(
    item: Item,
    dublinCoreType: DublinCoreType | null,
    optionalFields?: IOptionalFields,
  ) {
    this.item = item;
    if (dublinCoreType) {
      this.dublinCoreType = dublinCoreType;
    }
    if (optionalFields) {
      this.dublinCoreLicenses = optionalFields.dublinCoreLicenses || [];
      this.dublinCoreLanguages = optionalFields.dublinCoreLanguages || [];
    }
  }
}
