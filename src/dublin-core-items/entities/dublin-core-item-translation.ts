import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Language } from 'src/languages/entities/language.entity';
import { DublinCoreItem } from './dublin-core-item.entity';
import { IMaestroDublinCoreItemTranslation } from 'src/maestro/interfaces/maestro-dublin-core-item-translation.interface';

@Entity()
export class DublinCoreItemTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  creator: string;

  @Column()
  publisher: string;

  @Column()
  source: string;

  @Column()
  extent: string;

  @Column()
  subject: string;

  @Column()
  dateCreated: string;

  @Column({ nullable: true })
  indexId?: string;

  @ManyToOne(
    () => DublinCoreItem,
    (dublinCoreItem) => dublinCoreItem.dublinCoreItemTranslations,
    { onDelete: 'CASCADE' },
  )
  dublinCoreItem: DublinCoreItem;

  @ManyToOne(() => Language, (language) => language.dublinCoreItemTranslations)
  language: Language;

  constructor(
    maestroDublinCoreItemTranslation: IMaestroDublinCoreItemTranslation,
    language: Language,
  ) {
    if (!maestroDublinCoreItemTranslation) return;
    this.title = maestroDublinCoreItemTranslation.title;
    this.description = maestroDublinCoreItemTranslation.description;
    this.creator = maestroDublinCoreItemTranslation.creator;
    this.publisher = maestroDublinCoreItemTranslation.publisher;
    this.source = maestroDublinCoreItemTranslation.source;
    this.extent = maestroDublinCoreItemTranslation.extent;
    this.subject = maestroDublinCoreItemTranslation.subject;
    this.dateCreated = maestroDublinCoreItemTranslation.dateCreated;
    this.language = language;
  }
}
