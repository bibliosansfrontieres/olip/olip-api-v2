import { Test, TestingModule } from '@nestjs/testing';
import { LanguagesService } from 'src/languages/languages.service';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';
import { DublinCoreItemsService } from './dublin-core-items.service';
import { DublinCoreItem } from './entities/dublin-core-item.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DublinCoreItemTranslation } from './entities/dublin-core-item-translation';

describe('DublinCoreItemsService', () => {
  let service: DublinCoreItemsService;
  let dublinCoreItemRepository: Repository<DublinCoreItem>;
  let dublinCoreItemTranslationRepository: Repository<DublinCoreItemTranslation>;

  const DUBLIN_CORE_ITEM_REPOSITORY_TOKEN = getRepositoryToken(DublinCoreItem);
  const DUBLIN_CORE_ITEM_TRANSLATION_REPOSITORY_TOKEN = getRepositoryToken(
    DublinCoreItemTranslation,
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: DUBLIN_CORE_ITEM_REPOSITORY_TOKEN,
          useClass: Repository<DublinCoreItem>,
        },
        {
          provide: DUBLIN_CORE_ITEM_TRANSLATION_REPOSITORY_TOKEN,
          useClass: Repository<DublinCoreItemTranslation>,
        },
        DublinCoreItemsService,
        LanguagesService,
      ],
    })
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .compile();

    service = module.get<DublinCoreItemsService>(DublinCoreItemsService);
    dublinCoreItemRepository = module.get<Repository<DublinCoreItem>>(
      DUBLIN_CORE_ITEM_REPOSITORY_TOKEN,
    );
    dublinCoreItemTranslationRepository = module.get<
      Repository<DublinCoreItemTranslation>
    >(DUBLIN_CORE_ITEM_TRANSLATION_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
