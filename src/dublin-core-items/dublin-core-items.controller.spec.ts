import { Test, TestingModule } from '@nestjs/testing';
import { DublinCoreItemsController } from './dublin-core-items.controller';
import { DublinCoreItemsService } from './dublin-core-items.service';
import { dublinCoreItemsServiceMock } from 'src/tests/mocks/providers/dublin-core-items-service.mock';

describe('DublinCoreController', () => {
  let controller: DublinCoreItemsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DublinCoreItemsController],
      providers: [DublinCoreItemsService],
    })
      .overrideProvider(DublinCoreItemsService)
      .useValue(dublinCoreItemsServiceMock)
      .compile();

    controller = module.get<DublinCoreItemsController>(
      DublinCoreItemsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
