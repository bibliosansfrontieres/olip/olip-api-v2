import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LanguagesService } from 'src/languages/languages.service';
import { Item } from 'src/items/entities/item.entity';
import { DublinCoreItem } from './entities/dublin-core-item.entity';
import { DublinCoreItemTranslation } from './entities/dublin-core-item-translation';
import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';
import { IMaestroItem } from 'src/maestro/interfaces/maestro-item.interface';
import { IMaestroDublinCoreItemTranslation } from 'src/maestro/interfaces/maestro-dublin-core-item-translation.interface';

@Injectable()
export class DublinCoreItemsService {
  constructor(
    @InjectRepository(DublinCoreItem)
    private dublinCoreItemRepository: Repository<DublinCoreItem>,
    @InjectRepository(DublinCoreItemTranslation)
    private dublinCoreItemTranslationRepository: Repository<DublinCoreItemTranslation>,
    private languagesService: LanguagesService,
  ) {}

  async removeTranslation(
    dublinCoreItemTranslation: DublinCoreItemTranslation,
  ) {
    return this.dublinCoreItemTranslationRepository.remove(
      dublinCoreItemTranslation,
    );
  }

  async createTranslation(
    item: Item,
    maestroDublinCoreItemTranslation: IMaestroDublinCoreItemTranslation,
  ) {
    const language = await this.languagesService.findOneByIdentifier(
      maestroDublinCoreItemTranslation.languageIdentifier,
    );
    let dublinCoreItemTranslation = new DublinCoreItemTranslation(
      maestroDublinCoreItemTranslation,
      language,
    );
    dublinCoreItemTranslation =
      await this.dublinCoreItemTranslationRepository.save(
        dublinCoreItemTranslation,
      );
    item.dublinCoreItem.dublinCoreItemTranslations = [
      ...item.dublinCoreItem.dublinCoreItemTranslations,
      dublinCoreItemTranslation,
    ];
    await this.dublinCoreItemRepository.save(item.dublinCoreItem);
    return dublinCoreItemTranslation;
  }

  async updateTranslation(
    itemTranslation: DublinCoreItemTranslation,
    maestroDublinCoreItemTranslation: IMaestroDublinCoreItemTranslation,
  ) {
    itemTranslation.title = maestroDublinCoreItemTranslation.title;
    itemTranslation.description = maestroDublinCoreItemTranslation.description;
    itemTranslation.creator = maestroDublinCoreItemTranslation.creator;
    itemTranslation.publisher = maestroDublinCoreItemTranslation.publisher;
    itemTranslation.source = maestroDublinCoreItemTranslation.source;
    itemTranslation.extent = maestroDublinCoreItemTranslation.extent;
    itemTranslation.subject = maestroDublinCoreItemTranslation.subject;
    itemTranslation.dateCreated = maestroDublinCoreItemTranslation.dateCreated;
    return this.dublinCoreItemTranslationRepository.save(itemTranslation);
  }

  async create(
    item: Item,
    maestroItem: IMaestroItem,
    dublinCoreType: DublinCoreType | null,
  ) {
    const dublinCoreItemTranslations = [];
    for (const maestroDublinCoreItemTranslation of maestroItem.dublinCoreItemTranslations) {
      const language = await this.languagesService.findOneByIdentifier(
        maestroDublinCoreItemTranslation.languageIdentifier,
      );
      const dublinCoreItemTranslation = new DublinCoreItemTranslation(
        maestroDublinCoreItemTranslation,
        language,
      );
      dublinCoreItemTranslations.push(dublinCoreItemTranslation);
    }
    const dublinCoreItem = new DublinCoreItem(item, dublinCoreType);
    dublinCoreItem.dublinCoreItemTranslations = dublinCoreItemTranslations;
    return this.dublinCoreItemRepository.save(dublinCoreItem);
  }

  async saveTranslation(dublinCoreItemTranslation: DublinCoreItemTranslation) {
    return this.dublinCoreItemTranslationRepository.save(
      dublinCoreItemTranslation,
    );
  }

  async getTranslation(
    dublinCoreItem: DublinCoreItem,
    languageIdentifier: string,
  ) {
    return this.languagesService.getTranslation(
      'dublinCoreItem',
      dublinCoreItem,
      this.dublinCoreItemTranslationRepository,
      languageIdentifier,
    );
  }
}
