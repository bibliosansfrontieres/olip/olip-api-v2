import { Module } from '@nestjs/common';
import { DublinCoreTypesService } from './dublin-core-types.service';
import { DublinCoreTypesController } from './dublin-core-types.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DublinCoreType } from './entities/dublin-core-type.entity';
import { DublinCoreTypeTranslation } from './entities/dublin-core-type-translation.entity';
import { LanguagesModule } from 'src/languages/languages.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([DublinCoreType, DublinCoreTypeTranslation]),
    LanguagesModule,
  ],
  controllers: [DublinCoreTypesController],
  providers: [DublinCoreTypesService],
  exports: [DublinCoreTypesService],
})
export class DublinCoreTypesModule {}
