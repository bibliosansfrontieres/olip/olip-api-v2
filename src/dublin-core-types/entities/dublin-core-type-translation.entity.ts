import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { DublinCoreType } from './dublin-core-type.entity';
import { Language } from 'src/languages/entities/language.entity';
import { CreateDublinCoreTypeTranslationDto } from '../dto/create-dublin-core-type-translation.dto';

@Entity()
export class DublinCoreTypeTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  description: string;

  @ManyToOne(
    () => DublinCoreType,
    (dublinCoreType) => dublinCoreType.dublinCoreTypeTranslations,
  )
  dublinCoreType: DublinCoreType;

  @ManyToOne(() => Language, (language) => language.dublinCoreTypeTranslations)
  language: Language;

  constructor(
    createDublinCoreTypeTranslationDto: CreateDublinCoreTypeTranslationDto,
    dublinCoreType: DublinCoreType,
    language: Language,
  ) {
    if (!createDublinCoreTypeTranslationDto) return;
    this.label = createDublinCoreTypeTranslationDto.label;
    this.description = createDublinCoreTypeTranslationDto.description;
    this.dublinCoreType = dublinCoreType;
    this.language = language;
  }
}
