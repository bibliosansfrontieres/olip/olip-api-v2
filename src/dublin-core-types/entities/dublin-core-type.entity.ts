import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { DublinCoreTypeTranslation } from './dublin-core-type-translation.entity';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';

@Entity()
export class DublinCoreType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  image: string;

  @OneToMany(
    () => DublinCoreItem,
    (dublinCoreItem) => dublinCoreItem.dublinCoreType,
  )
  dublinCoreItems: DublinCoreItem[];

  @OneToMany(
    () => DublinCoreTypeTranslation,
    (dublinCoreTypeTranslation) => dublinCoreTypeTranslation.dublinCoreType,
  )
  dublinCoreTypeTranslations: DublinCoreTypeTranslation[];

  constructor(image: string) {
    if (!image) return;
    this.image = image;
  }
}
