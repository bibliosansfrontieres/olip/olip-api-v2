import { Test, TestingModule } from '@nestjs/testing';
import { DublinCoreTypesService } from './dublin-core-types.service';
import { Repository } from 'typeorm';
import { DublinCoreType } from './entities/dublin-core-type.entity';
import { DublinCoreTypeTranslation } from './entities/dublin-core-type-translation.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LanguagesService } from 'src/languages/languages.service';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';

describe('DublinCoreTypesService', () => {
  let service: DublinCoreTypesService;
  let dublinCoreTypeRepository: Repository<DublinCoreType>;
  let dublinCoreTypeTranslationRepository: Repository<DublinCoreTypeTranslation>;

  const DUBLIN_CORE_TYPE_REPOSITORY_TOKEN = getRepositoryToken(DublinCoreType);
  const DUBLIN_CORE_TYPE_TRANSLATION_REPOSITORY_TOKEN = getRepositoryToken(
    DublinCoreTypeTranslation,
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: DUBLIN_CORE_TYPE_REPOSITORY_TOKEN,
          useClass: Repository<DublinCoreType>,
        },
        {
          provide: DUBLIN_CORE_TYPE_TRANSLATION_REPOSITORY_TOKEN,
          useClass: Repository<DublinCoreTypeTranslation>,
        },
        DublinCoreTypesService,
        LanguagesService,
      ],
    })
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .compile();

    service = module.get<DublinCoreTypesService>(DublinCoreTypesService);
    dublinCoreTypeRepository = module.get<Repository<DublinCoreType>>(
      DUBLIN_CORE_TYPE_REPOSITORY_TOKEN,
    );
    dublinCoreTypeTranslationRepository = module.get<
      Repository<DublinCoreTypeTranslation>
    >(DUBLIN_CORE_TYPE_TRANSLATION_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
