import { IsNumber, IsString } from 'class-validator';

export class CreateDublinCoreTypeTranslationDto {
  @IsString()
  label: string;

  @IsString()
  description: string;

  @IsNumber()
  dublinCoreTypeId: number;

  @IsNumber()
  languageId: number;
}
