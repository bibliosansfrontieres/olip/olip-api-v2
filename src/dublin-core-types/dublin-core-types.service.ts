import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DublinCoreType } from './entities/dublin-core-type.entity';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { CreateDublinCoreTypeTranslationDto } from './dto/create-dublin-core-type-translation.dto';
import { DublinCoreTypeTranslation } from './entities/dublin-core-type-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';

@Injectable()
export class DublinCoreTypesService {
  constructor(
    @InjectRepository(DublinCoreType)
    private dublinCoreTypeRepository: Repository<DublinCoreType>,
    @InjectRepository(DublinCoreTypeTranslation)
    private dublinCoreTypeTranslationRepository: Repository<DublinCoreTypeTranslation>,
    private languagesService: LanguagesService,
  ) {}

  count() {
    return this.dublinCoreTypeRepository.count();
  }

  create(imagePath: string) {
    const dublinCoreType = new DublinCoreType(imagePath);
    return this.dublinCoreTypeRepository.save(dublinCoreType);
  }

  async createTranslation(
    createDublinCoreTypeTranslationDto: CreateDublinCoreTypeTranslationDto,
  ): Promise<DublinCoreTypeTranslation | null> {
    const dublinCoreType = await this.findOne({
      where: { id: createDublinCoreTypeTranslationDto.dublinCoreTypeId },
    });
    if (dublinCoreType === null) return null;

    const language = await this.languagesService.findOne({
      where: { id: createDublinCoreTypeTranslationDto.languageId },
    });
    if (language === null) return null;

    const dublinCoreTypeTranslation = new DublinCoreTypeTranslation(
      createDublinCoreTypeTranslationDto,
      dublinCoreType,
      language,
    );

    return this.dublinCoreTypeTranslationRepository.save(
      dublinCoreTypeTranslation,
    );
  }

  findOne(options?: FindOneOptions<DublinCoreType>) {
    return this.dublinCoreTypeRepository.findOne(options);
  }

  async getAll(languageIdentifier: string) {
    let dublinCoreTypes = await this.dublinCoreTypeRepository.find();
    dublinCoreTypes = await this.getDublinCoreTypesTranslations(
      languageIdentifier,
      dublinCoreTypes,
    );
    return dublinCoreTypes;
  }

  private async getDublinCoreTypesTranslations(
    languageIdentifier: string,
    dublinCoreTypes: DublinCoreType[],
  ) {
    for (const dublinCoreType of dublinCoreTypes) {
      const dublinCoreTypeTranslation = await this.getTranslation(
        dublinCoreType,
        languageIdentifier,
      );
      dublinCoreType.dublinCoreTypeTranslations = [dublinCoreTypeTranslation];
    }
    return dublinCoreTypes;
  }

  async getTranslation(
    dublinCoreType: DublinCoreType,
    languageIdentifier: string,
  ) {
    return this.languagesService.getTranslation(
      'dublinCoreType',
      dublinCoreType,
      this.dublinCoreTypeTranslationRepository,
      languageIdentifier,
    );
  }

  find(options?: FindManyOptions<DublinCoreType>) {
    return this.dublinCoreTypeRepository.find(options);
  }
}
