import { Controller, Get, Query } from '@nestjs/common';
import { DublinCoreTypesService } from './dublin-core-types.service';

@Controller('api/dublin-core-types')
export class DublinCoreTypesController {
  constructor(private dublinCoreTypesService: DublinCoreTypesService) {}

  @Get()
  getDublinCoreTypes(@Query('languageIdentifier') languageIdentifier: string) {
    return this.dublinCoreTypesService.getAll(languageIdentifier);
  }
}
