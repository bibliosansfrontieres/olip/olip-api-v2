import { forwardRef, Module } from '@nestjs/common';
import { DockerService } from './docker.service';
import { ApplicationsModule } from 'src/applications/applications.module';

@Module({
  imports: [forwardRef(() => ApplicationsModule)],
  providers: [DockerService],
  exports: [DockerService],
})
export class DockerModule {}
