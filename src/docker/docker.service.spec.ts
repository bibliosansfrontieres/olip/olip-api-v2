import { Test, TestingModule } from '@nestjs/testing';
import { DockerService } from './docker.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';

describe('DockerService', () => {
  let service: DockerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DockerService, ConfigService, ApplicationsService],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .compile();

    service = module.get<DockerService>(DockerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
