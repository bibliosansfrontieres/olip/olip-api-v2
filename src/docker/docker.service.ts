import { forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { Application } from 'src/applications/entities/application.entity';
import { join } from 'path';
import { existsSync, mkdirSync, readFileSync, rmSync, writeFileSync } from 'fs';
import { writeEnvFile } from 'src/utils/write-env-file.util';
import { IHook, IHookAction } from 'src/maestro/interfaces/hook.interface';
import { HookNamesEnum } from 'src/maestro/enums/hook-names.enum';
import { IUseHookOptions } from './interfaces/use-hook-options.interface';
import { Docker } from 'src/utils/docker.utils';
import { ConfigService } from '@nestjs/config';
import { ApplicationTypeNameEnum } from 'src/maestro/enums/application-type-name.enum';
import * as YAML from 'yaml';
import { replaceAll } from 'src/utils/replace-all.util';
import { ApplicationsService } from 'src/applications/applications.service';

@Injectable()
export class DockerService {
  private docker: Docker;

  constructor(
    @Inject(forwardRef(() => ApplicationsService))
    private applicationsService: ApplicationsService,
    private configService: ConfigService,
  ) {
    this.docker = new Docker();
  }

  async onModuleInit() {
    Logger.log(
      'Login into private bibliosansfrontieres Gitlab registry...',
      'DockerService',
    );
    await this.docker.login(
      'olip-apps-registry',
      this.configService.get('DOCKER_PRIVATE_APPS_REPOSITORY_TOKEN'),
      this.configService.get('DOCKER_PRIVATE_APPS_REGISTRY'),
    );
    Logger.log(
      'Successfully logged in into private bibliosansfrontieres Gitlab registry !',
      'DockerService',
    );
  }

  async runOlipUpgrade() {
    const vmId = this.configService.get('VM_ID') || 'olip';
    const olipUpgradeImage =
      'registry.gitlab.com/bibliosansfrontieres/olip-ng/bsf-core/olip-upgrade:latest';
    await this.docker.pull(olipUpgradeImage);
    return this.docker.run(olipUpgradeImage, {
      name: `${vmId}-olip-upgrade`,
      network: process.env.OLIP_NETWORK,
      volumes: [
        '/var/run/docker.sock:/var/run/docker.sock',
        `${process.env.DEPLOY_REPOSITORY_VOLUME}:/deploy`,
        '/olip-files:/olip-files',
        '/:/host',
      ],
      environment: ['WE_NEED_VERSIONING=true'],
    });
  }

  getApplicationPath(application: Application) {
    return join(__dirname, '../../data/applications', application.name);
  }

  private createApplicationFolder(application: Application) {
    const applicationPath = this.getApplicationPath(application);
    if (!existsSync(applicationPath)) {
      mkdirSync(applicationPath);
    }
    const applicationDataPath = join(applicationPath, 'data');
    if (!existsSync(applicationDataPath)) {
      mkdirSync(applicationDataPath);
    }
    if (
      application.applicationType.name === ApplicationTypeNameEnum.CONTENT_HOST
    ) {
      const applicationContentPath = join(applicationDataPath, 'content');

      if (!existsSync(applicationContentPath)) {
        mkdirSync(applicationContentPath);
      }
    }
  }

  private async configureDatabase(application: Application) {
    const olipHost = await this.configService.get('OLIP_HOST');
    application.url = `${application.name}.${olipHost}`;
    await this.applicationsService.save(application);
  }

  private configureEnvFile(application: Application) {
    const envPath = join(this.getApplicationPath(application), '.env');

    const vmId = process.env.VM_ID || 'olip';

    const envValues = new Map();
    // Docker configuration
    envValues.set('OLIP_APP_CONTAINER_NAME', `${vmId}-${application.name}`);
    envValues.set(
      'OLIP_APP_VOLUME',
      `${process.env.OLIP_VOLUME}/applications/${application.name}`,
    );
    envValues.set('OLIP_APP_NETWORK', process.env.OLIP_NETWORK);
    envValues.set('OLIP_APP_PORT', application.port);
    // TODO generate unique expose ports
    //envValues.set('OLIP_APP_EXPOSE_PORT', '8181');

    // Traefik configuration
    envValues.set(
      'OLIP_APP_HOST',
      `${application.name}.${process.env.OLIP_HOST}`,
    );
    envValues.set('OLIP_APP_APPLICATION_ID', `${vmId}-${application.name}`);
    envValues.set('OLIP_APP_RESOLVER_ID', process.env.OLIP_RESOLVER_ID);
    envValues.set('OLIP_APP_TLS_ENABLED', process.env.TLS_ENABLED);
    envValues.set('OLIP_HOST', process.env.OLIP_HOST || 'ideascube.io');
    envValues.set('VM_ID', vmId);
    envValues.set('TLS_ENABLED', process.env.TLS_ENABLED || 'false');
    envValues.set('HAO_URL', process.env.HAO_URL || 'http://hao');
    envValues.set(
      'HTTP_SCHEME',
      (process.env.TLS_ENABLED || 'false') === 'true' ? 'https' : 'http',
    );

    writeEnvFile(envPath, envValues);
  }

  private writeComposeFiles(application: Application) {
    const applicationPath = this.getApplicationPath(application);

    const stringsToReplace: { old: string; new: string }[] = [];
    const vmId = process.env.VM_ID || 'olip';

    const composePath = join(applicationPath, 'compose.yml');
    const composeContent = application.compose;
    const composeYaml = YAML.parse(composeContent);
    const composeServices = {};
    const mainServices = [];
    const exposeServices = [];
    const serviceServices = [];
    for (let i = 0; i < Object.keys(composeYaml.services).length; i++) {
      const [key, entryValue] = Object.entries(composeYaml.services)[i];
      let value = entryValue;
      if (value === null) {
        value = {};
      }
      const oldKey = key;
      let newKey = key;
      if (key.startsWith('olip-app-main')) {
        newKey = key.replace('olip-app-main', `${vmId}-${application.name}`);
        mainServices.push(newKey);
      } else if (key.startsWith('olip-app-expose')) {
        newKey = key.replace('olip-app-expose', `${vmId}-${application.name}`);
        exposeServices.push(newKey);
      } else if (key.startsWith('olip-app-service')) {
        newKey = key.replace('olip-app-service', `${vmId}-${application.name}`);
        serviceServices.push(newKey);
      }

      if (oldKey !== newKey) {
        stringsToReplace.push({ old: oldKey, new: newKey });
      }

      value['container_name'] = newKey;
      if (value['image']) {
        value['image'] = value['image'].replace(
          '{{APPS_IMAGES_TAG}}',
          this.configService.get('APPS_IMAGES_TAG'),
        );
      }
      composeServices[newKey] = value;
    }

    let finalComposeContent = YAML.stringify({
      ...composeYaml,
      services: composeServices,
    });

    for (const stringToReplace of stringsToReplace) {
      finalComposeContent = replaceAll(
        finalComposeContent,
        stringToReplace.old,
        stringToReplace.new,
      );
    }

    const composeOverridePath = join(applicationPath, 'compose.override.yml');
    const composeOverrideTemplatePath = join(
      __dirname,
      '../../data/templates/applications/compose.override.yml',
    );
    const composeOverrideTemplateContent = readFileSync(
      composeOverrideTemplatePath,
    ).toString();
    const composeOverrideTemplateYaml = YAML.parse(
      composeOverrideTemplateContent,
    );

    const olipAppMainTemplate =
      composeOverrideTemplateYaml.services['olip-app-main'];

    const composeOverrideServices = {};
    for (const mainService of mainServices) {
      composeOverrideServices[mainService] = olipAppMainTemplate;
      if (application.id === 'integration') {
        composeOverrideServices[mainService].build = join(
          __dirname,
          '../../data/integration/application',
        );
      }
    }

    const olipAppExposeTemplate =
      composeOverrideTemplateYaml.services['olip-app-expose'];
    for (const exposeService of exposeServices) {
      const exposeServiceTemplate = { ...olipAppExposeTemplate };
      if (application.exposeServices) {
        const exposeServiceConfig = JSON.parse(application.exposeServices).find(
          (config) =>
            config.name.replace(
              'olip-app-expose',
              `${vmId}-${application.name}`,
            ) === exposeService,
        );
        if (exposeServiceConfig) {
          const applicationId = `${application.name}-${exposeServiceConfig.name.replace(
            'olip-app-expose-',
            '',
          )}`;
          exposeServiceTemplate.labels = exposeServiceTemplate.labels.map(
            (label) => {
              if (label.includes('${OLIP_APP_EXPOSE_APPLICATION_ID}')) {
                label = replaceAll(
                  label,
                  '${OLIP_APP_EXPOSE_APPLICATION_ID}',
                  `${vmId}-${applicationId}`,
                );
              }
              if (label.includes('${OLIP_APP_EXPOSE_HOST}')) {
                label = replaceAll(
                  label,
                  '${OLIP_APP_EXPOSE_HOST}',
                  `${applicationId}.${process.env.OLIP_HOST}`,
                );
              }
              if (label.includes('${OLIP_APP_EXPOSE_PORT}')) {
                // TODO Check if port is filled in config
                label = replaceAll(
                  label,
                  '${OLIP_APP_EXPOSE_PORT}',
                  exposeServiceConfig.port,
                );
              }
              return label;
            },
          );
        }
      }
      composeOverrideServices[exposeService] = exposeServiceTemplate;
    }

    const olipAppServiceTemplate =
      composeOverrideTemplateYaml.services['olip-app-service'];

    for (const serviceService of serviceServices) {
      composeOverrideServices[serviceService] = olipAppServiceTemplate;
    }

    const finalComposeOverrideContent = YAML.stringify({
      ...composeOverrideTemplateYaml,
      services: composeOverrideServices,
    });

    writeFileSync(composePath, finalComposeContent);
    writeFileSync(composeOverridePath, finalComposeOverrideContent);
  }

  private getComposePaths(application: Application) {
    const applicationPath = this.getApplicationPath(application);
    const composePath = join(applicationPath, 'compose.yml');
    const composeOverridePath = join(applicationPath, 'compose.override.yml');
    return { composePath, composeOverridePath };
  }

  private async compose(
    application: Application,
    command: 'up' | 'down' | 'pull' | 'restart' | 'build',
    options?: { forceRecreate?: boolean; detach?: boolean },
  ) {
    const commandOptions = [];
    if (options?.detach !== false && command === 'up') {
      commandOptions.push('-d');
    }
    if (options?.forceRecreate) {
      commandOptions.push('--force-recreate');
    }

    const result = await this.docker.compose(
      this.getComposePaths(application),
      command,
      commandOptions,
    );
    return result;
  }

  private async exec(
    application: Application,
    shellCommand: string,
  ): Promise<string | undefined> {
    const result = await this.docker.exec(
      `exec ${this.configService.get('VM_ID') || 'olip'}-${application.name} sh -c "${shellCommand}"`,
    );
    if (result && result.raw) {
      return result.raw;
    }
    return undefined;
  }

  private replaceHookVariables(value: string, options?: IUseHookOptions) {
    if (options.application) {
      value = value.replace('{{application.url}}', options.application.url);
    }

    if (options.item) {
      if (options.item.file) {
        const splitFileName = options.item.file.name.split('.') || '';
        const extension = splitFileName[splitFileName.length - 1] || '';
        const fileName = options.item.file.name.replace(`.${extension}`, '');
        value = value.replace('{{file.basename}}', fileName);
        value = value.replace('{{file.name}}', options.item.file.name);
      }
    }

    return value;
  }

  async useHook(
    hook: IHook,
    application: Application,
    options?: IUseHookOptions,
  ): Promise<{ result: boolean; value?: string }> {
    if (!hook.actions && !hook.returnValue && !hook.returnExec)
      return { result: true };

    let result = true;

    options = { ...(options || {}), application };

    if (hook.actions) {
      for (const action of hook.actions) {
        const actionResult = await this.useAction(action, application, options);
        if (!actionResult) {
          result = false;
          break;
        }
      }
    }

    let value = undefined;

    if (hook.returnValue) {
      value = this.replaceHookVariables(hook.returnValue, options);
    }

    if (hook.returnExec) {
      const shellCommand = this.replaceHookVariables(hook.returnExec, options);
      value = await this.exec(application, shellCommand);
    }

    return { result, value };
  }

  private async useAction(
    action: IHookAction,
    application: Application,
    options?: IUseHookOptions,
  ) {
    let result = true;
    if (!action.docker && !action.exec) return result;

    if (action.docker) {
      if (action.docker === 'restart') {
        result = await this.restart(application);
      }
      if (action.docker === 'stop') {
        result = await this.stop(application);
      }
      if (action.docker === 'up') {
        result = await this.run(application);
      }
    }
    if (action.exec) {
      const shellCommand = this.replaceHookVariables(action.exec, options);
      try {
        await this.exec(application, shellCommand);
      } catch (e) {
        console.error(e);
        result = false;
      }
    }
    return result;
  }

  async install(application: Application) {
    if (application.isOlip) return true;
    try {
      this.createApplicationFolder(application);
      this.configureEnvFile(application);
      await this.configureDatabase(application);
      this.writeComposeFiles(application);
      if (application.id === 'integration') {
        return this.compose(application, 'build', {
          detach: false,
        });
      } else {
        return this.compose(application, 'pull', {
          detach: false,
        });
      }
    } catch (error) {
      console.error(error);
      return false;
    }
  }

  async run(application: Application) {
    if (application.isOlip) return true;
    this.configureEnvFile(application);
    await this.configureDatabase(application);
    return this.compose(application, 'up', {
      forceRecreate: true,
    });
  }

  async stop(application: Application) {
    if (application.isOlip) return;
    return this.compose(application, 'down');
  }

  private async restart(application: Application) {
    if (application.isOlip) return;
    return this.compose(application, 'restart');
  }

  async uninstall(application: Application) {
    if (application.isOlip) return;
    const applicationPath = this.getApplicationPath(application);
    if (!existsSync(applicationPath)) return;
    rmSync(applicationPath, { force: true, recursive: true });
  }

  async update(application: Application) {
    if (application.isOlip) return;
    await this.compose(application, 'pull');
    await this.compose(application, 'down');
    this.configureEnvFile(application);
    await this.configureDatabase(application);
    this.writeComposeFiles(application);
    return this.compose(application, 'up', { forceRecreate: true });
  }

  private getApplicationHooks(application: Application): IHook[] | undefined {
    const hooks = JSON.parse(application.hooks) || undefined;
    if (!hooks.hooks) return undefined;
    return hooks.hooks;
  }

  getApplicationHook(
    application: Application,
    hookName: HookNamesEnum,
  ): IHook | undefined {
    const hooks = this.getApplicationHooks(application);
    if (!hooks) return undefined;
    return hooks.find((h) => h.name === hookName);
  }
}
