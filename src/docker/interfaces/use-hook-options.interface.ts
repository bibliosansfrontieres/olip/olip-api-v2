import { Application } from 'src/applications/entities/application.entity';
import { Item } from 'src/items/entities/item.entity';

export interface IUseHookOptions {
  item?: Item;
  application?: Application;
}
