import { Module } from '@nestjs/common';
import { DublinCoreAudiencesService } from './dublin-core-audiences.service';
import { DublinCoreAudiencesController } from './dublin-core-audiences.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DublinCoreAudience } from './entities/dublin-core-audience.entity';
import { DublinCoreAudienceTranslation } from './entities/dublin-core-audience-translation.entity';
import { LanguagesModule } from 'src/languages/languages.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      DublinCoreAudience,
      DublinCoreAudienceTranslation,
    ]),
    LanguagesModule,
  ],
  controllers: [DublinCoreAudiencesController],
  providers: [DublinCoreAudiencesService],
  exports: [DublinCoreAudiencesService],
})
export class DublinCoreAudiencesModule {}
