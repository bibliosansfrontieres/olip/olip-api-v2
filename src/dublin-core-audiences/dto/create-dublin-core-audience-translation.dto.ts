import { IsNumber, IsString } from 'class-validator';

export class CreateDublinCoreAudienceTranslationDto {
  @IsString()
  label: string;

  @IsString()
  description: string;

  @IsNumber()
  dublinCoreAudienceId: number;

  @IsNumber()
  languageId: number;
}
