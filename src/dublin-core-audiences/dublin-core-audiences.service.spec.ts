import { Test, TestingModule } from '@nestjs/testing';
import { DublinCoreAudiencesService } from './dublin-core-audiences.service';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';
import { LanguagesService } from 'src/languages/languages.service';
import { Repository } from 'typeorm';
import { DublinCoreAudience } from './entities/dublin-core-audience.entity';
import { DublinCoreAudienceTranslation } from './entities/dublin-core-audience-translation.entity';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('DublinCoreAudiencesService', () => {
  let service: DublinCoreAudiencesService;
  let dublinCoreAudienceRepository: Repository<DublinCoreAudience>;
  let dublinCoreAudienceTranslationRepository: Repository<DublinCoreAudienceTranslation>;

  const DUBLIN_CORE_AUDIENCE_REPOSITORY_TOKEN =
    getRepositoryToken(DublinCoreAudience);
  const DUBLIN_CORE_AUDIENCE_TRANSLATION_REPOSITORY_TOKEN = getRepositoryToken(
    DublinCoreAudienceTranslation,
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: DUBLIN_CORE_AUDIENCE_REPOSITORY_TOKEN,
          useClass: Repository<DublinCoreAudience>,
        },
        {
          provide: DUBLIN_CORE_AUDIENCE_TRANSLATION_REPOSITORY_TOKEN,
          useClass: Repository<DublinCoreAudienceTranslation>,
        },
        DublinCoreAudiencesService,
        LanguagesService,
      ],
    })
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .compile();

    service = module.get<DublinCoreAudiencesService>(
      DublinCoreAudiencesService,
    );

    dublinCoreAudienceRepository = module.get<Repository<DublinCoreAudience>>(
      DUBLIN_CORE_AUDIENCE_REPOSITORY_TOKEN,
    );
    dublinCoreAudienceTranslationRepository = module.get<
      Repository<DublinCoreAudienceTranslation>
    >(DUBLIN_CORE_AUDIENCE_TRANSLATION_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
