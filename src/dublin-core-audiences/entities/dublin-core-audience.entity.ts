import { Entity, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { DublinCoreAudienceTranslation } from './dublin-core-audience-translation.entity';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';

@Entity()
export class DublinCoreAudience {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToMany(
    () => DublinCoreItem,
    (dublinCoreItem) => dublinCoreItem.dublinCoreAudiences,
  )
  dublinCoreItems: DublinCoreItem[];

  @OneToMany(
    () => DublinCoreAudienceTranslation,
    (dublinCoreAudienceTranslation) =>
      dublinCoreAudienceTranslation.dublinCoreAudience,
  )
  dublinCoreAudienceTranslations: DublinCoreAudienceTranslation[];
}
