import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { DublinCoreAudience } from './dublin-core-audience.entity';
import { Language } from 'src/languages/entities/language.entity';
import { CreateDublinCoreAudienceTranslationDto } from '../dto/create-dublin-core-audience-translation.dto';

@Entity()
export class DublinCoreAudienceTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  description: string;

  @ManyToOne(
    () => DublinCoreAudience,
    (dublinCoreAudience) => dublinCoreAudience.dublinCoreAudienceTranslations,
  )
  dublinCoreAudience: DublinCoreAudience;

  @ManyToOne(
    () => Language,
    (language) => language.dublinCoreAudienceTranslations,
  )
  language: Language;

  constructor(
    createDublinCoreAudienceTranslationDto: CreateDublinCoreAudienceTranslationDto,
    dublinCoreAudience: DublinCoreAudience,
    language: Language,
  ) {
    if (!createDublinCoreAudienceTranslationDto) return;
    this.label = createDublinCoreAudienceTranslationDto.label;
    this.description = createDublinCoreAudienceTranslationDto.description;
    this.dublinCoreAudience = dublinCoreAudience;
    this.language = language;
  }
}
