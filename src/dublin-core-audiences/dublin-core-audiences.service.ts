import { Injectable } from '@nestjs/common';
import { FindManyOptions, Repository } from 'typeorm';
import { CreateDublinCoreAudienceTranslationDto } from './dto/create-dublin-core-audience-translation.dto';
import { LanguagesService } from 'src/languages/languages.service';
import { InjectRepository } from '@nestjs/typeorm';
import { DublinCoreAudience } from './entities/dublin-core-audience.entity';
import { DublinCoreAudienceTranslation } from './entities/dublin-core-audience-translation.entity';

@Injectable()
export class DublinCoreAudiencesService {
  constructor(
    @InjectRepository(DublinCoreAudience)
    private dublinCoreAudienceRepository: Repository<DublinCoreAudience>,
    @InjectRepository(DublinCoreAudienceTranslation)
    private dublinCoreAudienceTranslationRepository: Repository<DublinCoreAudienceTranslation>,
    private languagesService: LanguagesService,
  ) {}

  async count() {
    return this.dublinCoreAudienceRepository.count();
  }

  async create() {
    const dublinCoreAudience = new DublinCoreAudience();
    return this.dublinCoreAudienceRepository.save(dublinCoreAudience);
  }

  async createTranslation(
    createDublinCoreAudienceTranslationDto: CreateDublinCoreAudienceTranslationDto,
  ) {
    const dublinCoreAudience = await this.findOne({
      where: {
        id: createDublinCoreAudienceTranslationDto.dublinCoreAudienceId,
      },
    });
    if (dublinCoreAudience === null) return null;

    const language = await this.languagesService.findOne({
      where: { id: createDublinCoreAudienceTranslationDto.languageId },
    });
    if (language === null) return null;

    const dublinCoreAudienceTranslation = new DublinCoreAudienceTranslation(
      createDublinCoreAudienceTranslationDto,
      dublinCoreAudience,
      language,
    );

    return this.dublinCoreAudienceTranslationRepository.save(
      dublinCoreAudienceTranslation,
    );
  }

  private async findOne(options?: FindManyOptions<DublinCoreAudience>) {
    return this.dublinCoreAudienceRepository.findOne(options);
  }
}
