import { Test, TestingModule } from '@nestjs/testing';
import { HardwareController } from './hardware.controller';
import { HardwareService } from './hardware.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { SettingsService } from 'src/settings/settings.service';
import { settingsServiceMock } from 'src/tests/mocks/providers/settings-service.mock';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { eventEmitterMock } from 'src/tests/mocks/providers/event-emitter.mock';
import { SocketGateway } from 'src/socket/socket.gateway';
import { socketGatewayMock } from 'src/tests/mocks/providers/socket-gateway.mock';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { FilesService } from 'src/files/files.service';
import { filesServiceMock } from 'src/tests/mocks/providers/files-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';

describe('HardwareController', () => {
  let controller: HardwareController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HardwareController],
      providers: [
        HardwareService,
        ConfigService,
        SettingsService,
        EventEmitter2,
        SocketGateway,
        JwtService,
        UsersService,
        FilesService,
        ApplicationsService,
      ],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(SettingsService)
      .useValue(settingsServiceMock)
      .overrideProvider(EventEmitter2)
      .useValue(eventEmitterMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(FilesService)
      .useValue(filesServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .compile();

    controller = module.get<HardwareController>(HardwareController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
