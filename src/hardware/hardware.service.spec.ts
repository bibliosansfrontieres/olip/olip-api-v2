import { Test, TestingModule } from '@nestjs/testing';
import { HardwareService } from './hardware.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { SettingsService } from 'src/settings/settings.service';
import { settingsServiceMock } from 'src/tests/mocks/providers/settings-service.mock';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { eventEmitterMock } from 'src/tests/mocks/providers/event-emitter.mock';
import { settingMock } from 'src/tests/mocks/objects/setting.mock';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import { SocketGateway } from 'src/socket/socket.gateway';
import { socketGatewayMock } from 'src/tests/mocks/providers/socket-gateway.mock';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { FilesService } from 'src/files/files.service';
import { ApplicationsService } from 'src/applications/applications.service';
import { filesServiceMock } from 'src/tests/mocks/providers/files-service.mock';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';

describe('HardwareService', () => {
  let service: HardwareService;
  let settingsService: SettingsService;
  let eventEmitter: EventEmitter2;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        HardwareService,
        ConfigService,
        SettingsService,
        EventEmitter2,
        SocketGateway,
        JwtService,
        UsersService,
        FilesService,
        ApplicationsService,
      ],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(SettingsService)
      .useValue(settingsServiceMock)
      .overrideProvider(EventEmitter2)
      .useValue(eventEmitterMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(FilesService)
      .useValue(filesServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .compile();

    service = module.get<HardwareService>(HardwareService);
    settingsService = module.get<SettingsService>(SettingsService);
    eventEmitter = module.get<EventEmitter2>(EventEmitter2);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('checkInternet', () => {
    const lastInternetValueSetting = settingMock({
      key: SettingKey.WAS_INTERNET_CONNECTED,
      value: 'true',
    });

    it('should be defined', () => {
      expect(service.checkInternet).toBeDefined();
    });

    it('should call settingsService.findOne', async () => {
      jest
        .spyOn(settingsService, 'findOne')
        .mockResolvedValue(lastInternetValueSetting);
      jest
        .spyOn(service, 'getInternetConnection')
        .mockResolvedValue({ isConnected: true });

      await service.checkInternet();

      expect(settingsService.findOne).toHaveBeenCalledWith(
        lastInternetValueSetting.key,
      );
    });

    it('should call getInternetConnection', async () => {
      jest
        .spyOn(settingsService, 'findOne')
        .mockResolvedValue(lastInternetValueSetting);
      jest
        .spyOn(service, 'getInternetConnection')
        .mockResolvedValue({ isConnected: true });

      await service.checkInternet();

      expect(service.getInternetConnection).toHaveBeenCalled();
    });

    it('should call manageInternetEvents', async () => {
      jest
        .spyOn(settingsService, 'findOne')
        .mockResolvedValue(lastInternetValueSetting);
      jest
        .spyOn(service, 'getInternetConnection')
        .mockResolvedValue({ isConnected: true });
      jest.spyOn(service, 'manageInternetEvents').mockResolvedValue();

      await service.checkInternet();

      expect(service.manageInternetEvents).toHaveBeenCalledWith(true, true);
    });
  });

  describe('internetValueChanged', () => {
    it('should be defined', () => {
      expect(service.internetValueChanged).toBeDefined();
    });

    it('should return true if internet has reconnected', async () => {
      const result = service.internetValueChanged(false, true);
      expect(result).toBe(true);
    });

    it('should return true if internet has disconnected', async () => {
      const result = service.internetValueChanged(true, false);
      expect(result).toBe(true);
    });

    it('should return false if values did not changed and are true', async () => {
      const result = service.internetValueChanged(true, true);
      expect(result).toBe(false);
    });

    it('should return false if values did not changed and are false', async () => {
      const result = service.internetValueChanged(false, false);
      expect(result).toBe(false);
    });
  });

  describe('manageInternetEvents', () => {
    const mockSetting = settingMock({
      key: SettingKey.WAS_INTERNET_CONNECTED,
      value: 'true',
    });

    it('should be defined', () => {
      expect(service.manageInternetEvents).toBeDefined();
    });

    it('should do nothing if internet value did not changed', async () => {
      await service.manageInternetEvents(true, true);
      expect(eventEmitter.emit).not.toHaveBeenCalled();
    });

    it('should emit `internet.connected` if internet is reconnected', async () => {
      await service.manageInternetEvents(false, true);
      expect(eventEmitter.emit).toHaveBeenCalledWith('internet.connected');
    });

    it('should emit `internet.disconnected` if internet is disconnected', async () => {
      await service.manageInternetEvents(true, false);
      expect(eventEmitter.emit).toHaveBeenCalledWith('internet.disconnected');
    });

    it('should update WAS_INTERNET_CONNECTED to true if internet reconnected', async () => {
      await service.manageInternetEvents(false, true);
      expect(settingsService.update).toHaveBeenCalledWith(mockSetting.key, {
        value: 'true',
      });
    });

    it('should update WAS_INTERNET_CONNECTED to false if internet disconnected', async () => {
      await service.manageInternetEvents(true, false);
      expect(settingsService.update).toHaveBeenCalledWith(mockSetting.key, {
        value: 'false',
      });
    });
  });

  describe('getInternetConnection', () => {
    it('should be defined', () => {
      expect(service.getInternetConnection).toBeDefined();
    });

    it('should call API', async () => {
      jest.spyOn(service, 'api');
      await service.getInternetConnection();
      expect(service.api).toHaveBeenCalledWith('/network/internet-connection');
    });

    it('should return undefined if API call fails', async () => {
      jest.spyOn(service, 'api').mockRejectedValue(undefined);
      const result = await service.getInternetConnection();
      expect(result).toBeUndefined();
    });
  });

  describe('hasInternet', () => {
    it('should be defined', () => {
      expect(service.hasInternet).toBeDefined();
    });

    it('should call getInternetConnection', async () => {
      jest
        .spyOn(service, 'getInternetConnection')
        .mockResolvedValue({ isConnected: true });
      await service.hasInternet();
      expect(service.getInternetConnection).toHaveBeenCalled();
    });

    it('should return isConnected value', async () => {
      jest
        .spyOn(service, 'getInternetConnection')
        .mockResolvedValue({ isConnected: true });
      const result = await service.hasInternet();
      expect(result).toBe(true);
    });
  });
});
