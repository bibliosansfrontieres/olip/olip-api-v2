export interface BatteryIsPluggedIn {
  isPluggedIn: boolean;
}
