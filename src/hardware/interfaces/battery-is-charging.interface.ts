export interface BatteryIsCharging {
  isCharging: boolean;
}
