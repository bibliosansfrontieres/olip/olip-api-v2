export interface BatteryHasBattery {
  hasBattery: boolean;
}
