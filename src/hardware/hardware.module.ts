import { forwardRef, Module } from '@nestjs/common';
import { HardwareService } from './hardware.service';
import { HardwareController } from './hardware.controller';
import { ConfigModule } from '@nestjs/config';
import { SettingsModule } from 'src/settings/settings.module';
import { SocketModule } from 'src/socket/socket.module';
import { FilesModule } from 'src/files/files.module';
import { ApplicationsModule } from 'src/applications/applications.module';

@Module({
  imports: [
    ConfigModule,
    SettingsModule,
    forwardRef(() => SocketModule),
    FilesModule,
    forwardRef(() => ApplicationsModule),
  ],
  controllers: [HardwareController],
  providers: [HardwareService],
  exports: [HardwareService],
})
export class HardwareModule {}
