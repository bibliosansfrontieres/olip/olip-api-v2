import { Controller, Get } from '@nestjs/common';
import { HardwareService } from './hardware.service';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { NetworkHotspotSsid } from './interfaces/network-hotspot-ssid.interface';
import { StoragePathInfos } from './interfaces/storage-path-infos.interface';

@ApiTags('hardware')
@Controller('api/hardware')
export class HardwareController {
  constructor(private readonly hardwareService: HardwareService) {}

  @ApiOperation({ summary: 'Get hotspot SSID' })
  @ApiOkResponse({ type: NetworkHotspotSsid })
  @Get('ssid')
  getSsid() {
    return this.hardwareService.getSsid();
  }

  @ApiOperation({ summary: 'Get storage informations' })
  @ApiOkResponse({ type: StoragePathInfos })
  @Get('storage')
  getStorage() {
    return this.hardwareService.getStorage();
  }
}
