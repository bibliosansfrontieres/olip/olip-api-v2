import { forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios, { AxiosRequestConfig } from 'axios';
import { NetworkInternetConnection } from './interfaces/network-internet-connection.interface';
import ms, { StringValue } from 'ms';
import { SettingsService } from 'src/settings/settings.service';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { SocketGateway } from 'src/socket/socket.gateway';
import { BatteryHasBattery } from './interfaces/battery-has-battery.interface';
import { BatteryPercentage } from './interfaces/battery-percentage.interface';
import { BatteryIsCharging } from './interfaces/battery-is-charging.interface';
import { BatteryIsPluggedIn } from './interfaces/battery-is-plugged-in.interface';
import { isVm } from 'src/utils/is-vm.util';
import { FilesService } from 'src/files/files.service';
import { ApplicationsService } from 'src/applications/applications.service';

@Injectable()
export class HardwareService {
  constructor(
    private configService: ConfigService,
    private settingsService: SettingsService,
    private eventEmitter: EventEmitter2,
    @Inject(forwardRef(() => SocketGateway))
    private socketGateway: SocketGateway,
    private filesService: FilesService,
    @Inject(forwardRef(() => ApplicationsService))
    private applicationsService: ApplicationsService,
  ) {}

  async onApplicationBootstrap() {
    const checkInternetInterval = ms(
      this.configService.get<StringValue>('CHECK_INTERNET_INTERVAL'),
    );
    Logger.log(
      `Registering check internet interval every ${checkInternetInterval}ms`,
      'HardwareService',
    );

    setInterval(this.checkInternet.bind(this), checkInternetInterval);
  }

  async wasInternetConnected(): Promise<boolean> {
    const { value: wasInternetConnectedValue } =
      await this.settingsService.findOne(SettingKey.WAS_INTERNET_CONNECTED);
    return wasInternetConnectedValue === 'true';
  }

  async checkInternet() {
    const wasInternetConnected = await this.wasInternetConnected();
    const isConnected = await this.hasInternet();

    await this.manageInternetEvents(wasInternetConnected, isConnected);
  }

  async manageInternetEvents(
    wasInternetConnected: boolean,
    isConnected: boolean,
  ) {
    if (!this.internetValueChanged(wasInternetConnected, isConnected)) return;
    const internetEvent = `internet.${isConnected ? 'connected' : 'disconnected'}`;
    await this.settingsService.update(SettingKey.WAS_INTERNET_CONNECTED, {
      value: isConnected ? 'true' : 'false',
    });
    Logger.log(
      `Internet value changed, running ${internetEvent} and internet.changed event...`,
      'HardwareService',
    );
    this.eventEmitter.emit(internetEvent);
    this.eventEmitter.emit('internet.changed');
  }

  api(route: string, options?: AxiosRequestConfig) {
    const haoUrl = this.configService.get<string>('HAO_URL');
    return axios.get(`${haoUrl}${route}`, options);
  }

  async getSsid() {
    try {
      const result = await this.api('/network/hotspot-ssid');
      return result.data;
    } catch {
      return undefined;
    }
  }

  async getStorage() {
    if (isVm(this.configService)) {
      const filesSize = await this.filesService.getFilesTotalSize();
      const applicationsSize =
        await this.applicationsService.getApplicationsTotalSize();
      const totalSize = filesSize + applicationsSize;

      return {
        availableBytes: totalSize,
        totalBytes: totalSize,
        paths: [
          { path: '/', availableBytes: totalSize, totalBytes: totalSize },
        ],
      };
    } else {
      try {
        const result = await this.api('/storage');
        return result.data;
      } catch {
        return undefined;
      }
    }
  }

  async getInternetConnection() {
    try {
      const result = await this.api('/network/internet-connection');
      return result.data as NetworkInternetConnection;
    } catch (e) {
      return undefined;
    }
  }

  async hasInternet(): Promise<boolean> {
    const internetConnection = await this.getInternetConnection();
    return internetConnection?.isConnected ?? false;
  }

  internetValueChanged(
    wasInternetConnected: boolean,
    isConnected: boolean,
  ): boolean {
    return wasInternetConnected !== isConnected;
  }

  async getHasBattery() {
    try {
      const result = await this.api('/battery/has-battery');
      return result.data as BatteryHasBattery;
    } catch (e) {
      console.error(`Error getting battery hasBattery: ${e}`);
      return undefined;
    }
  }

  async getPercentage() {
    try {
      const result = await this.api('/battery/percentage');
      return result.data as BatteryPercentage;
    } catch (e) {
      console.error(`Error getting battery percentage: ${e}`);
      return undefined;
    }
  }

  async getIsCharging() {
    try {
      const result = await this.api('/battery/is-charging');
      return result.data as BatteryIsCharging;
    } catch (e) {
      console.error(`Error getting battery isCharging: ${e}`);
      return undefined;
    }
  }

  async getIsPluggedIn() {
    try {
      const result = await this.api('/battery/is-plugged-in');
      return result.data as BatteryIsPluggedIn;
    } catch (e) {
      console.error(`Error getting battery isPluggedIn: ${e}`);
      return undefined;
    }
  }

  async getBatteryInformation() {
    const hasBattery = await this.getHasBattery();
    const percentage = await this.getPercentage();
    const isCharging = await this.getIsCharging();
    const isPluggedIn = await this.getIsPluggedIn();
    const lowBatteryPercentage = this.configService.get<number>(
      'LOW_BATTERY_PERCENTAGE',
    );

    return {
      ...hasBattery,
      ...percentage,
      ...isCharging,
      ...isPluggedIn,
      lowBatteryPercentage,
    };
  }
}
