import { Test, TestingModule } from '@nestjs/testing';
import { ItemLanguageLevelsService } from './item-language-levels.service';

describe('ItemLanguageLevelsService', () => {
  let service: ItemLanguageLevelsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ItemLanguageLevelsService],
    }).compile();

    service = module.get<ItemLanguageLevelsService>(ItemLanguageLevelsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
