import { Item } from 'src/items/entities/item.entity';
import { Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ItemLanguageLevelTranslation } from './item-language-level-translation.entity';

@Entity()
export class ItemLanguageLevel {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany(() => Item, (item) => item.itemLanguageLevel)
  items: Item[];

  @OneToMany(
    () => ItemLanguageLevelTranslation,
    (itemLanguageLevelTranslation) =>
      itemLanguageLevelTranslation.itemLanguageLevel,
  )
  itemLanguageLevelTranslations: ItemLanguageLevelTranslation[];
}
