import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Language } from 'src/languages/entities/language.entity';
import { ItemLanguageLevel } from './item-language-level.entity';

@Entity()
export class ItemLanguageLevelTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  description: string;

  @ManyToOne(
    () => ItemLanguageLevel,
    (itemLanguageLevel) => itemLanguageLevel.itemLanguageLevelTranslations,
  )
  itemLanguageLevel: ItemLanguageLevel;

  @ManyToOne(
    () => Language,
    (language) => language.itemLanguageLevelTranslations,
  )
  language: Language;
}
