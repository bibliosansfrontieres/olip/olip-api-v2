import { Module } from '@nestjs/common';
import { ItemLanguageLevelsService } from './item-language-levels.service';
import { ItemLanguageLevelsController } from './item-language-levels.controller';

@Module({
  controllers: [ItemLanguageLevelsController],
  providers: [ItemLanguageLevelsService],
})
export class ItemLanguageLevelsModule {}
