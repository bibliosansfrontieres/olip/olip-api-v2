import { Test, TestingModule } from '@nestjs/testing';
import { ItemLanguageLevelsController } from './item-language-levels.controller';
import { ItemLanguageLevelsService } from './item-language-levels.service';

describe('ItemLanguageLevelsController', () => {
  let controller: ItemLanguageLevelsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ItemLanguageLevelsController],
      providers: [ItemLanguageLevelsService],
    }).compile();

    controller = module.get<ItemLanguageLevelsController>(
      ItemLanguageLevelsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
