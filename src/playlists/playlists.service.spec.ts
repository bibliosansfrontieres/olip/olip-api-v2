import { Test, TestingModule } from '@nestjs/testing';
import { PlaylistsService } from './playlists.service';
import { Repository } from 'typeorm';
import { Playlist } from './entities/playlist.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { PlaylistTranslation } from './entities/playlist-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';
import { ItemsService } from 'src/items/items.service';
import { itemsServiceMock } from 'src/tests/mocks/providers/items-service.mock';
import { ConfigService } from '@nestjs/config';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { DublinCoreItemsService } from 'src/dublin-core-items/dublin-core-items.service';
import { dublinCoreItemsServiceMock } from 'src/tests/mocks/providers/dublin-core-items-service.mock';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { dublinCoreTypesServiceMock } from 'src/tests/mocks/providers/dublin-core-types-service.mock';
import { MaestroService } from 'src/maestro/maestro.service';
import { maestroServiceMock } from 'src/tests/mocks/providers/maestro-service.mock';
import { UploadService } from 'src/upload/upload.service';
import { uploadServiceMock } from 'src/tests/mocks/providers/upload-service.mock';
import { DownloadService } from 'src/maestro/download.service';
import { downloadServiceMock } from 'src/tests/mocks/providers/download-service.mock';
import { CategoriesService } from 'src/categories/categories.service';
import { categoriesServiceMock } from 'src/tests/mocks/providers/categories-service.mock';
import { CategoryPlaylistsService } from 'src/category-playlists/category-playlists.service';
import { categoryPlaylistsServiceMock } from 'src/tests/mocks/providers/category-playlists-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';
import { MaestroApiService } from 'src/maestro/maestro-api.service';
import { maestroApiServiceMock } from 'src/tests/mocks/providers/maestro-api-service.mock';
import { DockerService } from 'src/docker/docker.service';
import { dockerServiceMock } from 'src/tests/mocks/providers/docker-service.mock';
import { SocketGateway } from 'src/socket/socket.gateway';
import { socketGatewayMock } from 'src/tests/mocks/providers/socket-gateway.mock';
import { CreateService } from 'src/maestro/create.service';
import { createServiceMock } from 'src/tests/mocks/providers/create-service.mock';
import { QueuedFilesService } from 'src/maestro/queued-files.service';
import { queuedFilesServiceMock } from 'src/tests/mocks/providers/queued-files-service.mock';
import { SearchService } from 'src/search/search.service';
import { searchServiceMock } from 'src/tests/mocks/providers/search-service.mock';
import { ContentService } from 'src/maestro/content.service';
import { contentServiceMock } from 'src/tests/mocks/providers/content-service.mock';
import { PlaylistsUpdateService } from 'src/update/playlists-update.service';
import { playlistsUpdateServiceMock } from 'src/tests/mocks/providers/playlists-update-service.mock';
import { playlistMock } from 'src/tests/mocks/objects/playlist.mock';
import { categoryPlaylistMock } from 'src/tests/mocks/objects/category-playlist.mock';
import { categoryMock } from 'src/tests/mocks/objects/category.mock';
import { playlistTranslationMock } from 'src/tests/mocks/objects/playlist-translation.mock';
import { NotFoundException } from '@nestjs/common';
import { itemMock } from 'src/tests/mocks/objects/item.mock';

describe('PlaylistsService', () => {
  let service: PlaylistsService;
  let playlistsRepository: Repository<Playlist>;
  let playlistTranslationsRepository: Repository<PlaylistTranslation>;
  const PLAYLISTS_REPOSITORY_TOKEN = getRepositoryToken(Playlist);
  const PLAYLIST_TRANSLATIONS_REPOSITORY_TOKEN =
    getRepositoryToken(PlaylistTranslation);
  let categoriesService: CategoriesService;

  let itemsService: ItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: PLAYLISTS_REPOSITORY_TOKEN, useClass: Repository<Playlist> },
        {
          provide: PLAYLIST_TRANSLATIONS_REPOSITORY_TOKEN,
          useClass: Repository<PlaylistTranslation>,
        },
        PlaylistsService,
        LanguagesService,
        ItemsService,
        JwtService,
        UsersService,
        ConfigService,
        DublinCoreItemsService,
        DublinCoreTypesService,
        MaestroService,
        UploadService,
        DownloadService,
        ApplicationsService,
        CategoriesService,
        CategoryPlaylistsService,
        MaestroApiService,
        DockerService,
        SocketGateway,
        CreateService,
        QueuedFilesService,
        SearchService,
        ContentService,
        DownloadService,
        PlaylistsUpdateService,
      ],
    })
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .overrideProvider(DublinCoreItemsService)
      .useValue(dublinCoreItemsServiceMock)
      .overrideProvider(DublinCoreTypesService)
      .useValue(dublinCoreTypesServiceMock)
      .overrideProvider(MaestroService)
      .useValue(maestroServiceMock)
      .overrideProvider(UploadService)
      .useValue(uploadServiceMock)
      .overrideProvider(DownloadService)
      .useValue(downloadServiceMock)
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(CategoryPlaylistsService)
      .useValue(categoryPlaylistsServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(MaestroApiService)
      .useValue(maestroApiServiceMock)
      .overrideProvider(DockerService)
      .useValue(dockerServiceMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(CreateService)
      .useValue(createServiceMock)
      .overrideProvider(QueuedFilesService)
      .useValue(queuedFilesServiceMock)
      .overrideProvider(SearchService)
      .useValue(searchServiceMock)
      .overrideProvider(ContentService)
      .useValue(contentServiceMock)
      .overrideProvider(DownloadService)
      .useValue(downloadServiceMock)
      .overrideProvider(PlaylistsUpdateService)
      .useValue(playlistsUpdateServiceMock)
      .compile();

    service = module.get<PlaylistsService>(PlaylistsService);
    playlistsRepository = module.get<Repository<Playlist>>(
      PLAYLISTS_REPOSITORY_TOKEN,
    );
    playlistTranslationsRepository = module.get<
      Repository<PlaylistTranslation>
    >(PLAYLIST_TRANSLATIONS_REPOSITORY_TOKEN);
    categoriesService = module.get<CategoriesService>(CategoriesService);
    itemsService = module.get<ItemsService>(ItemsService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(playlistsRepository).toBeDefined();
    expect(playlistTranslationsRepository).toBeDefined();
  });

  describe('deletePlaylistsWithConfirm', () => {
    beforeEach(() => {
      jest.spyOn(service, 'getPlaylistsDependencies').mockResolvedValue([]);
      jest.spyOn(service, 'deletePlaylists').mockResolvedValue();
    });

    const deletePlaylistsDtoWithConfirm = {
      languageIdentifier: 'eng',
      playlistIds: ['1', '2'],
      confirm: true,
    };

    const deletePlaylistsDtoWithoutConfirm = {
      languageIdentifier: 'fra',
      playlistIds: ['1', '2', '3'],
    };

    it('should be defined', () => {
      expect(service.deletePlaylistsWithConfirm).toBeDefined();
    });

    it('should call getPlaylistsDependencies if no confirm', async () => {
      await service.deletePlaylistsWithConfirm(
        deletePlaylistsDtoWithoutConfirm,
      );
      expect(service.getPlaylistsDependencies).toHaveBeenCalledWith(
        deletePlaylistsDtoWithoutConfirm.playlistIds,
        deletePlaylistsDtoWithoutConfirm.languageIdentifier,
      );
    });

    it('should return {playlist: Playlist, categories: Category[]}[] if no confirm', async () => {
      const result = await service.deletePlaylistsWithConfirm(
        deletePlaylistsDtoWithoutConfirm,
      );

      if (!result) {
        throw new Error('Your tests are not working mate ;)');
      }

      expect(result).toBeInstanceOf(Array);
      expect(result.length).not.toBeUndefined();
    });

    it('should call deletePlaylists if confirm is true', async () => {
      await service.deletePlaylistsWithConfirm(deletePlaylistsDtoWithConfirm);
      expect(service.deletePlaylists).toHaveBeenCalledWith(
        deletePlaylistsDtoWithConfirm.playlistIds,
      );
    });

    it('should return undefined if confirm is true', async () => {
      const result = await service.deletePlaylistsWithConfirm(
        deletePlaylistsDtoWithConfirm,
      );
      expect(result).toBeUndefined();
    });
  });

  describe('getPlaylistsDependencies', () => {
    const deletePlaylistsDto = {
      languageIdentifier: 'eng',
      playlistIds: ['1', '2'],
    };

    const mockCategory = categoryMock();
    const mockCategories = [categoryMock()];

    const mockPlaylist = playlistMock({
      id: '1',
      categoryPlaylists: [
        categoryPlaylistMock({
          category: mockCategory,
        }),
      ],
    });

    beforeEach(() => {
      jest.spyOn(service, 'findOne').mockResolvedValue(mockPlaylist);
      jest
        .spyOn(service, 'getTranslation')
        .mockResolvedValue(playlistTranslationMock());
      jest
        .spyOn(categoriesService, 'findAll')
        .mockResolvedValue(mockCategories);
      jest
        .spyOn(categoriesService, 'getTranslations')
        .mockResolvedValue(mockCategories);
    });

    it('should be defined', () => {
      expect(service.getPlaylistsDependencies).toBeDefined();
    });

    it('should call findOne for each playlist in deletePlaylistsDto and each playlist in getPlaylistsDependencies', async () => {
      await service.getPlaylistsDependencies(
        deletePlaylistsDto.playlistIds,
        deletePlaylistsDto.languageIdentifier,
      );
      for (const playlistId of deletePlaylistsDto.playlistIds) {
        expect(service.findOne).toHaveBeenCalledWith({
          where: { id: playlistId },
          relations: { categoryPlaylists: { category: true } },
        });
      }
      expect(service.findOne).toHaveBeenCalledTimes(
        deletePlaylistsDto.playlistIds.length,
      );
    });

    it('should throw NotFoundExcepton if playlist not found', async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(undefined);
      expect(
        service.getPlaylistsDependencies(
          deletePlaylistsDto.playlistIds,
          deletePlaylistsDto.languageIdentifier,
        ),
      ).rejects.toThrow(NotFoundException);
    });

    it('should continue if no categoryPlaylists', async () => {
      jest
        .spyOn(service, 'findOne')
        .mockResolvedValue(playlistMock({ categoryPlaylists: [] }));

      await service.getPlaylistsDependencies(
        deletePlaylistsDto.playlistIds,
        deletePlaylistsDto.languageIdentifier,
      );

      expect(service.getTranslation).not.toHaveBeenCalled();
      expect(categoriesService.getTranslations).not.toHaveBeenCalled();
    });

    it('should call getTranslation for each playlist in getPlaylistsDependencies', async () => {
      await service.getPlaylistsDependencies(
        deletePlaylistsDto.playlistIds,
        deletePlaylistsDto.languageIdentifier,
      );
      expect(service.getTranslation).toHaveBeenCalledTimes(
        deletePlaylistsDto.playlistIds.length,
      );
      expect(service.getTranslation).toHaveBeenCalledWith(mockPlaylist, 'eng');
    });

    it('should call categoriesService.getTranslations for each playlist in getPlaylistsDependencies', async () => {
      await service.getPlaylistsDependencies(
        deletePlaylistsDto.playlistIds,
        deletePlaylistsDto.languageIdentifier,
      );
      expect(categoriesService.getTranslations).toHaveBeenCalledTimes(
        deletePlaylistsDto.playlistIds.length,
      );
      expect(categoriesService.getTranslations).toHaveBeenCalledWith(
        mockCategories,
        deletePlaylistsDto.languageIdentifier,
      );
    });

    it('should return playlistsDependencies', async () => {
      const results = await service.getPlaylistsDependencies(
        deletePlaylistsDto.playlistIds,
        deletePlaylistsDto.languageIdentifier,
      );

      if (!results) {
        throw new Error('Your tests are not working mate ;)');
      }

      expect(results).toBeInstanceOf(Array);
      expect(results.length).toEqual(deletePlaylistsDto.playlistIds.length);

      for (const result of results) {
        expect(result.playlist).toBeDefined();
        expect(result.playlist).toEqual(mockPlaylist);
        expect(result.categories).toBeDefined();
        expect(result.categories).toBeInstanceOf(Array);
      }
    });
  });

  describe('deletePlaylists', () => {
    const playlistIds = ['1', '2'];
    const mockItem1 = itemMock({ id: Date.now().toString() });
    const mockItem2 = itemMock({ id: (Date.now() + 1).toString() });

    const mockPlaylist = playlistMock({
      id: '1',
      items: [mockItem1, mockItem2],
    });

    beforeEach(() => {
      jest.spyOn(service, 'findOne').mockResolvedValue(mockPlaylist);
      jest.spyOn(itemsService, 'uninstall').mockResolvedValue(mockItem1);
      jest.spyOn(service, 'delete').mockResolvedValue(mockPlaylist);
    });

    it('should be defined', () => {
      expect(service.deletePlaylists).toBeDefined();
    });

    it('should call findOne with right arguments for each playlist', async () => {
      await service.deletePlaylists(playlistIds);

      for (const playlistId of playlistIds) {
        expect(service.findOne).toHaveBeenCalledWith({
          where: { id: playlistId },
          relations: { items: true },
        });
      }

      expect(service.findOne).toHaveBeenCalledTimes(playlistIds.length);
    });

    it('should throw NotFoundExcepton if no playlist is found', async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(null);
      expect(service.deletePlaylists(playlistIds)).rejects.toThrow(
        NotFoundException,
      );
    });

    it('should call itemsService.uninstall for each item in playlist', async () => {
      await service.deletePlaylists(playlistIds);

      for (const item of mockPlaylist.items) {
        expect(itemsService.uninstall).toHaveBeenCalledWith(
          item.id,
          playlistIds,
        );
      }

      expect(itemsService.uninstall).toHaveBeenCalledTimes(
        mockPlaylist.items.length * playlistIds.length,
      );
    });

    it('should call delete for each playlist', async () => {
      await service.deletePlaylists(playlistIds);

      for (let i = 0; i < playlistIds.length; i++) {
        expect(service.delete).toHaveBeenCalledWith(mockPlaylist);
      }

      expect(service.delete).toHaveBeenCalledTimes(playlistIds.length);
    });
  });
});
