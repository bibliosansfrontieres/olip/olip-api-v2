import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Playlist } from '../entities/playlist.entity';
import { Item } from 'src/items/entities/item.entity';
import { PlaylistTranslation } from '../entities/playlist-translation.entity';
import { Category } from 'src/categories/entities/category.entity';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';

export class GetPlaylistResponse {
  @ApiProperty({ example: 1 })
  id: string;

  @ApiProperty({
    example:
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==',
  })
  image: string;

  @ApiProperty({ example: true })
  isInstalled: boolean;

  @ApiProperty({ example: false })
  isUpdateNeeded: boolean;

  @ApiProperty({ example: false })
  isBroken: boolean;

  @ApiProperty({ example: false })
  isManuallyInstalled: boolean;

  @ApiProperty({ example: Math.floor(Date.now() / 1000) })
  version: string | null;

  @ApiPropertyOptional({ type: Item, isArray: true, example: [] })
  items: Item[];

  @ApiPropertyOptional({
    type: PlaylistTranslation,
    isArray: true,
    example: [],
  })
  playlistTranslations: PlaylistTranslation[];

  @ApiPropertyOptional({ type: CategoryPlaylist, isArray: true, example: [] })
  categoryPlaylists: CategoryPlaylist[];

  @ApiPropertyOptional({ type: Category, isArray: true, example: [] })
  categories: Category[];

  constructor(playlist: Playlist) {
    this.id = playlist.id;
    this.image = playlist.image;
    this.isInstalled = playlist.isInstalled;
    this.isUpdateNeeded = playlist.isUpdateNeeded;
    this.isBroken = playlist.isBroken;
    this.isManuallyInstalled = playlist.isManuallyInstalled;
    this.version = playlist.version;
    this.items = playlist.items;
    this.playlistTranslations = playlist.playlistTranslations;
    this.categoryPlaylists = playlist.categoryPlaylists;
  }
}
