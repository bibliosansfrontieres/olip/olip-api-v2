import { Category } from 'src/categories/entities/category.entity';
import { Playlist } from '../entities/playlist.entity';

export class DeletePlaylistNotAcceptableResponse {
  errors: { playlist: Playlist; categories: Category[] }[];
  constructor(errors: { playlist: Playlist; categories: Category[] }[]) {
    this.errors = errors;
  }
}
