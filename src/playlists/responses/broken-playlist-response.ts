import { ApiProperty } from '@nestjs/swagger';
import { Playlist } from '../entities/playlist.entity';

export class BrokenPlaylistResponse {
  @ApiProperty({ example: '1' })
  id: string;

  @ApiProperty({
    example:
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==',
  })
  image: string;

  @ApiProperty({ example: false })
  isInstalled: boolean;

  @ApiProperty({ example: false })
  isUpdateNeeded: boolean;

  @ApiProperty({ example: true })
  isBroken: boolean;

  @ApiProperty({ example: false })
  isManuallyInstalled: boolean;

  @ApiProperty({ example: Math.floor(Date.now() / 1000) })
  version: string | null;

  constructor(playlist: Playlist) {
    this.id = playlist.id;
    this.image = playlist.image;
    this.isInstalled = playlist.isInstalled;
    this.isUpdateNeeded = playlist.isUpdateNeeded;
    this.isBroken = playlist.isBroken;
    this.isManuallyInstalled = playlist.isManuallyInstalled;
    this.version = playlist.version;
  }
}
