import { Item } from 'src/items/entities/item.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { PlaylistTranslation } from './playlist-translation.entity';
import { Category } from 'src/categories/entities/category.entity';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { Application } from 'src/applications/entities/application.entity';
import { IMaestroPlaylist } from 'src/maestro/interfaces/maestro-playlist.interface';

@Entity()
export class Playlist {
  @PrimaryColumn()
  id: string;

  @Column()
  image: string;

  @Column()
  isInstalled: boolean;

  @Column()
  isUpdateNeeded: boolean;

  @Column()
  isBroken: boolean;

  @Column()
  isManuallyInstalled: boolean;

  @Column({ nullable: true })
  version: string | null;

  @Column()
  size: number;

  @ManyToOne(() => Application, (application) => application.playlists)
  application: Application;

  @ManyToMany(() => Item, (item) => item.playlists)
  @JoinTable()
  items: Item[];

  @OneToMany(
    () => PlaylistTranslation,
    (playlistTranslation) => playlistTranslation.playlist,
    { cascade: true },
  )
  playlistTranslations: PlaylistTranslation[];

  @OneToMany(
    () => CategoryPlaylist,
    (categoryPlaylist) => categoryPlaylist.playlist,
  )
  categoryPlaylists: CategoryPlaylist[];

  @ManyToMany(() => Category, (category) => category.playlists)
  categories: Category[];

  @CreateDateColumn({
    default: () => Date.now(),
  })
  createdAt: number;

  @UpdateDateColumn({
    default: () => Date.now(),
    onUpdate: Date.now().toString(),
  })
  updatedAt: number;

  constructor(
    maestroPlaylist: IMaestroPlaylist,
    application: Application,
    isManuallyInstalled = false,
  ) {
    if (!maestroPlaylist) return;
    this.id = maestroPlaylist.id;
    this.application = application;
    this.image = maestroPlaylist.image;
    this.isInstalled = true;
    this.isUpdateNeeded = false;
    this.isManuallyInstalled = isManuallyInstalled;
    this.items = [];
    this.version = maestroPlaylist.version || null;
    this.isBroken = false;
    this.size = maestroPlaylist.size;
  }
}
