import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Playlist } from './playlist.entity';
import { Language } from 'src/languages/entities/language.entity';
import { IMaestroPlaylistTranslation } from 'src/maestro/interfaces/maestro-playlist-translation.interface';

@Entity()
export class PlaylistTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column({ nullable: true })
  indexId: string | null;

  @ManyToOne(() => Playlist, (playlist) => playlist.playlistTranslations, {
    onDelete: 'CASCADE',
  })
  playlist: Playlist;

  @ManyToOne(() => Language, (language) => language.playlistTranslations)
  language: Language;

  constructor(
    maestroPlaylistTranslation: IMaestroPlaylistTranslation,
    language: Language,
  ) {
    if (!maestroPlaylistTranslation) return;
    this.title = maestroPlaylistTranslation.title;
    this.description = maestroPlaylistTranslation.description;
    this.language = language;
  }
}
