import { Module, forwardRef } from '@nestjs/common';
import { PlaylistsService } from './playlists.service';
import { PlaylistsController } from './playlists.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Playlist } from './entities/playlist.entity';
import { PlaylistTranslation } from './entities/playlist-translation.entity';
import { LanguagesModule } from 'src/languages/languages.module';
import { ItemsModule } from 'src/items/items.module';
import { DublinCoreItemsModule } from 'src/dublin-core-items/dublin-core-items.module';
import { DublinCoreTypesModule } from 'src/dublin-core-types/dublin-core-types.module';
import { MaestroModule } from 'src/maestro/maestro.module';
import { UploadModule } from 'src/upload/upload.module';
import { CategoriesModule } from 'src/categories/categories.module';
import { CategoryPlaylistsModule } from 'src/category-playlists/category-playlists.module';
import { ApplicationsModule } from 'src/applications/applications.module';
import { ConfigModule } from '@nestjs/config';
import { DockerModule } from 'src/docker/docker.module';
import { SearchModule } from 'src/search/search.module';
import { UpdateModule } from 'src/update/update.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Playlist, PlaylistTranslation]),
    LanguagesModule,
    forwardRef(() => ItemsModule),
    DublinCoreItemsModule,
    DublinCoreTypesModule,
    forwardRef(() => MaestroModule),
    UploadModule,
    forwardRef(() => CategoriesModule),
    CategoryPlaylistsModule,
    ApplicationsModule,
    ConfigModule,
    DockerModule,
    SearchModule,
    MaestroModule,
    forwardRef(() => UpdateModule),
  ],
  controllers: [PlaylistsController],
  providers: [PlaylistsService],
  exports: [PlaylistsService],
})
export class PlaylistsModule {}
