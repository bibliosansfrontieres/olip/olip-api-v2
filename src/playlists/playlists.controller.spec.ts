import { Test, TestingModule } from '@nestjs/testing';
import { PlaylistsController } from './playlists.controller';
import { PlaylistsService } from './playlists.service';
import { playlistsServiceMock } from 'src/tests/mocks/providers/playlists-service.mock';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { PlaylistsUpdateService } from 'src/update/playlists-update.service';
import { playlistsUpdateServiceMock } from 'src/tests/mocks/providers/playlists-update-service.mock';

describe('PlaylistsController', () => {
  let controller: PlaylistsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PlaylistsController],
      providers: [
        PlaylistsService,
        { provide: CACHE_MANAGER, useValue: {} },
        PlaylistsUpdateService,
      ],
    })
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .overrideProvider(PlaylistsUpdateService)
      .useValue(playlistsUpdateServiceMock)
      .compile();

    controller = module.get<PlaylistsController>(PlaylistsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
