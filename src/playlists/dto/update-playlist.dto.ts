import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class UpdatePlaylistDto {
  @IsString()
  @IsOptional()
  version?: string;

  @IsBoolean()
  @IsOptional()
  isUpdateNeeded?: boolean;
}
