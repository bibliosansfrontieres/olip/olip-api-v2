import { ApiProperty } from '@nestjs/swagger';
import { IsBooleanString } from 'class-validator';

export class GetPlaylistOptionsDto {
  @IsBooleanString()
  @ApiProperty({ example: 'true' })
  withItems?: string;
}
