import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetUninstalledPlaylistsDto {
  @ApiProperty({ example: 'eng' })
  @IsString()
  languageIdentifier: string;

  @ApiPropertyOptional({ example: 'my search query' })
  @IsString()
  @IsOptional()
  query?: string;

  @ApiPropertyOptional({ example: '1,2,3' })
  @IsString()
  @IsOptional()
  applicationIds?: string;
}
