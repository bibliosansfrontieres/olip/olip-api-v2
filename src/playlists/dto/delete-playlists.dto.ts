import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  ArrayMinSize,
  IsArray,
  IsBoolean,
  IsOptional,
  IsString,
} from 'class-validator';

export class DeletePlaylistsDto {
  @ApiProperty({ example: 'eng' })
  @IsString()
  languageIdentifier: string;

  @ApiProperty({ example: ['1', '2'] })
  @IsArray()
  @ArrayMinSize(1)
  playlistIds: (string | number)[];

  @ApiPropertyOptional({ example: true })
  @IsBoolean()
  @IsOptional()
  confirm?: boolean;
}
