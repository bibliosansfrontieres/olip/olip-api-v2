import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetInstalledPlaylistsDto {
  @ApiProperty({ example: '1' })
  @IsString()
  categoryId: string;

  @ApiProperty({ example: '1,2' })
  @IsOptional()
  @IsString()
  applicationIds?: string;

  @ApiProperty({ example: 'eng' })
  @IsString()
  languageIdentifier: string;

  @ApiProperty({ example: 'my search query' })
  @IsString()
  @IsOptional()
  query?: string;
}
