import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsArray } from 'class-validator';

export class InstallPlaylistsDto {
  @ApiProperty({ example: [1, '11-d5f-12e'] })
  @IsArray()
  playlistIds: string[];

  @ApiPropertyOptional({ example: true })
  linkToCategories?: boolean;
}
