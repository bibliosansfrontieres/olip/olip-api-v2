import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetUpdateNeededOptionsDto {
  @ApiProperty({ example: '1' })
  @IsString()
  categoryId: string;

  @ApiProperty({ example: 'eng' })
  languageIdentifier: string;

  @ApiPropertyOptional({ example: '1,2' })
  @IsString()
  @IsOptional()
  applicationIds?: string;
}
