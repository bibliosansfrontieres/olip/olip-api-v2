import { ApiProperty } from '@nestjs/swagger';
import { IsArray } from 'class-validator';

export class UpdatePlaylistsDto {
  @ApiProperty({ example: ['1', '2'] })
  @IsArray()
  playlistIds: string[];
}
