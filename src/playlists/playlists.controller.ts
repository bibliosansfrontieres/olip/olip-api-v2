import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  NotFoundException,
  Param,
  Post,
  Query,
  forwardRef,
} from '@nestjs/common';
import { PlaylistsService } from './playlists.service';
import {
  ApiNotAcceptableResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { BrokenPlaylistResponse } from './responses/broken-playlist-response';
import { GetPlaylistResponse } from './responses/get-playlist-response';
import { cachedMethod } from 'src/utils/caching.util';
import { Playlist } from './entities/playlist.entity';
import { SetEndpointCache } from 'src/utils/decorators/set-endpoint-cache.decorator';
import { GetPlaylistOptionsDto } from './dto/get-playlist-options.dto';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { DeletePlaylistsDto } from './dto/delete-playlists.dto';
import { DeletePlaylistNotAcceptableResponse } from './responses/delete-playlist-not-acceptable-response';
import { InstallPlaylistsDto } from './dto/install-playlists.dto';
import { GetInstalledPlaylistsDto } from './dto/get-installed-playlists.dto';
import { GetUninstalledPlaylistsDto } from './dto/get-uninstalled-playlists.dto';
import { GetUpdateNeededOptionsDto } from './dto/get-update-needed-options.dto';
import { UpdatePlaylistsDto } from './dto/update-playlists.dto';
import { PlaylistsUpdateService } from 'src/update/playlists-update.service';

@ApiTags('playlists')
@Controller('api/playlists')
export class PlaylistsController {
  constructor(
    private readonly playlistsService: PlaylistsService,
    @Inject(forwardRef(() => PlaylistsUpdateService))
    private playlistsUpdateService: PlaylistsUpdateService,
  ) {}

  @Get(':id')
  @ApiOperation({ summary: 'Get playlist by ID' })
  @ApiOkResponse({ type: GetPlaylistResponse })
  @ApiNotFoundResponse({ description: 'Playlist not found' })
  async getPlaylist(
    @Param('id') id: string,
    @Query('languageIdentifier') languageIdentifier: string,
    @Query() options: GetPlaylistOptionsDto,
  ) {
    return cachedMethod(
      'playlistsService',
      'getPlaylist',
      [{ id, languageIdentifier }, options],
      async () => {
        const playlist = await this.playlistsService.getPlaylist(
          id,
          languageIdentifier,
          options,
        );
        if (playlist === null)
          throw new NotFoundException('playlist not found');
        return new GetPlaylistResponse(playlist);
      },
      [Playlist],
    );
  }

  @Get('broken')
  @SetEndpointCache('playlists', 'broken', Playlist)
  @ApiOperation({ summary: 'Get list of broken playlists' })
  @ApiOkResponse({ type: BrokenPlaylistResponse, isArray: true })
  async getAllBroken() {
    const playlists = await this.playlistsService.findAllBrokenPlaylists();
    return playlists.map((playlist) => new BrokenPlaylistResponse(playlist));
  }

  @Get('installed')
  getInstalledPlaylists(
    @Query() getInstalledPlaylistsOptionsDto: GetInstalledPlaylistsDto,
    @Query() pageOptionsDto: PageOptionsDto,
  ) {
    return this.playlistsService.getInstalledPlaylists(
      getInstalledPlaylistsOptionsDto,
      pageOptionsDto,
    );
  }

  @Get('uninstalled')
  getUninstalledPlaylists(
    @Query()
    getUninstalledPlaylistsOptionsDto: GetUninstalledPlaylistsDto,
    @Query() pageOptionsDto: PageOptionsDto,
  ) {
    return this.playlistsService.getUninstalledPlaylists(
      getUninstalledPlaylistsOptionsDto,
      pageOptionsDto,
    );
  }

  @Post('install')
  @ApiOperation({
    summary: 'Install playlists by ID (from Maestro)',
  })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  installPlaylist(@Body() installPlaylistsDto: InstallPlaylistsDto) {
    return this.playlistsService.install(installPlaylistsDto);
  }

  @Get('update-needed')
  getUpdateNeeded(
    @Query() getUpdateNeededOptionsDto: GetUpdateNeededOptionsDto,
    @Query() pageOptionsDto: PageOptionsDto,
  ) {
    return this.playlistsService.getUpdateNeeded(
      getUpdateNeededOptionsDto,
      pageOptionsDto,
    );
  }

  @Post('update')
  @ApiOperation({
    summary: 'Update playlists by ID (from Maestro)',
  })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  update(@Body() updatePlaylistsDto: UpdatePlaylistsDto) {
    return this.playlistsUpdateService.updatePlaylists(
      updatePlaylistsDto.playlistIds,
    );
  }

  @Delete()
  @ApiOperation({
    summary:
      'Delete playlists by ID (remove all categoryPlaylists and items linked to it)',
  })
  @ApiOkResponse()
  @ApiNotAcceptableResponse({ type: DeletePlaylistNotAcceptableResponse })
  @ApiNotFoundResponse()
  deletePlaylists(@Body() deletePlaylistsDto: DeletePlaylistsDto) {
    return this.playlistsService.deletePlaylistsWithConfirm(deletePlaylistsDto);
  }

  @Get('uninstall/:id')
  uninstall(@Param('id') id: string) {
    return this.playlistsService.uninstall(id);
  }
}
