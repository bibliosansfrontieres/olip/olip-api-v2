export interface IFindPlaylistOptions {
  relations?: {
    items?: boolean;
    categories?: boolean;
  };
}
