import {
  Inject,
  Injectable,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { Playlist } from './entities/playlist.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions, In, Repository } from 'typeorm';
import { PlaylistTranslation } from './entities/playlist-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';
import { Item } from 'src/items/entities/item.entity';
import { IFindPlaylistOptions } from './interfaces/find-playlist-options.interface';
import { ItemsService } from 'src/items/items.service';
import { invalidateCache } from 'src/utils/caching.util';
import { GetPlaylistOptionsDto } from './dto/get-playlist-options.dto';
import { DublinCoreItemsService } from 'src/dublin-core-items/dublin-core-items.service';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { PageOptions } from 'src/utils/classes/page-options';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { PageDto } from 'src/utils/classes/page.dto';
import { DeletePlaylistsDto } from './dto/delete-playlists.dto';
import { Category } from 'src/categories/entities/category.entity';
import { CategoriesService } from 'src/categories/categories.service';
import { InstallPlaylistsDto } from './dto/install-playlists.dto';
import { GetInstalledPlaylistsDto } from './dto/get-installed-playlists.dto';
import { GetUninstalledPlaylistsDto } from './dto/get-uninstalled-playlists.dto';
import { CategoryPlaylistsService } from 'src/category-playlists/category-playlists.service';
import { GetUpdateNeededOptionsDto } from './dto/get-update-needed-options.dto';
import { ApplicationsService } from 'src/applications/applications.service';
import { MaestroApiService } from 'src/maestro/maestro-api.service';
import { CategoryPlaylist } from 'src/category-playlists/entities/category-playlist.entity';
import { IMaestroPlaylist } from 'src/maestro/interfaces/maestro-playlist.interface';
import { SearchService } from 'src/search/search.service';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { ContentService } from 'src/maestro/content.service';
import { DownloadService } from 'src/maestro/download.service';
import { getContext } from 'src/utils/get-context.util';

@Injectable()
export class PlaylistsService {
  constructor(
    @InjectRepository(Playlist)
    private playlistsRepository: Repository<Playlist>,
    @InjectRepository(PlaylistTranslation)
    private playlistTranslationsRepository: Repository<PlaylistTranslation>,
    private languagesService: LanguagesService,
    @Inject(forwardRef(() => ItemsService))
    private itemsService: ItemsService,
    private dublinCoreItemsService: DublinCoreItemsService,
    private dublinCoreTypesService: DublinCoreTypesService,
    @Inject(forwardRef(() => CategoriesService))
    private categoriesService: CategoriesService,
    private categoryPlaylistsService: CategoryPlaylistsService,
    private applicationsService: ApplicationsService,
    private maestroApiService: MaestroApiService,
    private searchService: SearchService,
    private contentService: ContentService,
    private downloadService: DownloadService,
  ) {}

  async indexAll() {
    const playlists = await this.find();
    for (const playlist of playlists) {
      await this.searchService.indexPlaylist(playlist.id);
    }
  }

  async createOrUpdate(
    maestroPlaylist: IMaestroPlaylist,
    isManuallyInstalled = false,
  ) {
    const playlist = await this.findOne({
      where: { id: maestroPlaylist.id },
      relations: { playlistTranslations: { language: true }, items: true },
    });
    if (!playlist) {
      return this.createPlaylist(maestroPlaylist, isManuallyInstalled);
    } else {
      return this.update(playlist, maestroPlaylist);
    }
  }

  async createPlaylist(
    maestroPlaylist: IMaestroPlaylist,
    isManuallyInstalled = false,
  ) {
    const application = await this.applicationsService.findOne({
      where: { id: maestroPlaylist.applicationId },
    });
    if (!application) throw new NotFoundException('application not found');

    const playlistTranslations = [];
    for (const maestroPlaylistTranslation of maestroPlaylist.playlistTranslations) {
      const language = await this.languagesService.findOneByIdentifier(
        maestroPlaylistTranslation.languageIdentifier,
      );
      if (!language) throw new NotFoundException('language not found');
      const playlistTranslation = new PlaylistTranslation(
        maestroPlaylistTranslation,
        language,
      );
      playlistTranslations.push(playlistTranslation);
    }

    const playlist = new Playlist(
      maestroPlaylist,
      application,
      isManuallyInstalled,
    );
    playlist.playlistTranslations = playlistTranslations;

    await this.playlistsRepository.save(playlist);
    await this.searchService.indexPlaylist(playlist.id);

    await invalidateCache([Playlist, PlaylistTranslation]);
  }

  async addItemToPlaylist(id: string, item: Item) {
    let playlist = await this.findOnePlaylistById(id, {
      relations: { items: true },
    });
    playlist.items.push(item);
    playlist = await this.playlistsRepository.save(playlist);
    await invalidateCache([Playlist, Item]);
    return playlist;
  }

  async removeItemFromPlaylist(id: string, item: Item) {
    let playlist = await this.findOnePlaylistById(id, {
      relations: { items: true },
    });
    playlist.items = playlist.items.filter((i) => i.id !== item.id);
    playlist = await this.playlistsRepository.save(playlist);
    await invalidateCache([Playlist, Item]);
    return playlist;
  }

  findAllBrokenPlaylists() {
    return this.playlistsRepository.find({
      where: { isBroken: true },
    });
  }

  async getUninstalledPlaylists(
    getUninstalledPlaylistsOptionsDto: GetUninstalledPlaylistsDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const context = getContext();
    if (context && !context.isOutsideProjectDownloadsAuthorized) {
      return new PageDto([], 0, new PageOptions({}));
    }
    const uninstalledPlaylists =
      await this.maestroApiService.getUninstalledPlaylists(
        getUninstalledPlaylistsOptionsDto,
        pageOptionsDto,
      );
    for (const uninstalledPlaylist of uninstalledPlaylists.data) {
      uninstalledPlaylist.application = await this.applicationsService.findOne({
        where: { id: uninstalledPlaylist.application.id },
        relations: { applicationType: true },
      });
      await this.applicationsService.getApplicationsTranslations(
        [uninstalledPlaylist.application],
        getUninstalledPlaylistsOptionsDto.languageIdentifier,
      );
      // TODO : images are not uploaded in maestro
      /*
      if (uninstalledPlaylist.application.id !== 'olip') {
        uninstalledPlaylist.image = `${this.configService.get(
          'MAESTRO_API_URL',
        )}/${uninstalledPlaylist.image}`;
      }
      */
      for (const uninstalledItem of uninstalledPlaylist.items) {
        uninstalledItem.application = uninstalledPlaylist.application;
        // TODO : images are not uploaded in maestro
        /*
        if (uninstalledPlaylist.application.id !== 'olip') {
          uninstalledItem.thumbnail = `${this.configService.get(
            'MAESTRO_API_URL',
          )}/${uninstalledItem.thumbnail}`;
        }
        */
      }
    }
    uninstalledPlaylists.meta.totalItemsCount = uninstalledPlaylists.meta.count;
    return uninstalledPlaylists;
  }

  async getUpdateNeeded(
    getUpdateNeededOptionsDto: GetUpdateNeededOptionsDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const applicationsIds =
      getUpdateNeededOptionsDto && getUpdateNeededOptionsDto?.applicationIds
        ? getUpdateNeededOptionsDto?.applicationIds.split(',')
        : [];
    const pageOptions = new PageOptions(pageOptionsDto);
    const application =
      applicationsIds.length > 0 ? { id: In(applicationsIds) } : undefined;
    const playlists = await this.find({
      where: {
        categoryPlaylists: {
          category: { id: +getUpdateNeededOptionsDto.categoryId },
        },
        isUpdateNeeded: true,
        application,
      },
      relations: { application: { applicationType: true } },
      skip: pageOptions.skip,
      order: { id: pageOptions.order },
      take: pageOptions.take,
    });
    for (const playlist of playlists) {
      playlist.playlistTranslations = [
        await this.getTranslation(
          playlist,
          getUpdateNeededOptionsDto.languageIdentifier,
        ),
      ];
      await this.applicationsService.getApplicationsTranslations(
        [playlist.application],
        getUpdateNeededOptionsDto.languageIdentifier,
      );
    }
    const count = await this.playlistsRepository.count({
      where: {
        categoryPlaylists: {
          category: { id: +getUpdateNeededOptionsDto.categoryId },
        },
        isUpdateNeeded: true,
        application,
      },
    });
    return new PageDto(playlists, count, pageOptions);
  }

  async getInstalledPlaylists(
    getInstalledPlaylistsOptionsDto: GetInstalledPlaylistsDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const categoryPlaylists = await this.categoryPlaylistsService.find({
      where: { category: { id: +getInstalledPlaylistsOptionsDto.categoryId } },
      relations: { playlist: true },
    });
    const applicationIds =
      getInstalledPlaylistsOptionsDto &&
      getInstalledPlaylistsOptionsDto?.applicationIds
        ? getInstalledPlaylistsOptionsDto.applicationIds.split(',')
        : [];
    const playlistIds = categoryPlaylists.map(
      (categoryPlaylist) => categoryPlaylist.playlist.id,
    );
    const pageOptions = new PageOptions(pageOptionsDto);
    const queryBuilder =
      this.playlistsRepository.createQueryBuilder('playlist');
    let playlistsQueryBuilder = queryBuilder
      // Joins PlaylistItemsItem from Playlist
      .innerJoinAndSelect('playlist.items', 'item')
      // Joins DublinCoreItem from Item
      .innerJoinAndSelect(
        'item.dublinCoreItem',
        'dublinCoreItem',
        'dublinCoreItem.itemId = item.id',
      )
      .innerJoinAndSelect(
        'item.application',
        'itemApplication',
        'itemApplication.id = item.applicationId',
      )
      .innerJoinAndSelect(
        'itemApplication.applicationType',
        'itemApplicationType',
        'itemApplicationType.id = itemApplication.applicationTypeId',
      )
      // Joins PlaylistTranslation from Playlist
      .innerJoin(
        'playlist.playlistTranslations',
        'playlistTranslation',
        'playlistTranslation.playlistId = playlist.id',
      )
      // Joins DublinCoreType from DublinCoreItem
      .innerJoinAndSelect(
        'dublinCoreItem.dublinCoreType',
        'dublinCoreType',
        'dublinCoreType.id = dublinCoreItem.dublinCoreTypeId',
      )
      // Joins Application from Playlist
      .innerJoinAndSelect(
        'playlist.application',
        'application',
        'application.id = playlist.applicationId',
      )
      // Joins ApplicationType from Application
      .innerJoinAndSelect(
        'application.applicationType',
        'applicationType',
        'applicationType.id = application.applicationTypeId',
      )
      // Select from categoryId
      .where('playlist.isInstalled = 1')
      // Take from pageOptions
      .take(pageOptions.take)
      // Skip from pageOptions
      .skip(pageOptions.skip);

    if (getInstalledPlaylistsOptionsDto.categoryId) {
      playlistsQueryBuilder = playlistsQueryBuilder.andWhere(
        'playlist.id NOT IN (:...playlistIds)',
        { playlistIds },
      );
    }

    if (applicationIds.length > 0) {
      playlistsQueryBuilder = playlistsQueryBuilder.andWhere(
        'application.id IN (:...applicationIds)',
        { applicationIds },
      );
    }

    if (getInstalledPlaylistsOptionsDto.query) {
      playlistsQueryBuilder = playlistsQueryBuilder.andWhere(
        'playlistTranslation.title LIKE :query',
        { query: `%${getInstalledPlaylistsOptionsDto.query}%` },
      );
    }

    const playlistsResults = await getManyAndCount<Playlist>(
      playlistsQueryBuilder,
      ['playlist.id'],
      false,
    );

    const totalPlaylistsCount = playlistsResults[0];
    let playlists = playlistsResults[1];

    // get playlist translations
    playlists = await this.getPlaylistTranslations(
      getInstalledPlaylistsOptionsDto.languageIdentifier,
      playlists,
    );

    // get item translations
    playlists = await this.getDublinCoreItemTranslations(
      getInstalledPlaylistsOptionsDto.languageIdentifier,
      playlists,
    );

    // get dublinCoreType translations
    playlists = await this.getDublinCoreTypeTranslations(
      getInstalledPlaylistsOptionsDto.languageIdentifier,
      playlists,
    );

    for (const playlist of playlists) {
      for (const item of playlist.items) {
        await this.applicationsService.getApplicationsTranslations(
          [item.application],
          getInstalledPlaylistsOptionsDto.languageIdentifier,
        );
      }
      await this.applicationsService.getApplicationsTranslations(
        [playlist.application],
        getInstalledPlaylistsOptionsDto.languageIdentifier,
      );
    }

    return new PageDto(playlists, totalPlaylistsCount, pageOptions);
  }

  private async getDublinCoreTypeTranslations(
    languageIdentifier: string,
    playlists: Playlist[],
  ) {
    for (const playlist of playlists) {
      for (const playlistItem of playlist.items) {
        if (playlistItem.dublinCoreItem) {
          const dublinCoreTypeTranslation =
            await this.dublinCoreTypesService.getTranslation(
              playlistItem.dublinCoreItem.dublinCoreType,
              languageIdentifier,
            );
          playlistItem.dublinCoreItem.dublinCoreType.dublinCoreTypeTranslations =
            [dublinCoreTypeTranslation];
        }
      }
    }
    return playlists;
  }

  private async getDublinCoreItemTranslations(
    languageIdentifier: string,
    playlists: Playlist[],
  ) {
    for (const playlist of playlists) {
      for (const playlistItem of playlist.items) {
        if (playlistItem.dublinCoreItem) {
          const dublinCoreItemTranslation =
            await this.dublinCoreItemsService.getTranslation(
              playlistItem.dublinCoreItem,
              languageIdentifier,
            );
          playlistItem.dublinCoreItem.dublinCoreItemTranslations = [
            dublinCoreItemTranslation,
          ];
        }
      }
    }
    return playlists;
  }

  private async getPlaylistTranslations(
    languageIdentifier: string,
    playlists: Playlist[],
  ) {
    for (const playlist of playlists) {
      const playlistTranslation = await this.getPlaylistTranslation(
        playlist,
        languageIdentifier,
      );
      playlist.playlistTranslations = [playlistTranslation];
    }
    return playlists;
  }

  async getPlaylist(
    id: string,
    languageIdentifier: string,
    options?: GetPlaylistOptionsDto,
  ) {
    const relations = ['application', 'application.applicationType'];
    if (options) {
      if (options.withItems === 'true') {
        relations.push(
          'items',
          'items.dublinCoreItem',
          'items.application',
          'items.application.applicationType',
        );
      }
    }
    const playlist = await this.playlistsRepository.findOne({
      where: { id },
      relations,
    });
    if (playlist === null) return null;
    const playlistTranslation = await this.languagesService.getTranslation(
      'playlist',
      playlist,
      this.playlistTranslationsRepository,
      languageIdentifier,
    );
    playlist.playlistTranslations = [playlistTranslation];
    if (playlist.items && playlist.items.length > 0) {
      for (const item of playlist.items) {
        await this.applicationsService.getApplicationsTranslations(
          [item.application],
          languageIdentifier,
        );
        if (item.dublinCoreItem) {
          const dublinCoreItemTranslation =
            await this.dublinCoreItemsService.getTranslation(
              item.dublinCoreItem,
              languageIdentifier,
            );
          item.dublinCoreItem.dublinCoreItemTranslations = [
            dublinCoreItemTranslation,
          ];
        }
      }
    }
    await this.applicationsService.getApplicationsTranslations(
      [playlist.application],
      languageIdentifier,
    );
    return playlist;
  }

  private findOnePlaylistById(id: string, options?: IFindPlaylistOptions) {
    const relations = [];
    if (options) {
      if (options.relations) {
        if (options.relations.categories) {
          relations.push('categories');
        }
        if (options.relations.items) {
          relations.push('items');
        }
      }
    }
    return this.playlistsRepository.findOne({
      where: { id },
      relations,
    });
  }

  async remove(playlistId: string) {
    const playlist = await this.findOnePlaylistById(playlistId);
    if (playlist === null) return null;
    await this.playlistsRepository.remove(playlist);
    await invalidateCache(Playlist);
    return playlist;
  }

  private async getPlaylistTranslation(
    playlist: Playlist,
    languageIdentifier: string,
  ): Promise<PlaylistTranslation> {
    return this.languagesService.getTranslation<Playlist, PlaylistTranslation>(
      'playlist',
      playlist,
      this.playlistTranslationsRepository,
      languageIdentifier,
    );
  }

  async findOne(options?: FindOneOptions<Playlist>) {
    return this.playlistsRepository.findOne(options);
  }

  async find(options?: FindManyOptions<Playlist>) {
    return this.playlistsRepository.find(options);
  }

  async uninstall(playlistId: string) {
    const playlist = await this.findOne({
      where: { id: playlistId },
    });

    // Get items not linked to another playlist
    const itemsNotLinked = await this.itemsService
      .getQueryBuilder('item')
      .leftJoinAndSelect('item.playlists', 'playlist')
      .where('playlist.id = :playlistId', { playlistId })
      .andWhere(
        `NOT EXISTS (
        SELECT * FROM playlist_items_item
        WHERE playlist_items_item.itemId = item.id
        GROUP BY playlist_items_item.itemId
        HAVING COUNT(playlist_items_item.playlistId) > 1
      )`,
      )
      .getMany();

    for (const item of itemsNotLinked) {
      await this.itemsService.uninstall(item.id);
    }

    // TODO : delete image

    await this.remove(playlist.id);
    await invalidateCache([Playlist, CategoryPlaylist, Category]);
    return playlist;
  }

  async getTranslation(playlist: Playlist, languageIdentifier: string) {
    return this.languagesService.getTranslation(
      'playlist',
      playlist,
      this.playlistTranslationsRepository,
      languageIdentifier,
    );
  }

  async deletePlaylistsWithConfirm(deletePlaylistsDto: DeletePlaylistsDto) {
    if (!deletePlaylistsDto.confirm) {
      return this.getPlaylistsDependencies(
        deletePlaylistsDto.playlistIds,
        deletePlaylistsDto.languageIdentifier,
      );
    } else {
      return this.deletePlaylists(deletePlaylistsDto.playlistIds);
    }
  }

  async getPlaylistsDependencies(
    playlistIds: (string | number)[],
    languageIdentifier: string,
  ) {
    const playlistsDependencies: {
      playlist: Playlist;
      categories: Category[];
    }[] = [];

    for (const playlistId of playlistIds) {
      // Get playlist with its categories
      const playlist = await this.findOne({
        where: { id: playlistId.toString() },
        relations: { categoryPlaylists: { category: true } },
      });

      if (!playlist) {
        throw new NotFoundException(`playlist ${playlistId} not found`);
      }

      // Skip if not categoryPlaylists in playlist
      if (!playlist.categoryPlaylists.length) {
        continue;
      }

      // Get playlist translations
      playlist.playlistTranslations = [
        await this.getTranslation(playlist, languageIdentifier),
      ];

      // Get categories linked to playlist
      let categories = playlist.categoryPlaylists.map(
        (categoryPlaylist) => categoryPlaylist.category,
      );

      // Get categories translations
      categories = await this.categoriesService.getTranslations(
        categories,
        languageIdentifier,
      );

      playlistsDependencies.push({ playlist, categories });
    }

    return playlistsDependencies;
  }

  async deletePlaylists(playlistIds: (string | number)[]) {
    for (const playlistId of playlistIds) {
      const playlist = await this.findOne({
        where: { id: playlistId.toString() },
        relations: { items: true },
      });

      if (!playlist) {
        throw new NotFoundException(`playlist ${playlistId} not found`);
      }

      for (const item of playlist.items) {
        await this.itemsService.uninstall(item.id, playlistIds);
      }

      await this.delete(playlist);
    }
  }

  async save(playlist: Playlist) {
    return this.playlistsRepository.save(playlist);
  }

  async update(playlist: Playlist, maestroPlaylist: IMaestroPlaylist) {
    // Manage translations to remove
    const translationIdsToRemove = [];
    for (const playlistTranslation of playlist.playlistTranslations) {
      const maestroTranslation = maestroPlaylist.playlistTranslations.find(
        (translation) =>
          translation.languageIdentifier ===
          playlistTranslation.language.identifier,
      );
      if (!maestroTranslation) {
        await this.playlistTranslationsRepository.remove(playlistTranslation);
        translationIdsToRemove.push(playlistTranslation.id);
      }
    }

    // Manage translations to add/update
    const translationsToAdd = [];
    for (const maestroTranslation of maestroPlaylist.playlistTranslations) {
      let playlistTranslation = playlist.playlistTranslations.find(
        (translation) =>
          translation.language.identifier ===
          maestroTranslation.languageIdentifier,
      );
      if (!playlistTranslation) {
        // Create new translations
        const language = await this.languagesService.findOneByIdentifier(
          maestroTranslation.languageIdentifier,
        );
        playlistTranslation = new PlaylistTranslation(
          maestroTranslation,
          language,
        );
        playlistTranslation =
          await this.playlistTranslationsRepository.save(playlistTranslation);
        translationsToAdd.push(playlistTranslation);
      } else {
        // Update existing translation
        playlistTranslation.description = maestroTranslation.description;
        playlistTranslation.title = maestroTranslation.title;
        await this.playlistTranslationsRepository.save(playlistTranslation);
      }
    }

    playlist.isUpdateNeeded = false;
    playlist.version = maestroPlaylist.version;
    playlist.playlistTranslations = [
      // Add existing translations filtering removed translations
      ...playlist.playlistTranslations.filter(
        (playlistTranslation) =>
          !translationIdsToRemove.includes(playlistTranslation.id),
      ),
      // Add new translations
      ...translationsToAdd,
    ];

    playlist = await this.playlistsRepository.save(playlist);

    // TODO : We might need to only re-index playlist
    await this.searchService.removePlaylistIndex(playlist.id);
    await this.searchService.indexPlaylist(playlist.id);
    await this.categoriesService.removePlaylistUpdateNeeded(playlist.id);

    return;
  }

  async install(installPlaylistsDto: InstallPlaylistsDto) {
    let runDownloads = false;
    for (const id of installPlaylistsDto.playlistIds) {
      const result = await this.contentService.installOrUpdatePlaylist(
        id.toString(),
      );
      if (result) {
        runDownloads = true;
      }
    }
    await invalidateCache([
      Playlist,
      CategoryPlaylist,
      Item,
      CategoryPlaylistItem,
      Category,
    ]);
    if (runDownloads) {
      await this.downloadService.runQueue();
    }
    await invalidateCache([
      Playlist,
      CategoryPlaylist,
      Item,
      CategoryPlaylistItem,
      Category,
    ]);
  }

  async saveTranslation(playlistTranslation: PlaylistTranslation) {
    return this.playlistTranslationsRepository.save(playlistTranslation);
  }

  delete(playlist: Playlist) {
    return this.playlistsRepository.remove(playlist);
  }
}
