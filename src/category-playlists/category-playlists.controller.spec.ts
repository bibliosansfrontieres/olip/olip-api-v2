import { Test, TestingModule } from '@nestjs/testing';
import { CategoryPlaylistsController } from './category-playlists.controller';
import { CategoryPlaylistsService } from './category-playlists.service';
import { Repository } from 'typeorm';
import { CategoryPlaylist } from './entities/category-playlist.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { playlistsServiceMock } from 'src/tests/mocks/providers/playlists-service.mock';
import { CategoriesService } from 'src/categories/categories.service';
import { categoriesServiceMock } from 'src/tests/mocks/providers/categories-service.mock';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { dublinCoreTypesServiceMock } from 'src/tests/mocks/providers/dublin-core-types-service.mock';
import { DublinCoreItemsService } from 'src/dublin-core-items/dublin-core-items.service';
import { dublinCoreItemsServiceMock } from 'src/tests/mocks/providers/dublin-core-items-service.mock';
import { CategoryPlaylistItemsService } from 'src/category-playlist-items/category-playlist-items.service';
import { categoryPlaylistItemsServiceMock } from 'src/tests/mocks/providers/category-playlist-items-service.mock';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';
import { GetCategoryPlaylistBackOfficeCategoryPlaylistItemsDto } from './dto/get-category-playlist-back-office-category-playlist-items.dto';
import * as CachingUtil from 'src/utils/caching.util';
import { ApplicationTranslation } from 'src/applications/entities/application-translation.entity';
import { Application } from 'src/applications/entities/application.entity';
import { DublinCoreTypeTranslation } from 'src/dublin-core-types/entities/dublin-core-type-translation.entity';
import { DublinCoreItemTranslation } from 'src/dublin-core-items/entities/dublin-core-item-translation';
import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';
import { Item } from 'src/items/entities/item.entity';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { categoryPlaylistsServiceMock } from 'src/tests/mocks/providers/category-playlists-service.mock';

describe('CategoryPlaylistsController', () => {
  let controller: CategoryPlaylistsController;
  let categoryPlaylistRepository: Repository<CategoryPlaylist>;
  let categoryPlaylistsService: CategoryPlaylistsService;

  const CATEGORY_PLAYLIST_REPOSITORY_TOKEN =
    getRepositoryToken(CategoryPlaylist);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CategoryPlaylistsController],
      providers: [
        {
          provide: CATEGORY_PLAYLIST_REPOSITORY_TOKEN,
          useClass: Repository<CategoryPlaylist>,
        },
        CategoryPlaylistsService,
        PlaylistsService,
        CategoriesService,
        DublinCoreTypesService,
        DublinCoreItemsService,
        CategoryPlaylistItemsService,
        JwtService,
        UsersService,
        ConfigService,
        ApplicationsService,
      ],
    })
      .overrideProvider(CategoryPlaylistsService)
      .useValue(categoryPlaylistsServiceMock)
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(DublinCoreTypesService)
      .useValue(dublinCoreTypesServiceMock)
      .overrideProvider(DublinCoreItemsService)
      .useValue(dublinCoreItemsServiceMock)
      .overrideProvider(CategoryPlaylistItemsService)
      .useValue(categoryPlaylistItemsServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .compile();

    controller = module.get<CategoryPlaylistsController>(
      CategoryPlaylistsController,
    );
    categoryPlaylistRepository = module.get<Repository<CategoryPlaylist>>(
      CATEGORY_PLAYLIST_REPOSITORY_TOKEN,
    );
    categoryPlaylistsService = module.get<CategoryPlaylistsService>(
      CategoryPlaylistsService,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(categoryPlaylistRepository).toBeDefined();
  });

  describe('getCategoryPlaylistBackOfficeCategoryPlaylistItems', () => {
    const categoryPlaylistId = '1';
    const getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto: GetCategoryPlaylistBackOfficeCategoryPlaylistItemsDto =
      {
        languageIdentifier: 'fra',
      };

    it('should be defined', () => {
      expect(
        controller.getCategoryPlaylistBackOfficeCategoryPlaylistItems,
      ).toBeDefined();
    });

    it('should have @Get("back-office/:id/category-playlist-items") route', () => {
      const routeMetadata = Reflect.getMetadata(
        'path',
        controller.getCategoryPlaylistBackOfficeCategoryPlaylistItems,
      );
      expect(routeMetadata).toBe('back-office/:id/category-playlist-items');
    });

    it('should have @Auth() decorator', () => {
      const optionalAuthDecorator = Reflect.getMetadata(
        'auth',
        Object.getPrototypeOf(controller),
        'getCategoryPlaylistBackOfficeCategoryPlaylistItems',
      );
      expect(optionalAuthDecorator).toBe(true);
    });

    it('should use cache', async () => {
      const cachedMethodSpy = jest.spyOn(CachingUtil, 'cachedMethod');
      await controller.getCategoryPlaylistBackOfficeCategoryPlaylistItems(
        categoryPlaylistId,
        getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto,
      );
      expect(cachedMethodSpy).toHaveBeenCalled();
    });

    it('should call cachedMethod with correct parameters', async () => {
      const cachedMethodSpy = jest.spyOn(CachingUtil, 'cachedMethod');

      await controller.getCategoryPlaylistBackOfficeCategoryPlaylistItems(
        categoryPlaylistId,
        getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto,
      );

      expect(cachedMethodSpy).toHaveBeenCalledWith(
        'categoryPlaylistsService',
        'getCategoryPlaylistBackOfficeCategoryPlaylistItems',
        [
          categoryPlaylistId,
          getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto,
        ],
        expect.any(Function),
        [
          CategoryPlaylist,
          CategoryPlaylistItem,
          Item,
          DublinCoreItem,
          DublinCoreType,
          DublinCoreItemTranslation,
          DublinCoreTypeTranslation,
          Application,
          ApplicationTranslation,
        ],
      );
    });

    it('should call categoryPlaylistsService.getCategoryPlaylistBackOfficeCategoryPlaylistItems with correct arguments', async () => {
      jest.spyOn(
        categoryPlaylistsService,
        'getCategoryPlaylistBackOfficeCategoryPlaylistItems',
      );
      await controller.getCategoryPlaylistBackOfficeCategoryPlaylistItems(
        categoryPlaylistId,
        getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto,
      );

      expect(
        categoryPlaylistsService.getCategoryPlaylistBackOfficeCategoryPlaylistItems,
      ).toHaveBeenCalledWith(
        +categoryPlaylistId,
        getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto,
      );
    });
  });
});
