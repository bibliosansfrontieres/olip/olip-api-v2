import {
  Inject,
  Injectable,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { CreateCategoryPlaylistDto } from './dto/create-category-playlist.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryPlaylist } from './entities/category-playlist.entity';
import { FindManyOptions, FindOneOptions, In, Repository } from 'typeorm';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { CategoriesService } from 'src/categories/categories.service';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { PageOptions } from 'src/utils/classes/page-options';
import { GetCategoryOptionsDto } from 'src/categories/dto/get-category-options.dto';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { Category } from 'src/categories/entities/category.entity';
import { PageDto } from 'src/utils/classes/page.dto';
import { DeleteCategoryPlaylistDto } from './dto/delete-category-playlist.dto';
import { invalidateCache } from 'src/utils/caching.util';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { CategoryPlaylistItemsService } from 'src/category-playlist-items/category-playlist-items.service';
import { CreateCategoryPlaylistItemDto } from 'src/category-playlist-items/dto/create-category-playlist-item.dto';
import { OrderCategoryPlaylistsDto } from './dto/order-category-playlists.dto';
import { ApplicationsService } from 'src/applications/applications.service';
import { getApplicationIdsFromCategoryPlaylists } from 'src/utils/application/get-application-ids.util';
import { sortApplicationsTranslations } from 'src/utils/language/sort-translations/sort-applications-translations.util';
import { sortCategoryPlaylistsPlaylistsTranslations } from 'src/utils/language/sort-translations/sort-category-playlists-playlists-translations.util';
import { GetCategoryPlaylistBackOfficeCategoryPlaylistItemsDto } from './dto/get-category-playlist-back-office-category-playlist-items.dto';
import { GetCategoryPlaylistBackOfficeCategoryPlaylistItemsResponse } from './responses/get-category-playlist-back-office-category-playlist-items-response';
import { Application } from 'src/applications/entities/application.entity';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { getDublinCoreIdsFromCategoryPlaylistItems } from 'src/utils/dublin-core-type/get-dublin-core-type-ids.util';
import { sortDublinCoreTypesTranslations } from 'src/utils/language/sort-translations/sort-dublin-core-types-translations.util';

@Injectable()
export class CategoryPlaylistsService {
  constructor(
    @InjectRepository(CategoryPlaylist)
    private categoryPlaylistRepository: Repository<CategoryPlaylist>,
    @Inject(forwardRef(() => CategoryPlaylistItemsService))
    private categoryPlaylistItemsService: CategoryPlaylistItemsService,
    @Inject(forwardRef(() => PlaylistsService))
    private playlistsService: PlaylistsService,
    @Inject(forwardRef(() => CategoriesService))
    private categoriesService: CategoriesService,
    @Inject(forwardRef(() => ApplicationsService))
    private applicationsService: ApplicationsService,
    private dublinCoreTypesService: DublinCoreTypesService,
  ) {}

  /**
   * Retrieves category playlist items and their associated applications, dublinCoreTypes for back office purposes.
   *
   * @param {number} id - The ID of the category playlist.
   * @param {GetCategoryPlaylistBackOfficeCategoryPlaylistItemsDto} getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto - DTO containing options for retrieving category playlist items, including the language identifier for translations.
   * @returns {Promise<GetCategoryPlaylistBackOfficeCategoryPlaylistItemsResponse>} A promise that resolves to a response object containing the applications and category playlist items.
   */
  async getCategoryPlaylistBackOfficeCategoryPlaylistItems(
    id: number,
    getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto: GetCategoryPlaylistBackOfficeCategoryPlaylistItemsDto,
  ): Promise<GetCategoryPlaylistBackOfficeCategoryPlaylistItemsResponse> {
    const categoryPlaylistItems =
      await this.categoryPlaylistItemsService.getCategoryPlaylistCategoryPlaylistItems(
        id,
        getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto.languageIdentifier,
      );

    const dublinCoreTypes = await this.getCategoryPlaylistItemsDublinCoreTypes(
      categoryPlaylistItems,
      getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto.languageIdentifier,
    );

    const applications =
      await this.categoryPlaylistItemsService.getCategoryPlaylistItemsApplications(
        categoryPlaylistItems,
        getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto.languageIdentifier,
      );

    return new GetCategoryPlaylistBackOfficeCategoryPlaylistItemsResponse(
      applications,
      dublinCoreTypes,
      categoryPlaylistItems,
    );
  }

  async getCategoryPlaylistItemsDublinCoreTypes(
    categoryPlaylistItems: CategoryPlaylistItem[],
    languageIdentifier: string,
  ) {
    const dublinCoreTypeIds = getDublinCoreIdsFromCategoryPlaylistItems(
      categoryPlaylistItems,
    );

    const dublinCoreTypes = await this.dublinCoreTypesService.find({
      where: { id: In(dublinCoreTypeIds) },
      relations: {
        dublinCoreTypeTranslations: { language: true },
      },
      select: {
        id: true,
        image: true,
        dublinCoreTypeTranslations: {
          id: true,
          label: true,
          description: true,
          language: { identifier: true, label: true },
        },
      },
    });

    sortDublinCoreTypesTranslations(dublinCoreTypes, languageIdentifier);

    return dublinCoreTypes;
  }

  /**
   * Retrieve all categoryPlaylists from a given category, with playlist data and translations.
   * The results are ordered by the order of the categoryPlaylists.
   * The playlist translations are sorted by the given languageIdentifier first, then by the identifier of the language.
   * @param {number} categoryId the id of the category
   * @param {string} languageIdentifier the identifier of the language to prioritize the translations
   * @return {Promise<CategoryPlaylist[]>} an array of categoryPlaylists with playlist data and translations
   */
  async getCategoryCategoryPlaylists(
    categoryId: number,
    languageIdentifier: string,
  ): Promise<CategoryPlaylist[]> {
    const categoryPlaylistsQueryBuilder = this.categoryPlaylistRepository
      .createQueryBuilder('categoryPlaylist')
      .addSelect(['categoryPlaylist.id', 'categoryPlaylist.order'])
      .leftJoin('categoryPlaylist.category', 'category')
      .leftJoin('categoryPlaylist.playlist', 'playlist')
      .addSelect([
        'playlist.id',
        'playlist.image',
        'playlist.isInstalled',
        'playlist.isUpdateNeeded',
        'playlist.isBroken',
        'playlist.isManuallyInstalled',
        'playlist.version',
        'playlist.size',
      ])
      .leftJoin('playlist.playlistTranslations', 'playlistTranslations')
      .addSelect([
        'playlistTranslations.id',
        'playlistTranslations.title',
        'playlistTranslations.description',
      ])
      .leftJoin('playlistTranslations.language', 'language')
      .addSelect(['language.identifier', 'language.label'])
      .leftJoin('playlist.application', 'application')
      .addSelect('application.id')
      .orderBy('categoryPlaylist.order', 'ASC')
      .where('category.id = :categoryId', { categoryId });

    const categoryPlaylists = await categoryPlaylistsQueryBuilder.getMany();

    sortCategoryPlaylistsPlaylistsTranslations(
      categoryPlaylists,
      languageIdentifier,
    );

    return categoryPlaylists;
  }

  /**
   * Retrieve all applications associated with the given categoryPlaylists, with translations.
   * The translations are sorted by the specified languageIdentifier.
   *
   * @param {CategoryPlaylist[]} categoryPlaylists - The categoryPlaylists to retrieve applications from.
   * @param {string} languageIdentifier - The identifier of the language to prioritize the translations.
   * @return {Promise<Application[]>} A promise that resolves to an array of applications with translations.
   */
  async getCategoryPlaylistsApplications(
    categoryPlaylists: CategoryPlaylist[],
    languageIdentifier: string,
  ): Promise<Application[]> {
    const applicationIds =
      getApplicationIdsFromCategoryPlaylists(categoryPlaylists);

    const applications = await this.applicationsService.find({
      where: { id: In(applicationIds) },
      relations: {
        applicationTranslations: { language: true },
        applicationType: { applicationTypeTranslations: { language: true } },
      },
      select: {
        id: true,
        name: true,
        displayName: true,
        version: true,
        logo: true,
        image: true,
        url: true,
        size: true,
        isInstalled: true,
        isDownloading: true,
        isInstalling: true,
        isUp: true,
        isUpdateNeeded: true,
        isUpdating: true,
        isOlip: true,
        applicationType: {
          id: true,
          name: true,
          applicationTypeTranslations: {
            id: true,
            label: true,
            description: true,
            language: { identifier: true, label: true },
          },
        },
        applicationTranslations: {
          id: true,
          longDescription: true,
          shortDescription: true,
          language: {
            identifier: true,
            label: true,
          },
        },
      },
    });

    sortApplicationsTranslations(applications, languageIdentifier);

    return applications;
  }

  private async getLastOrder() {
    const query =
      this.categoryPlaylistRepository.createQueryBuilder('categoryPlaylist');
    query.select('MAX(categoryPlaylist.order)', 'order');
    const lastOrder = await query.getRawOne();
    return lastOrder.order || 0;
  }

  async create(createCategoryPlaylistDto: CreateCategoryPlaylistDto) {
    const order = await this.getLastOrder();
    const category = await this.categoriesService.findOne({
      where: { id: createCategoryPlaylistDto.categoryId },
      relations: { categoryPlaylists: { playlist: true } },
    });
    if (category === null) return null;
    const playlist = await this.playlistsService.findOne({
      where: { id: createCategoryPlaylistDto.playlistId },
      relations: { items: true },
    });
    if (playlist === null) return null;

    let isPlaylistAlreadyInside = false;
    for (const categoryPlaylist of category.categoryPlaylists) {
      if (
        categoryPlaylist.playlist.id === createCategoryPlaylistDto.playlistId
      ) {
        isPlaylistAlreadyInside = true;
      }
    }

    if (isPlaylistAlreadyInside) {
      return;
    }

    let categoryPlaylist = new CategoryPlaylist(playlist, category, order + 1);
    categoryPlaylist =
      await this.categoryPlaylistRepository.save(categoryPlaylist);
    // create categoryPlaylistItems
    for (const item of playlist.items) {
      const createCategoryPlaylistItemDto: CreateCategoryPlaylistItemDto = {
        itemId: item.id,
        categoryPlaylistId: categoryPlaylist.id,
      };
      await this.categoryPlaylistItemsService.create(
        createCategoryPlaylistItemDto,
      );
    }
    await invalidateCache([Category, Playlist, CategoryPlaylist]);
    return categoryPlaylist;
  }

  async findOne(options?: FindOneOptions<CategoryPlaylist>) {
    return this.categoryPlaylistRepository.findOne(options);
  }

  async find(options?: FindManyOptions<CategoryPlaylist>) {
    return this.categoryPlaylistRepository.find(options);
  }

  async getCategoryPlaylistsPagination(
    pageOptionsDto: PageOptionsDto,
    getCategoryOptionsDto: GetCategoryOptionsDto,
    category: Category,
  ) {
    const pageOptions = new PageOptions(pageOptionsDto);
    // First query for categoryPlaylistItems matching item title
    const categoryPlaylistItemsQuery = this.categoryPlaylistRepository
      .createQueryBuilder('categoryPlaylist')
      .leftJoinAndSelect(
        'categoryPlaylist.categoryPlaylistItems',
        'categoryPlaylistItems',
        'categoryPlaylistItems.categoryPlaylistId = categoryPlaylist.id',
      )
      .leftJoinAndSelect(
        'categoryPlaylistItems.item',
        'item',
        'item.id = categoryPlaylistItems.itemId',
      )
      .leftJoinAndSelect(
        'item.dublinCoreItem',
        'dublinCoreItem',
        'dublinCoreItem.itemId = item.id',
      )
      .leftJoinAndSelect(
        'dublinCoreItem.dublinCoreItemTranslations',
        'dublinCoreItemTranslations',
        'dublinCoreItemTranslations.dublinCoreItemId = dublinCoreItem.id',
      )
      .where('categoryPlaylist.categoryId = :categoryId', {
        categoryId: category.id,
      });

    if (getCategoryOptionsDto.titleLike) {
      categoryPlaylistItemsQuery.andWhere(
        'dublinCoreItemTranslations.title LIKE :titleLike',
        {
          titleLike: `%${getCategoryOptionsDto.titleLike}%`,
        },
      );
    }

    // Second query for categoryPlaylistItems matching playlist title
    const playlistItemsQuery = this.categoryPlaylistRepository
      .createQueryBuilder('categoryPlaylist')
      .leftJoinAndSelect(
        'categoryPlaylist.categoryPlaylistItems',
        'categoryPlaylistItems',
        'categoryPlaylistItems.categoryPlaylistId = categoryPlaylist.id',
      )
      .leftJoinAndSelect(
        'categoryPlaylist.playlist',
        'playlist',
        'playlist.id = categoryPlaylist.playlistId',
      )
      .leftJoinAndSelect(
        'playlist.playlistTranslations',
        'playlistTranslations',
        'playlistTranslations.playlistId = playlist.id',
      )
      .where('categoryPlaylist.categoryId = :categoryId', {
        categoryId: category.id,
      });

    if (getCategoryOptionsDto.titleLike) {
      playlistItemsQuery.andWhere(
        'playlistTranslations.title LIKE :titleLike',
        {
          titleLike: `%${getCategoryOptionsDto.titleLike}%`,
        },
      );
    }

    const results = [
      ...(await categoryPlaylistItemsQuery.getRawMany()),
      ...(await playlistItemsQuery.getRawMany()),
    ];

    const uniqueIds = new Set<number>();
    results.filter((result) => {
      if (!uniqueIds.has(result.categoryPlaylist_id)) {
        uniqueIds.add(result.categoryPlaylist_id);
        return true;
      }
      return false;
    });

    const categoryPlaylistsResult = await getManyAndCount<CategoryPlaylist>(
      this.categoryPlaylistRepository
        .createQueryBuilder('categoryPlaylist')
        .innerJoinAndSelect(
          'categoryPlaylist.categoryPlaylistItems',
          'categoryPlaylistItems',
          'categoryPlaylistItems.categoryPlaylistId = categoryPlaylist.id',
        )
        // Joins Playlist from CategoryPlaylist
        .innerJoinAndSelect(
          'categoryPlaylist.playlist',
          'playlist',
          'playlist.id = categoryPlaylist.playlistId',
        )
        // Joins Playlist from CategoryPlaylist
        .innerJoinAndSelect(
          'playlist.application',
          'playlistApplication',
          'playlistApplication.id = playlist.applicationId',
        )
        // Joins Playlist from CategoryPlaylist
        .innerJoinAndSelect(
          'playlistApplication.applicationType',
          'playlistApplicationType',
          'playlistApplicationType.id = playlistApplication.applicationTypeId',
        )
        // Joins Item from CategoryPlaylistItems
        .innerJoinAndSelect(
          'categoryPlaylistItems.item',
          'item',
          'item.id = categoryPlaylistItems.itemId',
        )
        // Joins DublinCoreItem from Item
        .innerJoinAndSelect(
          'item.dublinCoreItem',
          'dublinCoreItem',
          'dublinCoreItem.itemId = item.id',
        )
        // Joins DublinCoreItem from Item
        .innerJoinAndSelect(
          'item.application',
          'application',
          'application.id = item.applicationId',
        )
        // Joins DublinCoreItem from Item
        .innerJoinAndSelect(
          'application.applicationType',
          'applicationType',
          'applicationType.id = application.applicationTypeId',
        )
        // Joins DublinCoreType from DublinCoreItem
        .innerJoinAndSelect(
          'dublinCoreItem.dublinCoreType',
          'dublinCoreType',
          'dublinCoreType.id = dublinCoreItem.dublinCoreTypeId',
        )
        // Joins DublinCoreType from DublinCoreItem
        .innerJoinAndSelect(
          'dublinCoreType.dublinCoreTypeTranslations',
          'dublinCoreTypeTranslation',
          'dublinCoreTypeTranslation.dublinCoreTypeId = dublinCoreType.id',
        )
        // Joins DublinCoreType from DublinCoreItem
        .innerJoinAndSelect(
          'dublinCoreTypeTranslation.language',
          'dublinCoreTypeTranslationLanguage',
          'dublinCoreTypeTranslation.languageId = dublinCoreTypeTranslationLanguage.id',
        )
        // Joins PlaylistTranslations from Playlist
        .innerJoinAndSelect(
          'playlist.playlistTranslations',
          'playlistTranslations',
          'playlistTranslations.playlistId = playlist.id',
        )
        // Joins DublinCoreItemTranslations from DublinCoreItem
        .innerJoinAndSelect(
          'dublinCoreItem.dublinCoreItemTranslations',
          'dublinCoreItemTranslations',
          'dublinCoreItemTranslations.dublinCoreItemId = dublinCoreItem.id',
        )
        // Joins DublinCoreItemTranslations from DublinCoreItem
        .innerJoinAndSelect(
          'dublinCoreItemTranslations.language',
          'language',
          'dublinCoreItemTranslations.languageId = language.id',
        )
        .where('categoryPlaylist.id IN (:...categoryPlaylistIds)', {
          categoryPlaylistIds: Array.from(uniqueIds),
        }),
      ['categoryPlaylist.playlist.id'],
      false,
    );

    const totalCategoryPlaylistsCount = categoryPlaylistsResult[0];
    let categoryPlaylists = categoryPlaylistsResult[1];

    categoryPlaylists = await this.getPlaylistsTranslations(
      getCategoryOptionsDto.languageIdentifier,
      categoryPlaylists,
    );

    categoryPlaylists = await this.getDublinCoreTypesTranslations(
      getCategoryOptionsDto.languageIdentifier,
      categoryPlaylists,
    );

    categoryPlaylists = await this.getDublinCoreItemTranslations(
      getCategoryOptionsDto.languageIdentifier,
      categoryPlaylists,
    );

    for (const categoryPlaylist of categoryPlaylists) {
      await this.applicationsService.getApplicationsTranslations(
        [categoryPlaylist.playlist.application],
        getCategoryOptionsDto.languageIdentifier,
      );
      for (const categoryPlaylistItem of categoryPlaylist.categoryPlaylistItems) {
        await this.applicationsService.getApplicationsTranslations(
          [categoryPlaylistItem.item.application],
          getCategoryOptionsDto.languageIdentifier,
        );
      }
    }

    return new PageDto(
      categoryPlaylists,
      totalCategoryPlaylistsCount,
      pageOptions,
    );
  }

  async getPlaylistsTranslations(
    languageIdentifier: string,
    categoryPlaylists: CategoryPlaylist[],
  ) {
    for (const categoryPlaylist of categoryPlaylists) {
      const playlistTranslation = await this.playlistsService.getTranslation(
        categoryPlaylist.playlist,
        languageIdentifier,
      );
      categoryPlaylist.playlist.playlistTranslations = [playlistTranslation];
    }
    return categoryPlaylists;
  }

  private async getDublinCoreTypesTranslations(
    languageIdentifier: string,
    categoryPlaylists: CategoryPlaylist[],
  ) {
    for (const categoryPlaylist of categoryPlaylists) {
      for (const categoryPlaylistItem of categoryPlaylist.categoryPlaylistItems) {
        let findTranslation =
          categoryPlaylistItem.item.dublinCoreItem.dublinCoreType.dublinCoreTypeTranslations.find(
            (dublinCoreTypeTranslation) =>
              dublinCoreTypeTranslation.language.identifier ===
              languageIdentifier,
          );
        if (!findTranslation) {
          findTranslation =
            categoryPlaylistItem.item.dublinCoreItem.dublinCoreType.dublinCoreTypeTranslations.find(
              (dublinCoreTypeTranslation) =>
                dublinCoreTypeTranslation.language.identifier === 'eng',
            );
        }
        if (!findTranslation) {
          findTranslation =
            categoryPlaylistItem.item.dublinCoreItem.dublinCoreType
              .dublinCoreTypeTranslations[0];
        }
        categoryPlaylistItem.item.dublinCoreItem.dublinCoreType.dublinCoreTypeTranslations =
          [findTranslation];
      }
    }
    return categoryPlaylists;
  }

  private async getDublinCoreItemTranslations(
    languageIdentifier: string,
    categoryPlaylists: CategoryPlaylist[],
  ) {
    for (const categoryPlaylist of categoryPlaylists) {
      for (const categoryPlaylistItem of categoryPlaylist.categoryPlaylistItems) {
        let findTranslation =
          categoryPlaylistItem.item.dublinCoreItem.dublinCoreItemTranslations.find(
            (dublinCoreItemTranslation) =>
              dublinCoreItemTranslation.language.identifier ===
              languageIdentifier,
          );
        if (!findTranslation) {
          findTranslation =
            categoryPlaylistItem.item.dublinCoreItem.dublinCoreItemTranslations.find(
              (dublinCoreItemTranslation) =>
                dublinCoreItemTranslation.language.identifier === 'eng',
            );
        }
        if (!findTranslation) {
          findTranslation =
            categoryPlaylistItem.item.dublinCoreItem
              .dublinCoreItemTranslations[0];
        }
        categoryPlaylistItem.item.dublinCoreItem.dublinCoreItemTranslations = [
          findTranslation,
        ];
      }
    }
    return categoryPlaylists;
  }

  async delete(deleteCategoryPlaylistDto: DeleteCategoryPlaylistDto) {
    if (!deleteCategoryPlaylistDto) return;

    const categoryPlaylists = [];

    for (const id of deleteCategoryPlaylistDto.ids) {
      const categoryPlaylist = await this.findOne({ where: { id } });

      if (categoryPlaylist === null) {
        throw new NotFoundException(`category ID ${id} not found`);
      }
      categoryPlaylists.push(categoryPlaylist);
    }

    await this.categoryPlaylistRepository.remove(categoryPlaylists);
    await invalidateCache([CategoryPlaylist]);
  }

  async order(orderCategoryPlaylistsDto: OrderCategoryPlaylistsDto) {
    const categoryPlaylists = await this.categoryPlaylistRepository.find({
      where: { category: { id: orderCategoryPlaylistsDto.categoryId } },
    });

    // Sort the existing categories based on the order of category IDs
    const sortedCategoryPlaylists =
      orderCategoryPlaylistsDto.categoryPlaylistIds
        .map((categoryPlaylistId) =>
          categoryPlaylists.find((cP) => cP.id === categoryPlaylistId),
        )
        .filter(Boolean);

    for (const categoryPlaylist of categoryPlaylists) {
      if (!sortedCategoryPlaylists.includes(categoryPlaylist)) {
        sortedCategoryPlaylists.push(categoryPlaylist);
      }
    }

    for (let i = 0; i < sortedCategoryPlaylists.length; i++) {
      const categoryPlaylist = sortedCategoryPlaylists[i];
      categoryPlaylist.order = i + 1;
      await this.categoryPlaylistRepository.save(categoryPlaylist);
    }

    await invalidateCache([CategoryPlaylist]);
  }
}
