import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';

export class OrderCategoryPlaylistsDto {
  @ApiProperty({ example: 1 })
  @IsNumber()
  categoryId: number;

  @ApiProperty({ example: [1, 2, 3] })
  @IsNumber(undefined, { each: true })
  categoryPlaylistIds: number[];
}
