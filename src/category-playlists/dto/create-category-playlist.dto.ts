import { IsNumber } from 'class-validator';

export class CreateCategoryPlaylistDto {
  @IsNumber()
  categoryId: number;

  @IsNumber()
  playlistId: string;
}
