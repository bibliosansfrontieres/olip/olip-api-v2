import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsArray, IsNumber } from 'class-validator';

export class DeleteCategoryPlaylistDto {
  @ApiPropertyOptional({ example: [1, 2] })
  @IsArray()
  @IsNumber({}, { each: true })
  ids: number[];
}
