import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class GetCategoryPlaylistBackOfficeCategoryPlaylistItemsDto {
  @ApiProperty({ example: 'eng' })
  @IsString()
  languageIdentifier: string;
}
