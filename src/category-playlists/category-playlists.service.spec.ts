import { Test, TestingModule } from '@nestjs/testing';
import { CategoryPlaylistsService } from './category-playlists.service';
import { Repository } from 'typeorm';
import { CategoryPlaylist } from './entities/category-playlist.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { CategoriesService } from 'src/categories/categories.service';
import { playlistsServiceMock } from 'src/tests/mocks/providers/playlists-service.mock';
import { categoriesServiceMock } from 'src/tests/mocks/providers/categories-service.mock';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { dublinCoreTypesServiceMock } from 'src/tests/mocks/providers/dublin-core-types-service.mock';
import { DublinCoreItemsService } from 'src/dublin-core-items/dublin-core-items.service';
import { dublinCoreItemsServiceMock } from 'src/tests/mocks/providers/dublin-core-items-service.mock';
import { CategoryPlaylistItemsService } from 'src/category-playlist-items/category-playlist-items.service';
import { categoryPlaylistItemsServiceMock } from 'src/tests/mocks/providers/category-playlist-items-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';

describe('CategoryPlaylistsService', () => {
  let service: CategoryPlaylistsService;

  const CATEGORY_PLAYLIST_REPOSITORY_TOKEN =
    getRepositoryToken(CategoryPlaylist);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: CATEGORY_PLAYLIST_REPOSITORY_TOKEN,
          useClass: Repository<CategoryPlaylist>,
        },
        CategoryPlaylistsService,
        PlaylistsService,
        CategoriesService,
        DublinCoreTypesService,
        DublinCoreItemsService,
        CategoryPlaylistItemsService,
        ApplicationsService,
      ],
    })
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(DublinCoreTypesService)
      .useValue(dublinCoreTypesServiceMock)
      .overrideProvider(DublinCoreItemsService)
      .useValue(dublinCoreItemsServiceMock)
      .overrideProvider(CategoryPlaylistItemsService)
      .useValue(categoryPlaylistItemsServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .compile();

    service = module.get<CategoryPlaylistsService>(CategoryPlaylistsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
