import { Module, forwardRef } from '@nestjs/common';
import { CategoryPlaylistsService } from './category-playlists.service';
import { CategoryPlaylistsController } from './category-playlists.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryPlaylist } from './entities/category-playlist.entity';
import { PlaylistsModule } from 'src/playlists/playlists.module';
import { CategoriesModule } from 'src/categories/categories.module';
import { DublinCoreTypesModule } from 'src/dublin-core-types/dublin-core-types.module';
import { DublinCoreItemsModule } from 'src/dublin-core-items/dublin-core-items.module';
import { CategoryPlaylistItemsModule } from 'src/category-playlist-items/category-playlist-items.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { ConfigModule } from '@nestjs/config';
import { ApplicationsModule } from 'src/applications/applications.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([CategoryPlaylist]),
    forwardRef(() => PlaylistsModule),
    forwardRef(() => CategoriesModule),
    DublinCoreTypesModule,
    DublinCoreItemsModule,
    forwardRef(() => CategoryPlaylistItemsModule),
    JwtModule,
    UsersModule,
    ConfigModule,
    forwardRef(() => ApplicationsModule),
  ],
  controllers: [CategoryPlaylistsController],
  providers: [CategoryPlaylistsService],
  exports: [CategoryPlaylistsService],
})
export class CategoryPlaylistsModule {}
