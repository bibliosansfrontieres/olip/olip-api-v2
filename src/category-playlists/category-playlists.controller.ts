import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Query,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { CategoryPlaylistsService } from './category-playlists.service';
import { DeleteCategoryPlaylistDto } from './dto/delete-category-playlist.dto';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { OrderCategoryPlaylistsDto } from './dto/order-category-playlists.dto';
import { GetCategoryPlaylistBackOfficeCategoryPlaylistItemsDto } from './dto/get-category-playlist-back-office-category-playlist-items.dto';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { Item } from 'src/items/entities/item.entity';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';
import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';
import { DublinCoreItemTranslation } from 'src/dublin-core-items/entities/dublin-core-item-translation';
import { DublinCoreTypeTranslation } from 'src/dublin-core-types/entities/dublin-core-type-translation.entity';
import { cachedMethod } from 'src/utils/caching.util';
import { Application } from 'src/applications/entities/application.entity';
import { ApplicationTranslation } from 'src/applications/entities/application-translation.entity';
import { GetCategoryPlaylistBackOfficeCategoryPlaylistItemsResponse } from './responses/get-category-playlist-back-office-category-playlist-items-response';
import { CategoryPlaylist } from './entities/category-playlist.entity';

@Controller('api/category-playlists')
@ApiTags('category-playlists')
export class CategoryPlaylistsController {
  constructor(private categoryPlaylistsService: CategoryPlaylistsService) {}

  @Auth()
  @ApiOperation({ summary: 'Get categoryPlaylistItems of a categoryPlaylist' })
  @ApiOkResponse({
    type: GetCategoryPlaylistBackOfficeCategoryPlaylistItemsResponse,
  })
  @Get('back-office/:id/category-playlist-items')
  getCategoryPlaylistBackOfficeCategoryPlaylistItems(
    @Param('id') id: string,
    @Query()
    getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto: GetCategoryPlaylistBackOfficeCategoryPlaylistItemsDto,
  ) {
    return cachedMethod(
      'categoryPlaylistsService',
      'getCategoryPlaylistBackOfficeCategoryPlaylistItems',
      [id, getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto],
      async () => {
        return this.categoryPlaylistsService.getCategoryPlaylistBackOfficeCategoryPlaylistItems(
          +id,
          getCategoryPlaylistBackOfficeCategoryPlaylistItemsDto,
        );
      },
      [
        CategoryPlaylist,
        CategoryPlaylistItem,
        Item,
        DublinCoreItem,
        DublinCoreType,
        DublinCoreItemTranslation,
        DublinCoreTypeTranslation,
        Application,
        ApplicationTranslation,
      ],
    );
  }

  @Delete()
  @ApiOperation({ summary: 'Delete category playlists' })
  @ApiBody({ type: DeleteCategoryPlaylistDto })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  deleteCategoryPlaylist(
    @Body() deleteCategoryPlaylistDto: DeleteCategoryPlaylistDto,
  ) {
    return this.categoryPlaylistsService.delete(deleteCategoryPlaylistDto);
  }

  @Auth(PermissionEnum.UPDATE_CATEGORY)
  @ApiOperation({ summary: 'Order category playlists' })
  @ApiBody({ type: OrderCategoryPlaylistsDto })
  @ApiCreatedResponse({ description: 'Category playlists ordered' })
  @ApiBadRequestResponse()
  @Patch('order')
  order(@Body() orderCategoryPlaylistsDto: OrderCategoryPlaylistsDto) {
    return this.categoryPlaylistsService.order(orderCategoryPlaylistsDto);
  }
}
