import { ApiProperty } from '@nestjs/swagger';
import { Application } from 'src/applications/entities/application.entity';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';
import { applicationTranslationMock } from 'src/tests/mocks/objects/application-translation.mock';
import { applicationMock } from 'src/tests/mocks/objects/application.mock';
import { categoryPlaylistItemMock } from 'src/tests/mocks/objects/category-playlist-item.mock';
import { dublinCoreItemTranslationMock } from 'src/tests/mocks/objects/dublin-core-item-translation.mock';
import { dublinCoreItemMock } from 'src/tests/mocks/objects/dublin-core-item.mock';
import { dublinCoreTypeTranslationMock } from 'src/tests/mocks/objects/dublin-core-type-translation.mock';
import { dublinCoreTypeMock } from 'src/tests/mocks/objects/dublin-core-type.mock';
import { itemMock } from 'src/tests/mocks/objects/item.mock';
import { simplifiedApplicationMock } from 'src/tests/mocks/objects/simplified-application.mock';
import { simplifiedDublinCoreTypeMock } from 'src/tests/mocks/objects/simplified-dublin-core-type.mock';
import { simplifiedLanguageMock } from 'src/tests/mocks/objects/simplified-language.mock';

export class GetCategoryPlaylistBackOfficeCategoryPlaylistItemsResponse {
  @ApiProperty({
    example: [
      applicationMock({
        applicationTranslations: [
          applicationTranslationMock({ language: simplifiedLanguageMock() }),
        ],
      }),
    ],
  })
  applications: Application[];

  @ApiProperty({
    example: [
      dublinCoreTypeMock({
        dublinCoreTypeTranslations: [
          dublinCoreTypeTranslationMock({ language: simplifiedLanguageMock() }),
        ],
      }),
    ],
  })
  dublinCoreTypes: DublinCoreType[];

  @ApiProperty({
    example: [
      categoryPlaylistItemMock({
        item: itemMock({
          dublinCoreItem: dublinCoreItemMock({
            dublinCoreItemTranslations: [
              dublinCoreItemTranslationMock({
                language: simplifiedLanguageMock(),
              }),
            ],
            dublinCoreType: simplifiedDublinCoreTypeMock(),
          }),
          application: simplifiedApplicationMock(),
        }),
      }),
    ],
  })
  categoryPlaylistItems: CategoryPlaylistItem[];

  constructor(
    applications: Application[],
    dublinCoreTypes: DublinCoreType[],
    categoryPlaylistItems: CategoryPlaylistItem[],
  ) {
    this.applications = applications;
    this.dublinCoreTypes = dublinCoreTypes;
    this.categoryPlaylistItems = categoryPlaylistItems;
  }
}
