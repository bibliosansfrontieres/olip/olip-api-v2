import { Playlist } from 'src/playlists/entities/playlist.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Category } from 'src/categories/entities/category.entity';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';

@Entity()
export class CategoryPlaylist {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  order: number;

  @ManyToOne(() => Playlist, (playlist) => playlist.categoryPlaylists, {
    onDelete: 'CASCADE',
  })
  playlist: Playlist;

  @ManyToOne(() => Category, (category) => category.categoryPlaylists, {
    onDelete: 'CASCADE',
  })
  category: Category;

  @OneToMany(
    () => CategoryPlaylistItem,
    (categoryPlaylistItem) => categoryPlaylistItem.categoryPlaylist,
  )
  categoryPlaylistItems: CategoryPlaylistItem[];

  constructor(playlist: Playlist, category: Category, order: number) {
    this.playlist = playlist;
    this.category = category;
    this.order = order;
  }
}
