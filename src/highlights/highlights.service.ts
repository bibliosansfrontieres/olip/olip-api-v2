import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Highlight } from './entities/highlight.entity';
import { Not, Repository } from 'typeorm';
import { CreateHighlightDto } from './dto/create-highlight.dto';
import { HighlightTranslation } from './entities/highlight-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';
import { HighlightContentsService } from 'src/highlight-contents/highlight-contents.service';
import { Language } from 'src/languages/entities/language.entity';
import { CreateHighlightTranslationDto } from './dto/create-highlight-translation.dto';
import { GetHighlightOptionsDto } from './dto/get-highlight-options.dto';
import { DublinCoreItemsService } from 'src/dublin-core-items/dublin-core-items.service';
import { GetHightlightDto } from './dto/get-highlight.dto';
import { ApplicationsService } from 'src/applications/applications.service';

@Injectable()
export class HighlightsService {
  constructor(
    @InjectRepository(Highlight)
    private highlightsRepository: Repository<Highlight>,
    @InjectRepository(HighlightTranslation)
    private highlightTranslationsRepository: Repository<HighlightTranslation>,
    private languagesService: LanguagesService,
    private highlightContentService: HighlightContentsService,
    private dublinCoreItemsService: DublinCoreItemsService,
    private applicationsService: ApplicationsService,
  ) {}

  /**
   * Counts the number of highlights in the repository.
   *
   * @return {Promise<number>} The total count of highlights.
   */
  count(): Promise<number> {
    return this.highlightsRepository.count();
  }

  /**
   * Checks the languages for the given highlight translations.
   * It throws an error if one language is not found.
   *
   * @param {CreateHighlightTranslationDto[]} createHighlightTranslations - The list of highlight translations to check.
   * @return {Promise<Language[]>} - A promise that resolves to an array of languages.
   */
  private async checkLanguages(
    createHighlightTranslations: CreateHighlightTranslationDto[],
  ): Promise<Language[]> {
    const languages: Language[] = [];
    for (let i = 0; i < createHighlightTranslations.length; i++) {
      const createHighlightTranslationDto = createHighlightTranslations[i];
      const language = await this.languagesService.findOne({
        where: { identifier: createHighlightTranslationDto.languageIdentifier },
      });
      if (language === null) {
        throw new NotFoundException(
          `Language ${createHighlightTranslationDto.languageIdentifier} not found`,
        );
      }
      languages.push(language);
    }
    return languages;
  }

  /**
   * Remove old highlights.
   *
   * @param {Highlight} actualHighlight - The actual highlight.
   * @return {Promise<void>} Returns a promise that resolves when the old highlights are removed.
   */
  private async removeOldHighlights(actualHighlight: Highlight): Promise<void> {
    const olderHighlights = await this.highlightsRepository.find({
      where: { id: Not(actualHighlight.id) },
    });
    if (olderHighlights.length === 0) return;
    await this.highlightsRepository.remove(olderHighlights);
  }

  /**
   * Copies the highlight contents from an old highlight to the current one.
   *
   * @param {Highlight} oldHighlight - The old highlight object.
   */
  private async copyOldHighlightContents(oldHighlight: Highlight) {
    if (oldHighlight === null) return;
    const oldHighlightContents = oldHighlight.highlightContents;
    for (let i = 0; i < oldHighlightContents.length; i++) {
      const oldHighlightContent = oldHighlightContents[i];
      await this.highlightContentService.create({
        itemId: oldHighlightContent.item.id.toString(),
        order: oldHighlightContent.order.toString(),
      });
    }
  }

  /**
   * Creates a new highlight with the given data.
   *
   * @param {CreateHighlightDto} createHighlightDto - The data for creating the highlight.
   * @return {Promise<Highlight>} The newly created highlight.
   */
  async create(createHighlightDto: CreateHighlightDto): Promise<Highlight> {
    const createHighlightTranslations =
      createHighlightDto.createHighlightTranslations;
    const languages = await this.checkLanguages(createHighlightTranslations);
    const oldHighlight = await this.get();

    let highlight = new Highlight();
    highlight = await this.highlightsRepository.save(highlight);
    const highlightTranslations = [];

    for (let i = 0; i < createHighlightTranslations.length; i++) {
      const createHighlightTranslationDto = createHighlightTranslations[i];
      let highlightTranslation = new HighlightTranslation(
        createHighlightTranslationDto,
        languages[i],
        highlight,
      );
      highlightTranslation =
        await this.highlightTranslationsRepository.save(highlightTranslation);
      delete highlightTranslation.highlight;
      highlightTranslations.push(highlightTranslation);
    }

    await this.copyOldHighlightContents(oldHighlight);
    await this.removeOldHighlights(highlight);

    highlight.highlightTranslations = highlightTranslations;
    return new GetHightlightDto(highlight);
  }

  async get(getHighlightOptionsDto?: GetHighlightOptionsDto) {
    if (getHighlightOptionsDto?.languageIdentifier !== undefined) {
      const highlights = await this.highlightsRepository.find({
        order: { id: 'DESC' },
        relations: {
          highlightContents: {
            item: {
              dublinCoreItem: { dublinCoreType: true },
              application: { applicationType: true },
            },
          },
        },
      });
      if (highlights.length === 0) return null;
      const highlight = highlights[0];
      await this.getHighlightTranslation(
        highlight,
        getHighlightOptionsDto.languageIdentifier,
      );
      await this.getItemsTranslation(
        highlight,
        getHighlightOptionsDto.itemsLanguageIdentifier,
      );
      return highlight;
    } else {
      const highlights = await this.highlightsRepository.find({
        order: { id: 'DESC' },
        relations: {
          highlightTranslations: {
            language: true,
          },
          highlightContents: {
            item: {
              dublinCoreItem: { dublinCoreType: true },
              application: { applicationType: true },
            },
          },
        },
      });
      if (highlights.length === 0) return null;
      const highlight = highlights[0];
      if (getHighlightOptionsDto) {
        await this.getItemsTranslation(
          highlight,
          getHighlightOptionsDto.itemsLanguageIdentifier,
        );
        for (const content of highlight.highlightContents) {
          await this.applicationsService.getApplicationsTranslations(
            [content.item.application],
            getHighlightOptionsDto.itemsLanguageIdentifier,
          );
        }
      }
      return new GetHightlightDto(highlight);
    }
  }

  private async getItemsTranslation(
    highlight: Highlight,
    languageIdentifier: string,
  ) {
    for (const highlightContent of highlight.highlightContents) {
      highlightContent.item.dublinCoreItem.dublinCoreItemTranslations = [
        await this.dublinCoreItemsService.getTranslation(
          highlightContent.item.dublinCoreItem,
          languageIdentifier,
        ),
      ];
    }
  }

  private async getHighlightTranslation(
    highlight: Highlight,
    languageIdentifier: string,
  ) {
    highlight.highlightTranslations = [
      await this.languagesService.getTranslation(
        'highlight',
        highlight,
        this.highlightTranslationsRepository,
        languageIdentifier,
      ),
    ];
  }
}
