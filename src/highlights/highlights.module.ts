import { Module } from '@nestjs/common';
import { HighlightsService } from './highlights.service';
import { HighlightsController } from './highlights.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Highlight } from './entities/highlight.entity';
import { HighlightTranslation } from './entities/highlight-translation.entity';
import { LanguagesModule } from 'src/languages/languages.module';
import { HighlightContentsModule } from 'src/highlight-contents/highlight-contents.module';
import { DublinCoreItemsModule } from 'src/dublin-core-items/dublin-core-items.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { ConfigModule } from '@nestjs/config';
import { ApplicationsModule } from 'src/applications/applications.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Highlight, HighlightTranslation]),
    LanguagesModule,
    HighlightContentsModule,
    DublinCoreItemsModule,
    JwtModule,
    UsersModule,
    ConfigModule,
    ApplicationsModule,
  ],
  controllers: [HighlightsController],
  providers: [HighlightsService],
  exports: [HighlightsService],
})
export class HighlightsModule {}
