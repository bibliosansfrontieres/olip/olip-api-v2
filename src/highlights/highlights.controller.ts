import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { CreateHighlightDto } from './dto/create-highlight.dto';
import { HighlightsService } from './highlights.service';
import {
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { GetHighlightOptionsDto } from './dto/get-highlight-options.dto';
import { GetHightlightDto } from './dto/get-highlight.dto';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';

@ApiTags('highlights')
@Controller('api/highlights')
export class HighlightsController {
  constructor(private highlightsService: HighlightsService) {}

  @Auth(PermissionEnum.UPDATE_HIGHLIGHT)
  @ApiOperation({ summary: 'Create/Update a highlight' })
  @ApiCreatedResponse({ description: 'Highlight created/updated' })
  @Post()
  create(@Body() createHighlightDto: CreateHighlightDto) {
    return this.highlightsService.create(createHighlightDto);
  }

  @ApiOperation({ summary: 'Get highlight' })
  @ApiNotFoundResponse({ description: 'Highlight not found' })
  @ApiOkResponse({ type: GetHightlightDto })
  @Get()
  get(@Query() getHighlightOptionsDto: GetHighlightOptionsDto) {
    return this.highlightsService.get(getHighlightOptionsDto);
  }
}
