import { Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { HighlightTranslation } from './highlight-translation.entity';
import { HighlightContent } from 'src/highlight-contents/entities/highlight-content.entity';

@Entity()
export class Highlight {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany(
    () => HighlightTranslation,
    (highlightTranslation) => highlightTranslation.highlight,
  )
  highlightTranslations: HighlightTranslation[];

  @OneToMany(
    () => HighlightContent,
    (highlightContent) => highlightContent.highlight,
  )
  highlightContents: HighlightContent[];
}
