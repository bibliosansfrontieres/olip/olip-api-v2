import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Highlight } from './highlight.entity';
import { Language } from 'src/languages/entities/language.entity';
import { CreateHighlightTranslationDto } from '../dto/create-highlight-translation.dto';

@Entity()
export class HighlightTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @ManyToOne(() => Highlight, (highlight) => highlight.highlightTranslations, {
    onDelete: 'CASCADE',
  })
  highlight: Highlight;

  @ManyToOne(() => Language, (language) => language.highlightTranslations)
  language: Language;

  constructor(
    createHighlightTranslationDto: CreateHighlightTranslationDto,
    language: Language,
    highlight: Highlight,
  ) {
    if (!createHighlightTranslationDto || !language || !highlight) {
      return;
    }
    this.title = createHighlightTranslationDto.title;
    this.language = language;
    this.highlight = highlight;
  }
}
