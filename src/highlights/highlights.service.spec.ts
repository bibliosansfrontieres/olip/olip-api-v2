import { Test, TestingModule } from '@nestjs/testing';
import { HighlightsService } from './highlights.service';
import { Repository } from 'typeorm';
import { Highlight } from './entities/highlight.entity';
import { HighlightTranslation } from './entities/highlight-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';
import { ItemsService } from 'src/items/items.service';
import { HighlightContentsService } from 'src/highlight-contents/highlight-contents.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';
import { itemsServiceMock } from 'src/tests/mocks/providers/items-service.mock';
import { highlightContentsServiceMock } from 'src/tests/mocks/providers/highlight-contents-service.mock';
import { DublinCoreItemsService } from 'src/dublin-core-items/dublin-core-items.service';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { dublinCoreItemsServiceMock } from 'src/tests/mocks/providers/dublin-core-items-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';

describe('HighlightsService', () => {
  let service: HighlightsService;
  let highlightsRepository: Repository<Highlight>;
  let highlightTranslationsRepository: Repository<HighlightTranslation>;
  const HIGHLIGHTS_REPOSITORY_TOKEN = getRepositoryToken(Highlight);
  const HIGHLIGHT_TRANSLATIONS_REPOSITORY_TOKEN =
    getRepositoryToken(HighlightTranslation);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        HighlightsService,
        {
          provide: HIGHLIGHTS_REPOSITORY_TOKEN,
          useClass: Repository<Highlight>,
        },
        {
          provide: HIGHLIGHT_TRANSLATIONS_REPOSITORY_TOKEN,
          useClass: Repository<HighlightTranslation>,
        },
        LanguagesService,
        ItemsService,
        HighlightContentsService,
        DublinCoreItemsService,
        JwtService,
        UsersService,
        ConfigService,
        ApplicationsService,
      ],
    })
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .overrideProvider(HighlightContentsService)
      .useValue(highlightContentsServiceMock)
      .overrideProvider(DublinCoreItemsService)
      .useValue(dublinCoreItemsServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .compile();

    service = module.get<HighlightsService>(HighlightsService);
    highlightsRepository = module.get<Repository<Highlight>>(
      HIGHLIGHTS_REPOSITORY_TOKEN,
    );
    highlightTranslationsRepository = module.get<
      Repository<HighlightTranslation>
    >(HIGHLIGHT_TRANSLATIONS_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
