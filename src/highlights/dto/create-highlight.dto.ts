import { ArrayMinSize, IsArray, ValidateNested } from 'class-validator';
import { CreateHighlightTranslationDto } from './create-highlight-translation.dto';
import { Type } from 'class-transformer';

export class CreateHighlightDto {
  @IsArray()
  @ValidateNested({ each: true })
  @ArrayMinSize(1)
  @Type(() => CreateHighlightTranslationDto)
  createHighlightTranslations: CreateHighlightTranslationDto[];
}
