import { HighlightContent } from 'src/highlight-contents/entities/highlight-content.entity';
import { HighlightTranslation } from '../entities/highlight-translation.entity';
import { Highlight } from '../entities/highlight.entity';
import { ApiProperty } from '@nestjs/swagger';

export class GetHightlightDto {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({
    example: [
      {
        item: {
          id: 1,
          thumbnail: 'images/path.extension',
          createdCountry: 'fr',
          isInstalled: true,
          isUpdateNeeded: false,
          isBroken: false,
          isManuallyInstalled: false,
          version: null,
          createdAt: 1697531755828,
          updatedAt: 1697531755828,
          dublinCoreItem: {
            id: 1,
            dublinCoreType: {
              id: 3,
              image: 'images/',
            },
            dublinCoreItemTranslations: [
              {
                id: 1,
                title: 'My item title',
                description: 'My item description',
                creator: '',
                publisher: '',
                source: '',
                extent: '',
                subject: '',
                dateCreated: '',
                language: {
                  id: 67,
                  identifier: 'eng',
                  oldIdentifier: 'en',
                  label: 'English',
                },
              },
            ],
          },
        },
        order: 1,
        isDisplayed: false,
        id: 1,
      },
    ],
  })
  highlightContents: HighlightContent[];

  @ApiProperty({
    example: [
      {
        id: 1,
        title: 'My first highlight !',
        language: {
          id: 67,
          identifier: 'eng',
          oldIdentifier: 'en',
          label: 'English',
        },
      },
    ],
  })
  highlightTranslations: HighlightTranslation[];

  constructor(highlight: Highlight) {
    this.id = highlight.id;
    this.highlightContents = highlight.highlightContents;
    this.highlightTranslations = highlight.highlightTranslations;
  }
}
