import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateHighlightTranslationDto {
  @ApiProperty({ example: 'eng' })
  @IsString()
  languageIdentifier: string;

  @ApiProperty({ example: 'My super title' })
  @IsString()
  title: string;
}
