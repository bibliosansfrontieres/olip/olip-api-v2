import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetHighlightOptionsDto {
  @ApiPropertyOptional({ example: 'eng' })
  @IsString()
  @IsOptional()
  languageIdentifier?: string;

  @ApiProperty({ example: 'eng' })
  @IsString()
  itemsLanguageIdentifier: string;
}
