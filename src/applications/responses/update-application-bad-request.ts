import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateApplicationBadRequest {
  @ApiProperty({ example: 400 })
  statusCode: number;

  @ApiProperty({
    example: ['Cannot set both userIds and roleIds at the same time'],
  })
  message: string | string[];

  @ApiPropertyOptional({ example: 'Bad Request' })
  error?: string;
}
