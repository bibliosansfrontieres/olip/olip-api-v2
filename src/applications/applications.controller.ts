import { Body, Controller, Get, Param, Patch, Query } from '@nestjs/common';
import { ApplicationsService } from './applications.service';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { GetApplicationsOptionsDto } from './dto/get-applications-options.dto';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiNotAcceptableResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ApiPaginatedResponse } from 'src/utils/classes/api-paginated-response';
import { Application } from './entities/application.entity';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { UpdateApplicationDto } from './dto/update-application.dto';
import { UpdateApplicationBadRequest } from './responses/update-application-bad-request';
import { OptionalAuth } from 'src/auth/decorators/optional-auth.decorator';
import { UserId } from 'src/auth/decorators/user-id.decorator';
import { GetApplicationUpdateDto } from './dto/get-application-update.dto';

@ApiTags('applications')
@Controller('api/applications')
export class ApplicationsController {
  constructor(private readonly applicationsService: ApplicationsService) {}

  @OptionalAuth()
  @ApiOperation({
    summary: 'Get applications',
  })
  @ApiPaginatedResponse(Application)
  @Get()
  getAll(
    @Query() getApplicationsOptionsDto: GetApplicationsOptionsDto,
    @Query() pageOptionsDto: PageOptionsDto,
    @UserId() userId: number | undefined,
  ) {
    return this.applicationsService.getAll(
      getApplicationsOptionsDto,
      pageOptionsDto,
      userId,
    );
  }

  @ApiOperation({
    summary: 'Get application by ID',
  })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  @Get(':id')
  getOne(@Param('id') id: string) {
    return this.applicationsService.getOne(id);
  }

  @ApiOperation({
    summary: 'Install application',
    description: 'Needs authentification and MANAGE_APPLICATIONS permission',
  })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  @ApiNotAcceptableResponse()
  @Get('install/:id')
  install(@Param('id') id: string) {
    return this.applicationsService.checkAndInstall(id);
  }

  @ApiOperation({
    summary: 'Uninstall application',
    description: 'Needs authentification and MANAGE_APPLICATIONS permission',
  })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  @Get('uninstall/:id')
  uninstall(@Param('id') id: string) {
    return this.applicationsService.uninstall(id);
  }

  @Auth(PermissionEnum.MANAGE_APPLICATIONS)
  @Patch(':id')
  @ApiOperation({ summary: 'Edit an application' })
  @ApiOkResponse()
  @ApiBody({ type: UpdateApplicationDto })
  @ApiNotFoundResponse({ description: 'category not found' })
  @ApiBadRequestResponse({ type: UpdateApplicationBadRequest })
  update(
    @Param('id') id: string,
    @Body() updateCategoryDto: UpdateApplicationDto,
  ) {
    return this.applicationsService.update(id, updateCategoryDto);
  }

  @Auth(PermissionEnum.MANAGE_APPLICATIONS)
  @Get('update/:id')
  @ApiOperation({ summary: 'Update an application' })
  @ApiOkResponse()
  @ApiBadRequestResponse({ type: UpdateApplicationBadRequest })
  updateApplication(
    @Param('id') id: string,
    @Query() getApplicationUpdateDto: GetApplicationUpdateDto,
  ) {
    return this.applicationsService.checkAndUpdateApplication(
      id,
      getApplicationUpdateDto,
    );
  }

  @Auth(PermissionEnum.MANAGE_APPLICATIONS)
  @Get('force-check-update')
  @ApiOperation({ summary: 'Force the check of applications update' })
  @ApiOkResponse()
  forceCheckUpdateApplication() {
    return this.applicationsService.forceCheckUpdateApplication();
  }
}
