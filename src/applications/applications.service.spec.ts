import { Test, TestingModule } from '@nestjs/testing';
import { ApplicationsService } from './applications.service';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { Application } from './entities/application.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ApplicationTranslation } from './entities/application-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';
import { languagesServiceMock } from 'src/tests/mocks/providers/languages-service.mock';
import { ApplicationTypesService } from 'src/application-types/application-types.service';
import { DockerService } from 'src/docker/docker.service';
import { VisibilitiesService } from 'src/visibilities/visibilities.service';
import { UsersService } from 'src/users/users.service';
import { HardwareService } from 'src/hardware/hardware.service';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { SearchService } from 'src/search/search.service';
import { ConfigService } from '@nestjs/config';
import { SettingsService } from 'src/settings/settings.service';
import { SocketGateway } from 'src/socket/socket.gateway';
import { UpdateService } from 'src/update/update.service';
import { applicationTypesServiceMock } from 'src/tests/mocks/providers/application-types-service.mock';
import { dockerServiceMock } from 'src/tests/mocks/providers/docker-service.mock';
import { visibilitiesServiceMock } from 'src/tests/mocks/providers/visibilities-service.mock';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { hardwareServiceMock } from 'src/tests/mocks/providers/hardware-service.mock';
import { playlistsServiceMock } from 'src/tests/mocks/providers/playlists-service.mock';
import { searchServiceMock } from 'src/tests/mocks/providers/search-service.mock';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { settingsServiceMock } from 'src/tests/mocks/providers/settings-service.mock';
import { socketGatewayMock } from 'src/tests/mocks/providers/socket-gateway.mock';
import { updateServiceMock } from 'src/tests/mocks/providers/update-service.mock';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { ApplicationStatusEnum } from './enums/application-status.enum';
import { TrueFalseEnum } from 'src/categories/enums/true-false.enum';
import { OrderEnum } from 'src/utils/classes/order.enum';
import * as getContextUtil from 'src/utils/get-context.util';
import { applicationMock } from 'src/tests/mocks/objects/application.mock';
import { contextMock } from 'src/tests/mocks/objects/context.mock';
import { maestroApplicationMock } from 'src/tests/mocks/objects/maestro-application.mock';
import * as getManyAndCountUtil from 'src/utils/get-many-and-count.util';
import * as enumToBooleanUtil from 'src/utils/enum-to-boolean.util';
import { userMock } from 'src/tests/mocks/objects/user.mock';
import { roleMock } from 'src/tests/mocks/objects/role.mock';
import { permissionMock } from 'src/tests/mocks/objects/permission.mock';
import { UnauthorizedException } from '@nestjs/common';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { PageDto } from 'src/utils/classes/page.dto';

describe('ApplicationsService', () => {
  let service: ApplicationsService;
  let usersService: UsersService;
  let applicationsRepository: Repository<Application>;
  let applicationTranslationsRepository: Repository<ApplicationTranslation>;
  const APPLICATIONS_REPOSITORY_TOKEN = getRepositoryToken(Application);
  const APPLICATION_TRANSLATIONS_REPOSITORY_TOKEN = getRepositoryToken(
    ApplicationTranslation,
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: APPLICATIONS_REPOSITORY_TOKEN,
          useClass: Repository<Application>,
        },
        {
          provide: APPLICATION_TRANSLATIONS_REPOSITORY_TOKEN,
          useClass: Repository<ApplicationTranslation>,
        },
        ApplicationsService,
        LanguagesService,
        ApplicationTypesService,
        DockerService,
        VisibilitiesService,
        UsersService,
        HardwareService,
        PlaylistsService,
        SearchService,
        ConfigService,
        SettingsService,
        SocketGateway,
        UpdateService,
        JwtService,
      ],
    })
      .overrideProvider(LanguagesService)
      .useValue(languagesServiceMock)
      .overrideProvider(ApplicationTypesService)
      .useValue(applicationTypesServiceMock)
      .overrideProvider(DockerService)
      .useValue(dockerServiceMock)
      .overrideProvider(VisibilitiesService)
      .useValue(visibilitiesServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(HardwareService)
      .useValue(hardwareServiceMock)
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .overrideProvider(SearchService)
      .useValue(searchServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(SettingsService)
      .useValue(settingsServiceMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(UpdateService)
      .useValue(updateServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .compile();

    service = module.get<ApplicationsService>(ApplicationsService);
    applicationsRepository = module.get<Repository<Application>>(
      APPLICATIONS_REPOSITORY_TOKEN,
    );
    applicationTranslationsRepository = module.get<
      Repository<ApplicationTranslation>
    >(APPLICATION_TRANSLATIONS_REPOSITORY_TOKEN);
    usersService = module.get<UsersService>(UsersService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(applicationsRepository).toBeDefined();
    expect(applicationTranslationsRepository).toBeDefined();
  });

  describe('getAll', () => {
    const mockQueryBuilder = {
      innerJoinAndSelect: jest.fn().mockReturnThis(),
      leftJoinAndSelect: jest.fn().mockReturnThis(),
      where: jest.fn().mockReturnThis(),
      andWhere: jest.fn().mockReturnThis(),
      take: jest.fn().mockReturnThis(),
      skip: jest.fn().mockReturnThis(),
    } as unknown as SelectQueryBuilder<Application>;

    const application1 = applicationMock();
    const application2 = applicationMock({ id: 'olip' });

    const mockApplications = [application1, application2];
    const mockFilteredApplications = [application2, application1];

    const getApplicationsOptionsDto = {
      languageIdentifier: 'eng',
      applicationTypeId: '1',
      status: ApplicationStatusEnum.INSTALLED,
      isVisibilityEnabled: TrueFalseEnum.TRUE,
      withOlip: TrueFalseEnum.TRUE,
    };
    const pageOptionsDto = {
      order: OrderEnum.ASC,
      page: 1,
      take: 12,
      skip: 0,
    };
    const userId = Date.now();
    const user = userMock({
      id: userId,
      role: roleMock({
        permissions: [permissionMock()],
      }),
    });

    const mockContext = contextMock({
      applications: [
        maestroApplicationMock({
          id: application1.id,
        }),
      ],
    });

    beforeEach(() => {
      jest.spyOn(getContextUtil, 'getContext').mockReturnValue(mockContext);
      jest.spyOn(enumToBooleanUtil, 'enumToBoolean').mockReturnValue(true);
      jest.spyOn(usersService, 'findOne').mockResolvedValue(user);
      jest
        .spyOn(applicationsRepository, 'createQueryBuilder')
        .mockReturnValue(mockQueryBuilder);
      jest.spyOn(service, 'checkPermission').mockReturnValue();
      jest
        .spyOn(service, 'filterByApplicationStatus')
        .mockReturnValue(mockQueryBuilder);
      jest
        .spyOn(service, 'filterByApplicationTypes')
        .mockReturnValue(mockQueryBuilder);
      jest
        .spyOn(service, 'filterByVisibility')
        .mockReturnValue(mockQueryBuilder);
      jest
        .spyOn(getManyAndCountUtil, 'getManyAndCount')
        .mockResolvedValue([2, mockApplications]);
      jest.spyOn(service, 'getApplicationsTranslations').mockResolvedValue();
      jest
        .spyOn(service, 'filterApplicationsOlipFirst')
        .mockReturnValue(mockFilteredApplications);
    });

    it('should be defined', () => {
      expect(service.getAll).toBeDefined();
    });

    it('should call getContext', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto);
      expect(getContextUtil.getContext).toHaveBeenCalled();
    });

    it('should call enumToBoolean twice', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto);
      expect(enumToBooleanUtil.enumToBoolean).toHaveBeenCalledTimes(2);
    });

    it('should call enumToBoolean with right arguments', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto);
      expect(enumToBooleanUtil.enumToBoolean).toHaveBeenCalledWith(
        getApplicationsOptionsDto.withOlip,
      );
    });

    it('should call enumToBoolean with right arguments', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto);
      expect(enumToBooleanUtil.enumToBoolean).toHaveBeenCalledWith(
        getApplicationsOptionsDto.isVisibilityEnabled,
        true,
      );
    });

    it('should call userService.findone with right arguments if userId', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto, userId);
      expect(usersService.findOne).toHaveBeenCalledWith({
        where: { id: userId },
        relations: { role: { permissions: true } },
      });
    });

    it('should not call userService.findone if no userId', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto);
      expect(usersService.findOne).not.toHaveBeenCalled();
    });

    it('should call checkPermission with right arguments if !visibilityEnabled', async () => {
      jest.spyOn(enumToBooleanUtil, 'enumToBoolean').mockReturnValue(false);

      await service.getAll(getApplicationsOptionsDto, pageOptionsDto, userId);
      expect(service.checkPermission).toHaveBeenCalledWith(userId, user);
    });

    it('should not call checkPermission if visibilityEnabled', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto, userId);
      expect(service.checkPermission).not.toHaveBeenCalled();
    });

    it('should call createQueryBuilder with right arguments', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto);
      expect(applicationsRepository.createQueryBuilder).toHaveBeenCalledWith(
        'application',
      );
    });

    it('should call queryBuilder.innerJoinAndSelect with right arguments', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto);
      expect(mockQueryBuilder.innerJoinAndSelect).toHaveBeenCalledWith(
        'application.applicationType',
        'applicationType',
        'applicationType.id = application.applicationTypeId',
      );
      expect(mockQueryBuilder.innerJoinAndSelect).toHaveBeenCalledWith(
        'application.visibility',
        'visibility',
        'visibility.id = application.visibilityId',
      );
    });

    it('should call queryBuilder.leftJoinAndSelect with right arguments', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto);
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'visibility.users',
        'users',
      );
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'visibility.roles',
        'roles',
      );
    });

    it('should call queryBuilder.where with right arguments', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto);
      expect(mockQueryBuilder.where).toHaveBeenCalledWith(
        'application.id IS NOT NULL',
      );
      expect(mockQueryBuilder.where).toHaveBeenCalledWith(
        '(application.id IN (:...applicationContextIds) OR application.id = "olip" OR application.isInstalled = 1)',
        {
          applicationContextIds: (mockContext?.applications || []).map(
            (app) => app.id,
          ),
        },
      );
    });

    it('should call queryBuilder pagination filter with right arguments', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto);
      expect(mockQueryBuilder.take).toHaveBeenCalledWith(pageOptionsDto.take);
      expect(mockQueryBuilder.skip).toHaveBeenCalledWith(pageOptionsDto.skip);
    });

    it('should call filterByApplicationStatus with right arguments', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto);
      expect(service.filterByApplicationStatus).toHaveBeenCalledWith(
        mockQueryBuilder,
        getApplicationsOptionsDto.status,
      );
    });

    it('should call filterByApplicationTypes with right arguments', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto);
      expect(service.filterByApplicationTypes).toHaveBeenCalledWith(
        mockQueryBuilder,
        getApplicationsOptionsDto.applicationTypeId,
      );
    });

    it('should call filterByVisibility with right arguments', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto, userId);
      expect(service.filterByVisibility).toHaveBeenCalledWith(
        mockQueryBuilder,
        true,
        true,
        user,
        userId,
      );
    });

    it('should call getManyAndCount with right arguments', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto, userId);
      expect(getManyAndCountUtil.getManyAndCount).toHaveBeenCalledWith(
        mockQueryBuilder,
        ['application.id'],
        false,
      );
    });

    it('should call getApplicationsTranslations with right arguments', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto, userId);
      expect(service.getApplicationsTranslations).toHaveBeenCalledWith(
        mockApplications,
        getApplicationsOptionsDto.languageIdentifier,
      );
    });

    it('should call filterApplicationsOlipFirst with right arguments', async () => {
      await service.getAll(getApplicationsOptionsDto, pageOptionsDto, userId);
      expect(service.filterApplicationsOlipFirst).toHaveBeenCalledWith(
        mockApplications,
      );
    });

    it('should return all applications', async () => {
      const result = await service.getAll(
        getApplicationsOptionsDto,
        pageOptionsDto,
        userId,
      );
      expect(result).toEqual(
        new PageDto(
          mockFilteredApplications,
          mockFilteredApplications.length,
          pageOptionsDto,
        ),
      );
      expect(result).toBeInstanceOf(PageDto);
    });
  });

  describe('checkPermission', () => {
    const userId = Date.now();
    const userAuthorized = userMock({
      id: userId,
      role: roleMock({
        permissions: [
          permissionMock({ name: PermissionEnum.MANAGE_APPLICATIONS }),
        ],
      }),
    });
    const userNotAuthorized = userMock({
      id: userId,
      role: roleMock({ permissions: [] }),
    });

    it('should be defined', () => {
      expect(service.checkPermission).toBeDefined();
    });

    it('should throw if userId is undefined', () => {
      const result = () => service.checkPermission(undefined);
      expect(result).toThrow(UnauthorizedException);
    });

    it('should throw if user is undefined', () => {
      const result = () => service.checkPermission(userId, undefined);
      expect(result).toThrow(UnauthorizedException);
    });

    it('should throw if user does not have the permission to manage application', () => {
      const result = () => service.checkPermission(userId, userNotAuthorized);
      expect(result).toThrow(UnauthorizedException);
    });

    it('should not throw if user has the permission to manage application', () => {
      expect(service.checkPermission(userId, userAuthorized)).toBeUndefined();
    });
  });

  describe('filterByApplicationStatus', () => {
    const mockQueryBuilder = {
      andWhere: jest.fn().mockReturnThis(),
    } as unknown as SelectQueryBuilder<Application>;

    it('should be defined', () => {
      expect(service.filterByApplicationStatus).toBeDefined();
    });

    it('should not call queryBuilder.andWhere if no status', () => {
      service.filterByApplicationStatus(mockQueryBuilder, undefined);
      expect(mockQueryBuilder.andWhere).not.toHaveBeenCalled();
    });

    it('should call queryBuilder.andWhere with right arguments if status is INSTALLED', () => {
      service.filterByApplicationStatus(
        mockQueryBuilder,
        ApplicationStatusEnum.INSTALLED,
      );
      expect(mockQueryBuilder.andWhere).toHaveBeenCalledWith(
        'application.isInstalled = 1',
      );
    });

    it('should call queryBuilder.andWhere with right arguments if status is UNINSTALLED', () => {
      service.filterByApplicationStatus(
        mockQueryBuilder,
        ApplicationStatusEnum.UNINSTALLED,
      );
      expect(mockQueryBuilder.andWhere).toHaveBeenCalledWith(
        'application.isInstalled = 0',
      );
    });

    it('should call queryBuilder.andWhere with right arguments if status is UPDATE_NEEDED', () => {
      service.filterByApplicationStatus(
        mockQueryBuilder,
        ApplicationStatusEnum.UPDATE_NEEDED,
      );
      expect(mockQueryBuilder.andWhere).toHaveBeenCalledWith(
        'application.isUpdateNeeded = 1',
      );
    });

    it('should return queryBuilder', () => {
      const result = service.filterByApplicationStatus(
        mockQueryBuilder,
        ApplicationStatusEnum.UPDATE_NEEDED,
      );
      expect(result).toEqual(mockQueryBuilder);
    });
  });

  describe('filterByApplicationTypes', () => {
    const mockQueryBuilder = {
      andWhere: jest.fn().mockReturnThis(),
    } as unknown as SelectQueryBuilder<Application>;

    const getApplicationsOptionsDtoApplicationTypeId = '1,2,3';
    const applicationTypeIds =
      getApplicationsOptionsDtoApplicationTypeId.split(',');

    it('should be defined', () => {
      expect(service.filterByApplicationTypes).toBeDefined();
    });

    it('should not call queryBuilder.andWhere if no applicationTypeId', () => {
      service.filterByApplicationTypes(mockQueryBuilder, undefined);
      expect(mockQueryBuilder.andWhere).not.toHaveBeenCalled();
    });

    it('should call queryBuilder.andWhere with right arguments if applicationType', () => {
      service.filterByApplicationTypes(
        mockQueryBuilder,
        getApplicationsOptionsDtoApplicationTypeId,
      );
      expect(mockQueryBuilder.andWhere).toHaveBeenCalledWith(
        'applicationType.id IN (:...applicationTypeIds)',
        { applicationTypeIds },
      );
    });

    it('should return queryBuilder', () => {
      const result = service.filterByApplicationTypes(
        mockQueryBuilder,
        getApplicationsOptionsDtoApplicationTypeId,
      );
      expect(result).toEqual(mockQueryBuilder);
    });
  });

  describe('filterByVisibility', () => {
    const mockQueryBuilder = {
      andWhere: jest.fn().mockReturnThis(),
    } as unknown as SelectQueryBuilder<Application>;

    const userId = Date.now();
    const user = userMock({
      id: userId,
      role: roleMock(),
    });

    it('should be defined', () => {
      expect(service.filterByVisibility).toBeDefined();
    });

    it('should not call queryBuilder.andWhere if not isVisibilityEnabled', () => {
      service.filterByVisibility(mockQueryBuilder);
      expect(mockQueryBuilder.andWhere).not.toHaveBeenCalled();
    });

    it('should call queryBuilder.andWhere with right arguments if not with olip', () => {
      service.filterByVisibility(mockQueryBuilder, true, false);
      expect(mockQueryBuilder.andWhere).toHaveBeenCalledWith(
        `application.id != 'olip'`,
      );
    });

    it('should call queryBuilder.andWhere twice if user and not with olip', () => {
      service.filterByVisibility(mockQueryBuilder, true, false, user, userId);
      expect(mockQueryBuilder.andWhere).toHaveBeenCalledTimes(2);
    });

    it('should call queryBuilder.andWhere with right arguments if user', () => {
      service.filterByVisibility(mockQueryBuilder, true, true, user, userId);
      expect(mockQueryBuilder.andWhere).toHaveBeenCalledWith(
        '(users.id = :userId OR roles.name = :roleName OR (users.id IS NULL AND roles.id IS NULL))',
        { userId, roleName: user.role.name },
      );
    });

    it('should call queryBuilder.andWhere with right arguments if no user', () => {
      service.filterByVisibility(mockQueryBuilder, true, true);
      expect(mockQueryBuilder.andWhere).toHaveBeenCalledWith(
        '(users.id IS NULL AND roles.id IS NULL)',
      );
    });

    it('should return queryBuilder', () => {
      const result = service.filterByVisibility(mockQueryBuilder, true, true);
      expect(result).toEqual(mockQueryBuilder);
    });
  });

  describe('filterApplicationsOlipFirst', () => {
    const olipApplication = applicationMock({ id: 'olip' });
    const application1 = applicationMock({ id: '1' });
    const application2 = applicationMock({ id: '1' });
    const applications = [application1, olipApplication, application2];
    const resultApplications = [olipApplication, application1, application2];

    it('should be defined', () => {
      expect(service.filterApplicationsOlipFirst).toBeDefined();
    });

    it('should return filtered applications', () => {
      const result = service.filterApplicationsOlipFirst(applications);
      expect(result).toEqual(resultApplications);
    });
  });
});
