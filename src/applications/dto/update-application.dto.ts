import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsArray, IsOptional } from 'class-validator';

export class UpdateApplicationDto {
  @ApiPropertyOptional({ example: [1, 2] })
  @IsOptional()
  @IsArray()
  userIds?: number[];

  @ApiPropertyOptional({ example: [1, 2] })
  @IsOptional()
  @IsArray()
  roleIds?: number[];
}
