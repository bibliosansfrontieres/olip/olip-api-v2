import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional } from 'class-validator';
import { TrueFalseEnum } from 'src/utils/enums/true-false.enum';

export class GetApplicationUpdateDto {
  @IsOptional()
  @IsEnum(TrueFalseEnum)
  @ApiPropertyOptional({ example: TrueFalseEnum.TRUE })
  force?: TrueFalseEnum;
}
