import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { ApplicationStatusEnum } from '../enums/application-status.enum';
import { TrueFalseEnum } from 'src/utils/enums/true-false.enum';

export class GetApplicationsOptionsDto {
  @IsString()
  @ApiProperty({ example: 'eng' })
  languageIdentifier: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    example: `1,2`,
  })
  applicationTypeId?: string;

  @ApiPropertyOptional({ example: ApplicationStatusEnum.INSTALLED })
  @IsOptional()
  @IsEnum(ApplicationStatusEnum)
  status?: ApplicationStatusEnum;

  @ApiPropertyOptional({ example: TrueFalseEnum.TRUE })
  @IsOptional()
  @IsEnum(TrueFalseEnum)
  isVisibilityEnabled?: TrueFalseEnum;

  @ApiProperty({ example: TrueFalseEnum.TRUE })
  @IsOptional()
  @IsEnum(TrueFalseEnum)
  withOlip?: TrueFalseEnum;
}
