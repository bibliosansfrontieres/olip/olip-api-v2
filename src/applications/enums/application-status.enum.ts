export enum ApplicationStatusEnum {
  INSTALLED = 'installed',
  UNINSTALLED = 'uninstalled',
  UPDATE_NEEDED = 'update_needed',
}
