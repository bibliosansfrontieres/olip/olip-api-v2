import { Module, forwardRef } from '@nestjs/common';
import { ApplicationsService } from './applications.service';
import { ApplicationsController } from './applications.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Application } from './entities/application.entity';
import { LanguagesModule } from 'src/languages/languages.module';
import { ApplicationTranslation } from './entities/application-translation.entity';
import { ApplicationTypesModule } from 'src/application-types/application-types.module';
import { DockerModule } from 'src/docker/docker.module';
import { VisibilitiesModule } from 'src/visibilities/visibilities.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { ConfigModule } from '@nestjs/config';
import { HardwareModule } from 'src/hardware/hardware.module';
import { PlaylistsModule } from 'src/playlists/playlists.module';
import { SearchModule } from 'src/search/search.module';
import { SettingsModule } from 'src/settings/settings.module';
import { SocketModule } from 'src/socket/socket.module';
import { UpdateModule } from 'src/update/update.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Application, ApplicationTranslation]),
    LanguagesModule,
    ApplicationTypesModule,
    forwardRef(() => DockerModule),
    VisibilitiesModule,
    JwtModule,
    UsersModule,
    ConfigModule,
    forwardRef(() => HardwareModule),
    forwardRef(() => PlaylistsModule),
    SearchModule,
    SettingsModule,
    forwardRef(() => SocketModule),
    UpdateModule,
  ],
  controllers: [ApplicationsController],
  providers: [ApplicationsService],
  exports: [ApplicationsService],
})
export class ApplicationsModule {}
