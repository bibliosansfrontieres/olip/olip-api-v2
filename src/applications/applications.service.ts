import {
  BadRequestException,
  Inject,
  Injectable,
  Logger,
  NotAcceptableException,
  NotFoundException,
  ServiceUnavailableException,
  UnauthorizedException,
  forwardRef,
} from '@nestjs/common';
import { Application } from './entities/application.entity';
import { InjectRepository } from '@nestjs/typeorm';
import {
  FindManyOptions,
  FindOneOptions,
  Repository,
  SelectQueryBuilder,
} from 'typeorm';
import { ApplicationTranslation } from './entities/application-translation.entity';
import { LanguagesService } from 'src/languages/languages.service';
import { Item } from 'src/items/entities/item.entity';
import { PageDto } from 'src/utils/classes/page.dto';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { PageOptions } from 'src/utils/classes/page-options';
import { ApplicationTypesService } from 'src/application-types/application-types.service';
import { DockerService } from 'src/docker/docker.service';
import { HookNamesEnum } from 'src/maestro/enums/hook-names.enum';
import { GetApplicationsOptionsDto } from './dto/get-applications-options.dto';
import { ApplicationStatusEnum } from './enums/application-status.enum';
import { UpdateApplicationDto } from './dto/update-application.dto';
import { invalidateCache } from 'src/utils/caching.util';
import { Visibility } from 'src/visibilities/entities/visibility.entity';
import { CreateVisibilityDto } from 'src/visibilities/dto/create-visibility.dto';
import { VisibilitiesService } from 'src/visibilities/visibilities.service';
import { TrueFalseEnum } from 'src/utils/enums/true-false.enum';
import { UsersService } from 'src/users/users.service';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import { HardwareService } from 'src/hardware/hardware.service';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { SearchService } from 'src/search/search.service';
import { IMaestroApplication } from 'src/maestro/interfaces/maestro-application.interface';
import { ConfigService } from '@nestjs/config';
import { SettingsService } from 'src/settings/settings.service';
import { SettingKey } from 'src/settings/enums/setting-key.enum';
import { getContext } from 'src/utils/get-context.util';
import { SocketGateway } from 'src/socket/socket.gateway';
import { GetApplicationUpdateDto } from './dto/get-application-update.dto';
import { UpdateService } from 'src/update/update.service';
import { User } from 'src/users/entities/user.entity';
import { enumToBoolean } from 'src/utils/enum-to-boolean.util';

@Injectable()
export class ApplicationsService {
  constructor(
    @InjectRepository(Application)
    private applicationsRepository: Repository<Application>,
    @InjectRepository(ApplicationTranslation)
    private applicationTranslationsRepository: Repository<ApplicationTranslation>,
    private languagesService: LanguagesService,
    private applicationTypesService: ApplicationTypesService,
    private dockerService: DockerService,
    private visibilitiesService: VisibilitiesService,
    private usersService: UsersService,
    private hardwareService: HardwareService,
    @Inject(forwardRef(() => PlaylistsService))
    private playlistsService: PlaylistsService,
    private searchService: SearchService,
    private configService: ConfigService,
    private settingsService: SettingsService,
    private socketGateway: SocketGateway,
    private updateService: UpdateService,
  ) {}

  async indexAll() {
    const applications = await this.find();
    for (const application of applications) {
      if (application.id === 'olip') continue;
      await this.searchService.indexApplication(application.id);
    }
  }

  async onModuleInit() {
    // Start up applications
    const vmId = this.configService.get('VM_ID');
    const isOlipInstalled = await this.settingsService.findOne(
      SettingKey.IS_OLIP_INSTALLED,
    );
    if (
      vmId === 'olip' &&
      (!isOlipInstalled || isOlipInstalled.value !== 'true')
    ) {
      return;
    }
    const applications = await this.applicationsRepository.find({
      where: { isUp: true },
    });
    if (applications.length <= 0) return;
    Logger.log(
      `Starting ${applications.length} applications...`,
      'ApplicationsService',
    );
    const applicationsStart = [];
    for (const application of applications) {
      applicationsStart.push(this.dockerService.run(application));
    }
    await Promise.all(applicationsStart);
    Logger.log(
      `${applications.length} applications started !`,
      'ApplicationsService',
    );
  }

  async onModuleDestroy() {
    // Shut down applications
    const applications = await this.applicationsRepository.find({
      where: { isUp: true },
    });
    if (applications.length <= 0) return;
    Logger.log(
      `Stopping ${applications.length} applications...`,
      'ApplicationsService',
    );
    const applicationsStop = [];
    for (const application of applications) {
      applicationsStop.push(this.dockerService.stop(application));
    }
    await Promise.all(applicationsStop);
    Logger.log(
      `${applications.length} applications stopped !`,
      'ApplicationsService',
    );
  }

  needUpdate(application: Application) {
    application.isUpdateNeeded = true;
    return this.save(application);
  }

  async updateMetadata(
    application: Application,
    maestroApplication: IMaestroApplication,
  ) {
    application.version = maestroApplication.version;
    application.logo = maestroApplication.logo;
    application.image = maestroApplication.image;
    application.compose = maestroApplication.compose;
    application.exposeServices = maestroApplication.exposeServices;
    application.hooks = maestroApplication.hooks;
    application.size = maestroApplication.size;
    // TODO : update application translations
    await this.applicationsRepository.save(application);
  }

  async create(maestroApplication: IMaestroApplication) {
    const applicationType = await this.applicationTypesService.findOne({
      where: { name: maestroApplication.applicationType.name },
    });
    if (!applicationType)
      throw new NotFoundException('application type not found');
    const applicationTranslations = [];
    for (const createApplicationTranslationDto of maestroApplication.applicationTranslations) {
      const language = await this.languagesService.findOneByIdentifier(
        createApplicationTranslationDto.languageIdentifier,
      );
      const applicationTranslation = new ApplicationTranslation(
        createApplicationTranslationDto,
        language,
      );
      applicationTranslations.push(applicationTranslation);
    }
    const olipHost = await this.configService.get('OLIP_HOST');
    let application = new Application(
      maestroApplication,
      applicationType,
      olipHost,
    );
    application.applicationTranslations = applicationTranslations;
    application = await this.applicationsRepository.save(application);

    const createVisibilityDto: CreateVisibilityDto = new CreateVisibilityDto({
      userIds: [],
      roleIds: [],
    });
    await this.visibilitiesService.create(
      createVisibilityDto,
      undefined,
      application,
    );

    return application;
  }

  async resetAllIsInstalled() {
    const applications = await this.find({ where: { isInstalled: true } });
    for (const application of applications) {
      application.isInstalled = false;
      const olipHost = await this.configService.get('OLIP_HOST');
      application.url = `${application.name}.${olipHost}`;
      await this.applicationsRepository.save(application);
    }
    return applications;
  }

  find(options?: FindManyOptions<Application>) {
    return this.applicationsRepository.find(options);
  }

  findOne(options?: FindOneOptions<Application>) {
    return this.applicationsRepository.findOne(options);
  }

  checkPermission(userId?: number, user?: User) {
    if (userId === undefined) {
      throw new UnauthorizedException();
    }
    if (!user) {
      throw new UnauthorizedException();
    }
    let authorized = false;
    for (const permission of user.role.permissions) {
      if (permission.name === PermissionEnum.MANAGE_APPLICATIONS) {
        authorized = true;
        break;
      }
    }
    if (!authorized) {
      throw new UnauthorizedException();
    }
  }

  filterByApplicationStatus(
    applicationQueryBuilder: SelectQueryBuilder<Application>,
    getApplicationsOptionsDtoStatus?: ApplicationStatusEnum,
  ) {
    if (getApplicationsOptionsDtoStatus) {
      switch (getApplicationsOptionsDtoStatus) {
        case ApplicationStatusEnum.INSTALLED:
          applicationQueryBuilder = applicationQueryBuilder.andWhere(
            'application.isInstalled = 1',
          );
          break;
        case ApplicationStatusEnum.UNINSTALLED:
          applicationQueryBuilder = applicationQueryBuilder.andWhere(
            'application.isInstalled = 0',
          );
          break;
        case ApplicationStatusEnum.UPDATE_NEEDED:
          applicationQueryBuilder = applicationQueryBuilder.andWhere(
            'application.isUpdateNeeded = 1',
          );
          break;
      }
    }
    return applicationQueryBuilder;
  }

  filterByApplicationTypes(
    applicationQueryBuilder: SelectQueryBuilder<Application>,
    getApplicationsOptionsDtoApplicationTypeId?: string,
  ) {
    if (getApplicationsOptionsDtoApplicationTypeId) {
      const applicationTypeIds =
        getApplicationsOptionsDtoApplicationTypeId.split(',');
      applicationQueryBuilder = applicationQueryBuilder.andWhere(
        'applicationType.id IN (:...applicationTypeIds)',
        { applicationTypeIds },
      );
    }
    return applicationQueryBuilder;
  }

  filterByVisibility(
    applicationQueryBuilder: SelectQueryBuilder<Application>,
    isVisibilityEnabled?: boolean,
    withOlip?: boolean,
    user?: User,
    userId?: number,
  ) {
    if (!isVisibilityEnabled) return applicationQueryBuilder;

    if (!withOlip) {
      applicationQueryBuilder = applicationQueryBuilder.andWhere(
        `application.id != 'olip'`,
      );
    }

    if (user) {
      applicationQueryBuilder = applicationQueryBuilder.andWhere(
        '(users.id = :userId OR roles.name = :roleName OR (users.id IS NULL AND roles.id IS NULL))',
        { userId, roleName: user.role.name },
      );
    } else {
      applicationQueryBuilder = applicationQueryBuilder.andWhere(
        '(users.id IS NULL AND roles.id IS NULL)',
      );
    }

    return applicationQueryBuilder;
  }

  filterApplicationsOlipFirst(applications: Application[]) {
    return applications.sort((a, b) => {
      if (a.id === 'olip') return -1;
      if (b.id === 'olip') return 1;
      return 0;
    });
  }

  async getAll(
    getApplicationsOptionsDto: GetApplicationsOptionsDto,
    pageOptionsDto: PageOptionsDto,
    userId?: number,
  ) {
    const context = getContext();

    const withOlip = enumToBoolean(getApplicationsOptionsDto?.withOlip);
    const isVisibilityEnabled = enumToBoolean(
      getApplicationsOptionsDto?.isVisibilityEnabled,
      true,
    );

    const user = userId
      ? await this.usersService.findOne({
          where: { id: userId },
          relations: { role: { permissions: true } },
        })
      : null;

    if (!isVisibilityEnabled) {
      this.checkPermission(userId, user);
    }

    const pageOptions = new PageOptions(pageOptionsDto);

    const queryBuilder =
      this.applicationsRepository.createQueryBuilder('application');

    let applicationQueryBuilder = queryBuilder
      // Joins DublinCoreItem from Item
      .innerJoinAndSelect(
        'application.applicationType',
        'applicationType',
        'applicationType.id = application.applicationTypeId',
      )
      .innerJoinAndSelect(
        'application.visibility',
        'visibility',
        'visibility.id = application.visibilityId',
      )
      .leftJoinAndSelect('visibility.users', 'users')
      .leftJoinAndSelect('visibility.roles', 'roles')
      .where('application.id IS NOT NULL')
      .where(
        '(application.id IN (:...applicationContextIds) OR application.id = "olip" OR application.isInstalled = 1)',
        {
          applicationContextIds: (context?.applications || []).map(
            (app) => app.id,
          ),
        },
      )
      // Take from pageOptions
      .take(pageOptions.take)
      // Skip from pageOptions
      .skip(pageOptions.skip);

    applicationQueryBuilder = this.filterByApplicationStatus(
      applicationQueryBuilder,
      getApplicationsOptionsDto.status,
    );

    applicationQueryBuilder = this.filterByApplicationTypes(
      applicationQueryBuilder,
      getApplicationsOptionsDto.applicationTypeId,
    );

    applicationQueryBuilder = this.filterByVisibility(
      applicationQueryBuilder,
      isVisibilityEnabled,
      withOlip,
      user,
      userId,
    );

    const applicationsResults = await getManyAndCount<Application>(
      applicationQueryBuilder,
      ['application.id'],
      false,
    );

    const [count, applications] = applicationsResults;

    await this.getApplicationsTranslations(
      applications,
      getApplicationsOptionsDto.languageIdentifier,
    );

    const applicationsFiltered = this.filterApplicationsOlipFirst(applications);

    return new PageDto(applicationsFiltered, count, pageOptions);
  }

  async getApplicationsTranslations(
    applications: Application[],
    languageIdentifier: string,
  ) {
    for (const application of applications) {
      const translation = await this.languagesService.getTranslation(
        'application',
        application,
        this.applicationTranslationsRepository,
        languageIdentifier,
      );
      if (translation) {
        application.applicationTranslations = [translation];
      }

      await this.applicationTypesService.getApplicationTypeTranslations(
        application.applicationType,
        languageIdentifier,
      );
    }
  }

  async uninstall(id: string) {
    const application = await this.findOne({
      where: { id, isInstalled: true },
    });
    if (!application) throw new NotFoundException('application not found');
    application.isUp = false;
    application.isUpdateNeeded = false;
    application.isInstalled = false;
    application.isInstalling = false;
    application.isDownloading = false;
    await this.dockerService.stop(application);
    await this.dockerService.uninstall(application);
    await this.applicationsRepository.save(application);
    this.socketGateway.emitApplication(application);
    // TODO : remove all items from application
    const playlists = await this.playlistsService.find({
      where: { application: { id: application.id } },
    });
    for (const playlist of playlists) {
      await this.playlistsService.remove(playlist.id);
      // TODO : delete files
    }
  }

  async checkAndInstall(id: string, waitInstall: boolean = false) {
    const context = getContext();
    if (context && id !== 'integration') {
      const findApplication = context.applications.find((app) => app.id === id);
      if (!findApplication) {
        throw new UnauthorizedException(
          'Unauthorized to install this application with context',
        );
      }
    }
    const application = await this.findOne({
      where: { id },
      relations: { applicationType: true },
    });
    if (!application) {
      Logger.error(`Application ${id} could not be installed (id not found)`);
      throw new NotFoundException('application not found');
    }

    Logger.log(
      `Installing application ${application.name} (${id})...`,
      'ApplicationsService',
    );

    const storage = await this.hardwareService.getStorage();

    if (storage && storage.availableBytes) {
      if (storage.availableBytes < application.size) {
        Logger.error(
          `Application ${id} could not be installed (not enough storage)`,
          'ApplicationsService',
        );
        throw new NotAcceptableException('not enough storage');
      }
    }
    if (!waitInstall) {
      this.install(application);
      return;
    }
    await this.install(application);
  }

  async install(application: Application) {
    try {
      application.isDownloading = true;
      await this.applicationsRepository.save(application);
      this.socketGateway.emitApplication(application);
      const isInstalled = await this.dockerService.install(application);

      if (!isInstalled) {
        application.isDownloading = false;
        await this.applicationsRepository.save(application);
        this.socketGateway.emitApplication(application);
        Logger.error(
          `Application ${application.name} (${application.id}) could not be installed (compose pull error)`,
          'ApplicationsService',
        );
        return false;
      }

      application.isDownloading = false;
      application.isInstalling = true;
      await this.applicationsRepository.save(application);
      this.socketGateway.emitApplication(application);
      const isRunning = await this.dockerService.run(application);

      if (!isRunning) {
        application.isInstalling = false;
        await this.applicationsRepository.save(application);
        this.socketGateway.emitApplication(application);
        Logger.error(
          `Application ${application.name} (${application.id}) could not be installed (compose up error)`,
          'ApplicationsService',
        );
        return false;
      }

      application.isInstalling = false;
      application.isUp = true;
      application.isInstalled = true;
      await this.applicationsRepository.save(application);
      this.socketGateway.emitApplication(application);
      await this.searchService.indexApplication(application.id);

      Logger.log(
        `Application ${application.name} (${application.id}) successfully installed !`,
        'ApplicationsService',
      );

      return true;
    } catch (e) {
      console.error(e);
      Logger.error(
        `Error while installing application ${application.name} (${application.id})`,
        'ApplicationsService',
      );
      application.isDownloading = false;
      application.isInstalling = false;
      application.isUp = false;
      await this.applicationsRepository.save(application);
      this.socketGateway.emitApplication(application);
      return false;
    }
  }

  async getOne(id: string) {
    const application = await this.findOne({ where: { id } });
    if (!application) throw new NotFoundException('application not found');
    return application;
  }

  async installContent(item: Item, application: Application) {
    const installContentHook = this.dockerService.getApplicationHook(
      application,
      HookNamesEnum.INSTALL_APPLICATION_CONTENT,
    );
    await this.dockerService.useHook(installContentHook, application, { item });
    const isInstalledContentHook = this.dockerService.getApplicationHook(
      application,
      HookNamesEnum.IS_APPLICATION_CONTENT_INSTALLED,
    );
    if (isInstalledContentHook) {
      const { result } = await this.dockerService.useHook(
        isInstalledContentHook,
        application,
        {
          item,
        },
      );
      if (result === true) {
        return true;
      }
      return false;
    }
    return true;
  }

  async uninstallContent(item: Item, application: Application) {
    const hook = this.dockerService.getApplicationHook(
      application,
      HookNamesEnum.UNINSTALL_APPLICATION_CONTENT,
    );
    await this.dockerService.useHook(hook, application, { item });
  }

  async checkAndUpdateApplication(
    id: string,
    getApplicationUpdateDto: GetApplicationUpdateDto,
  ) {
    const application = await this.findOne({ where: { id } });
    if (!application) throw new NotFoundException('application not found');
    if (
      !application.isUpdateNeeded &&
      (!getApplicationUpdateDto.force ||
        !getApplicationUpdateDto.force ||
        getApplicationUpdateDto.force !== TrueFalseEnum.TRUE)
    ) {
      throw new NotAcceptableException('no update needed');
    }

    if (application.id === 'olip') {
      return this.updateOlip(application);
    }

    // TODO : check storage application size

    this.updateApplication(application);
    return;
  }

  async updateApplication(application: Application) {
    application.isUpdating = true;
    await this.save(application);
    this.socketGateway.emitApplication(application);

    const isUp = await this.dockerService.update(application);

    if (!isUp) {
      application.isUpdating = false;
      await this.save(application);
      this.socketGateway.emitApplication(application);
      Logger.error(
        `Application ${application.name} (${application.id}) could not be updated (compose up error)`,
        'ApplicationsService',
      );
      return false;
    }

    application.isUpdateNeeded = false;
    application.isUpdating = false;
    await this.save(application);
    this.socketGateway.emitApplication(application);
    return application;
  }

  private async updateOlip(application: Application) {
    // TODO : olip should remove update flag and mark isUpdateNeeded = false
    application.isUpdateNeeded = false;
    //await this.save(application);
    await this.dockerService.runOlipUpgrade();
  }

  async forceCheckUpdateApplication() {
    await this.settingsService.update(SettingKey.LAST_APPLICATIONS_UPDATE, {
      value: '0',
    });
    const updateResult = await this.updateService.runCheckUpdates();
    if (!updateResult) {
      throw new ServiceUnavailableException();
    }
  }

  async getContentUrl(application: Application, item: Item) {
    const hook = this.dockerService.getApplicationHook(
      application,
      HookNamesEnum.GET_APPLICATION_CONTENT_URL,
    );
    if (hook) {
      const { result, value } = await this.dockerService.useHook(
        hook,
        application,
        {
          item,
        },
      );
      if (result && value && typeof value === 'string') return value;
      return item.file.name;
    }
    return item.file.name;
  }

  async update(id: string, updateApplicationDto: UpdateApplicationDto) {
    const application = await this.findOne({
      where: { id },
      relations: { visibility: true },
    });
    if (application === null) {
      throw new NotFoundException(`application ID ${id} not found`);
    }

    if (!updateApplicationDto) return;

    if (
      updateApplicationDto.userIds !== undefined &&
      updateApplicationDto.roleIds !== undefined
    ) {
      throw new BadRequestException(
        'Cannot set both userIds and roleIds at the same time',
      );
    }

    if (
      updateApplicationDto.userIds !== undefined ||
      updateApplicationDto.roleIds !== undefined
    ) {
      await this.visibilitiesService.update(application.visibility.id, {
        userIds: updateApplicationDto.userIds,
        roleIds: updateApplicationDto.roleIds,
      });
    }

    if (
      updateApplicationDto.roleIds === undefined &&
      updateApplicationDto.userIds === undefined
    ) {
      await this.visibilitiesService.update(application.visibility.id, {
        userIds: [],
        roleIds: [],
      });
    }

    await invalidateCache([Application, Visibility]);
    await this.applicationsRepository.save(application);
    return;
  }

  async saveTranslation(applicationTranslation: ApplicationTranslation) {
    return this.applicationTranslationsRepository.save(applicationTranslation);
  }

  async save(application: Application) {
    return this.applicationsRepository.save(application);
  }

  async delete(id: string) {
    return this.applicationsRepository.delete({ id });
  }

  async getApplicationsTotalSize(): Promise<number> {
    const applications = await this.applicationsRepository.find({
      where: { isInstalled: true },
      select: { size: true },
    });
    return applications.reduce<number>(
      (sum, application) => sum + application.size,
      0,
    );
  }
}
