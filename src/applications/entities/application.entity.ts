import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
} from 'typeorm';
import { ApplicationTranslation } from './application-translation.entity';
import { Item } from 'src/items/entities/item.entity';
import { ApplicationType } from 'src/application-types/entities/application-type.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { Visibility } from 'src/visibilities/entities/visibility.entity';
import { IMaestroApplication } from 'src/maestro/interfaces/maestro-application.interface';

@Entity()
export class Application {
  @PrimaryColumn({ unique: true })
  id: string;

  @Column()
  name: string;

  @Column()
  displayName: string;

  @Column()
  port: string;

  @Column({ nullable: true })
  exposeServices: string | null;

  @Column()
  version: string;

  @Column()
  logo: string;

  @Column()
  image: string;

  @Column()
  url: string;

  @Column()
  size: number;

  @Column()
  compose: string;

  @Column()
  isInstalled: boolean;

  @Column()
  isDownloading: boolean;

  @Column()
  isInstalling: boolean;

  @Column()
  isUp: boolean;

  @Column()
  isUpdateNeeded: boolean;

  @Column({ default: false })
  isUpdating: boolean;

  @Column()
  isOlip: boolean;

  @Column({ nullable: true })
  hooks: string | null;

  @OneToMany(
    () => ApplicationTranslation,
    (applicationTranslation) => applicationTranslation.application,
    { cascade: true },
  )
  applicationTranslations: ApplicationTranslation[];

  @ManyToOne(
    () => ApplicationType,
    (applicationType) => applicationType.applications,
  )
  applicationType: ApplicationType;

  @OneToMany(() => Item, (item) => item.application)
  items: Item[];

  @OneToMany(() => Playlist, (playlist) => playlist.application)
  playlists: Playlist[];

  @OneToOne(() => Visibility, (visibility) => visibility.application)
  @JoinColumn()
  visibility: Visibility;

  constructor(
    maestroApplication: IMaestroApplication,
    applicationType: ApplicationType,
    olipHost: string,
  ) {
    if (!maestroApplication) return;
    this.id = maestroApplication.id;
    this.logo = maestroApplication.logo;
    this.name = maestroApplication.name;
    this.displayName = maestroApplication.displayName;
    this.port = maestroApplication.port;
    this.image = maestroApplication.image;
    this.url = `${maestroApplication.name}.${olipHost}`;
    this.size = maestroApplication.size;
    this.version = maestroApplication.version;
    this.compose = maestroApplication.compose;
    this.isInstalled = maestroApplication.id === 'olip' ? true : false;
    this.isInstalling = false;
    this.isDownloading = false;
    this.isUp = false;
    this.isUpdateNeeded = false;
    this.isUpdating = false;
    this.isOlip = maestroApplication.id === 'olip' ? true : false;
    this.hooks = maestroApplication.hooks;
    this.applicationType = applicationType;
    this.exposeServices = maestroApplication.exposeServices;
  }
}
