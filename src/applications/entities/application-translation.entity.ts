import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Application } from './application.entity';
import { Language } from 'src/languages/entities/language.entity';
import { IMaestroApplicationTranslation } from 'src/maestro/interfaces/maestro-application-translation.interface';

@Entity()
export class ApplicationTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  shortDescription: string;

  @Column()
  longDescription: string;

  @Column({ nullable: true })
  indexId: string | null;

  @ManyToOne(
    () => Application,
    (application) => application.applicationTranslations,
    { onDelete: 'CASCADE' },
  )
  application: Application;

  @ManyToOne(() => Language, (language) => language.applicationTranslations)
  language: Language;

  constructor(
    maestroApplicationTranslation: IMaestroApplicationTranslation,
    language: Language,
  ) {
    if (!maestroApplicationTranslation) return;
    this.shortDescription = maestroApplicationTranslation.shortDescription;
    this.longDescription = maestroApplicationTranslation.longDescription;
    this.language = language;
  }
}
