import { Injectable, NotAcceptableException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Setting } from './entities/setting.entity';
import { Repository } from 'typeorm';
import { CreateSettingDto } from './dto/create-setting.dto';
import { UpdateSettingDto } from './dto/update-setting.dto';
import { SettingKey } from './enums/setting-key.enum';
import { invalidateCache } from 'src/utils/caching.util';
import { UploadService } from 'src/upload/upload.service';
import { join } from 'path';
import { existsSync } from 'fs';

@Injectable()
export class SettingsService {
  constructor(
    @InjectRepository(Setting) private settingRepository: Repository<Setting>,
    private uploadService: UploadService,
  ) {}

  async create(createSettingDto: CreateSettingDto): Promise<Setting | null> {
    let setting = await this.findOne(createSettingDto.key);
    if (setting !== null) return null;
    setting = new Setting(createSettingDto);
    setting = await this.settingRepository.save(setting);
    await invalidateCache(Setting);
    return setting;
  }

  async update(
    key: SettingKey,
    updateSettingDto: UpdateSettingDto,
  ): Promise<Setting | null> {
    let setting = await this.findOne(key);
    if (setting === null) return null;

    if (setting.key === SettingKey.LOGO) {
      if (!updateSettingDto.value) {
        return;
      }
      let image = '';
      if (updateSettingDto.value.startsWith('images/')) {
        const imagePath = join(
          __dirname,
          '../../data/static',
          updateSettingDto.value,
        );
        if (existsSync(imagePath)) {
          image = updateSettingDto.value;
        } else {
          throw new NotAcceptableException(
            `image ${updateSettingDto.value} not found`,
          );
        }
      } else {
        image = await this.uploadService.image(updateSettingDto.value);
      }
      setting.value = image;
    } else if (setting.key === SettingKey.PRINCIPAL_COLOR) {
      const hexColorRegex = /^#[0-9A-F]{3}([0-9A-F]{3})?$/i;
      if (!hexColorRegex.test(updateSettingDto.value)) {
        throw new NotAcceptableException(
          `principal color ${updateSettingDto.value} not valid`,
        );
      }
      setting.value = `${updateSettingDto.value}`;
    } else {
      setting.value = updateSettingDto.value;
    }

    setting = await this.settingRepository.save(setting);
    await invalidateCache(Setting);
    return setting;
  }

  findOne(key: SettingKey): Promise<Setting | null> {
    return this.settingRepository.findOne({
      where: {
        key,
      },
    });
  }
}
