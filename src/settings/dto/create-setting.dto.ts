import { IsEnum, IsString } from 'class-validator';
import { SettingKey } from '../enums/setting-key.enum';
import { ApiProperty } from '@nestjs/swagger';

export class CreateSettingDto {
  @ApiProperty({ enum: SettingKey, enumName: 'SettingKey' })
  @IsEnum(SettingKey)
  key: SettingKey;

  @ApiProperty({
    example:
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==',
  })
  @IsString()
  value: string;
}
