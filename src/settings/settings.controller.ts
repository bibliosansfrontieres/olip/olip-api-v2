import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
} from '@nestjs/common';
import { SettingsService } from './settings.service';
import { SettingKey } from './enums/setting-key.enum';
import { UpdateSettingDto } from './dto/update-setting.dto';
import {
  ApiBody,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { Setting } from './entities/setting.entity';
import { cachedMethod } from 'src/utils/caching.util';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';

@ApiTags('settings')
@Controller('api/settings')
export class SettingsController {
  constructor(private settingsService: SettingsService) {}

  @Patch(':key')
  @Auth(PermissionEnum.UPDATE_SETTING)
  @ApiOperation({
    summary: 'Update a setting',
    description: 'Needs authentification and UPDATE_SETTING permission',
  })
  @ApiBody({ type: UpdateSettingDto })
  @ApiParam({ name: 'key', enum: SettingKey })
  @ApiOkResponse({ type: Setting })
  @ApiNotFoundResponse({ description: 'Setting not found' })
  async update(
    @Param('key') key: SettingKey,
    @Body() updateSettingDto: UpdateSettingDto,
  ) {
    const setting = await this.settingsService.update(key, updateSettingDto);
    if (setting === null) {
      throw new NotFoundException(`Setting "${key}" not found`);
    }
    return this.settingsService.update(key, updateSettingDto);
  }

  @Get(':key')
  @ApiOperation({ summary: 'Get a setting value' })
  @ApiParam({ name: 'key', enum: SettingKey })
  @ApiOkResponse({ type: Setting })
  @ApiNotFoundResponse({ description: 'Setting not found' })
  async findOne(@Param('key') key: SettingKey) {
    return cachedMethod(
      'settingsService',
      'findOne',
      [{ key }],
      async () => {
        const setting = await this.settingsService.findOne(key);
        if (setting === null)
          throw new NotFoundException(`Setting "${key}" not found`);
        return setting;
      },
      [Setting],
    );
  }
}
