import { Test, TestingModule } from '@nestjs/testing';
import { SettingsService } from './settings.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Setting } from './entities/setting.entity';
import { UploadService } from 'src/upload/upload.service';
import { uploadServiceMock } from 'src/tests/mocks/providers/upload-service.mock';

describe('SettingsService', () => {
  let service: SettingsService;
  let settingsRepository: Repository<Setting>;

  const SETTINGS_REPOSITORY_TOKEN = getRepositoryToken(Setting);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SettingsService,
        { provide: SETTINGS_REPOSITORY_TOKEN, useClass: Repository<Setting> },
        UploadService,
      ],
    })
      .overrideProvider(UploadService)
      .useValue(uploadServiceMock)
      .compile();

    service = module.get<SettingsService>(SettingsService);
    settingsRepository = module.get<Repository<Setting>>(
      SETTINGS_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
