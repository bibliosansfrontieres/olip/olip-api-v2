import { Test, TestingModule } from '@nestjs/testing';
import { SettingsController } from './settings.controller';
import { SettingsService } from './settings.service';
import { Setting } from './entities/setting.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { ConfigService } from '@nestjs/config';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { UploadService } from 'src/upload/upload.service';
import { uploadServiceMock } from 'src/tests/mocks/providers/upload-service.mock';

describe('SettingsController', () => {
  let controller: SettingsController;
  let settingsRepository: Repository<Setting>;

  const SETTINGS_REPOSITORY_TOKEN = getRepositoryToken(Setting);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SettingsController],
      providers: [
        SettingsService,
        { provide: SETTINGS_REPOSITORY_TOKEN, useClass: Repository<Setting> },
        JwtService,
        UsersService,
        ConfigService,
        UploadService,
      ],
    })
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(UploadService)
      .useValue(uploadServiceMock)
      .compile();

    controller = module.get<SettingsController>(SettingsController);
    settingsRepository = module.get<Repository<Setting>>(
      SETTINGS_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
