import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { SettingKey } from '../enums/setting-key.enum';
import { CreateSettingDto } from '../dto/create-setting.dto';

@Entity()
export class Setting {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  key: SettingKey;

  @Column()
  value: string;

  constructor(createSettingDto: CreateSettingDto) {
    if (!createSettingDto) return;
    this.key = createSettingDto.key;
    this.value = createSettingDto.value;
  }
}
