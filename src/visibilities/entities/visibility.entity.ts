import { Application } from 'src/applications/entities/application.entity';
import { Category } from 'src/categories/entities/category.entity';
import { Role } from 'src/roles/entities/role.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  JoinTable,
  ManyToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Visibility {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToMany(() => Role, (role) => role.visibilities)
  @JoinTable()
  roles: Role[];

  @ManyToMany(() => User, (user) => user.visibilities)
  @JoinTable()
  users: User[];

  @OneToOne(() => Category, (category) => category.visibility, {
    nullable: true,
  })
  category: Category;

  @OneToOne(() => Application, (application) => application.visibility, {
    nullable: true,
    onDelete: 'CASCADE',
  })
  application: Application;

  constructor(
    roles: Role[],
    users: User[],
    category?: Category,
    application?: Application,
  ) {
    if (!category && !application) return;
    this.category = category;
    this.application = application;
    this.roles = roles;
    this.users = users;
  }
}
