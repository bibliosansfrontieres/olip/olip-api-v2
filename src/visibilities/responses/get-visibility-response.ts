import { Category } from 'src/categories/entities/category.entity';
import { Visibility } from '../entities/visibility.entity';
import { User } from 'src/users/entities/user.entity';
import { Role } from 'src/roles/entities/role.entity';
import { ApiProperty } from '@nestjs/swagger';

export class GetVisibilityResponse {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({
    isArray: true,
    example: [
      {
        id: 1,
        name: 'admin',
      },
    ],
  })
  roles: Role[];

  @ApiProperty({
    isArray: true,
    example: [
      {
        id: 1,
        username: 'username',
        photo: 'images/image.png',
        language: 'eng',
      },
    ],
  })
  users: User[];

  @ApiProperty({
    example: {
      id: 1,
      pictogram: 'images/image.png',
      isHomepageDisplayed: true,
      order: 0,
    },
  })
  category: Category;

  constructor(visibility: Visibility) {
    this.id = visibility.id;
    this.roles = visibility.roles;
    this.users = visibility.users;
    this.category = visibility.category;
  }
}
