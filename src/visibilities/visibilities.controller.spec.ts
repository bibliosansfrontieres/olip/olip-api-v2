import { Test, TestingModule } from '@nestjs/testing';
import { VisibilitiesController } from './visibilities.controller';
import { VisibilitiesService } from './visibilities.service';
import { Visibility } from './entities/visibility.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from 'src/users/users.service';
import { RolesService } from 'src/roles/roles.service';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { rolesServiceMock } from 'src/tests/mocks/providers/roles-service.mock';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';

describe('VisibilitiesController', () => {
  let controller: VisibilitiesController;

  beforeEach(async () => {
    let visibilityRepository: Repository<Visibility>;

    const VISIBILITY_REPOSITORY_TOKEN = getRepositoryToken(Visibility);

    const module: TestingModule = await Test.createTestingModule({
      controllers: [VisibilitiesController],
      providers: [
        {
          provide: VISIBILITY_REPOSITORY_TOKEN,
          useClass: Repository<Visibility>,
        },
        VisibilitiesService,
        UsersService,
        RolesService,
        JwtService,
        ConfigService,
      ],
    })
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(RolesService)
      .useValue(rolesServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    controller = module.get<VisibilitiesController>(VisibilitiesController);
    visibilityRepository = module.get<Repository<Visibility>>(
      VISIBILITY_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
