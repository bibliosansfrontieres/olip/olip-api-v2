import { IsNumber, IsOptional } from 'class-validator';

export class UpdateVisibilityDto {
  @IsOptional()
  @IsNumber({}, { each: true })
  userIds?: number[];

  @IsOptional()
  @IsNumber({}, { each: true })
  roleIds?: number[];
}
