import { CreateCategoryDto } from 'src/categories/dto/create-category.dto';

export class CreateVisibilityDto {
  userIds: number[];
  roleIds: number[];

  constructor(createVisibilityDto: CreateCategoryDto | CreateVisibilityDto) {
    this.userIds = createVisibilityDto.userIds || [];
    this.roleIds = createVisibilityDto.roleIds || [];
  }
}
