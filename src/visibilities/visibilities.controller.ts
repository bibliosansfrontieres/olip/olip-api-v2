import { Controller, Get, Param } from '@nestjs/common';
import { VisibilitiesService } from './visibilities.service';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionEnum } from 'src/permissions/enums/permission.enum';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { GetVisibilityResponse } from './responses/get-visibility-response';

@ApiTags('visibilities')
@Controller('api/visibilities')
export class VisibilitiesController {
  constructor(private readonly visibilitiesService: VisibilitiesService) {}

  @Auth(PermissionEnum.READ_USERS)
  @Get(':id')
  @ApiOperation({ summary: 'Get visibility users/roles' })
  @ApiOkResponse({ type: GetVisibilityResponse })
  @ApiNotFoundResponse()
  getVisibility(@Param('id') id: string) {
    return this.visibilitiesService.get(+id);
  }
}
