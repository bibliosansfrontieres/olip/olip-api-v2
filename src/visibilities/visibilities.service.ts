import { Injectable, NotFoundException } from '@nestjs/common';
import { Category } from 'src/categories/entities/category.entity';
import { Visibility } from './entities/visibility.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateVisibilityDto } from './dto/create-visibility.dto';
import { UsersService } from 'src/users/users.service';
import { RolesService } from 'src/roles/roles.service';
import { UpdateVisibilityDto } from './dto/update-visibility.dto';
import { GetVisibilityResponse } from './responses/get-visibility-response';
import { Application } from 'src/applications/entities/application.entity';
import { invalidateCache } from 'src/utils/caching.util';

@Injectable()
export class VisibilitiesService {
  constructor(
    @InjectRepository(Visibility)
    private visibilityRepository: Repository<Visibility>,
    private usersService: UsersService,
    private rolesService: RolesService,
  ) {}

  async get(id: number) {
    const visibility = await this.visibilityRepository.findOne({
      where: { id },
      relations: { users: true, roles: true },
    });
    if (visibility === null)
      throw new NotFoundException('visibility not found');
    return new GetVisibilityResponse(visibility);
  }

  async create(
    createVisibilityDto: CreateVisibilityDto,
    category?: Category,
    application?: Application,
  ) {
    const roles = [];
    const users = [];
    for (const userId of createVisibilityDto.userIds) {
      const user = await this.usersService.findOne({ where: { id: userId } });
      if (user === null) {
        throw new NotFoundException(`user ID ${userId} not found`);
      }
      users.push(user);
    }
    for (const roleId of createVisibilityDto.roleIds) {
      const role = await this.rolesService.findRoleById(roleId);
      if (role === null) {
        throw new NotFoundException(`role ID ${roleId} not found`);
      }
      roles.push(role);
    }
    let visibility = new Visibility(roles, users, category, application);
    visibility = await this.visibilityRepository.save(visibility);
    await invalidateCache([Visibility]);
    return visibility;
  }

  async update(id: number, updateVisibilityDto: UpdateVisibilityDto) {
    const visibility = await this.visibilityRepository.findOne({
      where: { id },
    });
    if (visibility === null) {
      throw new NotFoundException(`visibility ID ${id} not found`);
    }
    if (updateVisibilityDto.roleIds !== undefined) {
      const roles = [];
      for (const roleId of updateVisibilityDto.roleIds) {
        const role = await this.rolesService.findRoleById(roleId);
        if (role === null) {
          throw new NotFoundException(`role ID ${roleId} not found`);
        }
        roles.push(role);
      }
      visibility.users = [];
      visibility.roles = roles;
    } else if (updateVisibilityDto.userIds !== undefined) {
      const users = [];
      for (const userId of updateVisibilityDto.userIds) {
        const user = await this.usersService.findOne({ where: { id: userId } });
        if (user === null) {
          throw new NotFoundException(`user ID ${userId} not found`);
        }
        users.push(user);
      }
      visibility.roles = [];
      visibility.users = users;
    } else {
      visibility.roles = [];
      visibility.users = [];
    }
    await this.visibilityRepository.save(visibility);
    await invalidateCache([Visibility]);
  }
}
