import { Module } from '@nestjs/common';
import { VisibilitiesService } from './visibilities.service';
import { VisibilitiesController } from './visibilities.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Visibility } from './entities/visibility.entity';
import { UsersModule } from 'src/users/users.module';
import { RolesModule } from 'src/roles/roles.module';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    TypeOrmModule.forFeature([Visibility]),
    UsersModule,
    RolesModule,
    JwtModule,
    ConfigModule,
  ],
  controllers: [VisibilitiesController],
  providers: [VisibilitiesService],
  exports: [VisibilitiesService],
})
export class VisibilitiesModule {}
