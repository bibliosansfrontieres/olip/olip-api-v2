import { Test, TestingModule } from '@nestjs/testing';
import { VisibilitiesService } from './visibilities.service';
import { Repository } from 'typeorm';
import { Visibility } from './entities/visibility.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from 'src/users/users.service';
import { RolesService } from 'src/roles/roles.service';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { rolesServiceMock } from 'src/tests/mocks/providers/roles-service.mock';

describe('VisibilitiesService', () => {
  let service: VisibilitiesService;

  beforeEach(async () => {
    let visibilityRepository: Repository<Visibility>;

    const VISIBILITY_REPOSITORY_TOKEN = getRepositoryToken(Visibility);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: VISIBILITY_REPOSITORY_TOKEN,
          useClass: Repository<Visibility>,
        },
        VisibilitiesService,
        UsersService,
        RolesService,
      ],
    })
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(RolesService)
      .useValue(rolesServiceMock)
      .compile();

    service = module.get<VisibilitiesService>(VisibilitiesService);
    visibilityRepository = module.get<Repository<Visibility>>(
      VISIBILITY_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
