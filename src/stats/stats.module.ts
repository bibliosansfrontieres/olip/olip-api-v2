import { Module, forwardRef } from '@nestjs/common';
import { StatsService } from './stats.service';
import { StatsController } from './stats.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConnectedUser } from './entities/connected-user.entity';
import { SocketModule } from 'src/socket/socket.module';
import { ItemsModule } from 'src/items/items.module';
import { CategoryPlaylistItemsModule } from 'src/category-playlist-items/category-playlist-items.module';
import { CategoriesModule } from 'src/categories/categories.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([ConnectedUser]),
    forwardRef(() => SocketModule),
    forwardRef(() => ItemsModule),
    forwardRef(() => CategoryPlaylistItemsModule),
    forwardRef(() => CategoriesModule),
  ],
  controllers: [StatsController],
  providers: [StatsService],
  exports: [StatsService],
})
export class StatsModule {}
