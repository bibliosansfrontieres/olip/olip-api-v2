import { Test, TestingModule } from '@nestjs/testing';
import { StatsService } from './stats.service';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { ConnectedUser } from './entities/connected-user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { SocketGateway } from 'src/socket/socket.gateway';
import { socketGatewayMock } from 'src/tests/mocks/providers/socket-gateway.mock';
import { ItemsService } from 'src/items/items.service';
import { itemsServiceMock } from 'src/tests/mocks/providers/items-service.mock';
import { CategoryPlaylistItemsService } from 'src/category-playlist-items/category-playlist-items.service';
import { categoryPlaylistItemsServiceMock } from 'src/tests/mocks/providers/category-playlist-items-service.mock';
import { CategoriesService } from 'src/categories/categories.service';
import { categoriesServiceMock } from 'src/tests/mocks/providers/categories-service.mock';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';
import { GetInstalledContentsByCategoryOptionsDto } from './dto/get-installed-contents-by-category-options.dto';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { queryBuilderMock } from 'src/tests/mocks/objects/query-builder.mock';
import { categoryMock } from 'src/tests/mocks/objects/category.mock';
import { GetInstalledContentsByCategoryDto } from './dto/get-installed-contents-by-category.dto';

describe('StatsService', () => {
  let service: StatsService;
  let categoryPlaylistItemsService: CategoryPlaylistItemsService;
  let categoriesService: CategoriesService;
  let connectedUserRepository: Repository<ConnectedUser>;
  const CONNECTED_USER_REPOSITORY_TOKEN = getRepositoryToken(ConnectedUser);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StatsService,
        SocketGateway,
        ItemsService,
        CategoryPlaylistItemsService,
        CategoriesService,
        JwtService,
        UsersService,
        ConfigService,
        {
          provide: CONNECTED_USER_REPOSITORY_TOKEN,
          useClass: Repository<ConnectedUser>,
        },
      ],
    })
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .overrideProvider(CategoryPlaylistItemsService)
      .useValue(categoryPlaylistItemsServiceMock)
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    service = module.get<StatsService>(StatsService);
    connectedUserRepository = module.get<Repository<ConnectedUser>>(
      CONNECTED_USER_REPOSITORY_TOKEN,
    );
    categoryPlaylistItemsService = module.get(CategoryPlaylistItemsService);
    categoriesService = module.get(CategoriesService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(connectedUserRepository).toBeDefined();
  });

  describe('getInstalledContentsByCategory()', () => {
    const getInstalledContentsByCategoryOptionsDto: GetInstalledContentsByCategoryOptionsDto =
      {
        languageIdentifier: 'eng',
      };

    const categoriesMock = [categoryMock({ id: 1 }), categoryMock({ id: 2 })];

    beforeEach(() => {
      jest
        .spyOn(categoriesService, 'findAll')
        .mockResolvedValue(categoriesMock);
    });

    it('should be defined', () => {
      expect(service.getInstalledContentsByCategory).toBeDefined();
    });

    it('should call findAll() from CategoriesService', async () => {
      await service.getInstalledContentsByCategory(
        getInstalledContentsByCategoryOptionsDto,
      );

      expect(categoriesService.findAll).toHaveBeenCalledTimes(1);
    });

    it('should call getTranslation() from categoriesService', async () => {
      await service.getInstalledContentsByCategory(
        getInstalledContentsByCategoryOptionsDto,
      );

      for (const categoryMock of categoriesMock) {
        expect(categoriesService.getTranslation).toHaveBeenCalledWith(
          categoryMock,
          getInstalledContentsByCategoryOptionsDto.languageIdentifier,
        );
      }

      expect(categoriesService.getTranslation).toHaveBeenCalledTimes(
        categoriesMock.length,
      );
    });

    it('should call getQueryBuilder() from CategoryPlaylistItemsService', async () => {
      await service.getInstalledContentsByCategory(
        getInstalledContentsByCategoryOptionsDto,
      );
      expect(categoryPlaylistItemsService.getQueryBuilder).toHaveBeenCalled();
    });

    it('should call right relations', async () => {
      const mockQueryBuilder = queryBuilderMock<CategoryPlaylistItem>();

      jest
        .spyOn(categoryPlaylistItemsService, 'getQueryBuilder')
        .mockReturnValue(mockQueryBuilder);

      await service.getInstalledContentsByCategory(
        getInstalledContentsByCategoryOptionsDto,
      );

      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'categoryPlaylistItem.categoryPlaylist',
        'categoryPlaylist',
      );
      expect(mockQueryBuilder.innerJoinAndSelect).toHaveBeenCalledWith(
        'categoryPlaylist.category',
        'category',
      );
    });

    it('should call getMany()', async () => {
      const mockQueryBuilder = queryBuilderMock<CategoryPlaylistItem>();

      jest
        .spyOn(categoryPlaylistItemsService, 'getQueryBuilder')
        .mockReturnValue(mockQueryBuilder);

      await service.getInstalledContentsByCategory(
        getInstalledContentsByCategoryOptionsDto,
      );

      expect(mockQueryBuilder.getMany).toHaveBeenCalled();
    });

    it('should return GetInstalledContentsByCategoryDto', async () => {
      const result = await service.getInstalledContentsByCategory(
        getInstalledContentsByCategoryOptionsDto,
      );

      expect(result).toBeInstanceOf(GetInstalledContentsByCategoryDto);
    });
  });
});
