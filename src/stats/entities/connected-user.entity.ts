import { User } from 'src/users/entities/user.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateConnectedUserDto } from '../dto/create-connected-user.dto';

@Entity()
export class ConnectedUser {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: false })
  isActive: boolean;

  @Column()
  ip: string;

  @Column()
  socketId: string;

  @Column({ nullable: true })
  clientId: string | null;

  @ManyToMany(() => User, (user) => user.connectedUsers, {
    onDelete: 'CASCADE',
  })
  @JoinTable()
  users: User[];

  @Column({ default: null })
  connectedAt: string;

  @Column({ default: null })
  disconnectedAt: string;

  constructor(
    createConnectedUserDto: CreateConnectedUserDto,
    user: User | null,
  ) {
    if (!createConnectedUserDto) return;
    this.isActive = true;
    this.ip = createConnectedUserDto.ip;
    this.socketId = createConnectedUserDto.socketId;
    this.clientId = createConnectedUserDto.clientId;
    if (user !== null) {
      this.users = [user];
    }
    const date = new Date(Date.now()).toISOString();
    this.connectedAt = date;
    this.disconnectedAt = date;
  }
}
