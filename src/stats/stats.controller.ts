import { Controller, Get, Query } from '@nestjs/common';
import { StatsService } from './stats.service';
import {
  ApiOkResponse,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { GetInstalledContentsDto } from './dto/get-installed-contents.dto';
import { GetInstalledContentsByCategoryDto } from './dto/get-installed-contents-by-category.dto';
import { GetInstalledContentsByCategoryOptionsDto } from './dto/get-installed-contents-by-category-options.dto';
import { GetLiveUsersDto } from './dto/get-live-users.dto';

@ApiTags('stats')
@Controller('api/stats')
export class StatsController {
  constructor(private readonly statsService: StatsService) {}

  @ApiOperation({ description: 'Get how many contents are installed' })
  @ApiOkResponse({ type: GetInstalledContentsDto })
  @Get('installed-contents')
  getInstalledContents() {
    return this.statsService.getInstalledContents();
  }

  @ApiOperation({ description: 'Get how many users are connected' })
  @ApiOkResponse({ type: GetLiveUsersDto })
  @Get('live-users')
  getLiveUsers() {
    return this.statsService.getLiveUsers();
  }

  @ApiOperation({
    description: 'Get how many contents are installed by category',
  })
  @ApiOkResponse({ type: GetInstalledContentsByCategoryDto })
  @ApiQuery({ type: GetInstalledContentsByCategoryOptionsDto })
  @Get('installed-contents-by-category')
  getInstalledContentsByCategory(
    @Query()
    getInstalledContentsByCategoryOptionsDto: GetInstalledContentsByCategoryOptionsDto,
  ) {
    return this.statsService.getInstalledContentsByCategory(
      getInstalledContentsByCategoryOptionsDto,
    );
  }
}
