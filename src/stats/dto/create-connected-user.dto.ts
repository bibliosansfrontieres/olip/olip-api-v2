export class CreateConnectedUserDto {
  ip: string;
  socketId: string;
  clientId?: string;
}
