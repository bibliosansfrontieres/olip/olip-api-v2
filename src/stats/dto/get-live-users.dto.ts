export class GetLiveUsersDto {
  liveUsers: number;
  liveConnectedUsers: number;

  constructor(liveUsers: number, liveConnectedUsers: number) {
    this.liveUsers = liveUsers;
    this.liveConnectedUsers = liveConnectedUsers;
  }
}
