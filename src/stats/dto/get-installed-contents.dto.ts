export class GetInstalledContentsDto {
  installedContents: number;

  constructor(installedContents: number) {
    this.installedContents = installedContents;
  }
}
