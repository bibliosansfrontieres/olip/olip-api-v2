import { ApiProperty } from '@nestjs/swagger';
import { Category } from 'src/categories/entities/category.entity';

export interface CategoryStats {
  category: Category;
  totalItems: number;
  purcentage: number;
}

export class GetInstalledContentsByCategoryDto {
  @ApiProperty({ example: 50 })
  totalItems: number;
  @ApiProperty({
    isArray: true,
    example: [
      {
        category: {
          id: 1,
          pictogram: 'images/4a9b095f-3416-40da-b04a-8a35b943091a.svg',
          image: 'images/',
          isHomepageDisplayed: true,
          order: 1,
          categoryTranslations: [
            {
              id: 1,
              title: 'Information',
              description:
                'No description found in Omeka, this is a placeholder',
              language: {
                id: 67,
                identifier: 'eng',
                oldIdentifier: 'en',
                label: 'English',
              },
            },
          ],
        },
        totalItems: 50,
        purcentage: 100,
      },
    ],
  })
  categories: CategoryStats[];

  constructor(totalItems: number, categories: CategoryStats[]) {
    this.totalItems = totalItems;
    this.categories = categories;
  }
}
