import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class GetInstalledContentsByCategoryOptionsDto {
  @ApiProperty({ example: 'eng' })
  @IsString()
  languageIdentifier: string;
}
