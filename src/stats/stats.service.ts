import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ConnectedUser } from './entities/connected-user.entity';
import { Repository } from 'typeorm';
import { CreateConnectedUserDto } from './dto/create-connected-user.dto';
import { SocketGateway } from 'src/socket/socket.gateway';
import { GetLiveUsersDto } from './dto/get-live-users.dto';
import { User } from 'src/users/entities/user.entity';
import {
  filterConnectedUsers,
  filterDuplicatedClientId,
} from 'src/utils/filters.util';
import { ItemsService } from 'src/items/items.service';
import { GetInstalledContentsDto } from './dto/get-installed-contents.dto';
import { CategoryPlaylistItemsService } from 'src/category-playlist-items/category-playlist-items.service';
import { CategoriesService } from 'src/categories/categories.service';
import {
  CategoryStats,
  GetInstalledContentsByCategoryDto,
} from './dto/get-installed-contents-by-category.dto';
import { GetInstalledContentsByCategoryOptionsDto } from './dto/get-installed-contents-by-category-options.dto';

@Injectable()
export class StatsService {
  constructor(
    @InjectRepository(ConnectedUser)
    private connectedUsersRepository: Repository<ConnectedUser>,
    @Inject(forwardRef(() => SocketGateway))
    private socketGateway: SocketGateway,
    @Inject(forwardRef(() => ItemsService))
    private itemsService: ItemsService,
    @Inject(forwardRef(() => CategoryPlaylistItemsService))
    private categoryPlaylistItemsService: CategoryPlaylistItemsService,
    @Inject(forwardRef(() => CategoriesService))
    private categoriesService: CategoriesService,
  ) {}

  async createConnectedUser(
    createConnectedUserDto: CreateConnectedUserDto,
    user: User | null,
  ) {
    let connectedUser = new ConnectedUser(createConnectedUserDto, user);
    connectedUser = await this.connectedUsersRepository.save(connectedUser);
    await this.socketGateway.emitLiveUsers();
    return connectedUser;
  }

  async removeConnectedUser(socketId: string) {
    let connectedUser = await this.connectedUsersRepository.findOne({
      where: { socketId, isActive: true },
    });
    if (connectedUser === null) return null;
    connectedUser.isActive = false;
    connectedUser.disconnectedAt = new Date(Date.now()).toISOString();
    connectedUser = await this.connectedUsersRepository.save(connectedUser);
    await this.socketGateway.emitLiveUsers();
    return connectedUser;
  }

  async removeAllConnectedUsers() {
    const connectedUsers = await this.connectedUsersRepository.find({
      where: { isActive: true },
    });
    for (const connectedUser of connectedUsers) {
      connectedUser.isActive = false;
      connectedUser.disconnectedAt = new Date(Date.now()).toISOString();
    }
    await this.connectedUsersRepository.save(connectedUsers);
  }

  async getLiveUsers() {
    const connectedUsers = await this.connectedUsersRepository.find({
      where: { isActive: true },
      relations: { users: true },
    });
    const liveUsers = filterDuplicatedClientId(connectedUsers);
    const liveConnectedUsers = filterConnectedUsers(connectedUsers);
    return new GetLiveUsersDto(liveUsers.length, liveConnectedUsers.length);
  }

  async getInstalledContents() {
    const items = await this.itemsService.find();
    return new GetInstalledContentsDto(items.length);
  }

  async getInstalledContentsByCategory(
    getInstalledContentsByCategoryOptionsDto: GetInstalledContentsByCategoryOptionsDto,
  ) {
    const categories = await this.categoriesService.findAll();

    // Get categories translations
    for (const category of categories) {
      await this.categoriesService.getTranslation(
        category,
        getInstalledContentsByCategoryOptionsDto.languageIdentifier,
      );
    }

    // Get all categoryPlaylistItems
    const categoryPlaylistItems = await this.categoryPlaylistItemsService
      .getQueryBuilder()
      .leftJoinAndSelect(
        'categoryPlaylistItem.categoryPlaylist',
        'categoryPlaylist',
      )
      .innerJoinAndSelect('categoryPlaylist.category', 'category')
      .getMany();

    const totalCategoryPlaylistItems = categoryPlaylistItems.length;

    const categoriesStats: CategoryStats[] = [];
    for (const category of categories) {
      // Get categoryPlaylistItems of this category
      const categoryCategoryPlaylistItems = categoryPlaylistItems.filter(
        (item) => item.categoryPlaylist.category.id === category.id,
      );

      // Create category stats
      categoriesStats.push({
        category,
        totalItems: categoryCategoryPlaylistItems.length,
        purcentage: Math.floor(
          (categoryCategoryPlaylistItems.length * 100) /
            totalCategoryPlaylistItems,
        ),
      });
    }

    return new GetInstalledContentsByCategoryDto(
      totalCategoryPlaylistItems,
      categoriesStats,
    );
  }
}
