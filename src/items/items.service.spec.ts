import { Test, TestingModule } from '@nestjs/testing';
import { ItemsService } from './items.service';
import { Repository } from 'typeorm';
import { Item } from './entities/item.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { playlistsServiceMock } from 'src/tests/mocks/providers/playlists-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/tests/mocks/providers/applications-service.mock';
import { FilesService } from 'src/files/files.service';
import { filesServiceMock } from 'src/tests/mocks/providers/files-service.mock';
import { DublinCoreItemsService } from 'src/dublin-core-items/dublin-core-items.service';
import { dublinCoreItemsServiceMock } from 'src/tests/mocks/providers/dublin-core-items-service.mock';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { dublinCoreTypesServiceMock } from 'src/tests/mocks/providers/dublin-core-types-service.mock';
import { CategoriesService } from 'src/categories/categories.service';
import { categoriesServiceMock } from 'src/tests/mocks/providers/categories-service.mock';
import { SearchService } from 'src/search/search.service';
import { searchServiceMock } from 'src/tests/mocks/providers/search-service.mock';
import { applicationMock } from 'src/tests/mocks/objects/application.mock';
import fs from 'fs';
import path from 'path';
import * as cachingUtil from 'src/utils/caching.util';
import * as isVmUtil from 'src/utils/is-vm.util';
import { fileMock } from 'src/tests/mocks/objects/file.mock';
import { SocketGateway } from 'src/socket/socket.gateway';
import { DockerService } from 'src/docker/docker.service';
import { dockerServiceMock } from 'src/tests/mocks/providers/docker-service.mock';
import { socketGatewayMock } from 'src/tests/mocks/providers/socket-gateway.mock';
import { itemMock } from 'src/tests/mocks/objects/item.mock';
import { playlistMock } from 'src/tests/mocks/objects/playlist.mock';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { jwtServiceMock } from 'src/tests/mocks/providers/jwt-service.mock';
import { usersServiceMock } from 'src/tests/mocks/providers/users-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/tests/mocks/providers/config-service.mock';

describe('ItemsService', () => {
  let service: ItemsService;
  const ITEMS_REPOSITORY_TOKEN = getRepositoryToken(Item);
  let socketGateway: SocketGateway;
  let applicationsService: ApplicationsService;
  let dockerService: DockerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: ITEMS_REPOSITORY_TOKEN, useClass: Repository<Item> },
        ItemsService,
        PlaylistsService,
        DublinCoreItemsService,
        DublinCoreTypesService,
        ApplicationsService,
        FilesService,
        CategoriesService,
        SearchService,
        SocketGateway,
        DockerService,
        JwtService,
        UsersService,
        ConfigService,
      ],
    })
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .overrideProvider(DublinCoreItemsService)
      .useValue(dublinCoreItemsServiceMock)
      .overrideProvider(DublinCoreTypesService)
      .useValue(dublinCoreTypesServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(FilesService)
      .useValue(filesServiceMock)
      .overrideProvider(SearchService)
      .useValue(searchServiceMock)
      .overrideProvider(DockerService)
      .useValue(dockerServiceMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    service = module.get<ItemsService>(ItemsService);
    applicationsService = module.get<ApplicationsService>(ApplicationsService);
    dockerService = module.get<DockerService>(DockerService);
    socketGateway = module.get<SocketGateway>(SocketGateway);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('uninstall', () => {
    const mockApplication = applicationMock();
    const mockApplicationOlip = applicationMock({ id: 'olip' });
    const mockPlaylist1 = playlistMock();
    const mockPlaylist2 = playlistMock({ id: (Date.now() + 1).toString() });
    const mockPlaylists = [mockPlaylist1, mockPlaylist2];
    const playlistIdsToDeleteFalse = [mockPlaylist1.id, '3'];
    const playlistIdsToDeleteTrue = [mockPlaylist1.id, mockPlaylist2.id];

    const mockItem = itemMock({
      file: fileMock(),
      application: mockApplication,
      playlists: mockPlaylists,
    });

    const mockItemOlip = itemMock({
      file: fileMock(),
      application: mockApplicationOlip,
      playlists: mockPlaylists,
    });

    const mockItemWithoutFile = itemMock({
      application: mockApplicationOlip,
      playlists: mockPlaylists,
    });

    const filePath = '/path/to/file';

    beforeEach(() => {
      jest.spyOn(service, 'findOne').mockResolvedValue(mockItem);
      jest.spyOn(applicationsService, 'uninstallContent').mockResolvedValue();
      jest.spyOn(socketGateway, 'emitStorage').mockResolvedValue();
      jest.spyOn(service, 'remove').mockResolvedValue(mockItem);
      jest.spyOn(fs, 'rmSync').mockReturnValue();
      jest.spyOn(fs, 'existsSync').mockReturnValue(true);
      jest.spyOn(cachingUtil, 'invalidateCache').mockResolvedValue();
      jest.spyOn(service, 'getItemFilePath').mockResolvedValue(filePath);
      jest.spyOn(isVmUtil, 'isVm').mockReturnValue(false);
    });

    it('should be defined', () => {
      expect(service.uninstall).toBeDefined();
    });

    it('should call findOne with right argument', async () => {
      await service.uninstall(mockItem.id);
      expect(service.findOne).toHaveBeenCalledTimes(1);
      expect(service.findOne).toHaveBeenCalledWith({
        where: { id: mockItem.id },
        relations: { file: true, application: true, playlists: true },
      });
    });

    it('should return undefined if no item', async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(null);
      const result = await service.uninstall(mockItem.id);
      expect(result).toBeUndefined();
    });

    it('should return undefined if itemPlaylistIds are not all in playlistIdsToDelete', async () => {
      const result = await service.uninstall(
        mockItem.id,
        playlistIdsToDeleteFalse,
      );
      expect(result).toBeUndefined();
    });

    it('should not return undefined if itemPlaylistIds are all in playlistIdsToDelete', async () => {
      const result = await service.uninstall(
        mockItem.id,
        playlistIdsToDeleteTrue,
      );
      expect(result).not.toBeUndefined();
    });

    it('should call uninstallContent with right argument if application id is not olip', async () => {
      await service.uninstall(mockItem.id);
      expect(applicationsService.uninstallContent).toHaveBeenCalledTimes(1);
      expect(applicationsService.uninstallContent).toHaveBeenCalledWith(
        mockItem,
        mockApplication,
      );
    });

    it('should not call uninstallContent with right argument if application id is olip', async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(mockItemOlip);
      await service.uninstall(mockItemOlip.id);
      expect(applicationsService.uninstallContent).not.toHaveBeenCalled();
    });

    it('should call getItemFilePath with right argument if item.file.path', async () => {
      await service.uninstall(mockItemOlip.id);
      expect(service.getItemFilePath).toHaveBeenCalledTimes(1);
      expect(service.getItemFilePath).toHaveBeenCalledWith(
        mockItem.application,
        mockItem.file.path,
      );
    });

    it('should not call getItemFilePath if no item.file.path', async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(mockItemWithoutFile);
      await service.uninstall(mockItemOlip.id);
      expect(service.getItemFilePath).not.toHaveBeenCalled();
    });

    it('should call existsSync with right argument if item.file.path', async () => {
      await service.uninstall(mockItemOlip.id);
      expect(fs.existsSync).toHaveBeenCalledTimes(1);
      expect(fs.existsSync).toHaveBeenCalledWith(filePath);
    });

    it('should not call existsSynct if no item.file.path', async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(mockItemWithoutFile);
      await service.uninstall(mockItemOlip.id);
      expect(fs.existsSync).not.toHaveBeenCalled();
    });

    it('should call rmSync with right argument if item.file.path, existSync and not isVm', async () => {
      await service.uninstall(mockItemOlip.id);
      expect(fs.rmSync).toHaveBeenCalledTimes(1);
      expect(fs.rmSync).toHaveBeenCalledWith(filePath);
    });

    it('should not call rmSync with right argument if no item.file.path', async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(mockItemWithoutFile);
      await service.uninstall(mockItemOlip.id);
      expect(fs.rmSync).not.toHaveBeenCalled();
    });

    it('should not call rmSync if no existSync', async () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(false);
      await service.uninstall(mockItemOlip.id);
      expect(fs.rmSync).not.toHaveBeenCalled();
    });

    it('should not call rmSync if isVm', async () => {
      jest.spyOn(isVmUtil, 'isVm').mockReturnValue(true);
      await service.uninstall(mockItemOlip.id);
      expect(fs.rmSync).not.toHaveBeenCalled();
    });

    it('should call remove with right argument', async () => {
      await service.uninstall(mockItemOlip.id);
      expect(service.remove).toHaveBeenCalledTimes(1);
      expect(service.remove).toHaveBeenCalledWith(mockItem.id);
    });

    it('should call invalidateCache', async () => {
      await service.uninstall(mockItemOlip.id);
      expect(cachingUtil.invalidateCache).toHaveBeenCalledTimes(1);
      expect(cachingUtil.invalidateCache).toHaveBeenCalledWith([Item]);
    });

    it('should call socketGateway.emitStorage', async () => {
      await service.uninstall(mockItemOlip.id);
      expect(socketGateway.emitStorage).toHaveBeenCalledTimes(1);
    });

    it('should return item', async () => {
      const result = await service.uninstall(mockItemOlip.id);
      expect(result).toEqual(mockItem);
    });
  });

  describe('getItemFilePath', () => {
    const mockApplicationOlip = applicationMock({ id: 'olip' });
    const mockApplication = applicationMock();
    const itemFilePath = '/path/to/file';

    beforeEach(() => {
      jest.spyOn(path, 'join').mockReturnValue(itemFilePath + '/join');
      jest.spyOn(dockerService, 'getApplicationPath').mockReturnValue('');
    });

    it('should call join with right argument if application id is olip', async () => {
      const result = await service.getItemFilePath(
        mockApplicationOlip,
        itemFilePath,
      );
      expect(path.join).toHaveBeenCalledTimes(1);
      expect(path.join).toHaveBeenCalledWith(
        __dirname,
        '../../data/static',
        itemFilePath,
      );
      expect(result).toBe(itemFilePath + '/join');
    });

    it('should call join with right argument if application id is not olip', async () => {
      const result = await service.getItemFilePath(
        mockApplication,
        itemFilePath,
      );
      expect(path.join).toHaveBeenCalledTimes(1);
      expect(path.join).toHaveBeenCalledWith(
        dockerService.getApplicationPath(mockApplication),
        'data/content',
        itemFilePath,
      );
      expect(result).toBe(itemFilePath + '/join');
    });
  });
});
