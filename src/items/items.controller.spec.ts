import { Test, TestingModule } from '@nestjs/testing';
import { ItemsController } from './items.controller';
import { ItemsService } from './items.service';
import { itemsServiceMock } from 'src/tests/mocks/providers/items-service.mock';
import { CACHE_MANAGER } from '@nestjs/cache-manager';

describe('ItemsController', () => {
  let controller: ItemsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ItemsController],
      providers: [ItemsService, { provide: CACHE_MANAGER, useValue: {} }],
    })
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .compile();

    controller = module.get<ItemsController>(ItemsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
