import { Controller, Get, Param, Query } from '@nestjs/common';
import { ItemsService } from './items.service';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { ItemDto } from './dto/item.dto';
import { GetItemSuggestionsOptionsDto } from './dto/get-item-suggestions-options.dto';
import { cachedMethod } from 'src/utils/caching.util';
import { Item } from './entities/item.entity';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';
import { DublinCoreAudience } from 'src/dublin-core-audiences/entities/dublin-core-audience.entity';
import { DublinCoreItemTranslation } from 'src/dublin-core-items/entities/dublin-core-item-translation';
import { DublinCoreAudienceTranslation } from 'src/dublin-core-audiences/entities/dublin-core-audience-translation.entity';
import { DublinCoreLanguage } from 'src/dublin-core-languages/entities/dublin-core-language.entity';
import { File } from 'src/files/entities/file.entity';
import { DublinCoreLicense } from 'src/dublin-core-licenses/entities/dublin-core-license.entity';
import { DublinCoreLicenseTranslation } from 'src/dublin-core-licenses/entities/dublin-core-license-translation.entity';
import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';
import { DublinCoreTypeTranslation } from 'src/dublin-core-types/entities/dublin-core-type-translation.entity';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { GetItemsOptions } from './dto/get-items-options.dto';

@ApiTags('items')
@Controller('api/items')
export class ItemsController {
  constructor(private itemsService: ItemsService) {}

  @Get()
  getItems(
    @Query() getItemsOptions: GetItemsOptions,
    @Query() pageOptionsDto: PageOptionsDto,
  ) {
    return cachedMethod(
      'itemsService',
      'getItems',
      [getItemsOptions, pageOptionsDto],
      () => {
        return this.itemsService.getItems(getItemsOptions, pageOptionsDto);
      },
      [
        Item,
        File,
        DublinCoreItem,
        DublinCoreItemTranslation,
        DublinCoreAudience,
        DublinCoreAudienceTranslation,
        DublinCoreLanguage,
        DublinCoreLicense,
        DublinCoreLicenseTranslation,
        DublinCoreType,
        DublinCoreTypeTranslation,
      ],
    );
  }

  @ApiOperation({
    summary: 'Get an item by id',
  })
  @ApiParam({ name: 'id' })
  @ApiNotFoundResponse()
  @ApiOkResponse({ type: ItemDto })
  @Get(':id')
  getItem(
    @Param('id') id: string,
    @Query('languageIdentifier') languageIdentifier: string,
  ) {
    return cachedMethod(
      'itemsService',
      'getItem',
      [{ id, languageIdentifier }],
      () => {
        return this.itemsService.getItem(id, languageIdentifier);
      },
      [
        Item,
        File,
        DublinCoreItem,
        DublinCoreItemTranslation,
        DublinCoreAudience,
        DublinCoreAudienceTranslation,
        DublinCoreLanguage,
        DublinCoreLicense,
        DublinCoreLicenseTranslation,
        DublinCoreType,
        DublinCoreTypeTranslation,
      ],
    );
  }

  @ApiOperation({
    summary: 'Get item suggestions item by id',
  })
  @ApiParam({ name: 'id' })
  @ApiNotFoundResponse()
  @ApiOkResponse({ type: ItemDto, isArray: true })
  @Get('suggestions/:id')
  getItemSuggestions(
    @Param('id') id: string,
    @Query() getItemSuggestionsOptionsDto: GetItemSuggestionsOptionsDto,
    @Query() pageOptionsDto: PageOptionsDto,
  ) {
    return this.itemsService.getItemSuggestions(
      id,
      getItemSuggestionsOptionsDto,
      pageOptionsDto,
    );
  }
}
