import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetItemsOptions {
  @ApiPropertyOptional({ example: 'eng' })
  @IsString()
  languageIdentifier: string;

  @ApiPropertyOptional({ example: 'Title example' })
  @IsString()
  @IsOptional()
  titleLike?: string;

  @ApiPropertyOptional({ example: '1' })
  @IsString()
  @IsOptional()
  categoryId?: string;

  @ApiPropertyOptional({ example: '1,2' })
  @IsString()
  @IsOptional()
  applicationIds?: string;
}
