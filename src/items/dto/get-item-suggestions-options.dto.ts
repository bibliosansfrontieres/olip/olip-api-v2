import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetItemSuggestionsOptionsDto {
  @ApiProperty({ example: 'eng' })
  @IsString()
  languageIdentifier: string;

  @ApiPropertyOptional({ example: '1' })
  @IsString()
  @IsOptional()
  categoryId?: string;

  @ApiPropertyOptional({ example: '1' })
  @IsString()
  @IsOptional()
  playlistId?: string;
}
