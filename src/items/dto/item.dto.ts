import { File } from 'src/files/entities/file.entity';
import { Item } from '../entities/item.entity';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Application } from 'src/applications/entities/application.entity';

export class ItemDto {
  @ApiProperty({ example: '1' })
  id: string;

  @ApiProperty({ example: 'images/02-01-01/myimage.jpg' })
  thumbnail: string;

  @ApiProperty({ example: 'fr' })
  createdCountry: string;

  @ApiProperty({ example: true })
  isInstalled: boolean;

  @ApiProperty({ example: false })
  isUpdateNeeded: boolean;

  @ApiProperty({ example: false })
  isManuallyInstalled: boolean;

  @ApiProperty({ example: null })
  version: string | null;

  @ApiProperty({ example: 1692712640532 })
  createdAt: number;

  @ApiProperty({ example: 1692712640532 })
  updatedAt: number;

  @ApiProperty({
    example: {
      id: 1,
      name: 'myfile',
      path: '/contents/afolder/afile.extension',
      size: 0,
      duration: 0,
      pages: 0,
      extension: 'extension',
      mimeType: 'application/extension',
    },
  })
  file: File;

  @ApiProperty({
    example: {
      id: 1,
      dublinCoreType: {
        id: 1,
        image: 'images/02-01-01/myimage.jpg',
        dublinCoreTypeTranslations: [
          {
            id: 1,
            label: 'Text',
            description: 'My description',
            language: {
              id: 1,
              identifier: 'eng',
              oldIdentifier: 'en',
              label: 'English',
            },
          },
        ],
      },
      dublinCoreItemTranslations: [
        {
          id: 1,
          title: 'my item title in english',
          description: 'my description',
          creator: 'INA',
          publisher: 'INA',
          source: 'INA',
          extent: '',
          subject: '',
          dateCreated: '',
          language: {
            id: 1,
            identifier: 'eng',
            oldIdentifier: 'en',
            label: 'English',
          },
        },
      ],
    },
  })
  dublinCoreItem: DublinCoreItem;

  application: Application;

  url?: string;

  constructor(item: Item) {
    this.id = item.id;
    this.thumbnail = item.thumbnail;
    this.createdCountry = item.createdCountry;
    this.isInstalled = item.isInstalled;
    this.isUpdateNeeded = item.isUpdateNeeded;
    this.isManuallyInstalled = item.isManuallyInstalled;
    this.version = item.version;
    this.createdAt = item.createdAt;
    this.updatedAt = item.updatedAt;
    this.file = item.file;
    this.url = item.url;
    this.dublinCoreItem = item.dublinCoreItem;
    this.application = item.application;
  }
}
