import { Module } from '@nestjs/common';
import { ItemsService } from './items.service';
import { ItemsController } from './items.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Item } from './entities/item.entity';
import { LanguagesModule } from 'src/languages/languages.module';
import { PlaylistsModule } from 'src/playlists/playlists.module';
import { ApplicationsModule } from 'src/applications/applications.module';
import { FilesModule } from 'src/files/files.module';
import { DublinCoreItemsModule } from 'src/dublin-core-items/dublin-core-items.module';
import { DublinCoreTypesModule } from 'src/dublin-core-types/dublin-core-types.module';
import { CategoriesModule } from 'src/categories/categories.module';
import { SearchModule } from 'src/search/search.module';
import { DockerModule } from 'src/docker/docker.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Item]),
    DublinCoreItemsModule,
    DublinCoreTypesModule,
    LanguagesModule,
    PlaylistsModule,
    ApplicationsModule,
    FilesModule,
    CategoriesModule,
    SearchModule,
    DockerModule,
  ],
  controllers: [ItemsController],
  providers: [ItemsService],
  exports: [ItemsService],
})
export class ItemsModule {}
