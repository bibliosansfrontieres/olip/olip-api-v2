import { Injectable, NotFoundException } from '@nestjs/common';
import { Item } from './entities/item.entity';
import {
  FindManyOptions,
  FindOneOptions,
  FindOptionsWhere,
  In,
  Like,
  Repository,
} from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { ApplicationsService } from 'src/applications/applications.service';
import { Category } from 'src/categories/entities/category.entity';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';
import { DublinCoreItemsService } from 'src/dublin-core-items/dublin-core-items.service';
import { existsSync, rmSync } from 'fs';
import { join } from 'path';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { invalidateCache } from 'src/utils/caching.util';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { Application } from 'src/applications/entities/application.entity';
import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';
import { File } from 'src/files/entities/file.entity';
import { GetItemSuggestionsOptionsDto } from './dto/get-item-suggestions-options.dto';
import { CategoriesService } from 'src/categories/categories.service';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { PageOptionsDto } from 'src/utils/classes/page-options.dto';
import { PageOptions } from 'src/utils/classes/page-options';
import { ItemDto } from './dto/item.dto';
import { PageDto } from 'src/utils/classes/page.dto';
import { GetItemsOptions } from './dto/get-items-options.dto';
import { DublinCoreItemTranslation } from 'src/dublin-core-items/entities/dublin-core-item-translation';
import { IMaestroItem } from 'src/maestro/interfaces/maestro-item.interface';
import { SearchService } from 'src/search/search.service';
import { DockerService } from 'src/docker/docker.service';
import { SocketGateway } from 'src/socket/socket.gateway';
import { isVm } from 'src/utils/is-vm.util';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class ItemsService {
  constructor(
    @InjectRepository(Item) private itemsRepository: Repository<Item>,
    private playlistsService: PlaylistsService,
    private dublinCoreItemsService: DublinCoreItemsService,
    private dublinCoreTypesService: DublinCoreTypesService,
    private applicationsService: ApplicationsService,
    private categoriesService: CategoriesService,
    private searchService: SearchService,
    private dockerService: DockerService,
    private socketGateway: SocketGateway,
    private configService: ConfigService,
  ) {}

  async indexAll() {
    const items = await this.find();
    for (const item of items) {
      await this.searchService.indexItem(item.id);
    }
  }

  async createOrUpdate(maestroItem: IMaestroItem) {
    const item = await this.findOne({
      where: { id: maestroItem.id },
      relations: {
        file: true,
        dublinCoreItem: { dublinCoreItemTranslations: { language: true } },
      },
    });
    if (!item) {
      return this.createItem(maestroItem);
    } else {
      return this.update(item, maestroItem);
    }
  }

  async resetAllIsInstalled() {
    const items = await this.itemsRepository.find();
    for (const item of items) {
      item.isInstalled = false;
      item.isDownloading = true;
      await this.save(item);
    }
  }

  async find(options?: FindManyOptions<Item>) {
    return this.itemsRepository.find(options);
  }

  private async createItem(maestroItem: IMaestroItem): Promise<Item> {
    const application = await this.applicationsService.findOne({
      where: { id: maestroItem.applicationId },
    });
    if (application === null) return null;
    let item = new Item(maestroItem, application);
    item.file = new File(maestroItem);
    item = await this.itemsRepository.save(item);
    const dublinCoreType = maestroItem.dublinCoreTypeId
      ? await this.dublinCoreTypesService.findOne({
          where: { id: maestroItem.dublinCoreTypeId },
        })
      : null;

    const dublinCoreItem = await this.dublinCoreItemsService.create(
      item,
      maestroItem,
      dublinCoreType,
    );
    item.dublinCoreItem = dublinCoreItem;
    await this.searchService.indexItem(item.id);
    await invalidateCache([
      Item,
      DublinCoreItem,
      Playlist,
      Application,
      DublinCoreType,
      File,
    ]);
    return;
  }

  async create(maestroItem: IMaestroItem): Promise<DublinCoreItem> {
    const application = await this.applicationsService.findOne({
      where: { id: maestroItem.applicationId },
    });
    if (application === null) return null;
    let item = new Item(maestroItem, application);
    item.file = new File(maestroItem);
    item = await this.itemsRepository.save(item);
    const dublinCoreType = maestroItem.dublinCoreTypeId
      ? await this.dublinCoreTypesService.findOne({
          where: { id: maestroItem.dublinCoreTypeId },
        })
      : null;

    const dublinCoreItem = await this.dublinCoreItemsService.create(
      item,
      maestroItem,
      dublinCoreType,
    );
    await invalidateCache([
      Item,
      DublinCoreItem,
      Playlist,
      Application,
      DublinCoreType,
      File,
    ]);
    return dublinCoreItem;
  }

  async removeTranslation(
    dublinCoreItemTranslation: DublinCoreItemTranslation,
  ) {
    await this.dublinCoreItemsService.removeTranslation(
      dublinCoreItemTranslation,
    );
  }

  /**
   * Update an item from MaestroItem
   * @param item Needs file, dublinCoreItem->dublinCoreItemTranslations->Language relations
   * @param maestroItem MaestroItem
   * @returns Promise<Item>
   */
  async update(item: Item, maestroItem: IMaestroItem) {
    // Manage item metadatas
    item.isUpdateNeeded = false;
    item.version = maestroItem.version;
    item.thumbnail = maestroItem.thumbnail;
    item.createdCountry = maestroItem.createdCountry;

    // Manage file metadatas
    item.file.path = maestroItem.file.path;
    item.file.duration = maestroItem.file.duration;
    item.file.extension = maestroItem.file.extension;
    item.file.mimeType = maestroItem.file.mimeType;
    item.file.name = maestroItem.file.name;
    item.file.pages = maestroItem.file.pages;
    item.file.size = maestroItem.file.size;

    // Manage translations to add/update
    const translationsToAdd = [];
    for (const maestroTranslation of maestroItem.dublinCoreItemTranslations) {
      const translation = item.dublinCoreItem.dublinCoreItemTranslations.find(
        (translation) =>
          translation.language.identifier ===
          maestroTranslation.languageIdentifier,
      );
      if (!translation) {
        const translationToAdd =
          await this.dublinCoreItemsService.createTranslation(
            item,
            maestroTranslation,
          );
        translationsToAdd.push(translationToAdd);
      } else {
        await this.dublinCoreItemsService.updateTranslation(
          translation,
          maestroTranslation,
        );
      }
    }

    // Manage translations to remove
    const translationIdsToRemove = [];
    for (const translation of item.dublinCoreItem.dublinCoreItemTranslations) {
      const maestroTranslation = maestroItem.dublinCoreItemTranslations.find(
        (maestroTranslation) =>
          maestroTranslation.languageIdentifier ===
          translation.language.identifier,
      );
      if (!maestroTranslation) {
        await this.dublinCoreItemsService.removeTranslation(translation);
        translationIdsToRemove.push(translation.id);
      }
    }

    item.dublinCoreItem.dublinCoreItemTranslations = [
      ...item.dublinCoreItem.dublinCoreItemTranslations.filter((translation) =>
        translationIdsToRemove.includes(translation.id),
      ),
      ...translationsToAdd,
    ];

    await this.itemsRepository.save(item);
    await this.searchService.removeItemIndex(item.id);
    await this.searchService.indexItem(item.id);
    await invalidateCache([
      Item,
      DublinCoreItem,
      Playlist,
      Application,
      DublinCoreType,
      File,
    ]);
    return;
  }

  async findOne(options?: FindOneOptions<Item>): Promise<Item> {
    return this.itemsRepository.findOne(options);
  }

  async remove(id: string) {
    const item = await this.findOne({ where: { id } });
    if (item === null) return null;
    await this.itemsRepository.remove(item);
    await invalidateCache([Item]);
    return item;
  }

  getQueryBuilder(alias?: string) {
    return this.itemsRepository.createQueryBuilder(alias);
  }

  async uninstall(itemId: string, playlistIdsToDelete?: (string | number)[]) {
    const item = await this.findOne({
      where: { id: itemId },
      relations: { file: true, application: true, playlists: true },
    });

    if (!item) return;

    // If playlistsIdsToDelete are provided,
    // only uninstall the item if all playlistIdsToDelete are included
    // We don't want to delete items that are in playlists that we won't delete
    if (playlistIdsToDelete) {
      const itemPlaylistIds = item.playlists.map((playlist) => playlist.id);
      const areAllPlaylistIdsToDeleteIncluded = itemPlaylistIds.every(
        (itemPlaylistId) => playlistIdsToDelete.includes(itemPlaylistId),
      );

      if (!areAllPlaylistIdsToDeleteIncluded) return;
    }

    if (item.application.id !== 'olip') {
      await this.applicationsService.uninstallContent(item, item.application);
    }

    if (item.file && item.file.path) {
      const filePath = await this.getItemFilePath(
        item.application,
        item.file.path,
      );
      if (existsSync(filePath) && !isVm(this.configService)) {
        rmSync(filePath);
      }
    }

    await this.remove(item.id);

    await invalidateCache([Item]);
    await this.socketGateway.emitStorage();

    return item;
  }

  async getItemFilePath(application: Application, itemFilePath: string) {
    if (application.id === 'olip') {
      return join(__dirname, '../../data/static', itemFilePath);
    }

    return join(
      this.dockerService.getApplicationPath(application),
      'data/content',
      itemFilePath,
    );
  }

  async getItem(id: string, languageIdentifier: string) {
    const item = await this.findOne({
      where: { id },
      relations: {
        file: true,
        dublinCoreItem: { dublinCoreType: true },
        application: { applicationType: true },
      },
    });
    if (item === null) throw new NotFoundException('item not found');
    item.dublinCoreItem.dublinCoreType.dublinCoreTypeTranslations = [
      await this.dublinCoreTypesService.getTranslation(
        item.dublinCoreItem.dublinCoreType,
        languageIdentifier,
      ),
    ];
    item.dublinCoreItem.dublinCoreItemTranslations = [
      await this.dublinCoreItemsService.getTranslation(
        item.dublinCoreItem,
        languageIdentifier,
      ),
    ];
    await this.applicationsService.getApplicationsTranslations(
      [item.application],
      languageIdentifier,
    );

    if (item.file) {
      if (item.file.extension) {
        item.file.extension = item.file.extension.toLowerCase();
      }
      if (item.file.name) {
        const splitName = item.file.name.split('.');
        splitName[splitName.length - 1] =
          splitName[splitName.length - 1].toLowerCase();
        item.file.name = splitName.join('.');
      }
      if (item.file.path) {
        const splitPath = item.file.path.split('.');
        splitPath[splitPath.length - 1] =
          splitPath[splitPath.length - 1].toLowerCase();
        item.file.path = splitPath.join('.');
      }
    }

    return item;
  }

  async getItemSuggestions(
    id: string,
    getItemSuggestionsOptionsDto: GetItemSuggestionsOptionsDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const pageOptions = new PageOptions(pageOptionsDto);
    const item = await this.itemsRepository.findOne({ where: { id } });
    if (item === null) throw new NotFoundException('item not found');
    let categoriesWhere: FindOptionsWhere<Category> = {
      categoryPlaylists: {
        categoryPlaylistItems: { item: { id: item.id } },
      },
    };
    if (getItemSuggestionsOptionsDto.categoryId !== undefined) {
      categoriesWhere = {
        id: +getItemSuggestionsOptionsDto.categoryId,
      };
    }
    const categories = await this.categoriesService.findAll({
      where: categoriesWhere,
    });
    const playlistsWhere: FindOptionsWhere<Playlist> = {
      items: { id: item.id },
    };
    if (getItemSuggestionsOptionsDto.playlistId !== undefined) {
      categoriesWhere = {
        id: +getItemSuggestionsOptionsDto.playlistId,
      };
    }
    const playlists = await this.playlistsService.find({
      where: playlistsWhere,
    });

    const itemsQuery = this.itemsRepository
      .createQueryBuilder('item')
      .innerJoinAndSelect(
        'item.categoryPlaylistItems',
        'categoryPlaylistItems',
        'categoryPlaylistItems.itemId = item.id',
      )
      .innerJoin(
        'categoryPlaylistItems.categoryPlaylist',
        'categoryPlaylist',
        'categoryPlaylist.id = categoryPlaylistItems.categoryPlaylistId',
      )
      .innerJoin(
        'categoryPlaylist.category',
        'category',
        'category.id = categoryPlaylist.categoryId',
      )
      .innerJoinAndSelect(
        'item.dublinCoreItem',
        'dublinCoreItem',
        'dublinCoreItem.itemId = item.id',
      )
      .innerJoinAndSelect(
        'dublinCoreItem.dublinCoreType',
        'dublinCoreType',
        'dublinCoreType.id = dublinCoreItem.dublinCoreTypeId',
      )
      .innerJoinAndSelect(
        'item.application',
        'application',
        'application.id = item.applicationId',
      )
      .innerJoinAndSelect(
        'application.applicationType',
        'applicationType',
        'applicationType.id = application.applicationTypeId',
      )
      .innerJoin(
        'categoryPlaylist.playlist',
        'playlist',
        'playlist.id = categoryPlaylist.playlistId',
      )
      .where('category.id IN (:...categoriesIds)', {
        categoriesIds: categories.map((c) => c.id),
      })
      .where('NOT item.id = :itemContentId', {
        itemContentId: item.id,
      })
      .where('NOT item.id = :itemId', {
        itemId: id,
      })
      .orWhere('playlist.id IN (:...playlistsIds)', {
        playlistsIds: playlists.map((c) => c.id),
      })
      .addOrderBy('item.updatedAt', 'DESC')
      .addOrderBy('categoryPlaylistItems.isHighlightable', 'DESC')
      .take(pageOptions.take)
      .skip(pageOptions.skip);

    const itemsResult = await getManyAndCount<Item>(itemsQuery, ['item.id']);
    const totalItemsCount = itemsResult[0];
    let items = itemsResult[1];
    items = await this.getDublinCoreItemsTranslations(
      items,
      getItemSuggestionsOptionsDto.languageIdentifier,
    );
    for (const item of items) {
      await this.applicationsService.getApplicationsTranslations(
        [item.application],
        getItemSuggestionsOptionsDto.languageIdentifier,
      );
    }

    const data = items.map((item) => new ItemDto(item));
    return new PageDto(data, totalItemsCount, pageOptions);
  }

  async getDublinCoreItemsTranslations(
    items: Item[],
    languageIdentifier: string,
  ) {
    for (const item of items) {
      item.dublinCoreItem.dublinCoreItemTranslations = [
        await this.dublinCoreItemsService.getTranslation(
          item.dublinCoreItem,
          languageIdentifier,
        ),
      ];
    }
    return items;
  }

  async getItems(
    getItemsOptions: GetItemsOptions,
    pageOptionsDto: PageOptionsDto,
  ) {
    const applicationIds =
      getItemsOptions && getItemsOptions.applicationIds
        ? getItemsOptions.applicationIds.split(',')
        : [];
    const pageOptions = new PageOptions(pageOptionsDto);
    const titleLike = getItemsOptions.titleLike
      ? getItemsOptions.titleLike
      : '';
    const application =
      applicationIds.length > 0 ? { id: In(applicationIds) } : undefined;
    let items = await this.itemsRepository.find({
      skip: pageOptions.skip,
      take: pageOptions.take,
      where: {
        application,
        categoryPlaylistItems: {
          categoryPlaylist: {
            category: { id: +getItemsOptions.categoryId || undefined },
          },
        },
        dublinCoreItem: {
          dublinCoreItemTranslations: { title: Like(`%${titleLike}%`) },
        },
      },
      relations: {
        dublinCoreItem: { dublinCoreType: true },
        application: { applicationType: true },
      },
    });
    items = await this.getDublinCoreItemTranslations(
      items,
      getItemsOptions.languageIdentifier,
    );
    for (const item of items) {
      await this.applicationsService.getApplicationsTranslations(
        [item.application],
        getItemsOptions.languageIdentifier,
      );
    }
    const data = items.map((item) => new ItemDto(item));
    const totalItemsCount = await this.itemsRepository.count({
      where: {
        application,
        categoryPlaylistItems: {
          categoryPlaylist: {
            category: { id: +getItemsOptions.categoryId || undefined },
          },
        },
        dublinCoreItem: {
          dublinCoreItemTranslations: { title: Like(`%${titleLike}%`) },
        },
      },
    });
    return new PageDto(data, totalItemsCount, pageOptions);
  }

  private async getDublinCoreItemTranslations(
    items: Item[],
    languageIdentifier: string,
  ) {
    for (const item of items) {
      const translation = await this.dublinCoreItemsService.getTranslation(
        item.dublinCoreItem,
        languageIdentifier,
      );
      item.dublinCoreItem.dublinCoreItemTranslations = [translation];
    }
    return items;
  }

  async save(items: Item[] | Item) {
    if (Array.isArray(items)) {
      await this.itemsRepository.save(items);
    } else {
      await this.itemsRepository.save(items);
    }
    await invalidateCache([Item, DublinCoreItem, DublinCoreItemTranslation]);
  }

  async saveTranslation(dublinCoreItemTranslation: DublinCoreItemTranslation) {
    return this.dublinCoreItemsService.saveTranslation(
      dublinCoreItemTranslation,
    );
  }
}
