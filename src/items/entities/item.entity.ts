import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { File } from 'src/files/entities/file.entity';
import { Application } from 'src/applications/entities/application.entity';
import { CategoryPlaylistItem } from 'src/category-playlist-items/entities/category-playlist-item.entity';
import { DublinCoreItem } from 'src/dublin-core-items/entities/dublin-core-item.entity';
import { ItemLanguageLevel } from 'src/item-language-levels/entities/item-language-level.entity';
import { ItemDocumentType } from 'src/item-document-types/entities/item-document-type.entity';
import { HighlightContent } from 'src/highlight-contents/entities/highlight-content.entity';
import { IMaestroItem } from 'src/maestro/interfaces/maestro-item.interface';

@Entity()
export class Item {
  @PrimaryColumn()
  id: string;

  @Column()
  thumbnail: string;

  @Column()
  createdCountry: string;

  @Column()
  isInstalled: boolean;

  @Column()
  isDownloading: boolean;

  @Column()
  isUpdateNeeded: boolean;

  @Column()
  isBroken: boolean;

  @Column()
  isManuallyInstalled: boolean;

  @Column({ nullable: true })
  version: string | null;

  @Column({ nullable: true })
  url: string | null;

  @OneToOne(() => DublinCoreItem, (dublinCoreItem) => dublinCoreItem.item)
  dublinCoreItem: DublinCoreItem;

  @ManyToOne(
    () => ItemDocumentType,
    (itemDocumentType) => itemDocumentType.items,
  )
  itemDocumentType: ItemDocumentType;

  @ManyToOne(
    () => ItemLanguageLevel,
    (itemLanguageLevel) => itemLanguageLevel.items,
  )
  itemLanguageLevel: ItemLanguageLevel;

  @ManyToMany(() => Playlist, (playlist) => playlist.items)
  playlists: Playlist[];

  @OneToMany(
    () => CategoryPlaylistItem,
    (categoryPlaylistItem) => categoryPlaylistItem.item,
  )
  categoryPlaylistItems: CategoryPlaylistItem[];

  @OneToOne(() => File, (file) => file.item, { cascade: true })
  file: File;

  @ManyToOne(() => Application, (application) => application.items)
  application: Application;

  @OneToMany(
    () => HighlightContent,
    (highlightContent) => highlightContent.item,
  )
  highlightContents: HighlightContent[];

  @CreateDateColumn({
    default: () => Date.now(),
  })
  createdAt: number;

  @UpdateDateColumn({
    default: () => Date.now(),
    onUpdate: Date.now().toString(),
  })
  updatedAt: number;

  constructor(
    maestroItem: IMaestroItem,
    application: Application,
    isManuallyInstalled = false,
  ) {
    if (!maestroItem) return;
    this.id = maestroItem.id;
    this.thumbnail = maestroItem.thumbnail || '';
    this.createdCountry = maestroItem.createdCountry;
    this.isInstalled = false;
    this.isUpdateNeeded = false;
    this.isBroken = false;
    this.isManuallyInstalled = isManuallyInstalled;
    this.playlists = [];
    this.version = maestroItem.version || null;
    this.application = application;
    this.isDownloading = true;
  }
}
