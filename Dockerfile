########
# BASE #
########

FROM node:21-alpine3.19 AS base

RUN apk --no-cache add curl

# Pnpm configuration
ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable

WORKDIR /app

COPY package.json pnpm-lock.yaml ./

RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --prod --frozen-lockfile

#########
# BUILD #
#########

FROM base AS build

RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --frozen-lockfile

COPY . .

RUN pnpm run build

##############
# PRODUCTION #
##############

FROM base AS production

RUN apk add --no-cache docker-cli=25.0.5-r1 docker-cli-compose=2.23.3-r3

COPY --from=build /app/dist /app/dist
COPY --from=build /app/data /app/data
COPY --from=build /app/uploads /app/uploads

ENV NODE_ENV=production

CMD [ "node", "dist/main.js" ]